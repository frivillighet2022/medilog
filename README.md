# MediLog
![MediLog](./app/src/main/ic_launcher.png)

Android App to easily capture medical data. The following values are supported at present: Blood Pressure, Glucose, Oximetry, Weight + Body Fat, Temperature, Water intake and a simple Diary

[![Status: Active](./badges/status.svg)](#)
[![Status: Active](./badges/release.svg)](#)
[![API Reference](./badges/license.svg)](./LICENSE)

## Key features

The main goal of MediLog is not to cover the whole universe of medical data but to provide a solution for cases where the alternatives either don't respect privacy, are full of trackers or complicated.

Guiding principles for MediLog are:

- Be simple. Allow to capture data as quickly as possible to not get in the way. If you can think of ways to improve the UI, let me know.
- Be open. Export and import format is a simple CSV, if you can export CSV from your existing application it should be fairly easy to import into MediLog, and of course, import into your new application if you ever wish to move on with your data
- Be secure. PDF reports and ZIP file backups are password protected, the database is encrypted
- Be transparent. No data transfers to anyone but the one you choose. No hidden telemetry, no automatic crash logs, etc.
- In essence, offer approbriate measures to protect data as defined by art. 9 GDPR Processing of special categories of personal data

[![Awesome Humane Tech](https://raw.githubusercontent.com/humanetech-community/awesome-humane-tech/main/humane-tech-badge.svg?sanitize=true)](https://github.com/humanetech-community/awesome-humane-tech)

MediLog will not:

- Connect to your scale or blood pressure device. Data entry is manual
- Add data types which one measures/records infrequently (e.g. once every other week), there are more efficient ways to capture that data, a piece of paper for example

[![Awesome Humane Tech](https://raw.githubusercontent.com/humanetech-community/awesome-humane-tech/main/humane-tech-badge.svg?sanitize=true)](https://github.com/humanetech-community/awesome-humane-tech)

### Installation

The latest build (signed by me) can be found here: 

My Website: <a href=https://zell-mbc.com/documents/app-release.apk>Latest Build</a>

  [<img src="https://to.skydroid.app/assets/skydroid.svg"
     alt="Get it on SkyDroid"
     height="55">](https://to.skydroid.app/medilog.zell-mbc.com/)

[<img src="https://gitlab.com/IzzyOnDroid/repo/-/raw/master/assets/IzzyOnDroid.png"
     alt="Get it on IzzyOnDroid"
     height="80">](https://android.izzysoft.de/repo/apk/com.zell_mbc.medilog)

Or here (signed by F-Droid)…

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/com.zell_mbc.medilog/)
     

**Important note:** Be careful when mixing downloads from F-Droid with other locations. The F-Droid version is signed with a different developer key which means you will need to delete the existing app when changing. Make sure to backup your data before you change.

### Removal
Like with all Android applications, via a long click on the app icon.


**Uninstalling the application will delete all data! Unrecoverable! Make sure you export your data first!**

### User manual
Can be found here: https://gitlab.com/toz12/medilog/-/wikis/home 


### Privacy Policy

#### Stored data
Data entered by the user is stored inside an encrypted SQLite database. 

**To keep the input process as simple and fast as possible, the app/your data is not protected with an additional password. If your device has support for Biometric (Fingerprint) make sure to enable it. Otherwise, if someone is able to unlock your device they can access your health data!**

The app supports storing backups in encrypted ZIP files. Make use of it by adding a password whenever you create a file or setting a default one in the settings dialog. Don't forget to test (and remember) the password.
The app allows to send protected files so you can share over unprotected media (eg. email) reducing the risk of your data getting in the wrong hands.


#### Required permissions

- WRITE_EXTERNAL_STORAGE : Required to export backup files
- READ_EXTERNAL_STORAGE : Required to import bckup files


#### Tracking
This application does not use any tracking tools and does not run advertising.

#### Libraries
3rd party libraries in use are the below:

- AndroidPlot: For the charts
- Takisoft.fix: To fix an Android preference dialog bug (https://github.com/takisoft/preferencex-android#extra-types)
- Zip4J: For handling password protected ZIP files (https://github.com/srikanth-lingala/zip4j)
- SQLCipher: To encrypt the SQLite database (https://www.zetetic.net/sqlcipher/)
- Material-dialogs: For some of the dialog windows (https://github.com/afollestad/material-dialogs)
- ProgressView: Progress bar shown during data restore (https://github.com/skydoves/progressview)

## Contact

- Matrix: #medilog:zell-mbc.com
- Fediverse: https://social.zell-mbc.com/de/@thomas
- eMail: medilog@zell-mbc.com

## Languages
The number of translations is growing quickly. If your language isn't covered, have a look at https://weblate.bubu1.eu/projects/medilog/core/ or drop me a message and see if you can help the project.

- English
- German
- Basque (https://mastodon.technology/@mondstern)
- Chinese Simplified (https://gitlab.com/monyxie)
- Danish (https://mastodon.technology/@mondstern)
- Dutch (https://mastodon.technology/@mondstern)
- Esperanto (https://mastodon.technology/@mondstern)
- Finnish (https://gitlab.com/huuhaa)
- French (https://framapiaf.org/@djelouze)
- Hungarian (https://mastodon.technology/@mondstern)
- Italian (https://mastodon.technology/@mondstern)
- Norwegian (https://mastodon.technology/@mondstern)
- Polish (https://mastodon.technology/@mondstern)
- Portuguese (Brazil) (https://mastodon.technology/@mondstern)
- Russian (https://mastodon.technology/@mondstern)
- Spanish (https://mastodon.technology/@mondstern)
- Swedish (https://mastodon.technology/@mondstern)


Translation platform: https://weblate.bubu1.eu/projects/medilog/core/

A big thank you as well to https://chaos.social/@bubu for providing his translation platform to MediLog

## Donations
If you feel like it…

[<img src="https://www.paypalobjects.com/webstatic/de_DE/i/de-pp-logo-100px.png" border="0"
    alt="PayPal Logo">](https://www.paypal.com/paypalme/thomaszellmbc)

[<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Bitcoin_logo.svg/252px-Bitcoin_logo.svg.png" border="0"
height="20" alt="Bitcoin Logo">](https://live.blockcypher.com/btc/address/1EUrRpjDAGgpS8J46tmsVWKbgNqAv7rWC7/)



## Screenshots
![image](fastlane/metadata/android/en-US/images/phoneScreenshots/Weight.png)
![image](fastlane/metadata/android/en-US/images/phoneScreenshots/BloodPressure.png)
![image](fastlane/metadata/android/en-US/images/phoneScreenshots/Diary.png)
![image](fastlane/metadata/android/en-US/images/phoneScreenshots/WeightChart.png)
![image](fastlane/metadata/android/en-US/images/phoneScreenshots/BloodPressureChart.png)
![image](fastlane/metadata/android/en-US/images/phoneScreenshots/Settings.png)
![image](fastlane/metadata/android/en-US/images/phoneScreenshots/About.png)
![image](fastlane/metadata/android/en-US/images/phoneScreenshots/PDFReport.png)

## Changelog
[Change log](ChangeLog.md)


