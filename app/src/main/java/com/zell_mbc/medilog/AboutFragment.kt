package com.zell_mbc.medilog

import android.content.pm.PackageManager.GET_SIGNATURES
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.MaterialTheme.typography
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.documentfile.provider.DocumentFile
import androidx.fragment.app.Fragment
import com.zell_mbc.medilog.databinding.ActivityAboutBinding
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utility.ListTheme
import com.zell_mbc.medilog.utility.Preferences
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.security.cert.CertificateException
import java.security.cert.CertificateFactory
import java.text.DateFormat
import java.util.*
import java.security.cert.X509Certificate as X509Certificate


class AboutFragment: Fragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: ActivityAboutBinding? = null
    private val binding get() = _binding!!
    private lateinit var userOutputService : UserOutputService

    private fun initializeService(view: View) {
        userOutputService = UserOutputServiceImpl(requireContext(),view)
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = ActivityAboutBinding.inflate(inflater, container, false)
        initializeService(binding.root)
        return binding.root
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    @Composable
    fun ShowContent() {
        val preferences = Preferences.getSharedPreferences(requireContext())
        val scrollState = rememberScrollState()
        ListTheme {
            var s: String
            Column(modifier = Modifier.verticalScroll(state = scrollState).padding(start = 16.dp, top = 8.dp)) {
                Row {
                    Surface(
                        modifier = Modifier.size(50.dp),
                        shape = CircleShape,
                        color = MaterialTheme.colors.onSurface.copy(alpha = 0.2f)
                    ) {
                        Image(
                            painter = painterResource(id = R.drawable.ic_medilog2),
                            contentDescription = null
                        )
                    }
                    Column(modifier = Modifier.padding(start = 8.dp, top = 6.dp)) {
                        Text(
                            stringResource(id = R.string.appName),
                            style = typography.body1,
                            color = MaterialTheme.colors.primaryVariant,
                            textAlign = TextAlign.End
                        )
                        Text(
                            stringResource(id = R.string.appDescription),
                            style = typography.caption,
                            color = MaterialTheme.colors.primaryVariant
                        )
                    }
                }
                Text("Version " + BuildConfig.VERSION_NAME + ", build " + BuildConfig.VERSION_CODE,
                    modifier = Modifier.padding(top = 8.dp),
                    style = typography.caption, color = MaterialTheme.colors.primaryVariant
                )
                Text(
                    stringResource(id = R.string.zellMBC),
                    modifier = Modifier.padding(top = 8.dp),
                    style = typography.body2,
                    color = MaterialTheme.colors.primaryVariant)

                // Email
                val uriHandler = LocalUriHandler.current
                val eMail: AnnotatedString = buildAnnotatedString {
                    val str = stringResource(id = R.string.email)
                    val startIndex = 0
                    val endIndex = str.length
                    append(str)
                    addStyle(
                        style = SpanStyle(
                            color = Color(0xff64B5F6),
                            textDecoration = TextDecoration.Underline
                        ), start = startIndex, end = endIndex
                    )

                    // attach a string annotation that stores a URL to the text "link"
                    addStringAnnotation(
                        tag = "URL",
                        annotation = "mailto://$str",
                        start = startIndex,
                        end = endIndex
                    )
                }
                // Clickable text returns position of text that is clicked in onClick callback
                ClickableText(
                    modifier = Modifier.fillMaxWidth(),
                    text = eMail,
                    onClick = {
                        eMail
                            .getStringAnnotations("URL", it, it)
                            .firstOrNull()?.let { stringAnnotation -> uriHandler.openUri(stringAnnotation.item)
                            }
                    }, style = typography.caption)

                // AppUrl
                val appUrl: AnnotatedString = buildAnnotatedString {
                    val str = stringResource(id = R.string.AppURL)
                    val startIndex = 0
                    val endIndex = str.length
                    append(str)
                    addStyle(
                        style = SpanStyle(
                            color = Color(0xff64B5F6),
                            textDecoration = TextDecoration.Underline
                        ), start = startIndex, end = endIndex
                    )

                    // attach a string annotation that stores a URL to the text "link"
                    addStringAnnotation(
                        tag = "URL",
                        annotation = str,
                        start = startIndex,
                        end = endIndex
                    )
                }
                // Clickable text returns position of text that is clicked in onClick callback
                ClickableText(
                    modifier = Modifier.fillMaxWidth(),
                    text = appUrl,
                    onClick = {
                        appUrl
                            .getStringAnnotations("URL", it, it)
                            .firstOrNull()?.let { stringAnnotation -> uriHandler.openUri(stringAnnotation.item)
                            }
                    }, style = typography.caption
                )

                // ------------ Backup -----------
                Text("")
                var timestamp = preferences.getLong("LAST_BACKUP", 0L)
                if (timestamp > 0) {
                    val dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
                    val timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT)

                    val lastBackup: AnnotatedString = buildAnnotatedString {
                        val str = stringResource(id = R.string.lastBackup) + " " + dateFormat.format(timestamp) + " " + timeFormat.format(timestamp)
                        val startIndex = stringResource(id = R.string.lastBackup).length + 1
                        val endIndex = str.length
                        append(str)
                        addStyle(style = SpanStyle(fontWeight = FontWeight.Bold, color = MaterialTheme.colors.primaryVariant), start = 0, end = startIndex - 1)
                        addStyle(
                            style = SpanStyle(
                                fontWeight = FontWeight.Bold,
                                color = Color(0xff64B5F6),
                                textDecoration = TextDecoration.Underline,
                            ), start = startIndex, end = endIndex)

                        // attach a string annotation that stores a URL to the text "link"
                        addStringAnnotation(tag = "Backup", annotation = str, start = startIndex, end = endIndex)
                    }

                    // 🔥 Clickable text returns position of text that is clicked in onClick callback
                    ClickableText(
                        modifier = Modifier.fillMaxWidth(),
                        text = lastBackup,
                        onClick = {
                            lastBackup
                                .getStringAnnotations("Backup", it, it)
                                .firstOrNull()?.let {
                                    // Check if everything is in place for auto backups
                                    val uriString = preferences.getString(SettingsActivity.KEY_PREF_BACKUP_URI, "")
                                    if (uriString.isNullOrEmpty()) {
                                        userOutputService.showMessageAndWaitForLong(getString(R.string.missingBackupLocation))
                                        return@ClickableText
                                    }

                                    // Valid location?
                                    val uri = Uri.parse(uriString)
                                    val dFolder = DocumentFile.fromTreeUri(requireContext(), uri)
                                    if (dFolder == null) {
                                        userOutputService.showMessageAndWaitForLong(getString(R.string.missingBackupLocation))
                                        return@ClickableText
                                    }

                                    // Password
                                    var zipPassword = preferences.getString(SettingsActivity.KEY_PREF_PASSWORD, "")
                                    if (zipPassword.isNullOrEmpty()) zipPassword = ""

                                    userOutputService.showMessageAndWaitForLong(getString(R.string.backupStarted))
                                    MainActivity.viewModels[0].writeBackup(uri, zipPassword, false)

                                    timestamp = Date().time
                                    val editor = preferences.edit()
                                    editor.putLong("LAST_BACKUP_CHECK", timestamp)
                                    editor.apply()
                                }
                        }, style = typography.caption
                    )
                }
                else Text(stringResource(R.string.lastBackup) + " " + stringResource(id = R.string.never), style = typography.body2, color = MaterialTheme.colors.primaryVariant, fontWeight = FontWeight.Bold)
                Text("")
                // ---------------- End backup ----------------

                Text(stringResource(id = R.string.statistics), style = typography.body2, color = MaterialTheme.colors.primaryVariant)

                if (MainActivity.viewModels.size > 0) {
                    val dbSize = MainActivity.viewModels[0].getDataTableSize()
                    s = getString(R.string.database) + " " + getString(R.string.entries) + " " + dbSize.toString()
                    Text(s, style = typography.caption, color = MaterialTheme.colors.primaryVariant)
                }
                val dbName = "MediLogDatabase"
                val f = context?.getDatabasePath(dbName)
                val dbSize = f?.length()
                if (dbSize != null) {
                    s =
                        getString(R.string.database) + " " + getString(R.string.size) + " " + (dbSize / 1024).toString() + " kb"
                    Text(s, style = typography.caption, color = MaterialTheme.colors.primaryVariant)
                }

                // Security
                Text(stringResource(id = R.string.security),
                    modifier = Modifier.padding(top = 8.dp),
                    style = typography.body2, color = MaterialTheme.colors.primaryVariant)

                s = if (preferences.getBoolean("encryptedDB", false)) getString(R.string.databaseEncryptionOn)
                else getString(R.string.databaseEncryptionOff)

                Text(s, style = typography.caption, color = MaterialTheme.colors.primaryVariant)

                s = if (preferences.getBoolean(
                        SettingsActivity.KEY_PREF_BIOMETRIC,
                        false
                    )
                ) getString(R.string.biometricProtectionOn)
                else getString(R.string.biometricProtectionOff)
                Text(s, style = typography.caption, color = MaterialTheme.colors.primaryVariant)

                val ss = preferences.getString(SettingsActivity.KEY_PREF_PASSWORD, "")
                if (ss != null) {
                    s = if (ss.isNotEmpty()) getString(R.string.protectedBackupOn)
                    else getString(R.string.protectedBackupOff)
                    Text(s, style = typography.caption, color = MaterialTheme.colors.primaryVariant)
                }

                val sig = requireContext().packageManager.getPackageInfo(
                    requireContext().packageName,
                    GET_SIGNATURES
                ).signatures[0]
                val certStream: InputStream = ByteArrayInputStream(sig.toByteArray())
                val subject = try {
                    val certFactory = CertificateFactory.getInstance("X509")
                    val x509Cert: X509Certificate =
                        certFactory.generateCertificate(certStream) as X509Certificate
                    x509Cert.subjectDN.toString()
                } catch (e: CertificateException) {
                    "Unknown"
                }
                s = "Certificate: $subject"
                Text(s, style = typography.caption, color = MaterialTheme.colors.primaryVariant)

                // Copyright
                Text(stringResource(id = R.string.Copyright1),
                    modifier = Modifier.padding(top = 8.dp),
                    style = typography.caption, color = MaterialTheme.colors.primaryVariant)
                val license: AnnotatedString = buildAnnotatedString {
                    val str = stringResource(id = R.string.mediLogLicense)
                    val startIndex = 0
                    val endIndex = str.length
                    append(str)
                    addStyle(
                        style = SpanStyle(
                            color = Color(0xff64B5F6),
                            textDecoration = TextDecoration.Underline
                        ), start = startIndex, end = endIndex
                    )

                    // attach a string annotation that stores a URL to the text "link"
                    addStringAnnotation(
                        tag = "URL",
                        annotation = str,
                        start = startIndex,
                        end = endIndex
                    )
                }

                // Clickable text returns position of text that is clicked in onClick callback
                ClickableText(
                    modifier = Modifier.fillMaxWidth(),
                    text = license,
                    onClick = {
                        license
                            .getStringAnnotations("URL", it, it)
                            .firstOrNull()?.let { stringAnnotation -> uriHandler.openUri(stringAnnotation.item)
                            }
                    }, style = typography.caption
                )

                Text(" ")

                Text(stringResource(id = R.string.Copyright2), style = typography.caption, fontStyle = FontStyle.Italic, color = MaterialTheme.colors.primaryVariant)

                // ThirdParty
                val thirdParty: AnnotatedString = buildAnnotatedString {
                    val str = stringResource(id = R.string.thirdparty)
                    val startIndex = 0
                    val endIndex = str.length
                    append(str)
                    addStyle(style = SpanStyle(color = Color(0xff64B5F6),textDecoration = TextDecoration.Underline), start = startIndex, end = endIndex)

                    // attach a string annotation that stores a URL to the text "link"
                    addStringAnnotation(tag = "URL", annotation = str, start = startIndex, end = endIndex)
                }

                // 🔥 Clickable text returns position of text that is clicked in onClick callback
                ClickableText(
                    modifier = Modifier.fillMaxWidth(),
                    text = thirdParty,
                    onClick = {
                        thirdParty
                            .getStringAnnotations("URL", it, it)
                            .firstOrNull()?.let { stringAnnotation -> uriHandler.openUri(stringAnnotation.item)
                            }
                    }, style = typography.caption)
                Text("")
            }
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.aboutView.setContent {
                ShowContent()
        }
    }

}