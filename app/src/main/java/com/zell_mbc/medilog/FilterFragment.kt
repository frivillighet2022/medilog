package com.zell_mbc.medilog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.app.ActivityCompat.recreate
import androidx.fragment.app.DialogFragment
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.datetime.datePicker
import com.zell_mbc.medilog.MainActivity.Companion.FILTER_OFF
import com.zell_mbc.medilog.MainActivity.Companion.FILTER_ROLLING
import com.zell_mbc.medilog.MainActivity.Companion.FILTER_STATIC
import com.zell_mbc.medilog.data.ViewModel
import com.zell_mbc.medilog.databinding.FilterBinding
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.utility.Preferences
import java.text.DateFormat
import java.util.*
import kotlin.math.max


class FilterFragment: DialogFragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: FilterBinding? = null
    private val binding get() = _binding!!
    private lateinit var userOutputService : UserOutputService

    private var rollingTimeframe = 0
    private var rollingValue = 1
    private var filterMode = FILTER_OFF

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FilterBinding.inflate(inflater,container,false)

        initializeService(binding.root)

        return binding.root
    }

    private fun initializeService(view: View) {
        userOutputService = UserOutputServiceImpl(requireContext(),view)
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private var minStartCal: Calendar = Calendar.getInstance()
    private var maxEndCal: Calendar = Calendar.getInstance()

    // Temporary Calendars to hold filter settings
    private var filterStartCal: Calendar = Calendar.getInstance()
    private var filterEndCal: Calendar = Calendar.getInstance()  // Sets end and start dates to today

    lateinit var viewModel: ViewModel

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        var activeTab = -1
        if (arguments != null) activeTab = arguments?.getInt("activeTab")!!
        if (activeTab < 0) return super.onViewCreated(view, savedInstanceState)
        viewModel = MainActivity.viewModels[activeTab]

        // Initialize UI
        binding.tvHeader.text = getString(R.string.setFilter)

        val spinnerItems = arrayOf(getString(R.string.days), getString(R.string.weeks),getString(R.string.months), getString(R.string.years))
        val adapter = context?.let {
            ArrayAdapter(
                it,
                android.R.layout.simple_spinner_dropdown_item,
                spinnerItems)
        }

        val preferences = Preferences.getSharedPreferences(requireContext())
        rollingTimeframe = preferences.getInt(viewModel.rollingFilterTimeframePref, (this).getString(R.string.ROLLING_FILTER_TIMEFRAME_DEFAULT).toInt())
        rollingValue = preferences.getInt(viewModel.rollingFilterValuePref, (this).getString(R.string.ROLLING_FILTER_VALUE_DEFAULT).toInt())
        filterMode  = preferences.getInt(viewModel.filterModePref, (this).getString(R.string.FILTER_MODE_DEFAULT).toInt())
        setControlStatus()

        binding.rbOff.isChecked = (filterMode == FILTER_OFF)
        binding.rbStatic.isChecked = (filterMode == FILTER_STATIC)
        binding.rbRolling.isChecked = (filterMode == FILTER_ROLLING)
        binding.spRollingTimeframe.adapter = adapter
        binding.spRollingTimeframe.setSelection(rollingTimeframe)

        binding.etRollingValue.setText(rollingValue.toString()) //getString(R.string.setFilter)

        binding.spRollingTimeframe.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(arg0: AdapterView<*>?, arg1: View?, arg2: Int, arg3: Long) {
                rollingTimeframe = binding.spRollingTimeframe.selectedItemPosition
            }
            override fun onNothingSelected(arg0: AdapterView<*>?) {}
        }

        binding.rbStatic.setOnCheckedChangeListener  { _, checkedId ->
            if (checkedId) {
                binding.rbOff.isChecked = false
                binding.rbRolling.isChecked = false
                filterMode = FILTER_STATIC
                setControlStatus()
            }
        }
        binding.rbRolling.setOnCheckedChangeListener { _, checkedId ->
            if (checkedId) {
                binding.rbOff.isChecked = false
                binding.rbStatic.isChecked = false
                filterMode = FILTER_ROLLING
                setControlStatus()
            }
        }
        binding.rbOff.setOnCheckedChangeListener { _, checkedId ->
            if (checkedId) {
                binding.rbRolling.isChecked = false
                binding.rbStatic.isChecked = false
                filterMode = FILTER_OFF
                setControlStatus()
            }
        }

        // Set button text to date if a filter is set
        if (viewModel.filterStart != 0L) binding.btStartDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(viewModel.filterStart)
        if (viewModel.filterEnd != 0L) binding.btEndDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(viewModel.filterEnd) // Set to today

        // Define Min/Max date values
        val items = viewModel.getItems("ASC", filtered = false)
        if (items.count() > 0) minStartCal.timeInMillis = items[0].timestamp
        else  {
            minStartCal.timeInMillis = 0
            userOutputService.showMessageAndWaitForLong(getString(R.string.emptyTable))
            return
        }
        // MaxEndCal is implicitly set to now()

        // Initialize temporary filter calendars
        filterStartCal.timeInMillis = viewModel.filterStart
        filterEndCal.timeInMillis   = viewModel.filterEnd

        // Define listeners
        //##############
        binding.btCancel.setOnClickListener {
            dismiss()
        }

        binding.btSave.setOnClickListener {
            // Save current set of variables
            if (binding.rbRolling.isChecked) viewModel.filterMode = FILTER_ROLLING
            if (binding.rbStatic.isChecked) viewModel.filterMode = FILTER_STATIC
            if (binding.rbOff.isChecked) viewModel.filterMode = FILTER_OFF

            viewModel.rollingTimeframe = rollingTimeframe
            viewModel.rollingValue = binding.etRollingValue.text.toString().toInt()

            viewModel.setFilter(filterStartCal.timeInMillis, filterEndCal.timeInMillis) // Make sure Values are updated
            requireActivity().invalidateOptionsMenu()  // Make sure the filter icon is up to date

            dismiss()
        }

        // *************
        // Date pickers
        // *************

        // when you click on the button, show DatePickerDialog that is set with OnDateSetListener
        binding.btStartDate.setOnClickListener {
            val minC: Calendar = Calendar.getInstance()
            minC.timeInMillis = minStartCal.timeInMillis
            minC.add(Calendar.DATE, -1)

            val curC: Calendar = Calendar.getInstance()
            curC.timeInMillis = max(minC.timeInMillis, filterStartCal.timeInMillis)
            curC.add(Calendar.DATE, 1)
            //, filterStartCal.timeInMillis)

            val maxC: Calendar = Calendar.getInstance()
            maxC.timeInMillis = max(maxEndCal.timeInMillis, curC.timeInMillis)
            maxC.add(Calendar.DATE, 1)

            MaterialDialog(requireContext()).show {
                datePicker(minC, maxC, curC) { _, date->
                    filterStartCal.timeInMillis = date.timeInMillis
                    setButtonText(false)
                }
            }
        }

        binding.btEndDate.setOnClickListener {
            val tmpCal: Calendar = Calendar.getInstance()
            if (viewModel.filterEnd != 0L) tmpCal.timeInMillis = viewModel.filterEnd // Implicit else = now()

            val min: Calendar = Calendar.getInstance()
            min.timeInMillis = max(minStartCal.timeInMillis, filterStartCal.timeInMillis)+1
            MaterialDialog(requireContext()).show {
                datePicker(min, null, tmpCal) { _, date->
                    filterEndCal = date
                    setButtonText(true)
                }
            }
        }

        binding.btDeleteFilterStart.setOnClickListener {
            binding.btStartDate.text = getString(R.string.startDate)
            filterStartCal.timeInMillis = 0
        }


        binding.btDeleteFilterEnd.setOnClickListener {
            binding.btEndDate.text = getString(R.string.endDate)
            filterEndCal.timeInMillis = 0 //Calendar.getInstance().timeInMillis
        }

    }

    private fun setButtonText(end: Boolean) {
        if (end) binding.btEndDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(filterEndCal.time)
        else binding.btStartDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(filterStartCal.time)
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
    }

    override fun onDestroy() {
        super.onDestroy()
        recreate(requireActivity())  // Todo: This is a hack, find a proper solution
    }

    private fun setControlStatus() {
        val setRolling = (filterMode == FILTER_ROLLING)
        val setStatic  = (filterMode == FILTER_STATIC)

        binding.btStartDate.isEnabled = setStatic
        binding.btEndDate.isEnabled = setStatic
        binding.btDeleteFilterStart.isEnabled = setStatic
        binding.btDeleteFilterEnd.isEnabled = setStatic

        binding.spRollingTimeframe.isEnabled = setRolling
        binding.etRollingValue.isEnabled = setRolling
    }
}
