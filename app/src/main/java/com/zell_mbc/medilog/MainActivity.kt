package com.zell_mbc.medilog

import android.Manifest
import android.app.Activity
import android.content.ClipData
import android.content.Context
import android.content.Intent
import android.content.Intent.*
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.text.InputType.TYPE_TEXT_VARIATION_PASSWORD
import android.util.Log
import android.view.*
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.View.OnTouchListener
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate.*
import androidx.appcompat.widget.Toolbar
import androidx.biometric.BiometricPrompt
import androidx.biometric.BiometricPrompt.PromptInfo
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.getColor
import androidx.core.graphics.drawable.DrawableCompat
import androidx.documentfile.provider.DocumentFile
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.input.input
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.zell_mbc.medilog.R.*
import com.zell_mbc.medilog.bloodpressure.BloodPressureFragment
import com.zell_mbc.medilog.bloodpressure.BloodPressureViewModel
import com.zell_mbc.medilog.glucose.GlucoseFragment
import com.zell_mbc.medilog.glucose.GlucoseViewModel
import com.zell_mbc.medilog.data.ImportActivity
import com.zell_mbc.medilog.data.SettingsViewModel
import com.zell_mbc.medilog.data.ViewModel
import com.zell_mbc.medilog.databinding.ActivityMainBinding
import com.zell_mbc.medilog.diary.DiaryFragment
import com.zell_mbc.medilog.diary.DiaryViewModel
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.settings.SettingsActivity.Companion.KEY_PREF_APP_THEME
import com.zell_mbc.medilog.settings.SettingsActivity.Companion.KEY_PREF_AUTO_BACKUP
import com.zell_mbc.medilog.settings.SettingsActivity.Companion.KEY_PREF_BACKUP_WARNING
import com.zell_mbc.medilog.settings.SettingsActivity.Companion.KEY_PREF_FORCE_REAUTHENTICATION
import com.zell_mbc.medilog.temperature.TemperatureFragment
import com.zell_mbc.medilog.temperature.TemperatureViewModel
import com.zell_mbc.medilog.utility.DataTab
import com.zell_mbc.medilog.utility.Preferences
import com.zell_mbc.medilog.water.WaterFragment
import com.zell_mbc.medilog.water.WaterViewModel
import com.zell_mbc.medilog.weight.WeightFragment
import com.zell_mbc.medilog.weight.WeightViewModel
import com.zell_mbc.medilog.oximetry.OximetryFragment
import com.zell_mbc.medilog.oximetry.OximetryViewModel
import java.io.*
import java.net.ConnectException
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import java.util.*
import java.util.UUID.randomUUID
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import kotlin.concurrent.thread


class MainActivity : AppCompatActivity() {
    private lateinit var preferences: SharedPreferences

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private lateinit var binding: ActivityMainBinding

    lateinit var mediLog: MediLog

    private lateinit var userOutputService: UserOutputService
    private var hideMenus = false
    private val mContext: Context = this

    private var dragSource = -1 // Hold the source tab in a Drag&Drop transaction

    override fun onCreate(savedInstanceState: Bundle?) {
        preferences = Preferences.getSharedPreferences(this)

        initializeServices()

        val window = this@MainActivity.window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
//        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

        userFeedbackLevel = preferences.getInt("USER_FEEDBACK_SETTING", 0) // Drives level of feedback (nothing / standard / debug)
        themeColour = preferences.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, DEFAULT_THEME_COLOUR)
        val colours = resources.getStringArray(array.colorOptionsValues)
        when (themeColour) {
            colours[1] -> {
                theme.applyStyle(style.AppThemeGreenNoBar, true)
                window.statusBarColor = getColor(this,color.colorPrimaryDarkGreen)
            }
            colours[2] -> {
                theme.applyStyle(style.AppThemeRedNoBar, true)
                window.statusBarColor = getColor(this, color.colorPrimaryDarkRed)
            }
            colours[3] -> {
                theme.applyStyle(style.AppThemeGrayNoBar, true)
                window.statusBarColor = getColor(this, color.colorPrimaryDarkGray)
            }
            else -> {
                theme.applyStyle(style.AppThemeBlueNoBar, true)
                window.statusBarColor =  getColor(this, color.colorPrimaryDarkBlue)
            }
        }

        super.onCreate(savedInstanceState) // For some reason the theme needs to be set before the super call
        val darkMode = preferences.getString(KEY_PREF_APP_THEME, getString(string.DARK_MODE_DEFAULT))
        val themes = resources.getStringArray(array.themeValues)

        when (darkMode) {
            themes[1] -> setDefaultNightMode(MODE_NIGHT_NO)
            themes[2] -> setDefaultNightMode(MODE_NIGHT_YES)
            themes[3] -> setDefaultNightMode(MODE_NIGHT_AUTO_BATTERY)
            else      -> setDefaultNightMode(MODE_NIGHT_FOLLOW_SYSTEM)
        }

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

//        convertLegacySettings()

        mediLog = applicationContext as MediLog
        if (!preferences.getBoolean("DISCLAIMERCONFIRMED", false)) {
            // App runs for the first time -> set defaults
            val editor = preferences.edit()

            MaterialDialog(this).show {
                title(string.disclaimer)
                message(string.disclaimerText)
                positiveButton(string.agree) {
                    editor.putBoolean("DISCLAIMERCONFIRMED", true)
                    editor.apply()
                }
            }
        }

        // Initialize Settings Table
        // We need this table for generic data types, unused for now
        val settings = ViewModelProvider(this)[SettingsViewModel::class.java]
        settings.init()

        setContentView(view)

        // Initialise available tabs array
        availableTabs.clear()
        availableTabs.add(DataTab(WEIGHT, getString(string.tab_weight)))
        availableTabs.add(DataTab(BLOODPRESSURE, getString(string.tab_bloodpressure)))
        availableTabs.add(DataTab(DIARY, getString(string.tab_diary)))
        availableTabs.add(DataTab(WATER, getString(string.tab_water)))
        availableTabs.add(DataTab(GLUCOSE, getString(string.tab_glucose)))
        availableTabs.add(DataTab(TEMPERATURE, getString(string.tab_temperature)))
        availableTabs.add(DataTab(OXIMETRY, getString(string.tab_oximetry)))

        setTabOrder()
        setActiveTabs()
        val activeTab = preferences.getInt("activeTab", 0)

        // Initialize delimiter on first run based on locale
        var delimiter = preferences.getString(SettingsActivity.KEY_PREF_DELIMITER, "")
        if (delimiter.equals("")) {
            val currentLanguage = Locale.getDefault().displayLanguage
            delimiter = if (currentLanguage.contains("Deutsch")) {
                // Central Europe
                ";"
            } else ","

            val editor = preferences.edit()
            editor.putString(SettingsActivity.KEY_PREF_DELIMITER, delimiter)
            editor.apply()
        }

        hideMenus = false

        val tabAdapter = TabAdapter(this)
        viewModels.clear()

        val sortedByOrder = availableTabs.sortedWith(compareBy { it.order })
        for (tab in sortedByOrder) {
            if (!tab.active) continue // Nothing to do
            when (tab.id) { // Known data types
                WEIGHT -> {
                    viewModels.add(ViewModelProvider(this)[WeightViewModel::class.java])
                    fragments.add(WeightFragment())
                }
                BLOODPRESSURE -> {
                    viewModels.add(ViewModelProvider(this)[BloodPressureViewModel::class.java])
                    fragments.add(BloodPressureFragment())
                }
                DIARY -> {
                    viewModels.add(ViewModelProvider(this)[DiaryViewModel::class.java])
                    fragments.add(DiaryFragment())
                }
                WATER -> {
                    viewModels.add(ViewModelProvider(this)[WaterViewModel::class.java])
                    fragments.add(WaterFragment())
                }
                GLUCOSE -> {
                    viewModels.add(ViewModelProvider(this)[GlucoseViewModel::class.java])
                    fragments.add(GlucoseFragment())
                }
                TEMPERATURE -> {
                    viewModels.add(ViewModelProvider(this)[TemperatureViewModel::class.java])
                    fragments.add(TemperatureFragment())
                }
                OXIMETRY -> {
                    viewModels.add(ViewModelProvider(this)[OximetryViewModel::class.java])
                    fragments.add(OximetryFragment())
                }
                else -> {
                    continue
                }
            }
                viewModels[viewModels.size - 1].init(tab.id)
                tabAdapter.addFragment(fragments[fragments.size - 1], tab.label)
        }

        // No active tabs?
        if (viewModels.size == 0) {
            hideMenus = true
            userOutputService.showMessageAndWaitForLong(getString(string.noActiveTab))
        }

        binding.viewPager.adapter = tabAdapter
        binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
//                Log.d("Position:", tab?.position.toString())
//                 Log.d("ViewPager CurrentItem:", binding.viewPager.currentItem.toString())
                binding.viewPager.setCurrentItem(binding.viewPager.currentItem, false)
                invalidateOptionsMenu()
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {} // Define abstract function
            override fun onTabReselected(tab: TabLayout.Tab) {} // Define abstract function
        })

        val showTabText    = preferences.getBoolean(SettingsActivity.KEY_PREF_SHOWTABTEXT, getString(string.SHOW_TABTEXT_DEFAULT).toBoolean())
        val showTabIcon    = preferences.getBoolean(SettingsActivity.KEY_PREF_SHOWTABICON, getString(string.SHOW_TABICONS_DEFAULT).toBoolean())
        val scrollableTabs = preferences.getBoolean(SettingsActivity.KEY_PREF_SCROLLABLETABS, getString(string.SCROLL_TABS_DEFAULT).toBoolean())

        // If both text and icon are displayed we need a bit more space
        if (showTabIcon && showTabText) binding.viewPager.setPadding(0, 30, 0, 0)

        tabAdapter.showTitle(showTabText)
        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
            tab.text = tabAdapter.getPageTitle(position)
            Log.d("TabText:", tab.text.toString())
        }.attach()

        // Set up drag and drop listeners
        for (pos in 0..binding.tabLayout.tabCount) {

            var longPress = false

            val gestureDetector = GestureDetector(object : SimpleOnGestureListener() {
                override fun onLongPress(e: MotionEvent) {
                    longPress = true
                }
            })

            class MyTouchListener : OnTouchListener {
                override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
                    gestureDetector.onTouchEvent(motionEvent)
                    view.performClick()

                    return if (longPress) { //motionEvent.action == MotionEvent.ACTION_MOVE) {
                        val data = ClipData.newPlainText("", "")
                        val shadowBuilder = View.DragShadowBuilder(view)

                        @Suppress("DEPRECATION")
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) view.startDragAndDrop(data, shadowBuilder, view, 0)
                        else view.startDrag(data, shadowBuilder, view, 0)

                        val vv = view as (TabLayout.TabView)
                        if (vv.tab?.position != null) dragSource = vv.tab!!.position
                        true
                    } else {
                        false
                    }
                }
            }

            val v = binding.tabLayout.getTabAt(pos)?.view
            v?.setOnTouchListener(MyTouchListener())

            v?.setOnDragListener { vi, event ->
                when (event.action) {
                    DragEvent.ACTION_DROP -> {
                        val vv = vi as (TabLayout.TabView)
                        var dragTarget = -1
                        if (vv.tab?.position != null) dragTarget = vv.tab!!.position
                        reorderTabs(dragSource, dragTarget)
                    }
                }
                true
            }
        }

        if (scrollableTabs) binding.tabLayout.tabMode = TabLayout.MODE_SCROLLABLE
        else binding.tabLayout.tabMode = TabLayout.MODE_FIXED

        if (showTabIcon) {
            for (i in 0..viewModels.size) {
                binding.tabLayout.getTabAt(i)?.setIcon(viewModels[i].tabIcon)
            }
        }

        binding.viewPager.currentItem = activeTab

        val toolbar = findViewById<Toolbar>(id.toolbar)
        setSupportActionBar(toolbar)

        // Enforce white on the overflow menu, unbelievable :-(
        var drawable = toolbar.overflowIcon
        if (drawable != null) {
            drawable = DrawableCompat.wrap(drawable)
            DrawableCompat.setTint(drawable.mutate(), getColor(this, color.colorWhite))
            toolbar.overflowIcon = drawable
        }

        checkBackup()

        // https://stackoverflow.com/questions/67299835/viewpager2-selectcurrentitem-select-tab-but-place-wrong-fragment-inside-this
        binding.viewPager.setCurrentItem(binding.viewPager.currentItem, false)
    }

    private fun checkBackup() {
        // Is the DB empty? If yes, don't bother with backups
        // We consider everything below 5 entires to be simple tests
        val dbSize = viewModels[0].getDataTableSize()
        if (dbSize <5 ) return

        // Did we check already today? If yes, don't bother asking again
        val dateDiff = Date().time - preferences.getLong("LAST_BACKUP_CHECK", 0L)
        if (TimeUnit.MILLISECONDS.toDays(dateDiff) < 1L) return

        var editor = preferences.edit()
        editor.putLong("LAST_BACKUP_CHECK", Date().time)
        editor.apply()

        val lastBackup = preferences.getLong("LAST_BACKUP", 0L)
        val backupAgeMS = Date().time - lastBackup
        val backupAgeDays = TimeUnit.DAYS.convert(backupAgeMS, TimeUnit.MILLISECONDS)

        // Backup warning
        val backupWarningDays: Int
        // Check if backupWarning is enabled
        val backupWarning = preferences.getString(KEY_PREF_BACKUP_WARNING, getString(string.BACKUP_WARNING_DEFAULT))
        if (!backupWarning.isNullOrEmpty()) {
            backupWarningDays = try { backupWarning.toInt() }
                                catch (e: NumberFormatException) { getString(string.BACKUP_WARNING_DEFAULT).toInt() }
            if (backupAgeDays >= backupWarningDays) userOutputService.showMessageAndWaitForLong(getString(string.backupOverdue))
        }

        // Auto Backup
        // Check if autoBackup is enabled
        val autoBackup = preferences.getString(KEY_PREF_AUTO_BACKUP, getString(string.AUTO_BACKUP_DEFAULT))
        if (autoBackup.isNullOrEmpty()) return

        val autoBackupDays = try { autoBackup.toInt() }
                         catch (e: NumberFormatException) { getString(string.AUTO_BACKUP_DEFAULT).toInt() }

        if (backupAgeDays >= autoBackupDays) {
            // Check if everything is in place for auto backups
            val uriString = preferences.getString(SettingsActivity.KEY_PREF_BACKUP_URI, "")
            if (uriString.isNullOrEmpty()) {
                userOutputService.showMessageAndWaitForLong(getString(string.missingBackupLocation))
                return
            }

            // Valid location?
            val uri = Uri.parse(uriString)
            val dFolder = DocumentFile.fromTreeUri(this, uri)
            if (dFolder == null) {
                userOutputService.showMessageAndWaitForLong(getString(string.invalidBackupLocation))
                return
            }

            // Password
            var zipPassword = preferences.getString(SettingsActivity.KEY_PREF_PASSWORD, "")
            if (zipPassword.isNullOrEmpty()) zipPassword = ""


//            userOutputService.showMessageAndWaitForLong(getString(string.autoBackupStart))
            viewModels[0].writeBackup(uri, zipPassword, true)

            editor = preferences.edit()
            editor.putLong("LAST_BACKUP_CHECK", Date().time)
            editor.apply()
        }
    }


    private fun initializeServices() {
        userOutputService = UserOutputServiceImpl(applicationContext, findViewById(android.R.id.content))
    }

    override fun onStart() {
        super.onStart()
        checkTimeout()
    }

    private fun checkTimeout() {
        if (!preferences.getBoolean(SettingsActivity.KEY_PREF_BIOMETRIC, false)) {
            mediLog.setAuthenticated(true)
            return
        }

        val tmp = "" + preferences.getString(KEY_PREF_FORCE_REAUTHENTICATION, getString(string.FORCE_REAUTHENTICATION_DEFAULT))
        val threshold = 60000 * try { tmp.toLong() }
        catch (e: NumberFormatException) { getString(string.FORCE_REAUTHENTICATION_DEFAULT).toLong() }

        // Check timer
        val now = Calendar.getInstance().timeInMillis
        val then = preferences.getLong("STOPWATCH", 0L)
        val diff = now - then

        // First check if authentication is enabled
        // If yes, check if re-authentication needs to be forced = if more than threshold milliseconds passed after last onStop event was executed
        if (diff > threshold) {
            mediLog.setAuthenticated(false)
        }

        val biometricHelper = BiometricHelper(this)
        val canAuthenticate = biometricHelper.canAuthenticate(true)
        if (!mediLog.isAuthenticated() && canAuthenticate == 0) {
            // No idea why I need this
            val newExecutor: Executor = Executors.newSingleThreadExecutor()
            val activity: FragmentActivity = this
            val myBiometricPrompt = BiometricPrompt(activity, newExecutor, object : BiometricPrompt.AuthenticationCallback() {

                override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                    super.onAuthenticationError(errorCode, errString)
                    userOutputService.showMessageAndWaitForDurationAndAction(
                            "$errString " + getString(string.retryActionText),
                            30000,
                            getString(string.retryAction),
                            { onStart() },
                            Color.RED)
                }

                override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                    super.onAuthenticationSucceeded(result)
                    mediLog.setAuthenticated(true)
                    runOnUiThread { binding.viewPager.visibility = View.VISIBLE }
                }

            })
            runOnUiThread { binding.viewPager.visibility = View.INVISIBLE }
            if (Build.VERSION.SDK_INT > 29) {
                val promptInfo = PromptInfo.Builder()
                        .setTitle(getString(string.appName))
                        .setDeviceCredentialAllowed(true)  // Allow to use pin as well
                        .setSubtitle(getString(string.biometricLogin))
                        .setConfirmationRequired(false)
                        .build()
                myBiometricPrompt.authenticate(promptInfo)
            } else {
                // Pin fallback Disabled for now until bug https://issuetracker.google.com/issues/142740104 is fixed)
                val promptInfo = PromptInfo.Builder()
                        .setTitle(getString(string.appName))
                        .setNegativeButtonText(getString(string.cancel))
                        .setSubtitle(getString(string.biometricLogin)) //                        .setDescription(getString(R.string.biometricLogin)
                        .setConfirmationRequired(false)
                        .build()
                myBiometricPrompt.authenticate(promptInfo)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)

        val filterActive: Boolean

        val selectedTabPosition = binding.tabLayout.selectedTabPosition
        if (selectedTabPosition >= 0) {
            binding.viewPager.currentItem = selectedTabPosition
            filterActive = checkFilterState(selectedTabPosition)
        } else {
            binding.viewPager.currentItem = 0
            filterActive = checkFilterState(0)
        }

        val filterIcon = menu.findItem(id.action_setFilter).icon
        filterIcon.mutate()
        if (filterActive) {
            filterIcon.setTint(getColor(this, color.colorActiveFilter))
        }

        // Disable certain menus if no active Tab exists
        if (hideMenus) {
            menu.findItem(id.action_dataManagement).isVisible = false
            menu.findItem(id.actionPDF).isVisible = false
        }
        return true
    }

    private fun checkFilterState(selectedTabPosition: Int) = (
            viewModels.isNotEmpty() && (viewModels[selectedTabPosition].filterStart + viewModels[selectedTabPosition].filterEnd) > 0L
            )


    // Menu
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        val activeTab = binding.tabLayout.selectedTabPosition
        when (id) {
            R.id.action_setFilter -> {
                val bundle = Bundle()

                val vm = viewModels[binding.tabLayout.selectedTabPosition]
                bundle.putInt("activeTab", activeTab)

                if (vm.getSize(false) == 0) {
                    userOutputService.showMessageAndWaitForLong(getString(string.emptyTable))
                    return true
                }

                val dialogFragment = FilterFragment()
                dialogFragment.arguments = bundle

                val ft = supportFragmentManager.beginTransaction()
                val prev: Fragment? = supportFragmentManager.findFragmentByTag("FilterFragment")
                if (prev != null) ft.remove(prev)
                ft.addToBackStack(null)
                dialogFragment.show(ft, "FilterFragment")

                return true
            }

            R.id.action_settings -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
                return true
            }

            R.id.actionSendZIP -> {
                if (viewModels[binding.tabLayout.selectedTabPosition].getSize(true) == 0) userOutputService.showAndHideMessageForLong(getString(string.noDataToExport))
                else {
                    val zipPassword = preferences.getString(SettingsActivity.KEY_PREF_PASSWORD, "")
                    if (zipPassword.isNullOrEmpty()) getSendZipPassword()
                    else sendPdfFile(ZIP_INTENT, zipPassword)
                }
                return true
            }
            R.id.actionSendPDF -> {
                if (viewModels[binding.tabLayout.selectedTabPosition].getSize(true) == 0) userOutputService.showAndHideMessageForLong(getString(string.noDataToExport))
                else sendPdfFile(PDF_INTENT)
                    return true
            }
            R.id.actionOpenPDF -> {
                if (viewModels[binding.tabLayout.selectedTabPosition].getSize(true) == 0) userOutputService.showAndHideMessageForLong(getString(string.noDataToExport))
                else openFile(PDF_INTENT)
                return true
            }
            R.id.actionOpenCSV -> {
                if (viewModels[binding.tabLayout.selectedTabPosition].getSize(true) == 0) userOutputService.showAndHideMessageForLong(getString(string.noDataToExport))
                else openFile(CSV_INTENT)
                return true
            }
            R.id.actionSendCSV -> {
                if (viewModels[binding.tabLayout.selectedTabPosition].getSize(true) == 0) userOutputService.showAndHideMessageForLong(getString(string.noDataToExport))
                else sendCsvFile(CSV_INTENT)
                return true
            }
            R.id.action_restore -> {
                // Need to explicitly ask for permission once
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                if (!hasPermissions(mContext, *permissions)) {
                    ActivityCompat.requestPermissions((mContext as Activity), permissions, REQUEST)
                }

                // Pick backup folder
                var chooseFile = Intent(ACTION_GET_CONTENT)
                chooseFile.type = "*/*"
                chooseFile = createChooser(chooseFile, this.getString(string.selectFile))
                startActivityForResult(chooseFile, rcRESTORE)
                return true
            }
            R.id.action_backup -> {
                // Need to explicitly ask for permission once
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                if (!hasPermissions(mContext, *permissions)) {
                    ActivityCompat.requestPermissions((mContext as Activity), permissions, REQUEST)
                }
//                userOutputService.showMessageAndWaitForLong(this.getString(string.selectDirectory))

                // Pick backup folder
                val intent = Intent(ACTION_OPEN_DOCUMENT_TREE)
                intent.addCategory(CATEGORY_DEFAULT)
                intent.addFlags(FLAG_GRANT_PERSISTABLE_URI_PERMISSION)
                intent.addFlags(FLAG_GRANT_READ_URI_PERMISSION)
                intent.addFlags(FLAG_GRANT_WRITE_URI_PERMISSION)
                startActivityForResult(createChooser(intent, this.getString(string.selectDirectory)), rcBACKUP)
                return true
            }
            R.id.action_about -> {
                val intent = Intent(this, AboutActivity::class.java)
                startActivity(intent)
                return true
            }
/*            R.id.action_editCustomData -> {
                val intent = Intent(this, CustomTabsEditActivity::class.java)
                startActivity(intent)
                return true
            }*/
        }
        return super.onOptionsItemSelected(item)
    }

    private fun openFile(type: String) {
        var uri: Uri? = null
        when (type) {
            PDF_INTENT -> {
                val doc = viewModels[binding.tabLayout.selectedTabPosition].createPdfDocument(true)
                if (doc != null) uri = viewModels[binding.tabLayout.selectedTabPosition].getPdfFile(doc)
            }
            CSV_INTENT -> {
                uri = viewModels[binding.tabLayout.selectedTabPosition].writeCsvFile(true)
            }
        }

        if (uri != null) {
            val intent = Intent(ACTION_VIEW)
            intent.type = type
            intent.data = uri
            intent.flags = FLAG_GRANT_READ_URI_PERMISSION
            try {
                startActivity(intent)
            } catch (e: Exception) {
                userOutputService.showMessageAndWaitForLong(getString(string.eShareError) + ": " + e.localizedMessage)
            }
        }
    }

    private fun sendPdfFile(type: String, zipPassword: String = "") {
        // Need to explicitly ask for permission once
        val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (!hasPermissions(mContext, *permissions)) {
            ActivityCompat.requestPermissions((mContext as Activity), permissions, REQUEST)
        }

        val intent = Intent(ACTION_SEND)
        intent.type = type
        intent.flags = FLAG_GRANT_READ_URI_PERMISSION
        intent.putExtra(EXTRA_TEXT, getString(string.sendText))

        intent.putExtra(EXTRA_SUBJECT, "MediLog " + getString(string.data))
        val vm = viewModels[binding.tabLayout.selectedTabPosition]
        var uri: Uri? = null
        when (type) {
            ZIP_INTENT -> uri = vm.getZIP(vm.createPdfDocument(true), zipPassword)
            CSV_INTENT -> uri = vm.writeCsvFile(true)
            PDF_INTENT -> uri = vm.getPdfFile(viewModels[binding.tabLayout.selectedTabPosition].createPdfDocument(true))
        }

        if ((uri != null) && (intent.resolveActivity(packageManager) != null)) {
            intent.putExtra(EXTRA_STREAM, uri)
            try {
                startActivity(createChooser(intent, getString(string.sendText)))
            } catch (e: Exception) {
                userOutputService.showMessageAndWaitForLong(getString(string.eShareError) + ": " + e.localizedMessage)
            }
        }
    }

    private fun sendCsvFile(type: String) {
        // Need to explicitly ask for permission once
        val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (!hasPermissions(mContext, *permissions)) {
            ActivityCompat.requestPermissions((mContext as Activity), permissions, REQUEST)
        }

        val intent = Intent(ACTION_SEND)
        intent.type = type
        intent.flags = FLAG_GRANT_READ_URI_PERMISSION
        intent.putExtra(EXTRA_TEXT, getString(string.sendText))

        intent.putExtra(EXTRA_SUBJECT, "MediLog " + getString(string.data))
        val vm = viewModels[binding.tabLayout.selectedTabPosition]
        var uri: Uri? = null
        when (type) {
            CSV_INTENT -> uri = vm.writeCsvFile(true)
        }

        if ((uri != null) && (intent.resolveActivity(packageManager) != null)) {
            intent.putExtra(EXTRA_STREAM, uri)
            try {
                startActivity(createChooser(intent, getString(string.sendText)))
            } catch (e: Exception) {
                userOutputService.showMessageAndWaitForLong(getString(string.eShareError) + ": " + e.localizedMessage)
            }
        }
    }

    private fun getSendZipPassword() {
        MaterialDialog(this).show {
            title(string.enterPassword)
            val ret = input(inputType = InputType.TYPE_CLASS_TEXT or TYPE_TEXT_VARIATION_PASSWORD, allowEmpty = true, hintRes = string.password) { _, text ->
                sendPdfFile(ZIP_INTENT, text.toString())
            }
            positiveButton(string.submit)
            negativeButton(string.cancel)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            super.onActivityResult(requestCode, resultCode, data)
            data ?: return

            // Backup section
            if (requestCode == rcBACKUP && resultCode == Activity.RESULT_OK) {
                var backupFolderUri: Uri? = null

                // Check Uri is sound
                try {
                    backupFolderUri = data.data
                } catch (e: Exception) {
                    userOutputService.showMessageAndWaitForLong(this.getString(string.eSelectDirectory) + " " + data)
                    e.printStackTrace()
                }
                if (backupFolderUri == null) {
                    userOutputService.showMessageAndWaitForLong(this.getString(string.eSelectDirectory) + " " + data)
                    return
                }

                val zipPassword = preferences.getString(SettingsActivity.KEY_PREF_PASSWORD, "")
                if (zipPassword.isNullOrEmpty()) {
                    MaterialDialog(this).show {
                        title(string.enterPassword)
                        val ret = input(inputType = InputType.TYPE_CLASS_TEXT or TYPE_TEXT_VARIATION_PASSWORD, allowEmpty = true, hintRes = string.password) { _, text ->
                            viewModels[binding.tabLayout.selectedTabPosition].writeBackup(backupFolderUri, text.toString())
                        }
                        positiveButton(string.submit)
                        negativeButton(string.cancel)
                    }
                } else viewModels[binding.tabLayout.selectedTabPosition].writeBackup(backupFolderUri, zipPassword)
            }

            // Restore from backup
            if (requestCode == rcRESTORE  && resultCode == Activity.RESULT_OK) {
                var backupFileUri: Uri? = null
                try {
                    backupFileUri = data.data
                } catch (e: Exception) {
                    userOutputService.showMessageAndWaitForLong(this.getString(string.eSelectDirectory) + " " + data)
                }

                backupFileUri ?: return

                val existingRecords = viewModels[binding.tabLayout.selectedTabPosition].getDataTableSize()

                // Check if user needs to be warned
                if (existingRecords > 0) {
                    MaterialDialog(this).show {
                        title(string.warning)
                        message(text = getString(string.thisIsGoing, existingRecords) + "\n" + getString(string.doYouReallyWantToContinue))
                        negativeButton(string.cancel)
                        positiveButton(string.yes) { launchRestoreIntent(backupFileUri) }
                    }
                }
                else launchRestoreIntent(backupFileUri)
            }
        }

        private fun launchRestoreIntent(backupFileUri: Uri) {
            // Analyse file
            val cr = contentResolver

            when (val mimeType = cr.getType(backupFileUri)) {
                ZIP_INTENT -> {
                    val zipPassword = preferences.getString(SettingsActivity.KEY_PREF_PASSWORD, "")
                    if (zipPassword.isNullOrEmpty()) getOpenZipPassword(backupFileUri)
                    else {
                        val intent = Intent(application, ImportActivity::class.java)
                        intent.putExtra("URI", backupFileUri.toString())
                        intent.putExtra("PWD", zipPassword)
                        startActivity(intent)
                    }
                }
                "application/csv",
                "text/csv",
                "text/comma-separated-values" -> {
                    val intent = Intent(application, ImportActivity::class.java)
                    intent.putExtra("URI", backupFileUri.toString())
                    intent.putExtra("PWD", "CSV")
                    startActivity(intent)
                }
                else -> userOutputService.showMessageAndWaitForLong("Don't know how to handle file type: $mimeType")
            }
        }

    private fun getOpenZipPassword(zipUri: Uri) {
        MaterialDialog(this).show {
            title(string.enterPassword)
            val ret = input(inputType = InputType.TYPE_CLASS_TEXT or TYPE_TEXT_VARIATION_PASSWORD, allowEmpty = true, hintRes = string.password) { _, text ->
                val intent = Intent(application, ImportActivity::class.java)
                intent.putExtra("URI", zipUri.toString())
                intent.putExtra("PWD", text.toString())
                startActivity(intent)
            }
            positiveButton(string.submit)
            negativeButton(string.cancel)
        }
    }

    // Map hashset created by settings dialog to active flag in tab array
    private fun setActiveTabs() {
        // Check that activeTabs matches the hashSet
        val hashSet = preferences.getStringSet(SettingsActivity.KEY_PREF_ACTIVE_TABS_SET, HashSet())
                ?: return // hashSet == null shouldn't happen

        // 0 if tab settings were never opened
        if (hashSet.size == 0) {
            for (tab in availableTabs) tab.active =  true
        }
        var i: Int
        for (id in hashSet) {
            try {
                i = id.toInt()
                for (tab in availableTabs) if (tab.id == i) tab.active = true
            }
            catch (e: Exception) {
                // Try to match based on text label, might be pre v2.2 hashSet style
                for (tab in availableTabs) if (tab.label == id) tab.active = true
            }
        }
    }

    // Align availableTabs array (recreated) with tabOrder array (persistent)
    private fun setTabOrder() {
        // Check that activeTabs matches the hashSet
        val tabOrder = getIntArray("TAB_ORDER", TAB_ORDER_DEFAULT)
        var order = 0
        var i: Int
        for (id in tabOrder) {
            try {
                i = id
                for (tab in availableTabs) if (tab.id == i) tab.order = order++
            }
            catch (e: Exception) {
                Log.d("TabOrder Error", "i")
            }
        }
    }

    private fun reorderTabs(dragSource: Int, dragTarget: Int) {
        if (dragSource == dragTarget) return

        val tabOrder = getIntArray("TAB_ORDER", TAB_ORDER_DEFAULT)

        val tmp = tabOrder[dragSource]
        tabOrder.removeAt(dragSource)
        tabOrder.add(dragTarget, tmp)

        setIntArray("TAB_ORDER", tabOrder)
        this.recreate()
    }

    private fun getIntArray(key: String, default: String): ArrayList<Int> {
        val s = preferences.getString(key, default)
        val st = StringTokenizer(s, ",")
        val result = ArrayList<Int>()
        while (st.hasMoreTokens()) {
            result.add(st.nextToken().toInt())
        }
        return result
    }

    private fun setIntArray(key: String, a: ArrayList<Int>) {
        val str: StringBuilder = StringBuilder()
        for (i in a) {
            str.append(i.toString()).append(",")
        }

        val editor = preferences.edit()
        editor?.putString(key, str.toString())
        editor?.apply()
    }
// #####################################################
// Application independent code
// #####################################################
    public override fun onPause() {
        super.onPause()
        // Save active tab
        val editor = preferences.edit()
        editor.putInt("activeTab", binding.tabLayout.selectedTabPosition)
        editor.apply()
    }

    public override fun onStop() {
        super.onStop()
        // Save active tab
        val editor = preferences.edit()
        editor.putLong("STOPWATCH", Calendar.getInstance().timeInMillis) // Set Re-authentication timer
        editor.apply()
    }

/*    private fun convertLegacySettings() {
        // v2.4.3 switched to individual settings, read the previous / single one and if on, set all individual ones to on
        if (preferences.getBoolean("cbHighlightValues", false)) {
            val editor = preferences.edit()
            editor.putBoolean(SettingsActivity.KEY_PREF_BLOODPRESSURE_HIGHLIGHT_VALUES, true)
            editor.putBoolean(SettingsActivity.KEY_PREF_WEIGHT_HIGHLIGHT_VALUES, true)
            editor.putBoolean(SettingsActivity.KEY_PREF_OXIMETRY_HIGHLIGHT_VALUES, true)
            editor.putBoolean(SettingsActivity.KEY_PREF_GLUCOSE_HIGHLIGHT_VALUES, true)
            editor.remove("cbHighlightValues")
            editor.apply()
        }
    }
*/

    companion object {
        var activeTabs = ArrayList<String>()
        val availableTabs = ArrayList<DataTab>() // Holds all known tabs, active and inactive
        var fragments = ArrayList<Fragment>()

        var themeColour: String? = null

        const val DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss"
        const val DATE_PATTERN = "yyyy-MM-dd"
        const val THRESHOLD_DELIMITER = "-"
        const val rcRESTORE = 9998
        const val rcBACKUP  = 9999

        // Constants to be used with settings viewModel
        enum class Tabs {
            MEDILOG,
            WEIGHT,
            BLOODPRESSURE,
            DIARY,
            WATER,
            GLUCOSE,
            TEMPERATURE,
            OXIMETRY
        }

        const val MEDILOG = 0
        const val WEIGHT = 1
        const val BLOODPRESSURE = 2
        const val DIARY = 3
        const val WATER = 4
        const val GLUCOSE = 5
        const val TEMPERATURE = 6
        const val OXIMETRY = 7
        const val TAB_ORDER_DEFAULT = "1,2,3,4,5,6,7"

        const val MIN_TEXT_SIZE = "10"
        const val MAX_TEXT_SIZE = "40"
        const val DEFAULT_THEME_COLOUR = "Blue"

        const val ZIP_INTENT = "application/zip"
        const val CSV_INTENT = "application/txt"
        const val PDF_INTENT = "application/pdf"

        const val tmpComment = "temporaryEntry-YouShouldNeverSeeThis"
        const val stringDelimiter = '"'

        const val FILTER_OFF = 0
        const val FILTER_STATIC = 1
        const val FILTER_ROLLING = 2

        var viewModels = ArrayList<ViewModel>()
        var userFeedbackLevel = 0
        private const val REQUEST = 112
        private fun hasPermissions(context: Context?, vararg permissions: String): Boolean {
            if (context != null) {
                for (permission in permissions) {
                    if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                        return false
                    }
                }
            }
            return true
        }

        fun setTheme(context: Context?) {
            if (context == null) return

            val colours = context.resources.getStringArray(array.colorOptionsValues)
            when (themeColour) {
                colours[1] -> context.theme.applyStyle(style.AppThemeGreenBar, true)
                colours[2] -> context.theme.applyStyle(style.AppThemeRedBar, true)
                colours[3] -> context.theme.applyStyle(style.AppThemeGrayBar, true)
                else       -> context.theme.applyStyle(style.AppThemeBlueBar, true)
            }
        }

        fun resetReAuthenticationTimer(context: Context?) {
            if (context == null) return

            // Reset Stopwatch, after all we are still in MediLog
            val preferences = Preferences.getSharedPreferences(context)
            val editor = preferences.edit()
            editor.putLong("STOPWATCH", Calendar.getInstance().timeInMillis) // Set Re-authentication timer
            editor.apply()
        }

        fun getViewModel(dataType: Int):ViewModel? {
            var found:ViewModel? = null
            for (vm in viewModels) {
                if (vm.dataType == dataType) found = vm
            }
            return found
        }

        fun userFeedback(context: Context?):String {
            if (context == null) return ""

            val delimiter = ":"
            var tabs = delimiter
            for (tab in availableTabs) {
                tabs += viewModels[0].getSize(tab.id).toString() + delimiter
            }

            val preferences = Preferences.getSharedPreferences(context)
            val devID = preferences.getString("DEVICE_ID", randomUUID().toString())
            val data = devID + tabs.substring(0, tabs.length-1) // Cut off trailing delimiter

            val reqParam = URLEncoder.encode(data, "UTF-8")
            val mURL = URL( context.getString(string.userFeedbackUrl)) // + reqParam)

            thread {
                val https = mURL.openConnection() as HttpURLConnection
                https.requestMethod = "POST"
                https.doOutput = true

                val editor = preferences.edit()
                try {
                    val wr = OutputStreamWriter(https.outputStream)
                    wr.write(reqParam)
                    wr.flush()
                    val ret = https.responseCode // Required to close the POST request
                    editor.putString("DEVICE_ID", devID)
                    editor.putLong("LAST_FEEDBACK_RUN", Date().time)
                    if (ret == 200) {
                        editor.putString("USER_FEEDBACK_STRING", data)
                    }
                    else {
                        // Save error message
                        editor.putString("USER_FEEDBACK_STRING", "Error: $ret")
                    }
                    //Log.d("Success Return: ", ret.toString())
                  wr.close()
                }
                catch (e: ConnectException) { // Don't care. only need the ping
                    // Save error message
                    editor.putString("USER_FEEDBACK_STRING", "Exception: $e")
                    //Log.d("Return: Exception", e.toString())
                }
                editor.apply()
            }
            return data
        }
    }
}

