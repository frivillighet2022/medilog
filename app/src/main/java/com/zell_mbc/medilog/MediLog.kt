package com.zell_mbc.medilog

import android.app.Application
import android.content.SharedPreferences
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utility.Preferences

class MediLog : Application() {
    private var authenticated: Boolean = false
    private var hasBiometric = true
    private lateinit var preferences: SharedPreferences

    // Called when the application is starting, before any other application objects have been created.
    // Overriding this method is totally optional!
    override fun onCreate() {
        super.onCreate()
        preferences = Preferences.getSharedPreferences(this)

        // If Authenticated is false lets check if we want to and if this device can authenticate
          if (!authenticated) {
              if (!preferences.getBoolean(SettingsActivity.KEY_PREF_BIOMETRIC, false)) {
                  authenticated = true         // If user set biometric login off, we are always authenticated
              } else { // Biometric setting is on -> Let's check if authentication is possible
                  val biometricHelper = BiometricHelper(this)
                  // Check if biometric device  exists, if not remove biometric setting in settings activity and set authenticated to always true
                  if (biometricHelper.hasHardware(false)) {
                      authenticated = false    // Make sure we launch the Biometric logon process in onStart
                  } else {
                      authenticated = true
                      hasBiometric = false
                  }
              }
      }

      // Set to default on first launch
      if (preferences.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue)) == "") {
          val editor = preferences.edit()
          editor.putString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))
          editor.apply()
      }
  }

    fun setAuthenticated(b: Boolean) { authenticated = b }
    fun isAuthenticated () : Boolean { return authenticated }
}

