package com.zell_mbc.medilog

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import java.util.*

class TabAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {

private val mFragmentList: MutableList<Fragment> = ArrayList()
    private val mFragmentTitleList: MutableList<String> = ArrayList()
    private var showTitle = true

    override fun createFragment(position: Int): Fragment {
        return mFragmentList[position]
    }

    fun addFragment(fragment: Fragment, title: String) {
        mFragmentList.add(fragment)
        mFragmentTitleList.add(title)
    }

    fun getPageTitle(position: Int): CharSequence? {
        return if (showTitle) mFragmentTitleList[position]
        else null
    }

    override fun getItemCount(): Int {
        return mFragmentList.size
    }

    fun showTitle(setShowTitle: Boolean) { showTitle = setShowTitle}
}