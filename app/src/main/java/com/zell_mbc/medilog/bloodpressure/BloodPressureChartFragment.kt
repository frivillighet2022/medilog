package com.zell_mbc.medilog.bloodpressure

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.DashPathEffect
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.androidplot.ui.*
import com.androidplot.util.PixelUtils
import com.androidplot.xy.*
import com.zell_mbc.medilog.MainActivity.Companion.BLOODPRESSURE
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utility.*
import java.text.*
import java.util.*
import kotlin.math.roundToInt


class BloodPressureChartFragment : Fragment() {
    private var sysBand1 = ArrayList<Float>()
    private var sysBand2 = ArrayList<Float>()
    private var sysBand3 = ArrayList<Float>()
    private var sys = ArrayList<Float>()

    private var diaBand1 = ArrayList<Float>()
    private var diaBand2 = ArrayList<Float>()
    private var diaBand3 = ArrayList<Float>()
    private var dia = ArrayList<Float>()
    private var sysLinearTrend = ArrayList<Float>()
    private var diaLinearTrend = ArrayList<Float>()
    private var sysSMATrend = ArrayList<Float>()
    private var diaSMATrend = ArrayList<Float>()
    private var period = 5 // Minimum value = 2

    private fun calculateMovingAverage(source: ArrayList<Float>, target: ArrayList<Float>) {
        val sample = Array(period) { 0f} // Create array of float, with all values set to 0
        val n = source.size
        var sma: Float

        // the first n values in the sma will be off -> set them to the first weight value
        for (i in 0 until period) {
            sample[i] = source[0]
        }

        for (i in 0 until n) {
            for (ii in 0..period-2) {
                sample[ii] = sample[ii + 1]
            }
            sample[period - 1] = source[i]

            sma = 0f
            for (ii in 0 until period) {
                sma += sample[ii]
            }
            sma /= period
            target.add(sma)
        }
    }

    private fun calculateLinearTrendLine(source: ArrayList<Float>, target: ArrayList<Float>) {
        // https://classroom.synonym.com/calculate-trendline-2709.html
        var a = 0f
        val b: Float
        var b1 = 0
        var b2 = 0f
        var c = 0
        val f: Float
        val g: Float
        val m: Float
        val n = source.size
        for (i in 1..n) {
            a += i * source[i - 1]
            b1 += i
            b2 += source[i - 1]
            c += i * i
        }
        a *= n
        b = b1 * b2
        c *= n
        val d: Float = b1 * b1.toFloat()
        m = (a - b) / (c - d)
        val e: Float = b2
        f = m * b1
        g = (e - f) / n
        var value: Float
        for (i in 1..n) {
            value = m * i + g
            target.add(value)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.bloodpressure_chart, container, false)
    }

    @SuppressLint("SimpleDateFormat")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val c = context ?: return

        // create a couple arrays of y-values to plot:
        val labels = ArrayList<String>()
        val pulse = ArrayList<Float>()
        var wMax = 0f
        var wMin = 1000f

        val preferences = Preferences.getSharedPreferences(requireContext())
        val daySteppingMode = preferences.getBoolean(SettingsActivity.KEY_PREF_bpDayStepping, false)
        val barChart = preferences.getBoolean(SettingsActivity.KEY_PREF_bpBarChart, false)
        val showPulse =preferences.getBoolean(SettingsActivity.KEY_PREF_SHOWPULSE, true)
        val showGrid = preferences.getBoolean(SettingsActivity.KEY_PREF_showBloodPressureGrid, true)
        val isLegendVisible = preferences.getBoolean(SettingsActivity.KEY_PREF_showBloodPressureLegend, false)
        val showValues = preferences.getBoolean(SettingsActivity.KEY_PREF_BLOODPRESSURE_SHOW_VALUES, true)

        // Trendlines
        val bloodPressureLinarTrendLine = preferences.getBoolean(SettingsActivity.KEY_PREF_BLOODPRESSURE_LINEAR_TRENDLINE, false)
        var bloodPressureMovingAverageTrendLine = preferences.getBoolean(SettingsActivity.KEY_PREF_BLOODPRESSURE_MOVING_AVERAGE_TRENDLINE, false)

        // If showValues is false at least one trendline needs to be active, otherwise there would be nothing to show
        if (!showValues && !bloodPressureLinarTrendLine && !bloodPressureMovingAverageTrendLine) bloodPressureMovingAverageTrendLine = true

        var sTmp: String
        var fTmp: Float
        val lastDate = Calendar.getInstance()
        var currentDate: Date?
        val simpleDate = SimpleDateFormat("MM-dd")

        val viewModel = ViewModelProvider(this)[BloodPressureViewModel::class.java]
        viewModel.init(BLOODPRESSURE)
        val items = viewModel.getItems("ASC", filtered = true)
        for (bpI in items) {
            sTmp = simpleDate.format(bpI.timestamp)

            // Chart stepping by day
            if (daySteppingMode) {
                // Fill gap
                currentDate = try {
                    simpleDate.parse(sTmp)
                } catch (e: ParseException) {
                    continue
                }

                if (currentDate != null) {
                    while ((labels.size > 0) && (currentDate > lastDate.time)) {
                        sTmp = simpleDate.format(lastDate.time)
                        labels.add(sTmp)
                        sys.add(0f)
                        dia.add(0f)
                        if (showPulse) pulse.add(0f)
                        lastDate.add(Calendar.DAY_OF_MONTH, 1)
                    }
                }
                if (currentDate != null) lastDate.time = currentDate
            }

            labels.add(sTmp)
            sys.add(bpI.value1.toFloat())
            fTmp = bpI.value1.toFloat()
            if (fTmp > wMax) {
                wMax = fTmp
            }

            dia.add(bpI.value2.toFloat())
            fTmp = bpI.value2.toFloat()
            if (fTmp < wMin) {
                wMin = fTmp
            }

            if (showPulse) {
                pulse.add(bpI.value3.toFloat())
                fTmp = bpI.value3.toFloat()
                if (fTmp < wMin) {
                    wMin = fTmp
                }
            }
        }

        if (sys.size == 0) {
            return
        }

        val threshold = preferences.getBoolean(SettingsActivity.KEY_PREF_showBloodPressureThreshold, false)
        if (threshold) {
            val bpHelper = BloodPressureHelper(c)
            val sysB1 = bpHelper.hyperGrade1Sys.toFloat()
            val sysB2 = bpHelper.hyperGrade2Sys.toFloat()
            val sysB3 = bpHelper.hyperGrade3Sys.toFloat()
            val diaB1 = bpHelper.hyperGrade1Dia.toFloat()
            val diaB2 = bpHelper.hyperGrade2Dia.toFloat()
            val diaB3 = bpHelper.hyperGrade3Dia.toFloat()
            for (item in sys) {
                sysBand1.add(sysB1)
                sysBand2.add(sysB2)
                sysBand3.add(sysB3)
                diaBand1.add(diaB1)
                diaBand2.add(diaB2)
                diaBand3.add(diaB3)
            }
        }

        // https://github.com/halfhp/androidplot/blob/master/docs/plot_composition.md
        // initialize our XYPlot reference:
        val plot: XYPlot = view.findViewById(R.id.bloodPressurePlot)
        PanZoom.attach(plot)

        val backgroundColor = getBackgroundColor(requireContext())
        plot.graph.backgroundPaint.color = backgroundColor

        val fontSizeSmall: Int = getFontSizeSmallInPx(requireContext())

        val axisPaint = Paint()
        axisPaint.style = Paint.Style.FILL_AND_STROKE
        axisPaint.color = getTextColorPrimary(requireContext())
        axisPaint.textSize = fontSizeSmall.toFloat()
        axisPaint.isAntiAlias = true

        val gridPaint = Paint()
        gridPaint.style = Paint.Style.STROKE
        gridPaint.color = getTextColorSecondary(requireContext())
        gridPaint.isAntiAlias = false
        gridPaint.pathEffect = DashPathEffect(floatArrayOf(3f, 2f), 0F)

        if (!showGrid) {
            plot.graph.domainGridLinePaint = null
            plot.graph.rangeGridLinePaint = null
        }
        else {
            plot.graph.domainGridLinePaint = gridPaint
            plot.graph.rangeGridLinePaint = gridPaint // Horizontal lines
        }

        plot.legend.isVisible = isLegendVisible

        if (isLegendVisible) {
//          Old setting  plot.legend.position(10f, HorizontalPositioning.ABSOLUTE_FROM_LEFT, 100f, VerticalPositioning.ABSOLUTE_FROM_BOTTOM, Anchor.LEFT_TOP)
            plot.legend.position(-0.5f, HorizontalPositioning.RELATIVE_TO_CENTER, 100f, VerticalPositioning.ABSOLUTE_FROM_BOTTOM, Anchor.LEFT_TOP)
            plot.legend.setWidth(1.0f, SizeMode.RELATIVE)

            if (showPulse) plot.legend.setTableModel(DynamicTableModel(3, 1, TableOrder.ROW_MAJOR))
            else           plot.legend.setTableModel(DynamicTableModel(2, 1, TableOrder.ROW_MAJOR))

            //       plot.legend.position(-0.6f, HorizontalPositioning.RELATIVE_TO_RIGHT, 0f, VerticalPositioning.ABSOLUTE_FROM_BOTTOM, Anchor.LEFT_BOTTOM)
            plot.legend.setTableModel(DynamicTableModel(3, 1, TableOrder.ROW_MAJOR))
            plot.legend.isDrawIconBackgroundEnabled = false
            // Make sure that Pulse is last
            plot.legend.legendItemComparator = Comparator { p0, p1 -> /*                    var ret = 0
                            if (p0 != null && p1 != null) {
                                if (p0.type == p1.type) { ret = p0.title.compareTo(p1.title) }
                                else { ret =  p0.type.compareTo(p1.type) }
                            } */
                0
            }
        }

        val sysBorder = ContextCompat.getColor(requireContext(), R.color.chart_blue_border)
        val sysFill = ContextCompat.getColor(requireContext(), R.color.chart_blue_fill)
        val diaBorder = ContextCompat.getColor(requireContext(), R.color.chart_red_border)
        val diaFill = ContextCompat.getColor(requireContext(), R.color.chart_red_fill)
        val pulseBorder = ContextCompat.getColor(requireContext(), R.color.chart_green_border)
        val pulseFill = ContextCompat.getColor(requireContext(), R.color.chart_green_fill)

        val series1: XYSeries = SimpleXYSeries(
            sys,
            SimpleXYSeries.ArrayFormat.Y_VALS_ONLY,
            getString(R.string.systolic)
        )
        val series2: XYSeries = SimpleXYSeries(
            dia,
            SimpleXYSeries.ArrayFormat.Y_VALS_ONLY,
            getString(R.string.diastolic)
        )
        val series3: XYSeries = SimpleXYSeries(
            pulse,
            SimpleXYSeries.ArrayFormat.Y_VALS_ONLY,
            getString(R.string.pulse)
        )

        if (showValues) {
            // Bar chart
            if (barChart) {
                val series1Format = BarFormatter(Color.BLUE, Color.BLUE)
                val series2Format = BarFormatter(Color.RED, Color.RED)
                val series3Format = BarFormatter(Color.GREEN, Color.GREEN)
                // add a new series' to the xyplot:
                plot.addSeries(series1, series1Format)
                plot.addSeries(series2, series2Format)
                if (showPulse) plot.addSeries(series3, series3Format)
            } else {
                val series1Format = LineAndPointFormatter(sysBorder, null, sysFill, null)
                val series2Format = LineAndPointFormatter(diaBorder, null, diaFill, null)
                val series3Format = LineAndPointFormatter(pulseBorder, null, pulseFill, null)

                // add a new series' to the xyplot:
                series1Format.isLegendIconEnabled = isLegendVisible
                series2Format.isLegendIconEnabled = isLegendVisible
                series3Format.isLegendIconEnabled = isLegendVisible

                plot.addSeries(series1, series1Format)
                plot.addSeries(series2, series2Format)
                if (showPulse) plot.addSeries(series3, series3Format)
            }
        }

        if (threshold) {
            val series1FormatThreshold = LineAndPointFormatter(sysBorder, null, null, null)
            val series2FormatThreshold = LineAndPointFormatter(diaBorder, null, null, null)
            val series4: XYSeries = SimpleXYSeries(sysBand1, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "140")
            val series5: XYSeries = SimpleXYSeries(sysBand2, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "160")
            val series6: XYSeries = SimpleXYSeries(sysBand3, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "180")

            val series7: XYSeries = SimpleXYSeries(diaBand1, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "90")
            val series8: XYSeries = SimpleXYSeries(diaBand2, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "100")
            val series9: XYSeries = SimpleXYSeries(diaBand3, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "110")

            series1FormatThreshold.isLegendIconEnabled = false
            series2FormatThreshold.isLegendIconEnabled = false

            series2FormatThreshold.linePaint.pathEffect = DashPathEffect(floatArrayOf( // always use DP when specifying pixel sizes, to keep things consistent across devices:
                    PixelUtils.dpToPix(20f),
                    PixelUtils.dpToPix(15f)), 0f)

            series1FormatThreshold.linePaint.pathEffect = DashPathEffect(floatArrayOf( // always use DP when specifying pixel sizes, to keep things consistent across devices:
                    PixelUtils.dpToPix(20f),
                    PixelUtils.dpToPix(15f)), 0f)

            // Sys bands
            series1FormatThreshold.linePaint.strokeWidth = 1f
            plot.addSeries(series4, series1FormatThreshold)
            series1FormatThreshold.linePaint.strokeWidth = 1f
            plot.addSeries(series5, series1FormatThreshold)
            series1FormatThreshold.linePaint.strokeWidth = 1f
            plot.addSeries(series6, series1FormatThreshold)

            // Dia bands
            series2FormatThreshold.linePaint.strokeWidth = 1f
            plot.addSeries(series7, series2FormatThreshold)
            series2FormatThreshold.linePaint.strokeWidth = 1f
            plot.addSeries(series8, series2FormatThreshold)
            series2FormatThreshold.linePaint.strokeWidth = 1f
            plot.addSeries(series9, series2FormatThreshold)
        }

        if (bloodPressureLinarTrendLine) {
            calculateLinearTrendLine(sys, sysLinearTrend)
            calculateLinearTrendLine(dia, diaLinearTrend)

            val sysLinearTrendLine: XYSeries = SimpleXYSeries(sysLinearTrend, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, null)
            val sysLinearTrendFormat = LineAndPointFormatter(Color.BLUE, null, null, null)
            sysLinearTrendFormat.isLegendIconEnabled = false
            plot.addSeries(sysLinearTrendLine, sysLinearTrendFormat)
            val diaLinearTrendLine: XYSeries = SimpleXYSeries(diaLinearTrend, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, null)
            val diaLinearTrendFormat = LineAndPointFormatter(Color.RED, null, null, null)
            diaLinearTrendFormat.isLegendIconEnabled = false
            plot.addSeries(diaLinearTrendLine, diaLinearTrendFormat)
        }

        if (bloodPressureMovingAverageTrendLine) {
            period = preferences.getString(SettingsActivity.KEY_PREF_BLOODPRESSURE_MOVING_AVERAGE_SIZE, "5")!!.toInt()

            calculateMovingAverage(sys, sysSMATrend)
            calculateMovingAverage(dia, diaSMATrend)

            val sysMovingAverageTrendLine: XYSeries = SimpleXYSeries(sysSMATrend, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, null)
            val sysMovingAverageTrendFormat = LineAndPointFormatter(Color.BLUE, null, null, null)
            sysMovingAverageTrendFormat.isLegendIconEnabled = false
            plot.addSeries(sysMovingAverageTrendLine, sysMovingAverageTrendFormat)
            val diaMovingAverageTrendLine: XYSeries = SimpleXYSeries(diaSMATrend, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, null)
            val diaMovingAverageTrendFormat = LineAndPointFormatter(Color.RED, null, null, null)

            diaMovingAverageTrendFormat.isLegendIconEnabled = false
            plot.addSeries(diaMovingAverageTrendLine, diaMovingAverageTrendFormat)
        }

        val wMinBoundary = wMin.roundToInt()
        val wMaxBoundary = wMax.roundToInt()
        plot.setRangeBoundaries(wMinBoundary, wMaxBoundary, BoundaryMode.FIXED)
        plot.setUserRangeOrigin(wMinBoundary / 10 * 10) // Set to a 10er value
        plot.outerLimits.set(0, sys.size - 1, wMinBoundary, wMaxBoundary) // For pan&zoom

        plot.setRangeStep(StepMode.INCREMENT_BY_VAL, 10.0)
        if (series1.size() < 10) plot.setDomainStep(StepMode.SUBDIVIDE, series1.size().toDouble()) // Avoid showing the same day multiple times

        plot.graph.getLineLabelStyle(XYGraphWidget.Edge.LEFT).format = DecimalFormat("#") // Set integer y-Axis label
        plot.graph.getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).format = object : Format() {
            override fun format(obj: Any, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer {
                val i = (obj as Number).toFloat().roundToInt()
                return toAppendTo.append(labels[i])
            }

            override fun parseObject(source: String, pos: ParsePosition): Any {
                return 0
            }
        }
    }
}

/*
 plot.setDrawingCacheEnabled(true);
FileOutputStream fos = new FileOutputStream(“/sdcard/DCIM/img.png”, true);
plot.getDrawingCache().compress(CompressFormat.PNG, 100, fos);
fos.close();
plot.setDrawingCacheEnabled(false);
 */