package com.zell_mbc.medilog.bloodpressure

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.BLOODPRESSURE
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.EditFragment
import com.zell_mbc.medilog.databinding.BloodpressureEditformBinding
import com.zell_mbc.medilog.settings.SettingsActivity
import java.text.DateFormat

class BloodPressureEditFragment : EditFragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: BloodpressureEditformBinding? = null
    private val binding get() = _binding!!

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = BloodpressureEditformBinding.inflate(inflater,container,false)
        initializeService(binding.root)
        return binding.root
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private var heartRhythmIssue = false
    private var standardColors = 0 // : Int //ColorStateList


    private fun stringToInt(valueString: String, errorMessage: String): Int {
        var valueInt = -1
        if (valueString.isNotEmpty()) {
            try {
                valueInt = valueString.toInt()
                if (valueInt <= 9) valueInt = -2
            }
            catch(e: Exception) {
                valueInt = -3
            }
        }
        if  (valueInt < 1) userOutputService.showMessageAndWaitForLong(errorMessage)

        return valueInt
    }

    override fun updateItem() {
        val editItem = viewModel.getItem(viewModel.editItem)
        if (editItem == null) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.unknownError))
            return
        }

        // Safely convert string values
        var value: Int = stringToInt(binding.etSys.text.toString(), getString(R.string.sysMissing))
        if (value < 0) return
        editItem.value1 = binding.etSys.text.toString()

        value = stringToInt(binding.etDia.text.toString(), getString(R.string.diaMissing))
        if (value < 0) return
        editItem.value2 = binding.etDia.text.toString()

        value = stringToInt(binding.etPulse.text.toString(), getString(R.string.pulseMissing))
        if (value < 0) return
        editItem.value3 = binding.etPulse.text.toString()

        editItem.timestamp = timestampCal.timeInMillis
        editItem.comment = binding.etComment.text.toString()
        editItem.value4 = if (heartRhythmIssue) "1" else "0"

        viewModel.update(editItem)

        userOutputService.showMessageAndWaitForLong(getString(R.string.itemUpdated))
        requireActivity().onBackPressed()
    }

    private fun setHeartRhythm() {
        heartRhythmIssue = !heartRhythmIssue
        if (heartRhythmIssue) binding.btHeartRhythm.setColorFilter(Color.RED)
        else binding.btHeartRhythm.setColorFilter(standardColors)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val vm = MainActivity.getViewModel(BLOODPRESSURE) as BloodPressureViewModel?
        if (vm != null) viewModel = vm
        else {
            userOutputService.showMessageAndWaitForLong(getString(R.string.unknownError))
            // Returning here will make sure none of the controls are initialized = no error when e.g. delete is selected
            return
        }

        saveButton = binding.btSave
        deleteButton = binding.btDelete
        super.onViewCreated(view, savedInstanceState)

        val editItem = viewModel.getItem(viewModel.editItem)
        if (editItem == null) {
            // Close keyboard so error is visible
            (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken, 0)
            userOutputService.showMessageAndWaitForLong(getString(R.string.unknownError))
            return
        }

        val logHeartRhythm = preferences.getBoolean(SettingsActivity.KEY_PREF_LOG_HEART_RHYTHM, true)

        this.binding.etSys.setText(editItem.value1)
        this.binding.etDia.setText(editItem.value2)
        this.binding.etPulse.setText(editItem.value3)

        if (logHeartRhythm) {
            standardColors =  binding.etSys.currentHintTextColor //  textColors.defaultColor

            // Invert result because setHeartRhythm inverst again
//            binding.btHeartRhythm.setText(warning) // = (editItem.value4 != "1")
            heartRhythmIssue = (editItem.value4 != "1")
            setHeartRhythm()
        }
        else binding.btHeartRhythm.visibility = View.GONE

        // Check if we are really editing or if this is a new value
        if (viewModel.isTmpItem(editItem.comment)) {
            this.binding.etComment.setText("")
            this.binding.etSys.setText("") // Make sure fields are empty to avoid the need to delete the default 0
            this.binding.etDia.setText("")
            this.binding.etPulse.setText("")
        }
        else this.binding.etComment.setText(editItem.comment)

        this.binding.etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(editItem.timestamp)
        this.binding.etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(editItem.timestamp)

        // Respond to click events
        binding.btHeartRhythm.setOnClickListener { setHeartRhythm() }

        // ---------------------
        setDateTimePicker(editItem.timestamp, binding.etDate, binding.etTime)

        // Make sure first field is highlighted and keyboard is open
        this.binding.etSys.requestFocus()
    }
}