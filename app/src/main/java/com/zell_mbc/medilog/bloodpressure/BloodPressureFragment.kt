package com.zell_mbc.medilog.bloodpressure

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.*
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.data.TabFragment
import com.zell_mbc.medilog.databinding.BloodpressureTabBinding
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utility.Amber
import com.zell_mbc.medilog.utility.ListTheme
import com.zell_mbc.medilog.utility.Red
import java.util.*


class BloodPressureFragment : TabFragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: BloodpressureTabBinding? = null
    private val binding get() = _binding!!

    private var logHeartRhythm = false
    private lateinit var bpHelper: BloodPressureHelper

    private var sysValue = ""
    private var diaValue = ""
    private var pulseValue = ""
    private var heartRhythmValue = ""
    private var commentValue = ""
    private var setHeartRhythm = false
    private var heartRhythmIconAlpha = 0.5F

    private var valueWidth = 0.dp

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = BloodpressureTabBinding.inflate(inflater, container, false)
        initializeService(binding.root)

        return binding.root
    }

    private fun stringToInt(valueString: String, errorMessage: String): Int {
        var valueInt = -1
        if (valueString.isNotEmpty()) {
            try {
                valueInt = valueString.toInt()
                if (valueInt <= 9) valueInt = -2
            }
            catch (e: Exception) {
                valueInt = -3
            }
        }
        if  (valueInt < 1) userOutputService.showMessageAndWaitForLong(errorMessage)

        return valueInt
    }

    override fun addItem() {
        // If standard entry is active create a data element and launch the editActivity
        if (!quickEntry) {
            val tmpItem = Data(0, Date().time, MainActivity.tmpComment, viewModel.dataType, "", "", "", "")
            val itemID = viewModel.insert(tmpItem)
            if (itemID  != 0L) editItem(itemID.toInt())
            else userOutputService.showMessageAndWaitForLong("No entry with tmp value found!")
            return
        }

        // ###########################
        // Checks
        // ###########################
        // Close keyboard so errors are visible
        (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken, 0)

        // Savely convert string values
        if (stringToInt(sysValue, getString(R.string.sysMissing)) < 0) return
        if (stringToInt(diaValue, getString(R.string.diaMissing)) < 0) return
        if (stringToInt(pulseValue, getString(R.string.pulseMissing)) < 0) return

        val item = Data(0, Date().time, commentValue, viewModel.dataType, sysValue, diaValue, pulseValue, if (setHeartRhythm) "1" else "0")

        viewModel.insert(item)
        if ((viewModel.filterEnd > 0L) && (viewModel.filterEnd < item.timestamp)) userOutputService.showMessageAndWaitForDuration(getString(R.string.filteredOut), 4000)

        sysValue = ""
        diaValue = ""
        pulseValue = ""
        heartRhythmValue = ""
        commentValue = ""

        binding.itemList.setContent { ShowContent() }
    }

    // Measure up Dia / Sys string
    @Composable
    fun MeasureValueString() {
        MeasureTextWidth("100")
        {
            textWidth ->
            Text(text = "", fontSize = fontSize.sp)
            valueWidth = textWidth //+ (textWidth / 10) // Add a 10% buffer
        }
    }

    @Composable
    fun ShowRow(item: Data) {
        // Timestamp
        Text(dateFormat.format(item.timestamp) + " - " + timeFormat.format(item.timestamp),
            Modifier
                .padding(end = cellPadding.dp, top = rowPadding.dp, bottom = rowPadding.dp)
                .width(dateColumnWidth),
            color = MaterialTheme.colors.primary, fontSize = fontSize.sp)
        TabRowDefaults.Divider(color = MaterialTheme.colors.secondary, modifier = Modifier
            .width(1.dp)
            .fillMaxHeight()) // Vertical separator

        if (logHeartRhythm) {
            // Check value
            var heartRhythm = -1
            if (item.value4.isNotEmpty()) {
                heartRhythm = try { item.value4.toInt() }
                catch (e: NumberFormatException) { 0 }
            }
            // Show image only if value > 0
            val s = if (heartRhythm > 0) stringResource(id = R.string.warningSign)
            else "   "
            Text(s,Modifier.padding(start = cellPadding.dp, end = cellPadding.dp),color = MaterialTheme.colors.primary,fontSize = fontSize.sp)
            TabRowDefaults.Divider(color = MaterialTheme.colors.secondary, modifier = Modifier
                .width(1.dp)
                .fillMaxHeight()) // Vertical separator
        }

        // Sys
        var textColor = MaterialTheme.colors.primary
        var fontWeight = FontWeight.Normal
        if (highlightValues) {
            when (bpHelper.sysGrade(item.value1)) {
                bpHelper.hyperGrade3 -> {
                    textColor = MaterialTheme.colors.error
                    fontWeight = FontWeight.Bold
                }
                bpHelper.hyperGrade2 -> textColor = Red // MaterialTheme.colors.error
                bpHelper.hyperGrade1 -> textColor = Amber
                bpHelper.hyperGrade4 -> textColor = Amber // Low blood pressure
            }
        }
        Text(item.value1,
            Modifier
                .padding(start = cellPadding.dp, end = cellPadding.dp)
                .width(valueWidth),textAlign = TextAlign.Center, color = textColor,fontSize = fontSize.sp, fontWeight = fontWeight)
        TabRowDefaults.Divider(color = MaterialTheme.colors.secondary, modifier = Modifier
            .width(1.dp)
            .fillMaxHeight()) // Vertical separator

        // Dia
        textColor = MaterialTheme.colors.primary
        fontWeight = FontWeight.Normal
        if (highlightValues) {
            when (bpHelper.diaGrade(item.value2)) {
                bpHelper.hyperGrade3 -> {
                    textColor = MaterialTheme.colors.error
                    fontWeight = FontWeight.Bold
                }
                bpHelper.hyperGrade2 -> textColor = MaterialTheme.colors.error
                bpHelper.hyperGrade1 -> textColor = Amber
                bpHelper.hyperGrade4 -> textColor = Amber // Low blood pressure
            }
        }
        Text(item.value2,
            Modifier
                .padding(start = cellPadding.dp, end = cellPadding.dp)
                .width(valueWidth), textAlign = TextAlign.Center,color = textColor,fontSize = fontSize.sp, fontWeight = fontWeight)
        TabRowDefaults.Divider(color = MaterialTheme.colors.secondary, modifier = Modifier
            .width(1.dp)
            .fillMaxHeight()) // Vertical separator

        // Pulse
        Text(item.value3,
            Modifier
                .padding(start = cellPadding.dp, end = cellPadding.dp)
                .width(valueWidth), textAlign = TextAlign.Center, color = MaterialTheme.colors.primary ,fontSize = fontSize.sp)
        TabRowDefaults.Divider(color = MaterialTheme.colors.secondary, modifier = Modifier
            .width(1.dp)
            .fillMaxHeight()) // Vertical separator

        // Comment
        if (item.comment.isNotEmpty()) Text(item.comment,
            Modifier.padding(start = cellPadding.dp), color = MaterialTheme.colors.primary, fontSize = fontSize.sp)
    }

    @OptIn(ExperimentalComposeUiApi::class)
    @Composable
    fun ShowContent() {
        val fieldWidth = 95
        val pulseFieldWidth = 85
        val listState = rememberLazyListState()
        var selection: Data? by remember { mutableStateOf(null)}
        var triggerRefresh by remember { mutableStateOf(false) }
        val listItems = viewModel.getItems("DESC",true)

        var sys by remember { mutableStateOf(sysValue) }
        var dia by remember { mutableStateOf(diaValue) }
        var pulse by remember { mutableStateOf(pulseValue) }
        var heartRhythm by remember { mutableStateOf(heartRhythmValue) }
        var comment by remember { mutableStateOf(commentValue) }
        var commentVisible by remember { mutableStateOf(false) }

        if (sysValue.isEmpty()) sys = ""
        if (diaValue.isEmpty()) dia = ""
        if (pulseValue.isEmpty()) pulse = ""
        if (heartRhythmValue.isEmpty()) heartRhythm = ""
        if (commentValue.isEmpty()) comment = ""

        ListTheme {
            MeasureDateString()
            MeasureValueString()

            // Only show DataEntryRow if quick entry is enabled
            if (quickEntry) {

                Column (modifier = Modifier.fillMaxWidth()) {
                    val (focusSys, focusDia, focusPulse, focusComment) = remember { FocusRequester.createRefs() }
                    Row {
                        TextField(
                            value = sys,
                            colors = TextFieldDefaults.textFieldColors(textColor = MaterialTheme.colors.primary, backgroundColor = androidx.compose.ui.graphics.Color.Transparent),
                            onValueChange = { it ->
                                sys = it.filter  { it.isDigit() }
                                if (sys.isNotEmpty()) {
                                    // Jump if starts with 1 and is 3 digits or starts with !1 and is 2 digits
                                    val crit1 = sys.length == 3
                                    val crit2 = (sys[0] != '1' && sys.length == 2)
                                    if (crit1 || crit2) focusDia.requestFocus()
                                }
                            },
                            singleLine = true,
                            keyboardOptions = KeyboardOptions(
                                keyboardType = KeyboardType.Number
                            ),
                            textStyle = TextStyle(),
                            label = {
                                Text(
                                    text = stringResource(id = R.string.systolic) + "*",
                                    maxLines = 1,
                                    overflow = TextOverflow.Ellipsis
                                )
                            },
                            placeholder = { Text(text = stringResource(id = R.string.bpEntryHint) ) },
                            modifier = Modifier
                                .width(fieldWidth.dp)
                                .focusRequester(focusSys)
                                .padding(end = 10.dp),
                            keyboardActions = KeyboardActions(onDone = { addItem() })
                        )
                        // Dia
                        TextField(
                            value = dia,
                            colors = TextFieldDefaults.textFieldColors(textColor = MaterialTheme.colors.primary, backgroundColor = androidx.compose.ui.graphics.Color.Transparent),
                            onValueChange = {
                                dia = it.filter  { it.isDigit() }
                                if (dia.isNotEmpty()) {
                                    // Jump if starts with 1 and is 3 digits or starts with !1 and is 2 digits
                                    val crit1 = dia.length == 3
                                    val crit2 = (dia[0] != '1' && dia.length == 2)
                                    if (crit1 || crit2) focusPulse.requestFocus()
                                }
                            },
                            singleLine = true,
                            keyboardOptions = KeyboardOptions(
                                keyboardType = KeyboardType.Number
                            ),
                            textStyle = TextStyle(),
                            label = {
                                Text(
                                    text = stringResource(id = R.string.diastolic) + "*",
                                    maxLines = 1,
                                    overflow = TextOverflow.Ellipsis
                                )
                            },
                            placeholder = { Text(text = stringResource(id = R.string.bpEntryHint)) },
                            modifier = Modifier
                                .width(fieldWidth.dp)
                                .focusRequester(focusDia)
                                .padding(end = 10.dp),
                            keyboardActions = KeyboardActions(onDone = { addItem() })
                        )
                        // Pulse
                        if (logHeartRhythm)
                            TextField(
                                trailingIcon = {
                                    Icon(
                                        painter = painterResource(R.drawable.ic_baseline_warning_24),
                                        contentDescription = "Heart Rhythm State",
                                        modifier = Modifier
                                            .clickable {
                                                setEmoji()
                                                triggerRefresh = true
                                            }
                                            .alpha(heartRhythmIconAlpha))
                                },
                                value = pulse,
                                colors = TextFieldDefaults.textFieldColors(textColor = MaterialTheme.colors.primary, backgroundColor = androidx.compose.ui.graphics.Color.Transparent),
                                onValueChange = { pulse = it.filter { it.isDigit() } },
                                singleLine = true,
                                keyboardOptions = KeyboardOptions(
                                    keyboardType = KeyboardType.Number
                                ),
                                textStyle = TextStyle(),
                                label = {
                                    Text(
                                        text = stringResource(id = R.string.pulse),
                                        maxLines = 1,
                                        overflow = TextOverflow.Ellipsis
                                    )
                                },
                                placeholder = { Text(text = stringResource(id = R.string.bpEntryHint)) },
                                modifier = Modifier
                                    .width(130.dp)
                                    .focusRequester(focusPulse)
                                    .padding(end = 10.dp),
                                keyboardActions = KeyboardActions(onDone = { addItem() }))
                        else
                            TextField(
                                value = pulse,
                                colors = TextFieldDefaults.textFieldColors(textColor = MaterialTheme.colors.primary, backgroundColor = androidx.compose.ui.graphics.Color.Transparent),
                                onValueChange = { pulse = it.filter { it.isDigit() } },
                                singleLine = true,
                                keyboardOptions = KeyboardOptions(
                                    keyboardType = KeyboardType.Number
                                ),
                                textStyle = TextStyle(),
                                label = {
                                    Text(
                                        text = stringResource(id = R.string.pulse),
                                        maxLines = 1,
                                        overflow = TextOverflow.Ellipsis
                                    )
                                },
                                placeholder = { Text(text = stringResource(id = R.string.bpEntryHint)) },
                                modifier = Modifier
                                    .width(pulseFieldWidth.dp)
                                    .focusRequester(focusPulse)
                                    .padding(end = 10.dp),
                                keyboardActions = KeyboardActions(onDone = { addItem() }))

                        IconButton(onClick = { commentVisible = !commentVisible
                            triggerRefresh = true },
                            modifier = Modifier
                                .width(commentButtonWidth.dp)
                                .padding(top = (2 * cellPadding).dp)) {
                            Icon(painter = painterResource(R.drawable.ic_outline_comment_24),"Comment", tint = MaterialTheme.colors.primary) }
                    } // Row

                    if (triggerRefresh) {
                        this@BloodPressureFragment.ShowContent()
                        triggerRefresh = false
                    }

                    if (commentVisible) {
                        // Comment
                        TextField(
                            value = comment,
                            colors = TextFieldDefaults.textFieldColors(textColor = MaterialTheme.colors.primary, backgroundColor = androidx.compose.ui.graphics.Color.Transparent),
                            onValueChange = { comment = it },
                            keyboardOptions = KeyboardOptions(
                                keyboardType = KeyboardType.Text,
                                capitalization = KeyboardCapitalization.Sentences
                            ),
                            maxLines = 2,
                            textStyle = TextStyle(),
                            modifier = Modifier
                                .fillMaxWidth()
                                .focusRequester(focusComment),
                            label = { Text(text = stringResource(id = R.string.comment)) },
                            keyboardActions = KeyboardActions(onDone = { addItem() })
                        )
//                            focusComment.requestFocus()
                        binding.btAdd.visibility = View.VISIBLE // Need an add button because Enter is used for new line
                    }
                    else binding.btAdd.visibility = View.GONE // Hide Add Button

                    if (sys.isNotEmpty()) sysValue = try { sys.toInt().toString() } catch (e: Exception) { "" }
                    if (dia.isNotEmpty()) diaValue = try { dia.toInt().toString() } catch (e: Exception) { "" }
                    if (pulse.isNotEmpty()) pulseValue = try { pulse.toInt().toString() } catch (e: Exception) { "" }
                    if (heartRhythm.isNotEmpty()) heartRhythmValue = try { heartRhythm.toInt().toString() } catch (e: Exception) { "" }
                    commentValue = comment
                }
                topPadding = 80 + if (commentVisible) 65 else 0
            }
            LazyColumn(
                state = listState,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.padding(top = topPadding.dp)
            ) {
                items(listItems) { item ->
                    TabRowDefaults.Divider(color = MaterialTheme.colors.secondary, thickness = 1.dp) // Lines starting from the top
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(IntrinsicSize.Min)
                            .width(IntrinsicSize.Min)
                            .clickable { selection = item },
                            verticalAlignment = Alignment.CenterVertically
                    )
                    {
                        ShowRow(item)
                    }
                    if (selection != null) {
                        ItemClicked(selection!!._id)
                        selection = null
                    }
                }
            }
        }
    }

    private fun setEmoji() {
        setHeartRhythm = !setHeartRhythm
        heartRhythmIconAlpha = if (setHeartRhythm) 1F
        else 0.5F
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        editActivityClass  = BloodPressureEditActivity::class.java
        infoActivityClass  = BloodPressureInfoActivity::class.java
        chartActivityClass = BloodPressureChartActivity::class.java

        viewModel =
            ViewModelProvider(requireActivity())[BloodPressureViewModel::class.java]  // This should return the MainActivity ViewModel
        configureButtons(binding.btAdd,binding.btInfo, binding.btChart)

        bpHelper = BloodPressureHelper(requireContext())

        logHeartRhythm = preferences.getBoolean(SettingsActivity.KEY_PREF_LOG_HEART_RHYTHM, getString(R.string.LOG_HEARTRHYTHM_DEFAULT).toBoolean())
        itemUnit = " " + preferences.getString(SettingsActivity.KEY_PREF_BLOODPRESSURE_UNIT, getString(R.string.BLOODPRESSURE_UNIT_DEFAULT))
        highlightValues = preferences.getBoolean(SettingsActivity.KEY_PREF_BLOODPRESSURE_HIGHLIGHT_VALUES, getString(R.string.BLOODPRESSURE_HIGHLIGHT_VALUES_DEFAULT).toBoolean())

        // Preset values in case something was stored in viewModel during onDestroy
        commentValue = viewModel.comment
        sysValue = viewModel.value1
        diaValue = viewModel.value2
        pulseValue = viewModel.value3
        heartRhythmValue = viewModel.value4

        binding.itemList.setContent {ShowContent() }
    }

    // Save values to survive orientation change
    override fun onDestroy() {
        viewModel.value1 = sysValue
        viewModel.value2 = diaValue
        viewModel.value3 = pulseValue
        viewModel.value4 = heartRhythmValue
        viewModel.comment = commentValue
        super.onDestroy()
    }
}