package com.zell_mbc.medilog.bloodpressure

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.BLOODPRESSURE
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.databinding.BloodpressureInfoformBinding
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import java.util.Calendar.*

class BloodPressureInfoFragment : Fragment() {
    private var _binding: BloodpressureInfoformBinding? = null
    private val binding get() = _binding!!
    private lateinit var userOutputService : UserOutputService

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = BloodpressureInfoformBinding.inflate(inflater, container, false)

        return binding.root
    }

    private fun initializeService(view: View) {
        userOutputService = UserOutputServiceImpl(requireContext(),view)
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private lateinit var viewModel: BloodPressureViewModel  //by viewModels() //factoryProducer = { SavedStateViewModelFactory(this, ) })

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initializeService(view)

        viewModel = ViewModelProvider(requireActivity())[BloodPressureViewModel::class.java]
        viewModel.init(BLOODPRESSURE)

        val item = viewModel.getLast(false)
        if (item == null) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.noDataToShow))
            return
        }

        binding.tvBloodPressureHeader.text = "" //BloodPressure Header"         // Round to 2 digits

        val dayInMilliseconds: Long = 1000 * 60 * 60 * 24
        var totalDays = 0L
        val lastDay = item.timestamp
        val tmpItem = viewModel.getFirst(false)
        if (tmpItem != null) {
            val firstDay = tmpItem.timestamp
            val timeFrame = lastDay - firstDay
            totalDays = timeFrame / dayInMilliseconds + 1 // From ms to days
        }

        val annualDays = 365
        val monthDays = 30

        binding.tvTotalDays.text = totalDays.toString()
        binding.tvAnnualTimeframe.text = annualDays.toString()
        binding.tvOneMonth.text = monthDays.toString()

        val totalMeasurements = viewModel.getSize(false)
        binding.tvTotalMeasurements.text = totalMeasurements.toString()

        val now = getInstance().time

        val aYearAgo = now.time - 31557600000
        var sql = "SELECT count(*) FROM data WHERE timestamp >= $aYearAgo and type=$BLOODPRESSURE"
        val annualMeasurements = viewModel.getInt(sql)
        binding.tvAnnualMeasurements.text = annualMeasurements.toString()

        val aMonthAgo = now.time - 2629800000
        sql = "SELECT count(*) FROM data WHERE timestamp >= $aMonthAgo and type=$BLOODPRESSURE"
        val monthlyMeasurements = viewModel.getInt(sql)
        binding.tvMonthlyMeasurements.text = monthlyMeasurements.toString()

        // **** Systolic values
        // Total
        sql = "SELECT MIN(CAST(value1 as int)) FROM data WHERE type=$BLOODPRESSURE"
        var min = viewModel.getInt(sql)
        sql = "SELECT MAX(CAST(value1 as int)) FROM data WHERE type=$BLOODPRESSURE"
        var max = viewModel.getInt(sql)
        sql = "SELECT AVG(CAST(value1 as int)) FROM data WHERE type=$BLOODPRESSURE"
        var avg = viewModel.getInt(sql)

        binding.tvTotalSysMin.text = min.toString()
        binding.tvTotalSysMax.text = max.toString()
        binding.tvTotalSysAvg.text = avg.toString()

        // Annual
        sql = "SELECT MIN(CAST(value1 as int)) FROM data WHERE timestamp >= $aYearAgo and type=$BLOODPRESSURE"
        min = viewModel.getInt(sql)
        sql = "SELECT MAX(CAST(value1 as int)) FROM data WHERE timestamp >= $aYearAgo and type=$BLOODPRESSURE"
        max = viewModel.getInt(sql)
        sql = "SELECT AVG(CAST(value1 as int)) FROM data WHERE timestamp >= $aYearAgo and type=$BLOODPRESSURE"
        avg = viewModel.getInt(sql)

        binding.tvAnnualSysMin.text = min.toString()
        binding.tvAnnualSysMax.text = max.toString()
        binding.tvAnnualSysAvg.text = avg.toString()

        // Monthly
        sql = "SELECT MIN(CAST(value1 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=$BLOODPRESSURE"
        min = viewModel.getInt(sql)
        sql = "SELECT MAX(CAST(value1 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=$BLOODPRESSURE"
        max = viewModel.getInt(sql)
        sql = "SELECT AVG(CAST(value1 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=$BLOODPRESSURE"
        avg = viewModel.getInt(sql)

        binding.tvMonthSysMin.text = min.toString()
        binding.tvMonthSysMax.text = max.toString()
        binding.tvMonthSysAvg.text = avg.toString()

        // **** Diastolic values
        // Total
        sql = "SELECT MIN(CAST(value2 as int)) FROM data WHERE type=$BLOODPRESSURE"
        min = viewModel.getInt(sql)
        sql = "SELECT MAX(CAST(value2 as int)) FROM data WHERE type=$BLOODPRESSURE"
        max = viewModel.getInt(sql)
        sql = "SELECT AVG(CAST(value2 as int)) FROM data WHERE type=$BLOODPRESSURE"
        avg = viewModel.getInt(sql)

        binding.tvTotalDiaMin.text = min.toString()
        binding.tvTotalDiaMax.text = max.toString()
        binding.tvTotalDiaAvg.text = avg.toString()

        // Annual
        sql = "SELECT MIN(CAST(value2 as int)) FROM data WHERE timestamp >= $aYearAgo and type=$BLOODPRESSURE"
        min = viewModel.getInt(sql)
        sql = "SELECT MAX(CAST(value2 as int)) FROM data WHERE timestamp >= $aYearAgo and type=$BLOODPRESSURE"
        max = viewModel.getInt(sql)
        sql = "SELECT AVG(CAST(value2 as int)) FROM data WHERE timestamp >= $aYearAgo and type=$BLOODPRESSURE"
        avg = viewModel.getInt(sql)

        binding.tvAnnualDiaMin.text = min.toString()
        binding.tvAnnualDiaMax.text = max.toString()
        binding.tvAnnualDiaAvg.text = avg.toString()

        // Monthly
        sql = "SELECT MIN(CAST(value2 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=$BLOODPRESSURE"
        min = viewModel.getInt(sql)
        sql = "SELECT MAX(CAST(value2 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=$BLOODPRESSURE"
        max = viewModel.getInt(sql)
        sql = "SELECT AVG(CAST(value2 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=$BLOODPRESSURE"
        avg = viewModel.getInt(sql)

        binding.tvMonthDiaMin.text = min.toString()
        binding.tvMonthDiaMax.text = max.toString()
        binding.tvMonthDiaAvg.text = avg.toString()

        // **** Pulse values
        // Total
        sql = "SELECT MIN(CAST(value3 as int)) FROM data WHERE type=$BLOODPRESSURE"
        min = viewModel.getInt(sql)
        sql = "SELECT MAX(CAST(value3 as int)) FROM data WHERE type=$BLOODPRESSURE"
        max = viewModel.getInt(sql)
        sql = "SELECT AVG(CAST(value3 as int)) FROM data WHERE type=$BLOODPRESSURE"
        avg = viewModel.getInt(sql)

        binding.tvTotalPulseMin.text = min.toString()
        binding.tvTotalPulseMax.text = max.toString()
        binding.tvTotalPulseAvg.text = avg.toString()

        // Annual
        sql = "SELECT MIN(CAST(value3 as int)) FROM data WHERE timestamp >= $aYearAgo and type=$BLOODPRESSURE"
        min = viewModel.getInt(sql)
        sql = "SELECT MAX(CAST(value3 as int)) FROM data WHERE timestamp >= $aYearAgo and type=$BLOODPRESSURE"
        max = viewModel.getInt(sql)
        sql = "SELECT AVG(CAST(value3 as int)) FROM data WHERE timestamp >= $aYearAgo and type=$BLOODPRESSURE"
        avg = viewModel.getInt(sql)

        binding.tvAnnualPulseMin.text = min.toString()
        binding.tvAnnualPulseMax.text = max.toString()
        binding.tvAnnualPulseAvg.text = avg.toString()

        // Monthly
        sql = "SELECT MIN(CAST(value3 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=$BLOODPRESSURE"
        min = viewModel.getInt(sql)
        sql = "SELECT MAX(CAST(value3 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=$BLOODPRESSURE"
        max = viewModel.getInt(sql)
        sql = "SELECT AVG(CAST(value3 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=$BLOODPRESSURE"
        avg = viewModel.getInt(sql)

        binding.tvMonthPulseMin.text = min.toString()
        binding.tvMonthPulseMax.text = max.toString()
        binding.tvMonthPulseAvg.text = avg.toString()
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
        viewModel.deleteTmpItem() // Deletes all items with comment == tmpComment
    }
}
