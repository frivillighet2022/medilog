package com.zell_mbc.medilog.bloodpressure

import android.app.Application
import android.graphics.Color
import android.graphics.Paint
import android.graphics.pdf.PdfDocument
import android.widget.Toast
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.graphics.drawable.toBitmap
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.ViewModel
import com.zell_mbc.medilog.settings.SettingsActivity
import java.util.*

class BloodPressureViewModel(application: Application): ViewModel(application) {
    override var tabIcon  = R.drawable.ic_bloodpressure
    override val filterStartPref = "BLOODPRESSUREFILTERSTART"
    override val filterEndPref = "BLOODPRESSUREFILTEREND"
    override val filterModePref =  "BLOODPRESSURE_FILTER_MODE"
    override val rollingFilterValuePref = "BLOODPRESSURE_ROLLING_FILTER_VALUE"
    override val rollingFilterTimeframePref = "BLOODPRESSURE_ROLLING_FILTER_TIMEFRAME"

    override var itemName = app.getString(R.string.bloodPressure)

    val logHeartRhythm = preferences.getBoolean(SettingsActivity.KEY_PREF_LOG_HEART_RHYTHM, true)
    override var landscape = preferences.getBoolean(SettingsActivity.KEY_PREF_BLOODPRESSURE_LANDSCAPE, app.getString(R.string.BLOODPRESSURE_LANDSCAPE_DEFAULT).toBoolean())
    override var pageSize = preferences.getString(SettingsActivity.KEY_PREF_BLOODPRESSURE_PAPER_SIZE, app.getString(R.string.BLOODPRESSURE_PAPER_SIZE_DEFAULT))
    override var showAllTabs = false // Only used for Diary tab

    private val warningSign = app.getString(R.string.warningSign)

    private var section1 = 0f
    private var section2 = 0f
    private var section3 = 0f
    private var warningSignWidth = 0f
    private var timeTab = 0f
    private var sysTab = 0f
    private var diaTab = 0f
    private var pulseTab = 0f

    private val MORNING = 0
    private val AFTERNOON = 1
    private val EVENING = 2

    // Report timeframes
    // Morning, Afternoon, Evening
    // t0 - t1, t1 - t2,   t2 - t0
    private var t0 = 0
    private var t1 = 12
    private var t2 = 18

    override fun createPdfDocument(filtered: Boolean): PdfDocument? {
        if (getSize(filtered) == 0) {
            userOutputService.showAndHideMessageForLong( app.getString(R.string.bloodPressure) + ": " + app.getString(R.string.noDataToExport))
            return null
        }

        super.createPdfDocument(filtered)
        commentTab = 60f

        val bpHelper = BloodPressureHelper(app)

        var currentDay = ""
        var lastPeriod = -1
        var commentPrinted = false

        if (logHeartRhythm) {  // Need more space if warningSign is shown
            section2 += warningSignWidth
            section3 += (warningSignWidth * 2)
            commentTab += (warningSignWidth * 3)
            timeTab += warningSignWidth
            sysTab += warningSignWidth
            diaTab += warningSignWidth
            pulseTab += warningSignWidth
        } // Need more space if warningSign is shown

        setTimezones()
        setColumns(pdfPaintHighlight)
        drawHeader(pdfPaint, pdfPaintHighlight)
        var f = pdfDataTop + pdfLineSpacing

        for (item in pdfItems) {

            val dayPeriod = dayPeriod(item.timestamp)
            var activeSection = section1
            if (dayPeriod == AFTERNOON) activeSection = section2
            if (dayPeriod == EVENING) {
                // Special case where an early morning measurement should be treated like the previous day
                val cal = Calendar.getInstance()
                cal.timeInMillis = item.timestamp
                if (t0 > 0 && cal[Calendar.HOUR_OF_DAY] < t0) {
                    cal.add(Calendar.DAY_OF_YEAR, -1)
                    item.timestamp = cal.timeInMillis
                }
                activeSection = section3
            }

            // New line if a different day or (a second measurement in the same period) or (if a comment has been printed that day and a second one comes round)
            if ((currentDay != toStringDate(item.timestamp)) or
                (lastPeriod == dayPeriod) or
                (commentPrinted and !item.comment.isNullOrEmpty())) {
                f += pdfLineSpacing
                currentDay = toStringDate(item.timestamp)
            }
            lastPeriod = dayPeriod
            commentPrinted = !item.comment.isNullOrEmpty()

            // Start new page
            if (f > pdfDataBottom) {
                document.finishPage(page)
                createPage(document)
                drawHeader(pdfPaint, pdfPaintHighlight)
                f = pdfDataTop + (pdfLineSpacing * 2)
            }

            // Date
            canvas.drawText(toStringDate(item.timestamp), pdfLeftBorder, f, pdfPaint)

            // HeartRhythm
            if (logHeartRhythm) {
                pdfPaint.color = if (item.value4 == "1") Color.BLACK else Color.WHITE
                canvas.drawText(warningSign, activeSection, f, pdfPaint)
                pdfPaint.color = Color.BLACK
            }

            // Time
            canvas.drawText(toStringTime(item.timestamp), activeSection + timeTab, f, pdfPaint)

            if (highlightValues) {
                when(bpHelper.sysGrade(item.value1)) {
                    bpHelper.hyperGrade3,
                    bpHelper.hyperGrade2 -> {
                        pdfPaint.isFakeBoldText = true
                        pdfPaint.isUnderlineText = true
                    }
                    bpHelper.hyperGrade1 -> pdfPaint.isFakeBoldText = true
                    bpHelper.hyperGrade4 -> pdfPaint.isFakeBoldText = true
                    else                 -> pdfPaint.isFakeBoldText = false
                }
                canvas.drawText(item.value1, activeSection + sysTab, f, pdfPaint)
                pdfPaint.isFakeBoldText = false
                pdfPaint.isUnderlineText = false

                when(bpHelper.diaGrade(item.value2)) {
                    bpHelper.hyperGrade3,
                    bpHelper.hyperGrade2 -> {
                        pdfPaint.isFakeBoldText = true
                        pdfPaint.isUnderlineText = true
                    }
                    bpHelper.hyperGrade1 -> pdfPaint.isFakeBoldText = true
                    bpHelper.hyperGrade4 -> pdfPaint.isFakeBoldText = true
                    else                 -> pdfPaint.isFakeBoldText = false
                }
                canvas.drawText(item.value2, activeSection + diaTab, f, pdfPaint)
                pdfPaint.isFakeBoldText = false
                pdfPaint.isUnderlineText = false
            }
            else {  // Don't highlight values
                canvas.drawText(item.value1, activeSection + sysTab, f, pdfPaint)
                canvas.drawText(item.value2, activeSection + diaTab, f, pdfPaint)
            }
            canvas.drawText(item.value3, activeSection+ pulseTab, f, pdfPaint)
            canvas.drawText(item.comment, commentTab, f, pdfPaint)
        }
        // finish the page
        document.finishPage(page)

        // Add statistics page
        createPage(document)
        drawStatsPage(pdfPaint, pdfPaintHighlight)
        document.finishPage(page)

        return document
    }

    override fun setColumns(pdfPaintHighlight: Paint) {
        val sysDiaWidth = pdfPaintHighlight.measureText(app.getString(R.string.sysShort))

        if (logHeartRhythm) {
            warningSignWidth = pdfPaintHighlight.measureText(app.getString(R.string.warningSign))
            timeTab = warningSignWidth + space
        }
        else timeTab = 0f

        sysTab = timeTab + timeWidth + space
        diaTab = sysTab + sysDiaWidth + space
        pulseTab = diaTab + sysDiaWidth + space

        val subHeaderWidth = pulseTab + pdfPaintHighlight.measureText(app.getString(R.string.pulse))

        section1 = pdfLeftBorder + dateWidth + padding
        section2 = section1 + subHeaderWidth + padding
        section3 = section2 + subHeaderWidth + padding
        commentTab = section3 + subHeaderWidth + padding
    }

    override fun drawHeader(pdfPaint: Paint, pdfPaintHighlight: Paint) {
        drawHeader(pdfPaint) // VieModel function doesn't need to highlight

        // Data section
        canvas.drawText(app.getString(R.string.date), pdfLeftBorder, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.morning), section1, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.afternoon), section2, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.evening), section3, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.comment), commentTab, pdfDataTop, pdfPaintHighlight)

        val f = (pdfDataTop + pdfLineSpacing)
//        pdfPaint.textSkewX = -0.25f

        if (logHeartRhythm) canvas.drawText(app.getString(R.string.warningSign), section1, f, pdfPaint)
        canvas.drawText(app.getString(R.string.time), section1 + timeTab, f, pdfPaint)
        canvas.drawText(app.getString(R.string.sysShort), section1 + sysTab, f, pdfPaint)
        canvas.drawText(app.getString(R.string.diaShort), section1 + diaTab, f, pdfPaint)
        canvas.drawText(app.getString(R.string.pulse), section1 + pulseTab, f, pdfPaint)

        if (logHeartRhythm) canvas.drawText(app.getString(R.string.warningSign), section2, f, pdfPaint)
        canvas.drawText(app.getString(R.string.time), section2 + timeTab, f, pdfPaint)
        canvas.drawText(app.getString(R.string.sysShort), section2 + sysTab, f, pdfPaint)
        canvas.drawText(app.getString(R.string.diaShort), section2 + diaTab, f, pdfPaint)
        canvas.drawText(app.getString(R.string.pulse), section2 + pulseTab, f, pdfPaint)

        if (logHeartRhythm) canvas.drawText(app.getString(R.string.warningSign), section3, f, pdfPaint)
        canvas.drawText(app.getString(R.string.time), section3 + timeTab, f, pdfPaint)
        canvas.drawText(app.getString(R.string.sysShort), section3 + sysTab, f, pdfPaint)
        canvas.drawText(app.getString(R.string.diaShort), section3 + diaTab, f, pdfPaint)
        canvas.drawText(app.getString(R.string.pulse), section3 + pulseTab, f, pdfPaint)

        // ------------
        val space = (5).toFloat()
        pdfPaint.color = Color.DKGRAY
        canvas.drawLine(section1 - space, pdfHeaderBottom, section1 - space, pdfDataBottom, pdfPaint)
        canvas.drawLine(section2 - space, pdfHeaderBottom, section2 - space, pdfDataBottom, pdfPaint)
        canvas.drawLine(section3 - space, pdfHeaderBottom, section3 - space, pdfDataBottom, pdfPaint)
        pdfPaint.color = Color.BLACK
        canvas.drawLine(commentTab - space, pdfHeaderBottom, commentTab - space, pdfDataBottom, pdfPaint)
    }

    private fun drawStatsPage(pdfPaint: Paint, pdfPaintHighlight: Paint) {
        // Header
        val pdfHeaderDateColumn = pdfRightBorder - 150
        val logoTab = 30
        val bitmap = AppCompatResources.getDrawable(app, R.mipmap.ic_launcher)?.toBitmap(logoTab,30)
        if (bitmap != null) canvas.drawBitmap(bitmap, 5f, 5f, pdfPaint)

        // Draw header
        canvas.drawText(headerText, pdfLeftBorder + logoTab, pdfHeaderTop, pdfPaint)
        canvas.drawText(app.getString(R.string.date) + ": " + formatedDate, pdfHeaderDateColumn, pdfHeaderTop, pdfPaint)
        canvas.drawLine(pdfLeftBorder, pdfHeaderBottom, pdfRightBorder, pdfHeaderBottom, pdfPaint)
        canvas.drawLine(pdfLeftBorder, pdfDataBottom, pdfRightBorder, pdfDataBottom, pdfPaint)

        val timeframeLabel = app.getString(R.string.timeframeLabel)
        val totalLabel = app.getString(R.string.totalLabel)
        val annualLabel = app.getString(R.string.annualLabel)
        val monthLabel = app.getString(R.string.monthLabel)

        val space = 25
        val totalColumn =  pdfPaintHighlight.measureText(timeframeLabel) + space
        val annualColumn = totalColumn + pdfPaintHighlight.measureText(totalLabel) + space
        val monthColumn =  annualColumn + pdfPaintHighlight.measureText(annualLabel) + space

        var f = pdfDataTop + pdfLineSpacing
        canvas.drawText(app.getString(R.string.statistics) + ":", pdfLeftBorder, f, pdfPaintHighlight)

        f += pdfLineSpacing
        f += pdfLineSpacing
        canvas.drawText(totalLabel, totalColumn, f, pdfPaint)
        canvas.drawText(annualLabel, annualColumn, f, pdfPaint)
        canvas.drawText(monthLabel, monthColumn, f, pdfPaint)

        // Data section
        val msToDay: Long = 1000 * 60 * 60 * 24
        var totalDays = 0L
        val item = getLast(true)
        if (item != null) {
            val lastDay = item.timestamp
            val tmpItem = getFirst(true)
            if (tmpItem != null) {
                val firstDay = tmpItem.timestamp
                val timeFrame = lastDay - firstDay
                totalDays = timeFrame / msToDay // From ms to days
            }
        }

        val totalMeasurements = getSize(false)

        val now = Calendar.getInstance().time
        val aYearAgo = now.time - 31557600000
        var sql = "SELECT count(*) FROM data WHERE timestamp >= $aYearAgo and type=${dataType}"
        val annualMeasurements = getInt(sql)

        val aMonthAgo = now.time - 2629800000
        sql = "SELECT count(*) FROM data WHERE timestamp >= $aMonthAgo and type=${dataType}"
        val monthlyMeasurements = getInt(sql)

        f += pdfLineSpacing
        canvas.drawText(timeframeLabel, pdfLeftBorder, f, pdfPaint)
        canvas.drawText(totalDays.toString(), totalColumn, f, pdfPaint)
        canvas.drawText("365", annualColumn, f, pdfPaint)
        canvas.drawText("30", monthColumn, f, pdfPaint)

        f += pdfLineSpacing
        canvas.drawText(app.getString(R.string.measurementLabel), pdfLeftBorder, f, pdfPaint)
        canvas.drawText(totalMeasurements.toString(), totalColumn, f, pdfPaint)
        canvas.drawText(annualMeasurements.toString(), annualColumn, f, pdfPaint)
        canvas.drawText(monthlyMeasurements.toString(), monthColumn, f, pdfPaint)

        // **** Systolic values
        // Total
        sql = "SELECT MIN(CAST(value1 as int)) FROM data WHERE type=${dataType}"
        var minT = getInt(sql)
        sql = "SELECT MAX(CAST(value1 as int)) FROM data WHERE type=${dataType}"
        var maxT = getInt(sql)
        sql = "SELECT AVG(CAST(value1 as int)) FROM data WHERE type=${dataType}"
        var avgT = getInt(sql)

        // Annual
        sql = "SELECT MIN(CAST(value1 as int)) FROM data WHERE timestamp >= $aYearAgo and type=${dataType}"
        var minA = getInt(sql)
        sql = "SELECT MAX(CAST(value1 as int)) FROM data WHERE timestamp >= $aYearAgo and type=${dataType}"
        var maxA = getInt(sql)
        sql = "SELECT AVG(CAST(value1 as int)) FROM data WHERE timestamp >= $aYearAgo and type=${dataType}"
        var avgA = getInt(sql)

        // Monthly
        sql = "SELECT MIN(CAST(value1 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=${dataType}"
        var minM = getInt(sql)
        sql = "SELECT MAX(CAST(value1 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=${dataType}"
        var maxM = getInt(sql)
        sql = "SELECT AVG(CAST(value1 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=${dataType}"
        var avgM = getInt(sql)

        f += pdfLineSpacing
        f += pdfLineSpacing
        canvas.drawText(app.getString(R.string.systolic) + " " + app.getString(R.string.min), pdfLeftBorder, f, pdfPaint)
        canvas.drawText(minT.toString(), totalColumn, f, pdfPaint)
        canvas.drawText(minA.toString(), annualColumn, f, pdfPaint)
        canvas.drawText(minM.toString(), monthColumn, f, pdfPaint)
        f += pdfLineSpacing
        canvas.drawText(app.getString(R.string.max), pdfLeftBorder, f, pdfPaint)
        canvas.drawText(maxT.toString(), totalColumn, f, pdfPaint)
        canvas.drawText(maxA.toString(), annualColumn, f, pdfPaint)
        canvas.drawText(maxM.toString(), monthColumn, f, pdfPaint)
        f += pdfLineSpacing
        canvas.drawText(app.getString(R.string.avg), pdfLeftBorder, f, pdfPaint)
        canvas.drawText(avgT.toString(), totalColumn, f, pdfPaint)
        canvas.drawText(avgA.toString(), annualColumn, f, pdfPaint)
        canvas.drawText(avgM.toString(), monthColumn, f, pdfPaint)

        // **** Diastolic values
        // Total
        sql = "SELECT MIN(CAST(value2 as int)) FROM data WHERE type=${dataType}"
        minT = getInt(sql)
        sql = "SELECT MAX(CAST(value2 as int)) FROM data WHERE type=${dataType}"
        maxT = getInt(sql)
        sql = "SELECT AVG(CAST(value2 as int)) FROM data WHERE type=${dataType}"
        avgT = getInt(sql)

        // Annual
        sql = "SELECT MIN(CAST(value2 as int)) FROM data WHERE timestamp >= $aYearAgo and type=${dataType}"
        minA = getInt(sql)
        sql = "SELECT MAX(CAST(value2 as int)) FROM data WHERE timestamp >= $aYearAgo and type=${dataType}"
        maxA = getInt(sql)
        sql = "SELECT AVG(CAST(value2 as int)) FROM data WHERE timestamp >= $aYearAgo and type=${dataType}"
        avgA = getInt(sql)

        // Monthly
        sql = "SELECT MIN(CAST(value2 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=${MainActivity.BLOODPRESSURE}"
        minM = getInt(sql)
        sql = "SELECT MAX(CAST(value2 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=${MainActivity.BLOODPRESSURE}"
        maxM = getInt(sql)
        sql = "SELECT AVG(CAST(value2 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=${MainActivity.BLOODPRESSURE}"
        avgM = getInt(sql)

        f += pdfLineSpacing
        f += pdfLineSpacing
        canvas.drawText(app.getString(R.string.diastolic) + " " + app.getString(R.string.min), pdfLeftBorder, f, pdfPaint)
        canvas.drawText(minT.toString(), totalColumn, f, pdfPaint)
        canvas.drawText(minA.toString(), annualColumn, f, pdfPaint)
        canvas.drawText(minM.toString(), monthColumn, f, pdfPaint)
        f += pdfLineSpacing
        canvas.drawText(app.getString(R.string.max), pdfLeftBorder, f, pdfPaint)
        canvas.drawText(maxT.toString(), totalColumn, f, pdfPaint)
        canvas.drawText(maxA.toString(), annualColumn, f, pdfPaint)
        canvas.drawText(maxM.toString(), monthColumn, f, pdfPaint)
        f += pdfLineSpacing
        canvas.drawText(app.getString(R.string.avg), pdfLeftBorder, f, pdfPaint)
        canvas.drawText(avgT.toString(), totalColumn, f, pdfPaint)
        canvas.drawText(avgA.toString(), annualColumn, f, pdfPaint)
        canvas.drawText(avgM.toString(), monthColumn, f, pdfPaint)

        // **** Pulse values
        // Total
        val x = dataType
        sql = "SELECT MIN(CAST(value3 as int)) FROM data WHERE type=${dataType}"
        minT = getInt(sql)
        sql = "SELECT MAX(CAST(value3 as int)) FROM data WHERE type=${dataType}"
        maxT = getInt(sql)
        sql = "SELECT AVG(CAST(value3 as int)) FROM data WHERE type=${dataType}"
        avgT = getInt(sql)

        // Annual
        sql = "SELECT MIN(CAST(value3 as int)) FROM data WHERE timestamp >= $aYearAgo and type=${dataType}"
        minA = getInt(sql)
        sql = "SELECT MAX(CAST(value3 as int)) FROM data WHERE timestamp >= $aYearAgo and type=${dataType}"
        maxA = getInt(sql)
        sql = "SELECT AVG(CAST(value3 as int)) FROM data WHERE timestamp >= $aYearAgo and type=${dataType}"
        avgA = getInt(sql)

        // Monthly
        sql = "SELECT MIN(CAST(value3 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=${dataType}"
        minM = getInt(sql)
        sql = "SELECT MAX(CAST(value3 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=${dataType}"
        maxM = getInt(sql)
        sql = "SELECT AVG(CAST(value3 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=${dataType}"
        avgM = getInt(sql)

        f += pdfLineSpacing
        f += pdfLineSpacing
        canvas.drawText(app.getString(R.string.pulse) + " " + app.getString(R.string.min), pdfLeftBorder, f, pdfPaint)
        canvas.drawText(minT.toString(), totalColumn, f, pdfPaint)
        canvas.drawText(minA.toString(), annualColumn, f, pdfPaint)
        canvas.drawText(minM.toString(), monthColumn, f, pdfPaint)
        f += pdfLineSpacing
        canvas.drawText(app.getString(R.string.max), pdfLeftBorder, f, pdfPaint)
        canvas.drawText(maxT.toString(), totalColumn, f, pdfPaint)
        canvas.drawText(maxA.toString(), annualColumn, f, pdfPaint)
        canvas.drawText(maxM.toString(), monthColumn, f, pdfPaint)
        f += pdfLineSpacing
        canvas.drawText(app.getString(R.string.avg), pdfLeftBorder, f, pdfPaint)
        canvas.drawText(avgT.toString(), totalColumn, f, pdfPaint)
        canvas.drawText(avgA.toString(), annualColumn, f, pdfPaint)
        canvas.drawText(avgM.toString(), monthColumn, f, pdfPaint)
    }

    private fun dayPeriod(timestamp: Long): Int {
        val cal = Calendar.getInstance()
        cal.timeInMillis = timestamp
        val hour = cal[Calendar.HOUR_OF_DAY]

        val p = if (hour in t0 until t1) MORNING
        else  if (hour in t1 until t2) AFTERNOON
        else EVENING
        return p
    }

    // Read timezones for Morning/Midday/Evening sections in PDF report
    private fun setTimezones() {
 //       preferences.edit().remove(SettingsActivity.KEY_PREF_BLOODPRESSURE_TIMEZONES).apply()
        val timezones = "" + preferences.getString(SettingsActivity.KEY_PREF_BLOODPRESSURE_TIMEZONES, app.getString(R.string.BLOODPRESSURE_TIMEZONE_DEFAULT))
        val ta = timezones.split("-")
        // Morning, Afternoon, Evening
        // t0 - t1, t1 - t2,   t2 - t0
        try {
            t0 = ta[0].toInt()
            t1 = ta[1].toInt()
            t2 = ta[2].toInt()

            // Is each number larger than the previous one?
            if ( t0 > t1 || t1 > t2) "x".toInt() // force exception to get desired error handling
        } catch (e: NumberFormatException) {
            Toast.makeText(app, timezones + " " + app.getString(R.string.invalidTimezoneSetting), Toast.LENGTH_LONG).show()
            t0 = 0
            t1 = 12
            t2 = 18
        }
    }

}
