package com.zell_mbc.medilog.data

import android.annotation.SuppressLint
import android.app.Application
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Handler
import android.os.Looper
import androidx.documentfile.provider.DocumentFile
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utility.Preferences
import net.lingala.zip4j.io.outputstream.ZipOutputStream
import net.lingala.zip4j.model.ZipParameters
import net.lingala.zip4j.model.enums.EncryptionMethod
import java.io.BufferedOutputStream
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*


class Backup(application: Application, private val uri: Uri, private val zipPassword: String, private val viewModel: ViewModel) {
    private val app = application
    private var userOutputService: UserOutputService = UserOutputServiceImpl(app, null)
    private val preferences: SharedPreferences = Preferences.getSharedPreferences(app)

    private val datePrefix = preferences.getBoolean(SettingsActivity.KEY_PREF_ADD_TIMESTAMP, app.getString(R.string.ADD_TIMESTAMP_DEFAULT).toBoolean())

    // Create,write and protect backup ZIP file to backup location
    @SuppressLint("SimpleDateFormat")
    fun exportZIPFile(autoBackup: Boolean) {

        // persist permission to access backup folder across reboots
        val takeFlags = Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION
        app.contentResolver.takePersistableUriPermission(uri, takeFlags)

        // Create empty Zip file
        var zipFile = app.getString(R.string.appName) + "-Backup" + if (datePrefix) "-" + SimpleDateFormat(MainActivity.DATE_PATTERN).format(Date().time) else ""
        zipFile += ".zip"
        var dFile: DocumentFile? = null
        val dFolder: DocumentFile?
        try {
            dFolder = DocumentFile.fromTreeUri(app, uri)
        } catch (e: SecurityException) {
            Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(app.getString(R.string.eCreateFile) + ": " + dFile + " " + e.toString()) }
            return
        }
        if (dFolder == null) {
            Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(app.getString(R.string.eCreateFile) + ": " + dFile) }
            return
        }

        if (!dFolder.canWrite()) { // lost access rights, needs a manual backup
            Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(app.getString(R.string.eLostAccessRights) + ": " + dFolder.toString()) }
            return
        }

        try {
            // Check if file exists, delete if yes
            if (dFolder.findFile(zipFile) != null) dFolder.findFile(zipFile)?.delete()
            dFile = dFolder.createFile("application/zip", zipFile)
            if (dFile == null) {
                Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(app.getString(R.string.eUnableToWriteToFolder) + ": " + dFolder.toString()) }
                return
            }
        } catch (e: IllegalArgumentException) {
            Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(app.getString(R.string.eCreateFile) + ": " + dFile + " " + e.toString()) }
            return
        }

        // Add files to ZIP file
        val dest: OutputStream?
        val out: ZipOutputStream?

        try {
            dest = app.contentResolver.openOutputStream(dFile.uri)
            if (dest == null) {
                Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(app.getString(R.string.eCreateFile) + " " + dFile) }
                return
            }
            out = if (zipPassword.isEmpty()) ZipOutputStream(BufferedOutputStream(dest))
            else ZipOutputStream(BufferedOutputStream(dest), zipPassword.toCharArray())

        } catch (e: Exception) {
            Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(app.getString(R.string.eCreateFile) + " " + dFile) }
            e.printStackTrace()
            return
        }
        val zipParameters = ZipParameters()
        if (zipPassword.isNotEmpty()) {
            zipParameters.isEncryptFiles = true
            zipParameters.encryptionMethod = EncryptionMethod.AES
        }

        val fileNames = arrayOfNulls<String>(1)
        fileNames[0] = app.getString(R.string.appName) + "-" + SimpleDateFormat(MainActivity.DATE_PATTERN).format(Date().time) + ".csv"

        val zipData = arrayOfNulls<String>(1)
        zipData[0] = viewModel.dataToCSV(viewModel.dataRepository.dataBackup())
        for (i in zipData.indices) {
            try {
                zipParameters.fileNameInZip = fileNames[i]
                out.putNextEntry(zipParameters)
                out.write(zipData[i]!!.toByteArray())
                out.closeEntry()
            } catch (e: Exception) {
                Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(app.getString(R.string.eCreateFile) + " " + fileNames[i]) }
                e.printStackTrace()
                return
            }
        }
        try {
            out.close()
        } catch (e: Exception) {
            Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(app.getString(R.string.eCreateFile) + " 2 " + dFile) }
            e.printStackTrace()
            return
        }
        var msg = if (zipPassword.isEmpty()) app.getString(R.string.unprotectedZipFileCreated)
        else app.getString(R.string.protectedZipFileCreated)

        val size = viewModel.dataRepository.dataBackup().size
        val sizeString = "\n($size " + app.getString(R.string.entries) + ")"
        if (autoBackup) msg = "AutoBackup: $msg"
        Handler(Looper.getMainLooper()).post { userOutputService.showAndHideMessageForLong(msg + " " + dFile.name + sizeString) }

        // Capture last backup date
        val editor = preferences.edit()
        editor.putLong("LAST_BACKUP", Date().time)
        editor.putString(SettingsActivity.KEY_PREF_BACKUP_URI, uri.toString())
        editor.apply()
    }
}