package com.zell_mbc.medilog.data

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.sqlite.db.SimpleSQLiteQuery

@Dao
abstract class DataDao {
    @Query("DELETE from data where _id = :id")
    abstract suspend fun delete(id: Int)

    @Query("DELETE from data where comment = :tmpComment")
    abstract suspend fun deleteTmpItem(tmpComment: String)

    @Query("DELETE FROM data")
    abstract suspend fun deleteAll()

    @Query("DELETE FROM data WHERE type=:dataType")
    abstract suspend fun deleteAll(dataType: Int)

    @Query("DELETE FROM data WHERE timestamp >=:start and timestamp <=:end")
    abstract suspend fun deleteAllFiltered(start: Long, end: Long)

    // ------------------------

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insert(obj: Data): Long

    @Update
    abstract suspend fun update(obj: Data)

    @Query("SELECT * from data where _id = :id")
    abstract fun getItem(id: Int): Data

    @Query("SELECT SUM(CAST(value1 as int)) as value1 from data WHERE type=:dataType and value2 = :day")
    abstract fun getDay(dataType: Int, day: String): Int

    // ##########################
    @Query("SELECT * from data") // Used by Backup routine
    abstract fun backup(): List<Data>

    @RawQuery(observedEntities = [Data::class])
    abstract fun getLiveDataList(query: SimpleSQLiteQuery): LiveData<List<Data>>

    @RawQuery
    abstract fun getDataList(query: SimpleSQLiteQuery): List<Data>

    @RawQuery
    abstract fun getInt(query: SimpleSQLiteQuery): Int

    @RawQuery
    abstract fun getFloat(query: SimpleSQLiteQuery): Float

    @RawQuery
    abstract fun getItem(query: SimpleSQLiteQuery): Data

    // ############################################################
    // Settings table
    // ############################################################
    @Query("SELECT _id from settings WHERE type=:type AND _key=:key")
    abstract fun getId(type: Int, key: String): Int

    @Query("SELECT value from settings WHERE type=:type AND _key=:key")
    abstract fun getSetting(type: Int, key: String): String

//    @Query("UPDATE settings SET value=:value where type=:type and _key=:key")
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun setSetting(entry: Settings)

    @Query("UPDATE settings SET value=:value where type=:type and _key=:key")
    abstract fun setSetting(type: Int, key: String, value: String)

    // Count all settings
    @Query("SELECT COUNT(*) FROM settings")
    abstract fun countSettings(): Int

    // Count all settings for a datatype
    @Query("SELECT COUNT(*) FROM settings WHERE type=:dataType")
    abstract fun countSettings(dataType: Int): Int

    // Count dataTypes = unique tabs
    @Query("SELECT COUNT (DISTINCT type) FROM settings")
    abstract fun countTypes(): Int

    // Return unique data types = unique tabs
    @Query("SELECT DISTINCT type FROM settings")
    abstract fun getTypes(): List<Int>

    @Query("SELECT * from settings") // Used by Backup routine
    abstract fun backupSettings(): List<Settings>

    // Return integer type value for tab_label
    @Query("SELECT type FROM settings where _key='TAB_LABEL' AND value=:tabLabel")
    abstract fun getType(tabLabel: String): Int
}
