package com.zell_mbc.medilog.data

import androidx.lifecycle.LiveData
import androidx.sqlite.db.SimpleSQLiteQuery

class DataRepository(private val dataType: Int, private val dao: DataDao, fS:Long, fE: Long){ //: DataRepository() {
    var filterStart = 0L
    var filterEnd = 0L

    init {
        filterStart = fS
        filterEnd = fE
    }

    fun getDataList(query: SimpleSQLiteQuery): List<Data> = dao.getDataList(query)
    fun getLiveDataList(query: SimpleSQLiteQuery): LiveData<List<Data>> = dao.getLiveDataList(query)

    fun getInt(query: SimpleSQLiteQuery): Int = dao.getInt(query)
    fun getFloat(query: SimpleSQLiteQuery): Float = dao.getFloat(query)
    fun getItem(query: SimpleSQLiteQuery): Data = dao.getItem(query)

    suspend fun insert(item: Data): Long { return dao.insert(item) }
    suspend fun update(item: Data) { dao.update(item) }

    suspend fun delete(id: Int) = dao.delete(id)
    suspend fun deleteAll() = dao.deleteAll()
    suspend fun deleteAll(dataType: Int) = dao.deleteAll(dataType)
    suspend fun deleteAllFiltered(filterStart: Long, filterEnd: Long) = dao.deleteAllFiltered(filterStart, filterEnd)
    suspend fun deleteTmpItem(tmpComment: String) = dao.deleteTmpItem(tmpComment)

    // Return values summed up by day
    fun getDay(day: String):Int = dao.getDay(dataType, day)

    fun dataBackup() = dao.backup()
}