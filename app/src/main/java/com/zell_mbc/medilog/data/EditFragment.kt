package com.zell_mbc.medilog.data

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.SharedPreferences
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.afollestad.materialdialogs.MaterialDialog
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utility.Preferences
import java.text.DateFormat
import java.util.*

abstract class EditFragment: DialogFragment() {
    lateinit var userOutputService : UserOutputService
    lateinit var viewModel: ViewModel
    lateinit var preferences:SharedPreferences
    lateinit var saveButton: FloatingActionButton
    lateinit var deleteButton: FloatingActionButton

    val timestampCal = Calendar.getInstance()
    private var colourStyle = ""
    fun initializeService(view: View) {
        userOutputService = UserOutputServiceImpl(requireContext(),view)
    }

    private fun setColourStyle(v: FloatingActionButton, light: Boolean = false) {
        val preferences = Preferences.getSharedPreferences(requireContext())
        val colourStyle = preferences.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))

        if (light)
            when (colourStyle) {
                this.getString(R.string.green) -> v.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorLightGreen))
                this.getString(R.string.red) -> v.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorLightRed))
                this.getString(R.string.gray) -> v.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorLightGray))
                else -> v.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorLightBlue))
            }
        else
            when (colourStyle) {
                this.getString(R.string.green) -> v.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGreen))
                this.getString(R.string.red) -> v.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryRed))
                this.getString(R.string.gray) -> v.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGray))
                else -> v.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryBlue))
            }
        v.setColorFilter(Color.WHITE)
    }

    fun setDateTimePicker(timestamp: Long, etDate: TextView, etTime: TextView) {
        // Date/Time picker section
        timestampCal.timeInMillis = timestamp

        val dateListener = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            timestampCal.set(Calendar.YEAR, year)
            timestampCal.set(Calendar.MONTH, monthOfYear)
            timestampCal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
            etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
        }

        val timeListener = TimePickerDialog.OnTimeSetListener { _, hour, minute ->
            timestampCal.set(Calendar.HOUR_OF_DAY, hour)
            timestampCal.set(Calendar.MINUTE, minute)
            etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
            etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
        }

        etDate.setOnClickListener {
            DatePickerDialog(requireContext(),
                dateListener,
                timestampCal.get(Calendar.YEAR),
                timestampCal.get(Calendar.MONTH),
                timestampCal.get(Calendar.DAY_OF_MONTH)).show()
        }

        etTime.setOnClickListener {
            TimePickerDialog(requireContext(),
                timeListener,
                timestampCal.get(Calendar.HOUR_OF_DAY),
                timestampCal.get(Calendar.MINUTE),
                android.text.format.DateFormat.is24HourFormat(requireContext())).show()
        }
    }

    abstract fun updateItem()

    fun deleteItem() {
        val editItem = viewModel.getItem(viewModel.editItem)
        if (editItem == null) {
            userOutputService.showMessageAndWaitForLong("Unknown error!")
            return
        }

        // Do your really?
        val title = getString(R.string.deleteItem)
        MaterialDialog(requireContext()).show {
            title(text = title)
            message(R.string.doYouReallyWantToContinue)
            positiveButton(R.string.yes) {
                viewModel.delete(editItem._id)
                userOutputService.showMessageAndWaitForLong(requireContext().getString(R.string.word_item) + " " + requireContext().getString(
                    R.string.word_deleted))
                requireActivity().onBackPressed()
            }
            negativeButton(R.string.cancel)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        preferences = Preferences.getSharedPreferences(requireContext())
        colourStyle = "" + preferences.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))

        // Respond to click events
        saveButton.setOnClickListener { updateItem() }
        deleteButton.setOnClickListener { deleteItem() }

        setColourStyle(deleteButton, true)
        setColourStyle(saveButton, false)
    }

    override fun onPause() {
        super.onPause()
        MainActivity. resetReAuthenticationTimer(requireContext())
        viewModel.deleteTmpItem() // Deletes all items with comment == tmpComment
    }
}