package com.zell_mbc.medilog.data

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.zell_mbc.medilog.MainActivity

class ImportActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainActivity.setTheme(this)

        val intent: Intent = intent
        val b = intent.extras
        val uri: String? = b?.getString("URI")
        val pwd: String? = b?.getString("PWD")

        val f = ImportFragment().newInstance(uri, pwd)
        supportFragmentManager
            .beginTransaction()
            .replace(android.R.id.content, f)
            .commit()
    }
}