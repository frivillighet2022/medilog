package com.zell_mbc.medilog.data

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.room.Transaction
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.stringDelimiter
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.databinding.ActivityImportBinding
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import kotlinx.coroutines.*
import net.lingala.zip4j.io.inputstream.ZipInputStream
import net.lingala.zip4j.model.LocalFileHeader
import java.io.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class ImportFragment: Fragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: ActivityImportBinding? = null
    private val binding get() = _binding!!
    private lateinit var userOutputService : UserOutputService
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO

    @SuppressLint("SimpleDateFormat")
    var csvPattern = SimpleDateFormat(MainActivity.DATE_TIME_PATTERN)

    private var importDelimiter = ""
    private var zipPassword = ""
    private lateinit var uri: Uri
    lateinit var viewModel: ImportViewModel
    private lateinit var job: Job
    private var canceled = true

    fun newInstance(uri: String?, pwd: String?): ImportFragment {
        val f = ImportFragment()
        if (pwd != null) f.zipPassword = pwd
        if (uri != null) f.uri = uri.toUri()
        return f
    }

    private fun initializeService(view: View) {
        userOutputService = UserOutputServiceImpl(requireContext(),view)
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = ActivityImportBinding.inflate(inflater, container, false)
        initializeService(binding.root)
        return binding.root
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        // Do you really want to?
        viewModel.viewModelScope.cancel()
        if (canceled) userOutputService.showAndHideMessageForLong(getString(R.string.importCanceled))

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.button.setOnClickListener {
            requireActivity().onBackPressed()
        }

        binding.tvFileOpen.visibility = View.INVISIBLE
        binding.tvCheckHeader.visibility = View.INVISIBLE
        binding.tvLoadRawData.visibility = View.INVISIBLE
        binding.pgPrimary.visibility = View.INVISIBLE
        binding.pgSecondary.visibility = View.INVISIBLE
        binding.tvDeleteData.visibility = View.INVISIBLE
        binding.tvAddToDB.visibility = View.INVISIBLE
        binding.tvDbPercentage.visibility = View.INVISIBLE
        binding.tvError.visibility = View.INVISIBLE

        var msg1 = "1. " + getString(R.string.openFile)
        var msg2 = "2. " + getString(R.string.checkFileHeader)
        var msg3 = "3. " + getString(R.string.loadRawData)
        var msg4 = "4. " + getString(R.string.deleteExistingData)
        var msg5 = "5. " + getString(R.string.addRecords)

        viewModel = ViewModelProvider(requireActivity())[ImportViewModel::class.java]
        viewModel.viewModelScope.launch(dispatcher) {
            Handler(Looper.getMainLooper()).post {
                binding.tvFileOpen.text = msg1
                binding.tvFileOpen.visibility = View.VISIBLE
            }
            val inp = if (zipPassword == "CSV") importCSVFile()
            else importZIPFile()
            msg1 += " ✓"
            Handler(Looper.getMainLooper()).post {
                binding.tvFileOpen.text = msg1
                binding.tvCheckHeader.visibility = View.VISIBLE
            }
            Handler(Looper.getMainLooper()).post { binding.tvCheckHeader.text = msg2 }
            var reader: BufferedReader? = null
            if (inp != null)  reader = checkHeader(inp)
            if (reader == null) return@launch
            msg2 += " ✓"

            Handler(Looper.getMainLooper()).post {
                binding.tvCheckHeader.text = msg2
                binding.tvLoadRawData.text = msg3

                binding.tvLoadRawData.visibility = View.VISIBLE
                binding.pgPrimary.visibility = View.VISIBLE
                binding.tvRecordsFound.visibility = View.VISIBLE
            }

            var tmpItems: ArrayList<Data>? = null

            tmpItems = readV2Data(reader)
            if (tmpItems == null) return@launch

            val size = tmpItems.size
            msg3 = "3. " + size.toString() + " " + getString(R.string.linesRead) + " ✓"
            Handler(Looper.getMainLooper()).post {
                binding.tvLoadRawData.text = msg3
                binding.tvDeleteData.text = msg4
                binding.pgPrimary.visibility = View.INVISIBLE
                binding.tvDeleteData.visibility = View.VISIBLE
                binding.tvRecordsFound.visibility = View.INVISIBLE
            }
            // Delete only after a successful import
            viewModel.deleteAll()
            job.ensureActive() // Make sure fragment = job is still available
            msg4 += " ✓"
            Handler(Looper.getMainLooper()).post {
                binding.tvDeleteData.text = msg4
                binding.tvAddToDB.text = msg5
                binding.tvAddToDB.visibility = View.VISIBLE
                binding.pgSecondary.visibility = View.VISIBLE
                binding.tvDbPercentage.visibility = View.VISIBLE
            }
            val recordsAdded = bulkInsert(tmpItems, size)
            Handler(Looper.getMainLooper()).post { binding.tvAddToDB.text = msg5 }
            canceled = false
            job.ensureActive() // Make sure fragment = job is still available

            val msg6 = "$recordsAdded records added"
            msg5 += " ✓"
            Handler(Looper.getMainLooper()).post {
                binding.tvAddToDB.text = msg5
                binding.tvDbPercentage.text = msg6
                binding.pgSecondary.visibility = View.INVISIBLE
                binding.button.text = getString(R.string.done)
            }
        }.also { job = it }
    }

    private fun importCSVFile(): InputStream? {
        val cr = requireActivity().contentResolver
        return cr.openInputStream(uri)
    }

    private fun importZIPFile(): InputStream? {
        val localFileHeader: LocalFileHeader?
        var readLen: Int
        val readBuffer = ByteArray(4096)

        val zipInputStream = requireActivity().contentResolver.openInputStream(uri)
        var csvStream: InputStream? = null

        // Try to open ZIP file
        val zipIn: ZipInputStream
        // With default password from settings
        try {
            zipIn = ZipInputStream(zipInputStream, zipPassword.toCharArray())
            localFileHeader = zipIn.nextEntry
        } catch (e: Exception) {
            showError(getString(R.string.eOpenZipFile) + e.message)
            return null
        }

        // Store file in temporary folder
        val filePath = File(requireActivity().externalCacheDir, "")

        if (localFileHeader != null) {
            val extractedFile = File(filePath, localFileHeader.fileName)
            // Check extension, if not csv no need to bother
            if (!extractedFile.name.endsWith(".csv")) {
                showError(getString(R.string.eNoMediLogFile) + localFileHeader.fileName)
                return null
            }

            val outputStream = FileOutputStream(extractedFile)

            readLen = zipIn.read(readBuffer)
            while (readLen != -1) {
                outputStream.write(readBuffer, 0, readLen)
                readLen = zipIn.read(readBuffer)
            }
            outputStream.close()

            csvStream = FileInputStream(extractedFile)
        }
        return csvStream
    }

    private fun checkHeader(inStream: InputStream): BufferedReader? {
        val reader = BufferedReader(InputStreamReader(Objects.requireNonNull(inStream)))

        // Valid header?
        val line: String?
        try {
            line = reader.readLine()
        } catch (e: IOException) {
            showError(getString(R.string.eReadError))
            return null
        }

        // Empty file?
        if (line == null) {
            showError(getString(R.string.eNoMediLogFile) + " line == null")
            return null
        }

        // Validate header line
        val headerLine = line.replace("\\s".toRegex(), "") // Remove potential blanks

        for (testDelimiter in getString(R.string.csvSeparators)) {
            if (headerLine.indexOf(testDelimiter) > 0) importDelimiter = testDelimiter.toString()
        }

        // Check if separator exists in headerLine
        if (importDelimiter.isEmpty()) {
            var filterHint = ""
            for (c in getString(R.string.csvSeparators)) filterHint = "$filterHint$c " // Add blanks for better visibility
            showError(getString(R.string.noCsvDelimiterFound) + " $filterHint")
            return null
        }

        // Check header format
        val wantedHeader = "timestamp$importDelimiter" + "comment$importDelimiter" + "type$importDelimiter" + "value1$importDelimiter" + "value2$importDelimiter" + "value3$importDelimiter" + "value4"
        if (!headerLine.equals(wantedHeader,true)) { // Header does not match
            showError(getString(R.string.errorInCSVHeader) + ": '" + wantedHeader + "', \n" + getString(R.string.found) + ": " + headerLine)
            return null
        }

        return reader
    }

    private fun readV2Data(reader: BufferedReader?): ArrayList<Data>? {
        // Load data into buffer
        val tmpItems = ArrayList<Data>()
        if (reader == null) {
            return null
        }
        try {

            var tmpLine: String
            var line: String?
            var lineNo = 0
            while (reader.readLine().also { line = it } != null) {
                lineNo++

                val item = Data(0, 0, "", 0, "", "", "", "") // Start with empty item
                tmpLine = line.toString() + importDelimiter // Add a delimiter to make sure the last value item is not overlooked

                val data = parseCSVLine(tmpLine) //.split(importDelimiter.toRegex()).toTypedArray()

                try {
                    val date: Date? = csvPattern.parse(data[0].trim())
                    if (date != null) item.timestamp = date.time
                } catch (e: ParseException) {
                    showError("Error converting date in line $lineNo: ${System.err} , '$tmpLine'")
                    return null
                }

                val c = data[1].trim()
                // Look for line breaks
                if (c.contains("\\n")) item.comment = c.replace("\\n","\n")
                else item.comment = c

                if (c.contains("&quot;")) item.comment = item.comment.replace("&quot;", stringDelimiter.toString())  // Look for "

                try {
                    item.type = data[2].trim().toInt()
                } catch (e: NumberFormatException) {
                    showError("Error converting string to int in line $lineNo: ${System.err}, '$tmpLine'")
                    return null
                }

                val size = data.size

                try {
                    if (size > 3) item.value1 = data[3].trim() // No value should never happen but just in case
                    if (size > 4) item.value2 = data[4].trim()
                    if (size > 5) item.value3 = data[5].trim()
                    if (size > 6) item.value4 = data[6].trim()
                    if (item.type == MainActivity.WATER) {
                        item.value2 = SimpleDateFormat("yyyyMMdd").format(item.timestamp)
                    }

                } catch (e: ParseException) {
                    showError("Error setting value in line $lineNo: ${System.err}, '$tmpLine'")
                    return null
                }

                tmpItems.add(item)
                val msg = "$lineNo " + getString(R.string.linesRead)
                Handler(Looper.getMainLooper()).post { binding.tvRecordsFound.text = msg }

            }
        } catch (e: IOException) {
            return null
        }

        // If we arrived here, the import was successful
        val records = tmpItems.size
        if (records == 0) return null    // Last check before deleting

        return tmpItems
    }

    @Transaction
    fun bulkInsert(items: ArrayList<Data>, records: Int): Int {
        var percentage = 0
        var count = 0
        val div = records / 100
        var recordsAdded = 0
        for (item in items) {
            job.ensureActive()
            viewModel.insert(item)
            recordsAdded++
            if (++count >= div) {
                if (percentage < 100) percentage++
                count = 0
                    Handler(Looper.getMainLooper()).post {
                        binding.pgSecondary.progress = percentage
                    }
                    val msg = "$percentage%"
                    Handler(Looper.getMainLooper()).post { binding.tvDbPercentage.text = msg }
            }
        }
        if (recordsAdded != records) {
            showError(getString(R.string.importCanceled))
        }
        return recordsAdded
    }

    // Parser which respects quotes -> To allow text entry with ,
    private fun parseCSVLine(csvLine: String): Array<String> {
        val separator = importDelimiter[0]
        val lf = '\n'
        val cr = '\r'
        var quoteOpen = false
        val a = Array(10) { "" }
        var token = ""
        var column = 0
        for (c in csvLine.toCharArray()) when (c) {
            lf,
            cr -> {
                // not required as we are already read line
                quoteOpen = false
                a[column++] = token
                token = ""
            }
            stringDelimiter -> quoteOpen = !quoteOpen
            separator -> {
                if (!quoteOpen) {
                    a[column++] = token
                    token = ""
                } else token += c
            }
            else -> token += c
        }
        return a
    }

    fun showError(s: String) {
        Handler(Looper.getMainLooper()).post {
            binding.tvError.visibility = View.VISIBLE
            binding.tvError.text = s
        }
    }
}