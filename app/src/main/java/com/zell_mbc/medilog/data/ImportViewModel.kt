package com.zell_mbc.medilog.data

import android.app.Application
import android.content.SharedPreferences
import android.net.Uri
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.zell_mbc.medilog.utility.Preferences
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class ImportViewModel(application: Application): AndroidViewModel(application) {
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO

    val app = application

    private var dataDao     = MediLogDB.getDatabase(app).dataDao()

    lateinit var uri: Uri
    lateinit var zipPassword: String

    @JvmField
    val preferences: SharedPreferences = Preferences.getSharedPreferences(app)

    fun insert(item: Data): Long {
        var rowId = -1L
        runBlocking {
            val j = viewModelScope.launch(dispatcher) { rowId = dataDao.insert(item) }
            j.join()
        }
        return rowId
    }

    // Resets the filter an deletes all rows
    fun deleteAll() = viewModelScope.launch(dispatcher) { dataDao.deleteAll() }

}
