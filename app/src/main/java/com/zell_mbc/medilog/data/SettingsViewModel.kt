package com.zell_mbc.medilog.data

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class SettingsViewModel(application: Application): AndroidViewModel(application) {
    val app = application
    var dataType = 0
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
    private lateinit var repository: SettingsRepository

    fun init() {
        val dao = MediLogDB.getDatabase(app).settingsDao()
        repository = SettingsRepository(dao)
    }

    // Deletes all entries of a given type
    fun deleteSettings(dataType: Int) = viewModelScope.launch { repository.deleteSettings(dataType) }
    // Empty table
    fun deleteSettings() = viewModelScope.launch { repository.deleteSettings() }

    // Find type based on tab header
    fun getDataType(tabLabel: String): Int {
        var type = 0
        runBlocking {
            val j = viewModelScope.launch(Dispatchers.IO) {
                type = repository.getDataType(tabLabel)
            }
            j.join()
        }
        return type
    }

    fun get(dt: Int, key: String): String {
        var value: String? = ""
        runBlocking {
            val j = viewModelScope.launch(dispatcher) {
                value = repository.get(dt, key)
            }
            j.join()
        }
        return value ?: ""
    }

    fun getString(dt: Int, key: String): String = get(dt, key)

    fun getBoolean(dt: Int, key: String): Boolean? {
        val value = get(dt, key)
        return if (value.isEmpty()) null else value.toBoolean()
    }

    fun getInt(dt: Int, key: String): Int {
        val value: String = get(dt, key)
        return if (value.isEmpty()) 0
        else {
            val i = try {
                value.toInt()
            } catch (e: NumberFormatException) {
                0
            }
            i
        }
    }

    fun set(dt: Int, key: String, value: String) = viewModelScope.launch(dispatcher) {
        // Check if key exists
        val i = repository.getId(dt, key)
        val x = Settings(i, dt, key, value)
        repository.set(x)
    }

    // Convert boolean to string
    fun setBoolean(dt: Int, key: String, value: Boolean) {
        if (value) set(dt, key, "true")
        else set(dt, key, "false")
    }

    fun setString(dt: Int, key: String, value: String) {
        set(dt, key, value)
    }

    fun countDataTypes(): Int {
        var i = 0
        runBlocking {
            val j = viewModelScope.launch(dispatcher) {
                i = repository.count()
            }
            j.join()
        }
        return i
    }

    fun getDataTypes(): List<Int> {
        lateinit var i: List<Int>
        runBlocking {
            val j = viewModelScope.launch(dispatcher) {
                i = repository.getDataTypes()
            }
            j.join()
        }
        return i
    }
}