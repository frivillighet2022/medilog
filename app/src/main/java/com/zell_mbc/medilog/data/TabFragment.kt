package com.zell_mbc.medilog.data

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.format.DateUtils
import android.view.View
import android.widget.Toast
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.layout.SubcomposeLayout
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.zell_mbc.medilog.MainActivity.Companion.userFeedback
import com.zell_mbc.medilog.MainActivity.Companion.userFeedbackLevel
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utility.Preferences
import java.text.DateFormat
import java.util.*

abstract class TabFragment: Fragment() {
    lateinit var userOutputService: UserOutputService
    lateinit var viewModel: ViewModel

    //val _binding = null
    //binding get() = _binding!!

    var quickEntry = true
    var topPadding = 0 // Padding between tab selector and data part, will be calculated later on
    val cellPadding = 5 // Padding before and after a text in the grid
    var rowPadding = 1 // Padding tab list rows (top & bottom)
    val dividerWidth = 1 // Width of vertical divider line
    var dateFormat = DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault())
    val timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault())
    var highlightValues: Boolean = false

    var fontSize = 0
    var dateColumnWidth = 0.dp

    var itemUnit = ""
    var lowerThreshold = 0f
    var upperThreshold = 0f

    private var showDialog = mutableStateOf(false)
    var commentButtonWidth = 40

    lateinit var preferences: SharedPreferences
    lateinit var editActivityClass: Class<*>
    lateinit var infoActivityClass: Class<*>
    lateinit var chartActivityClass: Class<*>

    fun editItem(index: Int) {
        viewModel.editItem = index

        val intent = Intent(requireContext(), editActivityClass)
        launchActivity(intent, index)
    }

    override fun onResume() {
        super.onResume()
        //val t2 = DateUtils.isToday(preferences.getLong("LAST_FEEDBACK_RUN", 0L))
        //val t3 = Date(preferences.getLong("LAST_FEEDBACK_RUN", 0L))
        if (userFeedbackLevel > 0 && !DateUtils.isToday(preferences.getLong("LAST_FEEDBACK_RUN", 0L)))
            //Toast.makeText(context, "OnResume - Going to send user feedback, isToday = $t2", Toast.LENGTH_LONG).show()
            userFeedback(requireContext())
//        }
//        else
//            Toast.makeText(context, "OnResume - Not going to send user feedback, isToday = $t2", Toast.LENGTH_LONG).show()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        preferences = Preferences.getSharedPreferences(requireContext())
        quickEntry = preferences.getBoolean(SettingsActivity.KEY_PREF_QUICKENTRY, true)

        // For some reason en_DE does not set the correct date format?
        val devLocale =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) resources.configuration.locales.get(0)
            else resources.configuration.locale
        if (devLocale.language == "en" && devLocale.country == "DE") dateFormat =
            DateFormat.getDateInstance(DateFormat.SHORT, Locale.GERMANY)

        fontSize = getString(R.string.TEXT_SIZE_DEFAULT).toInt() // Set defined value in case the below fails
        var checkValue = preferences.getString(SettingsActivity.KEY_PREF_TEXT_SIZE, (context as Activity).getString(R.string.TEXT_SIZE_DEFAULT))
        if (!checkValue.isNullOrEmpty()) {
            var tmp: Int
            try {
                tmp = checkValue.toInt()
            } catch (e: NumberFormatException) {
                tmp = getString(R.string.TEXT_SIZE_DEFAULT).toInt()
                Toast.makeText(context, "Invalid Font Size value: $checkValue", Toast.LENGTH_LONG)
                    .show()
            }
            if (tmp > 0) fontSize = tmp
        }

        rowPadding =
            getString(R.string.ROW_PADDING_DEFAULT).toInt() // Set defined value in case the below fails
        checkValue = preferences.getString(
            SettingsActivity.KEY_PREF_ROW_PADDING,
            getString(R.string.ROW_PADDING_DEFAULT)
        )
        if (!checkValue.isNullOrEmpty()) {
            var tmp: Int
            try {
                tmp = checkValue.toInt()
            } catch (e: NumberFormatException) {
                tmp = getString(R.string.ROW_PADDING_DEFAULT).toInt()
                Toast.makeText(context, "Invalid Row Padding value: $checkValue", Toast.LENGTH_LONG)
                    .show()
            }
            if (tmp > 0) rowPadding = tmp
        }
    }

    fun initializeService(view: View) {
        userOutputService = UserOutputServiceImpl(requireContext(), view)
    }

    // Measure up date string
    @Composable
    fun MeasureDateString() {
        val sampleCal = Calendar.getInstance()
//        sampleCal.set(2022,12,12,23,59)
        sampleCal.set(2015,11,17,23,5)
        val millis = sampleCal.timeInMillis
        val sampleString = dateFormat.format(millis) + " - " + timeFormat.format(millis)
        MeasureTextWidth(sampleString)
        {
            textWidth ->
            Text(text = "", fontSize = fontSize.sp)
            dateColumnWidth = textWidth //+ (textWidth / 10) // Add a 10% buffer
        }
    }

    @Composable
    fun MeasureTextWidth(sampleText: String, content: @Composable (width: Dp) -> Unit) {
        SubcomposeLayout { constraints ->
            val textWidth = subcompose(sampleText) {
                Text(sampleText, fontSize = fontSize.sp)
            }[0].measure(Constraints()).width.toDp()

            val contentPlaceable = subcompose("content") {
                content(textWidth)
            }[0].measure(constraints)
            layout(contentPlaceable.width, contentPlaceable.height) {
                contentPlaceable.place(0, 0)
            }
        }
    }

    @Composable
    fun ItemClicked(selectedItem: Int) {
        val openDialog = remember { mutableStateOf(true) }

        if (openDialog.value) {
            editItem(selectedItem)
            showDialog.value = false
            openDialog.value = false
        }
    }

    private fun launchActivity(intent: Intent, index: Int) {
        val extras = Bundle()
        val item = viewModel.getItem(index)
        if (item != null) {
            extras.putInt("ID", item._id)
            extras.putInt("DATA_TYPE", viewModel.dataType)
            intent.putExtras(extras)
            startActivity(intent)
        } // Save id for lookup by EditFragment
    }

    private fun setColourStyle(v: FloatingActionButton, light: Boolean = false) {
        val preferences = Preferences.getSharedPreferences(requireContext())
        val colourStyle = preferences.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))

        if (light)
            when (colourStyle) {
                this.getString(R.string.green) -> v.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorLightGreen))
                this.getString(R.string.red) -> v.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorLightRed))
                this.getString(R.string.gray) -> v.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorLightGray))
                else -> v.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorLightBlue))
            }
        else
            when (colourStyle) {
                this.getString(R.string.green) -> v.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGreen))
                this.getString(R.string.red) -> v.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryRed))
                this.getString(R.string.gray) -> v.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGray))
                else -> v.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryBlue))
            }

        v.setColorFilter(Color.WHITE)
    }


    abstract fun addItem()

    fun configureButtons(buttonAdd: FloatingActionButton, buttonInfo: FloatingActionButton, buttonChart: FloatingActionButton) {
        buttonAdd.setOnClickListener { addItem() }
        setColourStyle(buttonAdd, false)

        if (quickEntry) buttonAdd.visibility = View.GONE // Hide Add Button

        setColourStyle(buttonInfo, true)
        setColourStyle(buttonChart, true)

        buttonInfo.setOnClickListener(View.OnClickListener {
            context ?: return@OnClickListener
            val intent = Intent(requireContext(), infoActivityClass)
            startActivity(intent)
        })

        buttonChart.setOnClickListener(View.OnClickListener {
            if (context == null) return@OnClickListener
            if (viewModel.getSize(true) < 2) {
                userOutputService.showMessageAndWaitForLong(requireContext().getString(R.string.notEnoughDataForChart))
                return@OnClickListener
            }
            val intent = Intent(requireContext(), chartActivityClass)
            startActivity(intent)
        })
    }
}
