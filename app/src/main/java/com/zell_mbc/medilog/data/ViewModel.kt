package com.zell_mbc.medilog.data

import android.annotation.SuppressLint
import android.app.Application
import android.content.SharedPreferences
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.pdf.PdfDocument
import android.graphics.text.LineBreaker
import android.graphics.text.MeasuredText
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.FileProvider
import androidx.core.graphics.drawable.toBitmap
import androidx.lifecycle.*
import androidx.sqlite.db.SimpleSQLiteQuery
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.DATE_PATTERN
import com.zell_mbc.medilog.MainActivity.Companion.DATE_TIME_PATTERN
import com.zell_mbc.medilog.MainActivity.Companion.DIARY
import com.zell_mbc.medilog.MainActivity.Companion.stringDelimiter
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.settings.SettingsActivity.Companion.KEY_PREF_DELIMITER
import com.zell_mbc.medilog.utility.Preferences
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import net.lingala.zip4j.io.outputstream.ZipOutputStream
import net.lingala.zip4j.model.ZipParameters
import net.lingala.zip4j.model.enums.EncryptionMethod
import java.io.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.min
import com.zell_mbc.medilog.MainActivity.Companion.FILTER_OFF
import com.zell_mbc.medilog.MainActivity.Companion.FILTER_ROLLING
import com.zell_mbc.medilog.MainActivity.Companion.FILTER_STATIC


abstract class ViewModel(application: Application): AndroidViewModel(application) {
    val app = application

    abstract var tabIcon: Int
    abstract val filterStartPref: String
    abstract val filterEndPref: String
    abstract val filterModePref: String
    abstract val rollingFilterValuePref: String
    abstract val rollingFilterTimeframePref: String

    abstract val itemName: String


    var userName = ""
    var headerText = ""
    var formatedDate = ""

    private var dataDao     = MediLogDB.getDatabase(app).dataDao()

    var userOutputService: UserOutputService = UserOutputServiceImpl(app, null)
    private var filterChanged = MutableLiveData(0L)
    private val authority = app.applicationContext.packageName + ".provider"
    private val lineSeparator = System.getProperty("line.separator")
    private val fileName = "MediLog.csv"

    // PDF Report variables
    var document = PdfDocument()
    val pdfPaint = Paint()
    val pdfPaintHighlight = Paint()
    var dateTabWidth = 0f
    var timeWidth = 0f
    var dateWidth = 0f
    var padding = 0f
    val space = 5f
    lateinit var pdfItems: List<Data>

    abstract val landscape: Boolean
    abstract val pageSize: String?

    lateinit var canvas: Canvas
    lateinit var page: PdfDocument.Page
    private var pageNumber = 1
    private val A4 = arrayOf(595, 842)
    private val letter = arrayOf(612, 792)

    var pdfRightBorder = 0f // Set in fragment based on canvas size - pdfLeftBorder
    var pdfDataBottom = 0f // Set in fragment based on canvas size
    val pdfHeaderTop = 30f
    val pdfHeaderBottom = 50f
    val pdfDataTop = 70f
    var pdfLineSpacing = 15f
    val pdfLeftBorder = 10f
    var dataTab = 0f
    var commentTab = 0f

    // Filter variables
    var filterStart = 0L
    var filterEnd = 0L
    var filterMode = 0
    var rollingTimeframe = 0
    var rollingValue = 0

    var dataType = 0
    var highlightValues = true

    var editItem = -1 // ID of the item which the edit dialog will lookup

    abstract var showAllTabs: Boolean // Only used for Diary Tab

    lateinit var dataRepository: DataRepository
    lateinit var items: LiveData<List<Data>>

    // Store data entry fields here so they survive an orientation change
    var value1 = ""
    var value2 = ""
    var value3 = ""
    var value4 = ""
    var comment = ""
    var availableWidth = 0

    @JvmField
    val preferences: SharedPreferences = Preferences.getSharedPreferences(app)
    @JvmField
    val separator = preferences.getString(KEY_PREF_DELIMITER, ",")

    @SuppressLint("SimpleDateFormat")
    var csvPattern = SimpleDateFormat(DATE_TIME_PATTERN)

    // Constructor
    open fun init(dt: Int) {
        dataType = dt
        getFilter()
        dataRepository = DataRepository(dataType, dataDao, filterStart, filterEnd)
        items = if (dataType == DIARY && showAllTabs) Transformations.switchMap(filterChanged) { getAllComments() }
        else Transformations.switchMap(filterChanged) { getItems(filterStart, filterEnd) }

        formatedDate = DateFormat.getDateInstance(DateFormat.SHORT).format(Calendar.getInstance().time) + " - " + DateFormat.getTimeInstance(DateFormat.SHORT).format(Calendar.getInstance().time)
        userName = preferences.getString(SettingsActivity.KEY_PREF_USER, "").toString()
        headerText =
            if (userName.isNotEmpty()) app.getString(R.string.reportTitle, itemName, userName )
            else app.getString(R.string.appName) + " - $itemName " + app.getString(R.string.actionPDF)
    }

    fun insert(item: Data): Long {
        var rowId = -1L
        runBlocking {
            val j = viewModelScope.launch(Dispatchers.IO) { rowId = dataRepository.insert(item) }
            j.join()
        }
        return rowId
    }

    fun update(item: Data) = viewModelScope.launch(Dispatchers.IO) {
        dataRepository.update(item)
    }

    // Compile filter string
    fun compileFilter(): String {
        var  string = ""
        if ((filterStart != 0L) && (filterEnd != 0L)) string += " AND timestamp >= $filterStart AND timestamp <= $filterEnd"
        if ((filterStart == 0L) && (filterEnd != 0L)) string += " AND timestamp <= $filterEnd"
        if ((filterStart != 0L) && (filterEnd == 0L)) string += " AND timestamp >= $filterStart"
        return string
    }

    // Raw types
    fun getItem(string: String): Data? {
        val query = SimpleSQLiteQuery(string)
        var item: Data? = null
        runBlocking {
            val j = viewModelScope.launch(Dispatchers.IO) {
                item = dataRepository.getItem(query)
            }
            j.join()
        }
        return item
    }

    fun getDataList(string: String): List<Data> {
        val query = SimpleSQLiteQuery(string)
        lateinit var items: List<Data>
        runBlocking {
            val j = viewModelScope.launch(Dispatchers.IO) {
                items = dataRepository.getDataList(query)
            }
            j.join()
        }
        return items
    }

    private fun getLiveDataList(string: String): LiveData<List<Data>> {
        val query = SimpleSQLiteQuery(string)
        lateinit var items: LiveData<List<Data>>
        runBlocking {
            val j = viewModelScope.launch(Dispatchers.IO) {
                items = dataRepository.getLiveDataList(query)
            }
            j.join()
        }
        return items

    }

    // Run query which returns an integer result
    fun getInt(string: String): Int {
        val query = SimpleSQLiteQuery(string)
        var i = 0
        runBlocking {
            val j = viewModelScope.launch(Dispatchers.IO) {
                i = dataRepository.getInt(query)
            }
            j.join()
        }
        return i
    }

    // Run query which returns a float result
    fun getFloat(string: String): Float {
        val query = SimpleSQLiteQuery(string)
        var f = 0f
        runBlocking {
            val j = viewModelScope.launch(Dispatchers.IO) {
                f = dataRepository.getFloat(query)
            }
            j.join()
        }
        return f
    }

    // Run query which returns a float result
    fun getFloatAsString(string: String, digits: Int = 1): String {
        val f = getFloat(string)
        val format = "%." + digits + "f"
        return format.format(f)
    }

    /////
    // Return value1 summed up by day
    @SuppressLint("SimpleDateFormat")
    fun getToday():Int {
        val today = SimpleDateFormat("yyyyMMdd").format(Date().time)
        var i = 0
        runBlocking {
            val j = viewModelScope.launch(Dispatchers.IO) {
                i = dataRepository.getDay(today)
            }
            j.join()
        }
        return i
    }

    // Higher level functions
    fun getItem(id: Int): Data? {
        val string = "SELECT * from data where _id = $id"
        return getItem(string)
    }

    // All but Diary use this version
    open fun getItems(order: String, filtered: Boolean): List<Data> {
        var string = "SELECT * FROM data WHERE type = $dataType"
        if (filtered) string += compileFilter()
        string += " ORDER BY timestamp $order"

        return getDataList(string)
    }

    fun getItems(filterStart: Long, filterEnd: Long): LiveData<List<Data>> {
        var string = "SELECT * FROM data WHERE type = $dataType"
        string += compileFilter()
        string += " ORDER BY timestamp DESC"

        return getLiveDataList(string)
    }


    fun getFirst(filtered: Boolean): Data? {
        val items = getItems("ASC", filtered )
        if (items.isEmpty()) return null
        return items[0]
    }

    fun getLast(filtered: Boolean): Data? {
        val items = getItems("DESC", filtered )
        if (items.isEmpty()) return null
        return items[0]
    }

    // Used by editItem Fragments to delete leftover temporary entries
    fun deleteTmpItem() = viewModelScope.launch(Dispatchers.IO) { dataRepository.deleteTmpItem(MainActivity.tmpComment) }

    fun isTmpItem(comment: String?): Boolean { return (comment.equals(MainActivity.tmpComment)) }

    fun toStringDate(l: Long): String { return DateFormat.getDateInstance().format(l) }

    @SuppressLint("SimpleDateFormat")
    fun toStringTime(l: Long): String { return SimpleDateFormat("HH:mm").format(l) }

    fun delete(id: Int) = viewModelScope.launch(Dispatchers.IO) { dataRepository.delete(id) }

    // Resets the filter an deletes all rows
    fun deleteAll(filtered: Boolean) = viewModelScope.launch(Dispatchers.IO) {
        if (filtered) {
            // If no end date specified set fake one to avoid a '...where timestamp <:0' query
            var tmpFilter = filterEnd
            if (tmpFilter == 0L) tmpFilter = Calendar.getInstance().timeInMillis + (86400000 * 10) // Add a 10 days worth of milliseconds
            dataRepository.deleteAllFiltered(filterStart, tmpFilter)
        } else dataRepository.deleteAll()
    }

    // Deletes all entries of a given type
    fun deleteAll(dataType: Int) = viewModelScope.launch(Dispatchers.IO) { dataRepository.deleteAll(dataType) }

    fun getDataTableSize(): Int {
        val string = "SELECT COUNT(*) FROM data"
        return getInt(string)
    }

    // Get # of records for a datatype
    fun getSize(filtered: Boolean): Int {
        var string = "SELECT COUNT(*) FROM data WHERE type= $dataType"
        if (filtered) string += compileFilter()
        return getInt(string)
    }

    // Get # of records for a specific data type
    fun getSize(type: Int): Int {
        val string = "SELECT COUNT(*) FROM data WHERE type= $type"
        return getInt(string)
    }

    // Min Max functions
    private fun getMinValue(field: String, filtered: Boolean): Float{
        var string = "SELECT MIN(CAST($field as float)) as $field from data WHERE type=$dataType"

        if (filtered) {
            getFilter()
            string += compileFilter()
        }
        return getFloat(string)
    }

    private fun getMaxValue(field: String, filtered: Boolean): Float{
        var string = "SELECT MAX(CAST($field as float)) as $field from data WHERE type=$dataType"

        if (filtered) {
            getFilter()
            string += compileFilter()
        }
        return getFloat(string)
    }

    private fun getMinValue(field: String, timeframe: Int): Float {
        val today = Calendar.getInstance()
        today.add(Calendar.DATE, -timeframe)
        val from = today.timeInMillis
        return getFloat("SELECT MAX(CAST($field as float)) as $field from data WHERE type=$dataType  AND timestamp>=$from")
    }

    private fun getMaxValue(field: String, timeframe: Int): Float {
        val today = Calendar.getInstance()
        today.add(Calendar.DATE, -timeframe)
        val from = today.timeInMillis
        return getFloat("SELECT MAX(CAST($field as float)) as $field from data WHERE type=$dataType  AND timestamp>=$from")
    }

    fun getAvgFloat(field: String, timeframe: Int): Float {
        val today = Calendar.getInstance()
        today.add(Calendar.DATE, -timeframe)
        val from = today.timeInMillis
        val string = "SELECT AVG(CAST($field as float)) as $field from data WHERE type=$dataType AND timestamp>=$from"
        return getFloat(string)
    }

    fun getAvgFloat(field: String, filtered: Boolean): Float {
        var string = "SELECT AVG(CAST($field as float)) as $field from data WHERE type=$dataType"

        if (filtered) {
            getFilter()
            string += compileFilter()
        }
        return getFloat(string)
    }

    fun getAvgInt(field: String, filtered: Boolean): Int {
        var string = "SELECT AVG(CAST($field as int)) as $field from data WHERE type=$dataType"

        if (filtered) {
            getFilter()
            string += compileFilter()
        }
        return getInt(string)
    }

    fun getMinValue1(filtered: Boolean): Float = getMinValue("value1", filtered)
    fun getMinValue2(filtered: Boolean): Float = getMinValue("value2", filtered)
    fun getMaxValue1(filtered: Boolean): Float = getMaxValue("value1", filtered)
    fun getMaxValue2(filtered: Boolean): Float = getMaxValue("value2", filtered)
    fun getMinValue1(timeframe: Int): Float = getMinValue("value1", timeframe)
    fun getMaxValue1(timeframe: Int): Float = getMaxValue("value1", timeframe)

    // End Min Max Functions

    private fun getAllComments(): LiveData<List<Data>> {
        var string = "SELECT * FROM data WHERE comment!=''"
        string += compileFilter()
        string += " ORDER BY timestamp DESC"

        return getLiveDataList(string)
    }

    fun getFilter() {
        filterMode = preferences.getInt(filterModePref, FILTER_OFF)
        when (filterMode) {
            FILTER_OFF -> {
                filterStart = 0L
                filterEnd = 0L
            }
            FILTER_STATIC -> {
                filterStart = preferences.getLong(filterStartPref, 0L)
                filterEnd = preferences.getLong(filterEndPref, 0L)
            }
            FILTER_ROLLING -> {
                rollingTimeframe = preferences.getInt(rollingFilterTimeframePref, 2) // (this).getString(R.string.ROLLING_FILTER_DEFAULT))
                rollingValue = preferences.getInt(rollingFilterValuePref, 1) // (this).getString(R.string.ROLLING_FILTER_VALUE_DEFAULT))
                filterEnd = 0

                val today = Calendar.getInstance()
                when (rollingTimeframe) {
                    0 -> today.add(Calendar.DATE,-rollingValue)
                    1 -> today.add(Calendar.DATE,-rollingValue * 7)
                    2 -> today.add(Calendar.DATE,-rollingValue * 30)
                    3 -> today.add(Calendar.DATE,-rollingValue * 365)
                }
                filterStart = today.timeInMillis
            }
        }
    }

    fun setFilter(start: Long, end: Long) {
        val editor = preferences.edit()
        editor.putInt(filterModePref, filterMode)
        editor.putInt(rollingFilterTimeframePref, rollingTimeframe)
        editor.putInt(rollingFilterValuePref,rollingValue)

        editor.putLong(filterStartPref, start)
        editor.putLong(filterEndPref, end)
        editor.apply()

        when (filterMode) {
        FILTER_OFF -> {
            filterStart = 0L
            filterEnd = 0L
            }
        FILTER_ROLLING -> {
            val today = Calendar.getInstance()
            when (rollingTimeframe) {
                0 -> today.add(Calendar.DATE,-rollingValue)
                1 -> today.add(Calendar.DATE,-rollingValue * 7)
                2 -> today.add(Calendar.DATE,-rollingValue * 30)
                3 -> today.add(Calendar.DATE,-rollingValue * 365)
            }
            filterStart = today.timeInMillis
            filterEnd = 0L
            }
        FILTER_STATIC -> {
            filterStart = start
            filterEnd = end
            }
        }
        filterChanged.value = (filterStart + filterEnd) // Trigger data refresh

        Log.d("DataViewModel : ", "New value: " + filterChanged.value)
    }

    fun resetFilter() {
        val editor = preferences.edit()
        editor.putLong(filterStartPref, 0L)
        editor.putLong(filterEndPref, 0L)
        editor.apply()
        filterStart = 0L
        filterEnd = 0L
        filterChanged.value = 0L
    }

    fun setTodayFilter() {
        // Set "today" filter
        val cal: Calendar = Calendar.getInstance()
        cal.set(Calendar.HOUR_OF_DAY, 0)
        cal.set(Calendar.MINUTE, 0)
        setFilter(cal.timeInMillis, 0)
    }

     // Write temporary CSV file
    fun writeCsvFile(filtered: Boolean): Uri? {
            val filePath = File(app.cacheDir, "")
            val fn = fileName.replace(".csv", "") + "-" + SimpleDateFormat(DATE_PATTERN).format(Date().time) + ".csv"
            val newFile = File(filePath, fn)

            val uri = FileProvider.getUriForFile(app, authority, newFile)
            try {
            val out = app.contentResolver.openOutputStream(uri)
            if (out != null) {
                val data = dataToCSV(getItems("ASC", filtered))
                out.run {
                    write(data.toByteArray(Charsets.UTF_8))
                    flush()
                    close()
                }
            }
        } catch (e: IOException) {
            userOutputService.showAndHideMessageForLong(app.getString(R.string.eCreateFile) + " " + newFile)
            return null
        }
        return uri
    }

    fun dataToCSV(items: List<Data>): String {
        if (items.isEmpty()) {
            userOutputService.showAndHideMessageForLong(app.getString(R.string.noDataToExport))
            return ""
        }

        val sb = StringBuilder()

        // Create header

        var s = "timestamp$separator comment$separator type$separator value1$separator value2$separator value3$separator value4$lineSeparator"
        sb.append(s)

        for (item in items) {
            if (item.comment.contains("\n")) item.comment = item.comment.replace("\n","\\n")  // Look for line breaks
            if (item.comment.contains(stringDelimiter)) item.comment = item.comment.replace(stringDelimiter.toString(),"&quot;")  // Look for "

            s = csvPattern.format(item.timestamp) + separator + stringDelimiter + item.comment + stringDelimiter + separator + item.type + separator + item.value1 + separator + item.value2 + separator + item.value3 + separator + item.value4 + lineSeparator
            sb.append(s)
        }
        return sb.toString()
    }

    fun getPdfFile(document: PdfDocument?): Uri? {
        if (document == null) {
            return null
        }
        val filePath = File(app.cacheDir, "")
        val fn = fileName.replace(".csv", "") + "-" + SimpleDateFormat(DATE_PATTERN).format(Date().time) + ".pdf"
        val newFile = File(filePath, fn)
        val uri = FileProvider.getUriForFile(app, authority, newFile)
        try {
            val out = app.contentResolver.openOutputStream(uri)
            if (out != null) {
                document.run { writeTo(out) }
                out.run {
                    flush()
                    close()
                }
            }
        } catch (e: IOException) {
            userOutputService.showAndHideMessageForLong(app.getString(R.string.eCreateFile) + " " + newFile)
            return null
        }
        return uri
    }

    fun writeBackup(uri: Uri, zipPassword: String, autoBackup: Boolean = false) {
        val vm = this
        viewModelScope.launch(Dispatchers.IO) {
            Backup(app, uri, zipPassword, vm).exportZIPFile(autoBackup)
        }
    }

    @SuppressLint("SimpleDateFormat")
    // https://github.com/srikanth-lingala/zip4j
    fun getZIP(document: PdfDocument?, zipPassword: String): Uri? {
        if (document == null) {
            userOutputService.showAndHideMessageForLong(fileName.replace(".csv", "") + " " + app.getString(R.string.noDataToExport))
            return null
        }

        val uri: Uri?

        val outputStream: OutputStream?
        val zipOutputStream: ZipOutputStream?

        val zipParameters = ZipParameters()
        if (zipPassword.isNotEmpty()) {
            zipParameters.isEncryptFiles = true
            zipParameters.encryptionMethod = EncryptionMethod.AES
        }

        // Create empty Zip file
        val zipFileName = app.getString(R.string.appName) + "-" + SimpleDateFormat(DATE_PATTERN).format(Date().time) + ".zip"
        try {
            val filePath = File(app.externalCacheDir, "")
            val newFile = File(filePath, zipFileName)
            uri = FileProvider.getUriForFile(app, authority, newFile)
            outputStream = app.contentResolver.openOutputStream(uri)
            if (outputStream == null) {
                userOutputService.showAndHideMessageForLong(app.getString(R.string.eCreateFile) + " " + zipFileName)
                return null
            }

            zipOutputStream = if (zipPassword.isEmpty()) ZipOutputStream(BufferedOutputStream(outputStream))
            else ZipOutputStream(BufferedOutputStream(outputStream), zipPassword.toCharArray())

        } catch (e: Exception) {
            userOutputService.showAndHideMessageForLong(app.getString(R.string.eCreateFile) + " " + zipFileName)
            e.printStackTrace()
            return null
        }

        // File 1
        val fileNames = arrayOfNulls<String>(1)
        fileNames[0] = app.getString(R.string.appName) + "-" + SimpleDateFormat(DATE_PATTERN).format(Date().time) + ".pdf"

        val zipContentOut = ByteArrayOutputStream()
        document.run {
            writeTo(zipContentOut)
            close()
        }

        zipParameters.fileNameInZip = fileNames[0]

            try {
                zipOutputStream.run {
                    putNextEntry(zipParameters)
                    write(zipContentOut.toByteArray())
                    closeEntry()
                }
            } catch (e: Exception) {
                userOutputService.showAndHideMessageForLong(app.getString(R.string.eCreateFile) + " " + fileNames[0])
                e.printStackTrace()
                return null
            }

        zipOutputStream.close()

        return uri
    }


    fun createPage(document: PdfDocument) {
        val pageInfo: PdfDocument.PageInfo?
        // ----------

        val width = getWidth(pageSize)
        val height = getHeight(pageSize)

        pageInfo = if (landscape) PdfDocument.PageInfo.Builder(height, width, pageNumber++).create()
        else PdfDocument.PageInfo.Builder(width, height, pageNumber++).create()

        page = document.startPage(pageInfo)
        canvas = page.canvas

        pdfRightBorder = canvas.width.toFloat() - pdfLeftBorder
        pdfDataBottom = canvas.height.toFloat() - 15
    }

    private fun getWidth(pageSize: String?): Int {
        return when (pageSize) {
            "Letter" -> letter[0]
            else -> A4[0]
        }
    }

    private fun getHeight(pageSize: String?): Int {
        return when (pageSize) {
            "Letter" -> letter[1]
            else -> A4[1]
        }
    }

    // ------------ PDF support functions

    fun multipleLines(comment: String, y: Float): Float {
        // ------ Break text into multiple lines

        var f = y
        var lines = listOf(comment)
        // any line breaks?
        if (comment.indexOf('\n') > 0) {
            lines = comment.split('\n')
        }
        for (line in lines) {
            // Next check if text fits…
            if (line.length <= availableWidth) {
                canvas.drawText(line, commentTab + space, f, pdfPaint)
                f = checkForNewPage(f + pdfLineSpacing)
            } else {
                if (Build.VERSION.SDK_INT > 28) { // Below 2 functions are not available on SDK prior to 29
                    val mt: MeasuredText = MeasuredText.Builder(line.toCharArray())
                        .appendStyleRun(pdfPaint, line.length, false) // Use paint for "Hello, "
                        .build()

                    val lb = LineBreaker.Builder() // Use simple line breaker
                        .setBreakStrategy(LineBreaker.BREAK_STRATEGY_SIMPLE) // Do not add hyphenation.
                        .setHyphenationFrequency(LineBreaker.HYPHENATION_FREQUENCY_NONE) // Build the LineBreaker
                        .build()

                    val c = LineBreaker.ParagraphConstraints()
                    c.width = pdfRightBorder - commentTab

                    val r = lb.computeLineBreaks(mt, c, 0)

                    var prevOffset = 0
                    Log.d("Line: ", line)
                    for (ii in 1..r.lineCount) {  // iterate over the lines
                        val nextOffset = r.getLineBreakOffset(ii - 1)
                        val t = line.substring(prevOffset, nextOffset)
//                            Log.d("t: ", t)
                        canvas.drawText(t, commentTab + space, f, pdfPaint)
                        f = checkForNewPage(f + pdfLineSpacing)
                        prevOffset = nextOffset
                    }
                } else {
                    // Old Android versions
                    var cutPosition = availableWidth
                    var cutLine = line.substring(0, cutPosition)
                    var remaining = line.substring(cutPosition)
                    while (cutLine.length > 0) {
                        canvas.drawText(cutLine, commentTab + space, f, pdfPaint)
                        cutPosition = min(availableWidth, remaining.length)
                        cutLine = remaining.substring(0, cutPosition)
                        remaining = remaining.substring(cutPosition)
                        f = checkForNewPage(f + pdfLineSpacing)
                    }
                }
            }
        }
        return f
        // --- End multiple lines
    }

    abstract fun setColumns(pdfPaintHighlight: Paint)
    abstract fun drawHeader(pdfPaint: Paint, pdfPaintHighlight: Paint)

    fun drawHeader(pdfPaint: Paint) {
        val logoTab = 30
        val bitmap =
            AppCompatResources.getDrawable(app, R.mipmap.ic_launcher)?.toBitmap(logoTab, 30)
        if (bitmap != null) canvas.drawBitmap(bitmap, 5f, 5f, pdfPaint)

        val dtString = app.getString(R.string.date) + " " + formatedDate
        val dateWidth = pdfPaint.measureText(dtString)
        val pdfHeaderDateColumn = pdfRightBorder - dateWidth

        // Draw header
        canvas.drawText(headerText, pdfLeftBorder + logoTab, pdfHeaderTop, pdfPaint)
        canvas.drawText(
            app.getString(R.string.date) + " " + formatedDate,
            pdfHeaderDateColumn,
            pdfHeaderTop,
            pdfPaint
        )
        canvas.drawLine(pdfLeftBorder, pdfHeaderBottom, pdfRightBorder, pdfHeaderBottom, pdfPaint)
        canvas.drawLine(pdfLeftBorder, pdfDataBottom, pdfRightBorder, pdfDataBottom, pdfPaint)
    }


    fun checkForNewPage(line: Float): Float {
        val ret = if (line > pdfDataBottom) {
            document.finishPage(page)
            createPage(document)
            drawHeader(pdfPaint, pdfPaintHighlight)
            pdfDataTop + pdfLineSpacing
        } else line
        return ret
    }

    fun measureColumn(availableWidth: Float): Int {
        // How many chars can we fit in our column
        var width = 1
        val sampleText = "x".repeat(availableWidth.toInt()+1)
        var x = pdfPaint.measureText(sampleText,0, width)
        var measure: String
        while (x < availableWidth) {
            measure = sampleText.substring(0,width++)
            x = pdfPaint.measureText(measure)
        }
        return width
    }

    open fun createPdfDocument(filtered: Boolean): PdfDocument? {
        if (getSize(filtered) == 0) return null // We should never get here!

        document = PdfDocument()  // Reset document

        val s = preferences.getString(SettingsActivity.KEY_PREF_PDF_TEXT_SIZE, app.getString(R.string.PDF_TEXT_SIZE_DEFAULT))
        var textSize = 0f
        if (!s.isNullOrEmpty()) textSize = s.toFloat()

        pdfPaint.isFakeBoldText = false
        pdfPaint.color = Color.BLACK
        pdfPaint.textSize = textSize

        pdfPaintHighlight.isFakeBoldText = true
        pdfPaintHighlight.textSize = textSize

        // Measure width of date tab
        pdfItems = getItems("ASC", filtered)
        if (pdfItems.isEmpty()) {
            userOutputService.showAndHideMessageForLong( app.getString(R.string.noDataToExport))
            return null
        }

        padding = 3 * space
        var dtString = toStringDate(pdfItems[0].timestamp) + "  " + toStringTime(pdfItems[0].timestamp)
        dateTabWidth = pdfPaintHighlight.measureText(dtString)

        dtString = toStringDate(pdfItems[0].timestamp)
        dateWidth = pdfPaintHighlight.measureText(dtString)

        dtString = toStringTime(pdfItems[0].timestamp)
        timeWidth = pdfPaintHighlight.measureText(dtString)

        val fm: Paint.FontMetrics = pdfPaintHighlight.fontMetrics
        pdfLineSpacing = fm.bottom - fm.top + fm.leading

        createPage(document)

        return null
    }


}