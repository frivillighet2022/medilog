package com.zell_mbc.medilog.diary

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.graphics.ColorUtils
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utility.Red
import java.time.*
import java.util.*
import com.zell_mbc.medilog.utility.ListTheme
import com.zell_mbc.medilog.utility.Preferences
import java.time.format.TextStyle

// Catch the API26 requirement at the beginning of OnCreate
@SuppressLint("NewApi")
class DiaryChartActivity : AppCompatActivity() {
    lateinit var preferences: SharedPreferences

    val days = ArrayList<Day>()

    private val COLOR_GOOD = Color(0xFFEDEDED)
    private val COLOR_BAD = Red
    private val PADDING1 = 2
    private val PADDING2 = 8

    private val BOX_SIZE = 20
    private val FILLER = -1f
    private val EMPTY = -2f
    private val GOOD = 0f

    private var columns = 0
    private var actualMonth = 0
    private var actualYear = 0

    var fontSize  = 0

    data class Day(
        var date: LocalDate,
        var state: Float,
        var count: Int
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT < 26) {
            Toast.makeText(this, getString(R.string.featureNotAvailable), Toast.LENGTH_LONG).show()
            finish()
            return
        }

        val viewModel = ViewModelProvider(this)[DiaryViewModel::class.java]
        viewModel.init(MainActivity.DIARY)

        // For the charts only native diary entries can be considered, no matter what showAllTabs is set to
        val tmp = viewModel.showAllTabs
        viewModel.showAllTabs = false
        val items = viewModel.getItems("ASC", filtered = true)
        viewModel.showAllTabs = tmp

        preferences = Preferences.getSharedPreferences(this)
        fontSize = getString(R.string.TEXT_SIZE_DEFAULT).toInt() // Set defined value in case the below fails
        val checkValue = preferences.getString(
            SettingsActivity.KEY_PREF_TEXT_SIZE, (this).getString(
                R.string.TEXT_SIZE_DEFAULT))
        if (!checkValue.isNullOrEmpty()) {
            var tmp2: Int
            try { tmp2 = checkValue.toInt() }
            catch (e: NumberFormatException) {
                tmp2 = getString(R.string.TEXT_SIZE_DEFAULT).toInt()
                Toast.makeText(this, "Invalid Font Size value: $checkValue", Toast.LENGTH_LONG).show()
            }
            if (tmp2 > 0) fontSize = tmp2
        }

        // Add empty items at the beginning so the chart always starts on first day of week
        //--------------------
        val firstDate = LocalDate.from(LocalDateTime.ofInstant(Instant.ofEpochMilli(items[0].timestamp),ZoneOffset.UTC))
        val dayInWeek = firstDate.dayOfWeek.ordinal
        actualMonth = firstDate.monthValue
        actualYear = firstDate.year

        for (i in dayInWeek downTo 1) {
            val emptyDay = Day(firstDate.minusDays(i.toLong()), EMPTY, 1)
            days.add(emptyDay)
        }
        //--------------------

        // Fill the matrix
        var previousDay = Day(LocalDate.now(), FILLER,1) // Today +1 as a temporary setting

        for (item in items) {
            val itemDay = Day(
                LocalDate.from(LocalDateTime.ofInstant(Instant.ofEpochMilli(item.timestamp), ZoneOffset.UTC)),
                if (item.value2.isEmpty()) GOOD else item.value2.toFloat(),
                1
            )

            // Another value on the same day
            if (itemDay.date == previousDay.date) {
                // Average up state
                previousDay.state = ((previousDay.state * previousDay.count) + itemDay.state) / (previousDay.count + 1)
                previousDay.count++
            }
            // New day
            else {
                // Fill the day gap if necessary
                var gapDay = Day(previousDay.date.plusDays(1), FILLER,1)
                while (gapDay.date < itemDay.date)  {
                    days.add(gapDay)
                    gapDay = Day(gapDay.date.plusDays(1), FILLER,1)
                }
                days.add(itemDay)
                previousDay = itemDay
            }
        }

        // Add an empty column for the looks
        for (i in 1..7) {
            val emptyDay = Day(previousDay.date.plusDays(i.toLong()), EMPTY, 1)
            days.add(emptyDay)
        }
        if (days.size == 0) {
            return
        }
        setContent { DrawCalendar() }
    }

    @Composable
    fun YearHeader(column: Int) {
        if (column >= columns)
            return // Last column

        // Check if the 1st is in this column
        val firstDayInColumn = ((column) * 7)
        if (firstDayInColumn >= days.size) return

        val startDay = days[firstDayInColumn].date
        val lastDayInColumn = firstDayInColumn + 7
        if (lastDayInColumn >= days.size) return // Last column

        val endDay  = days[lastDayInColumn].date
        // Always print year in first column
        if (column == 0 || startDay.dayOfYear > endDay.dayOfYear) Text(text = endDay.year.toString(), fontSize = fontSize.sp, color = MaterialTheme.colors.primary)
        else Spacer(modifier = Modifier
            .width(BOX_SIZE.dp)
            .absolutePadding(left = PADDING1.dp))
    }

    @Composable
    fun MonthHeader(column: Int) {
        if (column >= columns)
            return // Last column

        // Print if the 1st is in this column
        val firstDayInColumn = ((column) * 7)
        val lastDayInColumn = firstDayInColumn + 6

        if (lastDayInColumn -1 >= days.size)
            return // Last column

        val endDay  = days[lastDayInColumn].date
        if (endDay.monthValue != actualMonth ) {
            actualMonth = endDay.monthValue
           // Log.d("actualMonth: ", actualMonth.toString())
            Text(text = getMonthName(actualMonth, true),
                Modifier
                    .size((BOX_SIZE).dp)
                    .absolutePadding(left = PADDING1.dp)
                    .fillMaxWidth(), overflow = TextOverflow.Clip, fontSize = fontSize.sp, color = MaterialTheme.colors.primary)
        }
        else Spacer(
            Modifier
                .size(BOX_SIZE.dp)
                .absolutePadding(left = PADDING1.dp)
                .fillMaxWidth())
    }

    @Composable
    // First / Weekday column
    fun RowHeader() {
        Text("",fontSize= fontSize.sp) // Year row
        Text("", fontSize = fontSize.sp) // Month row

        val doW = DayOfWeek.values()
        for (i in 0..3) {
            Text(doW[i*2].getDisplayName(TextStyle.SHORT , Locale.getDefault()).substring(0, 1),
                Modifier.size(BOX_SIZE.dp), textAlign = TextAlign.Center, color = MaterialTheme.colors.primary,fontSize = fontSize.sp)
            Text("",
                Modifier
                    .size(BOX_SIZE.dp)
                    .absolutePadding(top = PADDING1.dp),fontSize = fontSize.sp)
        }
    }

    // EMPTY: Added to the beginning of the stack to make sure data starts on Monday
    // FILLER: Blank box
    // GOD: Box with 0 value
    // NOT_GOOD: Box with value 2
    @Composable
    fun TableCell(
        state: Float = 0f,
        date: String = ""
    ) {
        var leftBorder = false
        val leftPadding = PADDING1

/*
        // Month separators
        if (date.isNotEmpty()) {
            val d = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd")).dayOfMonth
            Log.d("Date: ", d.toString())
            leftBorder = (d < 8)
        }
        if (leftBorder) {
            leftPadding = 1
            Divider(
                color = MaterialTheme.colors.secondary,
                modifier = Modifier
                    .absolutePadding(left = leftPadding.dp)
                    .height(BOX_SIZE.dp)
                    .width(1.dp)
            )
        }

 */
        val COLOR_EMPTY = Color.White
        when (state) {
            EMPTY -> Box(modifier = Modifier
                .size(BOX_SIZE.dp)
                .clip(RectangleShape)
                .absolutePadding(left = leftPadding.dp, top = PADDING1.dp)
                .background(COLOR_EMPTY))
            FILLER -> Box(modifier = Modifier
                .size(BOX_SIZE.dp)
                .clip(RectangleShape)
                .absolutePadding(left = leftPadding.dp, top = PADDING1.dp)
                .background(COLOR_GOOD)
                .clickable {
                    if (date.isNotEmpty()) Toast
                        .makeText(applicationContext, date, Toast.LENGTH_SHORT)
                        .show()
                })
            GOOD -> {Box(modifier = Modifier
                .size(BOX_SIZE.dp)
                .clip(RectangleShape)
                .absolutePadding(left = leftPadding.dp, top = PADDING1.dp)
                .background(COLOR_GOOD)
                .border(1.dp, color = MaterialTheme.colors.secondary)
                .clickable {
                    if (date.isNotEmpty()) Toast
                        .makeText(applicationContext, "$date, $state", Toast.LENGTH_SHORT)
                        .show()
                })
                }
            else -> {
                val color = Color(ColorUtils.blendARGB(COLOR_GOOD.toArgb(), COLOR_BAD.toArgb(), state / 2))
                Box(modifier = Modifier
                    .size(BOX_SIZE.dp)
                    .clip(RectangleShape)
                    .absolutePadding(left = leftPadding.dp, top = PADDING1.dp)
                    .background(color)
                    .clickable {
                        if (date.isNotEmpty()) Toast
                            .makeText(applicationContext, "$date, $state", Toast.LENGTH_SHORT)
                            .show()
                    })
            }
        }
    }

    @Composable
    // We are drawing horizontal first, vertical second. In other words row by row
    fun DrawCalendar() {
        val rows = listOf(0,1,2,3,4,5,6)
        columns = (days.size / 7) +1 // items / 7 days a week

        val scrollState = rememberScrollState()
            ListTheme {
                Row(modifier = Modifier
                    .fillMaxWidth()
                    .absolutePadding(left = PADDING2.dp, right = PADDING2.dp)) {
                    Column(
                        Modifier
                            .width(BOX_SIZE.dp)
                            .fillMaxSize(),                        horizontalAlignment = Alignment.CenterHorizontally) { RowHeader() }

                    LazyColumn(Modifier.fillMaxSize()) {
                        // year row
                        item {
                            Row(modifier = Modifier.horizontalScroll(state = scrollState)) {
                                for (i in 0..columns) YearHeader(i)
                            }
                        }

                        // month row
                        item {
                            Row(modifier = Modifier.horizontalScroll(state = scrollState)) {
                                for (i in 0..columns) MonthHeader(i)
                            }
                        }

                        // 7 rows for the weekdays
                        items(rows) {
                            Row(
                                Modifier
                                    .fillMaxWidth()
                                    .horizontalScroll(state = scrollState)) {
                                // Draw all columns of a row
                                for (column in 0..columns) {
                                    val ii = (it) + ((column) * 7)
                                    if (ii >= days.size) TableCell(state = EMPTY)
                                    else TableCell(
                                        state = days[ii].state,
                                        date = days[ii].date.toString()
                                    )
                                }
                            }
                        }
                        item {
                            Row(modifier = Modifier.fillMaxWidth()) { Text( text = "") }
                            Row(Modifier.absolutePadding(top = PADDING1.dp)) {
                                Text(
                                    text = stringResource(id = R.string.stateNoValue),
                                    modifier = Modifier.width((BOX_SIZE * 8).dp),
                                    fontSize = fontSize.sp,
                                    color = MaterialTheme.colors.primary,
                                )
                                Box(
                                    modifier = Modifier
                                        .size(BOX_SIZE.dp)
                                        .clip(RectangleShape)
                                        .background(COLOR_GOOD)
                                )
                            }
                            Row(Modifier.absolutePadding(top = PADDING1.dp)) {
                                Text(
                                    text = stringResource(id = R.string.stateGood),
                                    modifier = Modifier.width((BOX_SIZE * 8).dp),
                                    fontSize = fontSize.sp,
                                    color = MaterialTheme.colors.primary
                                )
                                Box(
                                    modifier = Modifier
                                        .size(BOX_SIZE.dp)
                                        .clip(RectangleShape)
                                        .background(COLOR_GOOD)
                                        .border(1.dp, color = MaterialTheme.colors.secondary)
                                )
                            }
                            Row(Modifier.absolutePadding(top = PADDING1.dp)) {
                                Text(
                                    text = stringResource(id = R.string.stateBad),
                                    modifier = Modifier.width((BOX_SIZE * 8).dp),
                                    fontSize = fontSize.sp,
                                    color = MaterialTheme.colors.primary
                                )
                                Box(
                                    modifier = Modifier
                                        .size(BOX_SIZE.dp)
                                        .clip(RectangleShape)
                                        .background(COLOR_BAD)
                                )
                            }
                        }
                    }
                }
            }
    }

    // index can be 0 - 11
    private fun getMonthName(index: Int, shortName: Boolean = true): String {
        var format = "%tB"
        if (shortName) format = "%tb"
        val calendar = Calendar.getInstance()
        calendar[Calendar.MONTH] = index - 1
        calendar[Calendar.DAY_OF_MONTH] = 1
        return java.lang.String.format(format, calendar)
    }
}
