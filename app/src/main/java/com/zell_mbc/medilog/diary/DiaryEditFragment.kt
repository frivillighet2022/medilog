package com.zell_mbc.medilog.diary

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.DIARY
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.EditFragment
import com.zell_mbc.medilog.databinding.DiaryEditformBinding
import java.text.DateFormat

class DiaryEditFragment : EditFragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: DiaryEditformBinding? = null
    private val binding get() = _binding!!

    private val GOOD = 0
    private val NOT_GOOD = 1
    private val BAD = 2

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = DiaryEditformBinding.inflate(inflater,container,false)
        initializeService(binding.root)

        return binding.root
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun updateItem() {
        val editItem = viewModel.getItem(viewModel.editItem)
        if (editItem == null) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.unknownError))
            return
        }

        // Check empty variables
        val value = binding.etDiary.text.toString()
        if (value.isEmpty()) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.diaryMissing))
            return
        }

        editItem.comment = value
        editItem.timestamp = timestampCal.timeInMillis

        when (binding.rgState.checkedRadioButtonId) {
            binding.rbNotGood.id -> editItem.value2 = NOT_GOOD.toString()
            binding.rbBad.id     -> editItem.value2 = BAD.toString()
            else                 -> editItem.value2 = GOOD.toString()
        }

        viewModel.update(editItem)

        userOutputService.showMessageAndWaitForLong(requireContext().getString(R.string.itemUpdated))
        requireActivity().onBackPressed()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val vm = MainActivity.getViewModel(DIARY) as DiaryViewModel?
        if (vm != null) viewModel = vm
        else {
            userOutputService.showMessageAndWaitForLong(getString(R.string.unknownError))
            // Returning here will make sure none of the controls are initialized = no error when e.g. delete is selected
            return
        }

        saveButton = binding.btSave
        deleteButton = binding.btDelete
        super.onViewCreated(view, savedInstanceState)

        val tmp = getString(R.string.state) //+ " (" +getString(R.string.warningSign) + ")"
        binding.tvState.text = tmp

        val editItem = viewModel.getItem(viewModel.editItem)
        if (editItem == null) {
            // Close keyboard so error is visible
            (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken, 0)
            userOutputService.showMessageAndWaitForLong(getString(R.string.unknownError))
            return
        }
        // Check if we are really editing or if this is a new value, if new entry delete tmpComment
        if (viewModel.isTmpItem(editItem.comment)) editItem.comment = ""

        this.binding.etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(editItem.timestamp)
        this.binding.etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(editItem.timestamp)
        this.binding.etDiary.setText(editItem.comment)

        val state = try { editItem.value2.toInt() }
        catch  (e: NumberFormatException) { GOOD }

        when (state) {
            NOT_GOOD -> this.binding.rbNotGood.isChecked = true
            BAD      -> this.binding.rbBad.isChecked = true
            else     -> this.binding.rbGood.isChecked = true
        }

        setDateTimePicker(editItem.timestamp, binding.etDate, binding.etTime)

        // Make sure first field is highlighted and keyboard is open
        this.binding.etDiary.requestFocus()
    }
}