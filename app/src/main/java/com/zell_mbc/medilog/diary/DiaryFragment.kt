package com.zell_mbc.medilog.diary

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.*
import com.zell_mbc.medilog.MainActivity.Companion.DIARY
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.data.TabFragment
import com.zell_mbc.medilog.databinding.DiaryTabBinding
import com.zell_mbc.medilog.utility.ListTheme
import java.util.*

class DiaryFragment : TabFragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: DiaryTabBinding? = null
    private val binding get() = _binding!!

    private val GOOD = 0
    private val NOT_GOOD = 1
    private val BAD = 2
    private var state = GOOD

    private var commentValue = ""
    private var stateValue = R.drawable.ic_ok
    private var goodSmiley = "    " // Don't show emoji for good entries, so bad ones pop out visually
    private var notGoodSmiley = ""
    private var badSmiley = ""

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = DiaryTabBinding.inflate(inflater,container,false)
        initializeService(binding.root)

        return binding.root
    }

    override fun addItem() {
        if (!quickEntry) {
            val tmpItem = Data(0, Date().time, MainActivity.tmpComment, viewModel.dataType, "","","", "")
            val itemID = viewModel.insert(tmpItem)
            if (itemID  != 0L) editItem(itemID.toInt())
            else userOutputService.showMessageAndWaitForLong("No entry with tmp value found!")
            return
        }

        // If value is empty, replace with default text based on state
        if (commentValue.isEmpty()) {
            commentValue = when (state) {
                1 -> getString(R.string.notGood)
                2 -> getString(R.string.bad)
                else ->  getString(R.string.good)
            }
        }

        val item = Data(0, Date().time, commentValue, viewModel.dataType, "", state.toString(),"","")
        viewModel.insert(item)
        if ((viewModel.filterEnd > 0 ) && (viewModel.filterEnd < item.timestamp)) userOutputService.showMessageAndWaitForDuration(getString(R.string.filteredOut), 4000)

        state = BAD
        setEmoji()

        commentValue  = ""
        binding.itemList.setContent { ShowContent() }

        // Close keyboard after entry is done
        (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken, 0)
    }

    @Composable
    fun ShowRow(item: Data) {
            // Timestamp
        Text(dateFormat.format(item.timestamp) + " - " + timeFormat.format(item.timestamp), Modifier.padding(end = cellPadding.dp, top = rowPadding.dp, bottom = rowPadding.dp).
        width(dateColumnWidth), color = MaterialTheme.colors.primary, fontSize = fontSize.sp, )
        TabRowDefaults.Divider(color = MaterialTheme.colors.secondary, modifier = Modifier.width(dividerWidth.dp).fillMaxHeight()) // Vertical separator

        var emoji = goodSmiley // Blank entry
        var fontStyle = FontStyle.Normal
        if (item.type != DIARY) { // Merged items
            fontStyle = FontStyle.Italic
        }
        else {  // Proper Diary item
            val state = try {
                item.value2.toInt()
            } catch (e: NumberFormatException) {
                0
            }

            emoji = when (state) {
                1 -> notGoodSmiley
                2 -> badSmiley
                else -> goodSmiley
            }
        }

        Text(
            emoji,
            Modifier.padding(start = cellPadding.dp, end = cellPadding.dp, top = rowPadding.dp, bottom = rowPadding.dp).width(15.dp),
            color = MaterialTheme.colors.primary,
            fontSize = fontSize.sp
        )
        TabRowDefaults.Divider(color = MaterialTheme.colors.secondary, modifier = Modifier.width(dividerWidth.dp).fillMaxHeight()) // Vertical separator
        Text(
            item.comment,
            Modifier.padding(start = cellPadding.dp, top = rowPadding.dp, bottom = rowPadding.dp).fillMaxHeight(),
            color = MaterialTheme.colors.primary,
            fontSize = fontSize.sp,
            fontStyle = fontStyle,
        )
    }

    @Composable
    fun ShowContent() {
        val listState = rememberLazyListState()
        val listItems = viewModel.getItems("DESC",true)

        var selection: Data? by remember { mutableStateOf(null)}
        var comment by remember { mutableStateOf(commentValue) }
        var triggerRefresh by remember { mutableStateOf(false) }

        if (commentValue.isEmpty()) comment = ""

        ListTheme {
            MeasureDateString()

            // DataEntryRow(), Only show if quick entry is enabled
            if (quickEntry) {

                Column (modifier = Modifier.fillMaxWidth()){
                        Row {
                            TextField(
                                leadingIcon = { Icon(
                                    painter = painterResource(stateValue),
                                    contentDescription = "State",
                                    modifier = Modifier
                                        .clickable {  setEmoji()
                                        triggerRefresh = true })
                                },
                                value = comment,
                                onValueChange = {   comment = it },
                                singleLine = false,
                                maxLines = 2,
                                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text, capitalization = KeyboardCapitalization.Sentences),
                                label = { Text(text = stringResource(id = R.string.diary) ) },
                                colors = TextFieldDefaults.textFieldColors(textColor = MaterialTheme.colors.primary, backgroundColor = androidx.compose.ui.graphics.Color.Transparent),
                                modifier = Modifier
                                    .fillMaxWidth(),
//                                  .padding(end = cellPadding.dp),
                                keyboardActions = KeyboardActions( onDone = { addItem() })
                            )
                        } // Row

                        if (triggerRefresh) {
                            this@DiaryFragment.ShowContent()
                            triggerRefresh = false
                        }
                        commentValue = comment
                    }
                topPadding = 80
            }

            LazyColumn(
                state = listState,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.padding(top = topPadding.dp)
            ) {
                items(listItems) { item ->
                    TabRowDefaults.Divider(color = MaterialTheme.colors.secondary, thickness = 1.dp) // Lines starting from the top
                    Row(
                        modifier = Modifier
                            .height(IntrinsicSize.Min)
                            .fillMaxWidth()
                            .width(IntrinsicSize.Max)
                            .clickable { selection = item },
                        verticalAlignment = Alignment.CenterVertically
                    )
                    {
                        ShowRow(item)
                    }
                    if (selection != null) {
                        // Check if we are dealing with a genuine Diary item
                        if (selection!!.type == DIARY)  ItemClicked(selection!!._id)
                        else userOutputService.showMessageAndWaitForDuration(getString(R.string.notADiaryItem), 4000)
                        selection = null
                    }
                }
            }
        }
    }

    private fun setEmoji() {
        if (state == BAD) state = GOOD
        else              state++

        stateValue = when (state) {
            NOT_GOOD -> R.drawable.ic_neutral //getString(R.string.notGoodSmileyEmojy) //setColorFilter(Color.rgb(255, 191, 0)) //backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryAmber))
            BAD      -> R.drawable.ic_not_ok //getString(R.string.badSmileyEmojy)  //setColorFilter(Color.RED) //backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryRed))
            else     -> R.drawable.ic_ok //getString(R.string.goodSmileyEmojy)  //setColorFilter(standardColors)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        editActivityClass  = DiaryEditActivity::class.java
        infoActivityClass  = DiaryInfoActivity::class.java
        chartActivityClass = DiaryChartActivity::class.java

        viewModel =
            ViewModelProvider(requireActivity())[DiaryViewModel::class.java]  // This should return the MainActivity ViewModel
        configureButtons(binding.btAdd,binding.btInfo, binding.btChart)
        binding.btAdd.visibility = View.VISIBLE

        // -------------------------------
        notGoodSmiley = getString(R.string.notGoodSmileyEmojy)
        badSmiley = getString(R.string.badSmileyEmojy)
        state = BAD
        setEmoji()

        // Preset value in case something was stored in viewModel during onDestroy
        commentValue = viewModel.comment

        binding.itemList.setContent {ShowContent() }
    }

    override fun onDestroy() {
        viewModel.comment = commentValue
        super.onDestroy()
        }
    }