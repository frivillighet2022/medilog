package com.zell_mbc.medilog.diary

import android.app.Application
import android.graphics.Paint
import android.graphics.Typeface
import android.graphics.pdf.PdfDocument
import com.zell_mbc.medilog.MainActivity.Companion.DIARY
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.data.ViewModel
import com.zell_mbc.medilog.settings.SettingsActivity

class DiaryViewModel(application: Application): ViewModel(application) {
    override var tabIcon  = R.drawable.ic_diary
    override val filterStartPref = "DIARYFILTERSTART"
    override val filterEndPref = "DIARYFILTEREND"
    override val filterModePref =  "DIARY_FILTER_MODE"
    override val rollingFilterValuePref = "DIARY_ROLLING_FILTER_VALUE"
    override val rollingFilterTimeframePref = "DIARY_ROLLING_FILTER_TIMEFRAME"

    override var itemName = app.getString(R.string.diary)
    override var landscape = preferences.getBoolean(SettingsActivity.KEY_PREF_DIARY_LANDSCAPE, app.getString(R.string.DIARY_LANDSCAPE_DEFAULT).toBoolean())
    override var pageSize = preferences.getString(SettingsActivity.KEY_PREF_DIARY_PAPER_SIZE, app.getString(R.string.DIARY_PAPER_SIZE_DEFAULT))
    override var showAllTabs = preferences.getBoolean(SettingsActivity.KEY_PREF_DIARY_DIARY_SHOWALLTABS, app.getString(R.string.DIARY_SHOWALLTABS_DEFAULT).toBoolean())

    private var warningTab = 0f

    private var notGoodSmiley = app.getString(R.string.notGoodSmileyEmojy)
    private var badSmiley =  app.getString(R.string.badSmileyEmojy)
    private var goodSmiley =  "" // Don't show - app.getString(R.string.goodSmileyEmojy)

    override fun createPdfDocument(filtered: Boolean): PdfDocument? {
        if (getSize(filtered) == 0) {
            userOutputService.showAndHideMessageForLong( app.getString(R.string.diary) + ": " + app.getString(R.string.noDataToExport))
            return null
        }

        super.createPdfDocument(filtered)

        val notGood = "1"
        val bad = "2"

        val pdfPaintItalics = Paint()
        pdfPaintItalics.typeface = Typeface.create(Typeface.DEFAULT, Typeface.ITALIC)
        val pdfPaintBoldItalics = Paint()
        pdfPaintBoldItalics.typeface = Typeface.create(Typeface.DEFAULT, Typeface.ITALIC)
        pdfPaintBoldItalics.isFakeBoldText = true

        // Needs it's own getItems because we are blending in other tabs
        val string = if (showAllTabs) "SELECT * from data WHERE comment!='' ORDER BY timestamp ASC"
                     else "SELECT * FROM data WHERE type = $DIARY ORDER BY timestamp ASC"
        val pdfItems = getDataList(string)

        setColumns(pdfPaintHighlight)
        drawHeader(pdfPaint, pdfPaintHighlight)
        var f = pdfDataTop + pdfLineSpacing
        availableWidth = measureColumn(pdfRightBorder - commentTab)

        for (item in pdfItems) {
            f = checkForNewPage(f)

            canvas.drawText(toStringDate(item.timestamp) + " " + toStringTime(item.timestamp) , pdfLeftBorder, f, pdfPaint)
            val state = when (item.value2) {
                notGood -> notGoodSmiley
                bad      -> badSmiley
                else     -> goodSmiley
            }

            canvas.drawText(state, warningTab, f, pdfPaint)
            f = multipleLines(item.comment, f)
        }
        // finish the page
        document.finishPage(page)
        return document
    }

    override fun getItems(order: String, filtered: Boolean): List<Data> {
        var string = if (showAllTabs) "SELECT * from data WHERE comment!=''"
        else "SELECT * FROM data WHERE type = $DIARY"

        if (filtered) string += compileFilter()
        string += " ORDER BY timestamp $order"

        return getDataList(string)
    }

    override fun setColumns(pdfPaintHighlight: Paint) {
        val warningSignWidth = pdfPaintHighlight.measureText(app.getString(R.string.warningSign))
        warningTab = pdfLeftBorder + dateTabWidth + padding
        commentTab = warningTab + warningSignWidth + space + space
    }

    override fun drawHeader(pdfPaint: Paint, pdfPaintHighlight: Paint) {
        drawHeader(pdfPaint) // VieModel function doesn't need to highlight

        // Data section
       canvas.drawText(app.getString(R.string.date), pdfLeftBorder, pdfDataTop, pdfPaintHighlight)
       canvas.drawText(app.getString(R.string.diary), warningTab, pdfDataTop, pdfPaintHighlight)
       canvas.drawLine(warningTab - space, pdfHeaderBottom, warningTab - space, pdfDataBottom, pdfPaint)
    }
}
