package com.zell_mbc.medilog.glucose

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.DashPathEffect
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.androidplot.util.PixelUtils
import com.androidplot.util.SeriesUtils.minMax
import com.androidplot.xy.*
import com.zell_mbc.medilog.MainActivity.Companion.GLUCOSE
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utility.*
import java.text.*
import java.util.*
import kotlin.math.roundToInt


// Chart manual
// https://github.com/halfhp/androidplot/blob/master/docs/xyplot.md
class GlucoseChartFragment : Fragment() {
    private var glucoseLowerThreshold = ArrayList<Float>()
    private var glucoseUpperThreshold = ArrayList<Float>()
    private var glucoses = ArrayList<Float>()
    private var ketones = ArrayList<Float>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.glucose_chart, container, false)
    }

    @SuppressLint("SimpleDateFormat")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // create a couple arrays of y-values to plot:
        val labels = ArrayList<String>()
        var gMax = 0f
        var gMin = 1000f
        var kMax = 0f
        var kMin = 1000f

        val preferences = Preferences.getSharedPreferences(requireContext())
        var sTmp: String
        val simpleDate = SimpleDateFormat("MM-dd")

        val logKetone = preferences.getBoolean(SettingsActivity.KEY_PREF_LOG_KETONE, getString(R.string.LOG_KETONE_DEFAULT).toBoolean())
        val usStyle = (preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_UNIT, getString(R.string.GLUCOSE_UNIT)) == getString(R.string.GLUCOSE_UNIT_US))

        val viewModel = ViewModelProvider(this)[GlucoseViewModel::class.java]
        viewModel.init(GLUCOSE)
        val items = viewModel.getItems("ASC", filtered = true)
        for (item in items) {
            sTmp = simpleDate.format(item.timestamp)

            labels.add(sTmp)
            val sugar = try {
                item.value1.toFloat()
            } catch (e: NumberFormatException) {
                0f
            }
            glucoses.add(sugar)

            if (logKetone) {
                val f = try { item.value2.toFloat() } catch (e: NumberFormatException) { 0f }
//                ketones.add(if (usStyle) f * 10 else f)
                ketones.add(f)
                // Keep min and max values
                if (f > kMax) kMax = f
                if (f < kMin) kMin = f
            }

            // Keep min and max values
            if (sugar > gMax) gMax = sugar
            if (sugar < gMin) gMin = sugar
        }
        if (glucoses.size == 0) {
            return
        }

        // initialize our XYPlot reference:
        val glucosePlot: XYPlot = view.findViewById(R.id.glucosePlot)
        PanZoom.attach(glucosePlot)

        //glucosePlot.

        val backgroundColor = getBackgroundColor(requireContext())
        glucosePlot.graph.backgroundPaint.color = backgroundColor

        val fontSizeSmall: Int = getFontSizeSmallInPx(requireContext())

        val axisPaint = Paint()
        axisPaint.style = Paint.Style.FILL_AND_STROKE
        axisPaint.color = getTextColorPrimary(requireContext())
        axisPaint.textSize = fontSizeSmall.toFloat()
        axisPaint.isAntiAlias = true

        val gridPaint = Paint()
        gridPaint.style = Paint.Style.STROKE
        gridPaint.color = getTextColorSecondary(requireContext())
        gridPaint.isAntiAlias = false
        gridPaint.pathEffect = DashPathEffect(floatArrayOf(3f, 2f), 0F)

        val showGrid = preferences.getBoolean(SettingsActivity.KEY_PREF_GLUCOSE_SHOW_GRID, true)
        if (!showGrid) {
            glucosePlot.graph.domainGridLinePaint = null
            glucosePlot.graph.rangeGridLinePaint = null
        }
        else {
            glucosePlot.graph.domainGridLinePaint = gridPaint
            glucosePlot.graph.rangeGridLinePaint = gridPaint // Horizontal lines
        }

        val isLegendVisible = preferences.getBoolean(SettingsActivity.KEY_PREF_GLUCOSE_SHOW_LEGEND, false)
        glucosePlot.legend.isVisible = isLegendVisible
        glucosePlot.legend.isDrawIconBackgroundEnabled = false

        val s1 = SimpleXYSeries(glucoses, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, getString(R.string.glucose))
        val s4 = if (logKetone) SimpleXYSeries(ketones, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, getString(R.string.ketone)) else null

        val gMinMax: MutableList<Number> = mutableListOf(gMin, gMax)
        val kMinMax: MutableList<Number> = mutableListOf(kMin, kMax)

        val series1 = NormedXYSeries(s1, null, NormedXYSeries.Norm(minMax(gMinMax), 0.0, true ))
        val series4 = if (logKetone) NormedXYSeries(s4, null, NormedXYSeries.Norm(minMax(kMinMax), 0.1, true )) else null

        val glucoseBorder = ContextCompat.getColor(requireContext(), R.color.chart_blue_border)
        val glucoseFill = ContextCompat.getColor(requireContext(), R.color.chart_blue_fill)
        val series1Format = LineAndPointFormatter(glucoseBorder, null, glucoseFill, null)
        glucosePlot.addSeries(series1, series1Format)

        // If threshold is set create dedicated chart, otherwise show as origin
        val thresholds = preferences.getBoolean(SettingsActivity.KEY_PREF_GLUCOSE_SHOW_THRESHOLD, getString(R.string.GLUCOSE_SHOW_THRESHOLDS_DEFAULT).toBoolean())
        if (thresholds) {
            val glucoseLowerThresholdValue: Float
            val glucoseUpperThresholdValue: Float
            val s = "" + preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_THRESHOLDS, getString(R.string.GLUCOSE_THRESHOLDS_DEFAULT))
            val th = checkThresholds(requireContext(), s, getString(R.string.GLUCOSE_THRESHOLDS_DEFAULT), R.string.glucose)

            glucoseLowerThresholdValue = th[0].toFloat()
            glucoseUpperThresholdValue = th[1].toFloat()

            for (item in glucoses) {
                glucoseLowerThreshold.add(glucoseLowerThresholdValue)
                glucoseUpperThreshold.add(glucoseUpperThresholdValue)
            }

            val s2 = SimpleXYSeries(glucoseLowerThreshold, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "")
            val s3 = SimpleXYSeries(glucoseUpperThreshold, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "")
            val series2 = NormedXYSeries(s2, null, NormedXYSeries.Norm(minMax(gMinMax), 0.0, true ))
            val series3 = NormedXYSeries(s3, null, NormedXYSeries.Norm(minMax(gMinMax), 0.0, true ))
            val formatThreshold = LineAndPointFormatter(Color.BLUE, null, null, null)
            formatThreshold.linePaint.pathEffect = DashPathEffect(floatArrayOf( // always use DP when specifying pixel sizes, to keep things consistent across devices:
                    PixelUtils.dpToPix(20f),
                    PixelUtils.dpToPix(15f)), 0f)

            formatThreshold.isLegendIconEnabled = false
            formatThreshold.linePaint.strokeWidth = 2f
            glucosePlot.addSeries(series2, formatThreshold)
            glucosePlot.addSeries(series3, formatThreshold)
        }

        if (logKetone) {
            glucosePlot.graph.setLineLabelEdges(XYGraphWidget.Edge.BOTTOM, XYGraphWidget.Edge.LEFT, XYGraphWidget.Edge.RIGHT)
            glucosePlot.graph.getLineLabelStyle(XYGraphWidget.Edge.RIGHT).format = DecimalFormat("#.#")
            val x = glucosePlot.graph.marginLeft* 2
            glucosePlot.graph.marginRight = x
            glucosePlot.graph.lineLabelInsets.right = PixelUtils.dpToPix(-5f) // Move text 5 steps to the right

            val ketoneBorder = ContextCompat.getColor(requireContext(), R.color.chart_red_border)
            val ketoneFill = ContextCompat.getColor(requireContext(), R.color.chart_red_fill)
            val ketoneFormat = LineAndPointFormatter(ketoneBorder, null, ketoneFill, null)
            glucosePlot.addSeries(series4, ketoneFormat)

            val lm = glucosePlot.layoutManager
//            lm.a  addToRight(Widget)
        }
        else glucosePlot.graph.setLineLabelEdges(XYGraphWidget.Edge.BOTTOM, XYGraphWidget.Edge.LEFT)

        //glucosePlot.setMarkupEnabled(true);

        // X-Axis format
        glucosePlot.graph.getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).format = object : Format() {
            override fun format(obj: Any, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer {
                val i = (obj as Number).toFloat().roundToInt()
                return toAppendTo.append(labels[i])
            }

            override fun parseObject(source: String, pos: ParsePosition): Any { return 0 }
        }

        // First Y-Axis format
        glucosePlot.graph.getLineLabelStyle(XYGraphWidget.Edge.LEFT).format = object : Format() {
            override fun format(obj: Any, toAppendTo: StringBuffer, pos: FieldPosition?): StringBuffer? {
                // obj contains the raw Number value representing the position of the label being drawn.
                // customize the labeling however you want here:
                val i = series1.denormalizeYVal(obj as Number).toFloat()
                val s = if (usStyle)
                    i.toInt().toString()
                else
                    i.toString().substring(0,3)
                return toAppendTo.append(s)
            }

            override fun parseObject(source: String?, pos: ParsePosition?): Any? { return null }
        }

        // Second Y-Axis format
        glucosePlot.graph.getLineLabelStyle(XYGraphWidget.Edge.RIGHT).format = object : Format() {
            override fun format(obj: Any, toAppendTo: StringBuffer, pos: FieldPosition?): StringBuffer? {
                // obj contains the raw Number value representing the position of the label being drawn.
                // customize the labeling however you want here:
                val i = series4?.denormalizeYVal(obj as Number)?.toFloat()
                val s = i.toString().substring(0,3)
                return toAppendTo.append(s)
            }

            override fun parseObject(source: String?, pos: ParsePosition?): Any? { return null }
        }
    }

}
