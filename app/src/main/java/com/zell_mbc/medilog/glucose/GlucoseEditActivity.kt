package com.zell_mbc.medilog.glucose

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NavUtils
import com.zell_mbc.medilog.MainActivity

class GlucoseEditActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainActivity.setTheme(this)

        supportFragmentManager.beginTransaction()
            .replace(android.R.id.content, GlucoseEditFragment())
            .commit()
    }

    override fun onDestroy() {
        NavUtils.navigateUpFromSameTask(this) // Todo: This is a hack which should get replaced
        super.onDestroy()
    }

}
