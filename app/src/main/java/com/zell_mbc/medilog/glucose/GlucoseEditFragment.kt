package com.zell_mbc.medilog.glucose

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.view.isVisible
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.GLUCOSE
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.EditFragment
import com.zell_mbc.medilog.databinding.GlucoseEditformBinding
import com.zell_mbc.medilog.settings.SettingsActivity
import java.text.DateFormat

class GlucoseEditFragment : EditFragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: GlucoseEditformBinding? = null
    private val binding get() = _binding!!
    private var usStyle = false
    private var logKetone = false

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = GlucoseEditformBinding.inflate(inflater,container,false)
        initializeService(binding.root)

        return binding.root
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun updateItem() {
        // Close keyboard after entry is done so error messages are visible
        (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken,0)

        val editItem = viewModel.getItem(viewModel.editItem)
        if (editItem == null) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.unknownError))
            return
        }

        // Check empty variables
        if (binding.etGlucose.text.toString().isEmpty() && binding.etKetone.text.toString().isEmpty()) {
            userOutputService.showMessageAndWaitForLong(this.getString(R.string.valuesMissing))
            return
        }

        // Valid glucose?
        val glucoseString = binding.etGlucose.text.toString()
        var testValue = 0f
        if (glucoseString.isNotBlank()) {
            try {testValue = glucoseString.toFloat() } catch (e: Exception) {
                userOutputService.showMessageAndWaitForLong("Exception: " + this.getString(R.string.invalid) + " " + this.getString(
                        R.string.glucose
                    ) + " " + this.getString(R.string.value) + " $glucoseString")
                return
            }
        }

        // Valid Ketone?
        val ketoneString = binding.etKetone.text.toString()
        if (logKetone && ketoneString.isNotBlank()) {
            try { testValue = ketoneString.toFloat() } catch (e: Exception) {
                userOutputService.showMessageAndWaitForLong("Exception: " + this.getString(R.string.invalid) + " " + this.getString(R.string.ketone) + " " + this.getString(R.string.value) + " $ketoneString")
                return
            }
            editItem.value2 = ketoneString
        }

        editItem.timestamp = timestampCal.timeInMillis
        editItem.value1 = glucoseString
        editItem.value2 = ketoneString
        editItem.comment = binding.etComment.text.toString()

        viewModel.update(editItem)
        requireActivity().onBackPressed()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val vm = MainActivity.getViewModel(GLUCOSE) as GlucoseViewModel?
        if (vm != null) viewModel = vm
        else {
            userOutputService.showMessageAndWaitForLong(getString(R.string.unknownError))
            // Returning here will make sure none of the controls are initialized = no error when e.g. delete is selected
            return
        }

        saveButton = binding.btSave
        deleteButton = binding.btDelete
        super.onViewCreated(view, savedInstanceState)

        val editItem = viewModel.getItem(viewModel.editItem)
        if (editItem == null) {
            // Close keyboard so error is visible
            (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken, 0)
            userOutputService.showMessageAndWaitForLong(getString(R.string.unknownError))
            return
        }

        binding.tvUnit.text = preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_UNIT, getString(R.string.GLUCOSE_UNIT))
        binding.etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(editItem.timestamp)
        binding.etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(editItem.timestamp)
        logKetone = preferences.getBoolean(SettingsActivity.KEY_PREF_LOG_KETONE, getString(R.string.LOG_KETONE_DEFAULT).toBoolean())

        // Check if we are really editing or if this is a new value, if new entry delete tmpComment
        if (viewModel.isTmpItem(editItem.comment))
            binding.etComment.setText("")
        else {
            binding.etGlucose.setText(editItem.value1)
            binding.etKetone.setText(editItem.value2)
            binding.etComment.setText(editItem.comment)
        }

        if (!logKetone) {
            binding.tvKetone.isVisible = false
            binding.etKetone.isVisible = false
            binding.tvKetoneUnit.isVisible = false
            binding.tvGKIText.isVisible = false
            binding.tvGKIValue.isVisible = false
        }

        setDateTimePicker(editItem.timestamp, binding.etDate, binding.etTime)

        usStyle = (binding.tvUnit.text == getString(R.string.GLUCOSE_UNIT_US))
        binding.etGlucose.hint = (if (usStyle) getString(R.string.glucoseHintUS) else getString(R.string.glucoseHint))

        binding.tvGKIValue.text = getGKI(editItem.value1, editItem.value2, usStyle)

        // Make sure first field is highlighted and keyboard is open
        binding.etGlucose.requestFocus()
    }
}
