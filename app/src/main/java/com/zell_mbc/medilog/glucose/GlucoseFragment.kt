package com.zell_mbc.medilog.glucose

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.*
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.data.TabFragment
import com.zell_mbc.medilog.databinding.GlucoseTabBinding
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utility.ListTheme
import com.zell_mbc.medilog.utility.checkThresholds
import java.util.*

class GlucoseFragment : TabFragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: GlucoseTabBinding? = null
    private val binding get() = _binding!!

    private var usStyle = false
    private var glucoseHint = ""

    // String represenation of input values
    private var value1Value = ""
    private var value2Value = ""
    private var commentValue = ""
    private var showComment = false

    private var measureFieldWidthResult = 0.dp
    private var glycoseFieldWidth = 0.dp
    private var ketoneFieldWidth = 0.dp
    private var gkiFieldWidth = 0.dp

    private var logKetone = false

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = GlucoseTabBinding.inflate(inflater,container,false)
        initializeService(binding.root)

        return binding.root
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun addItem() {

        if (!quickEntry) {
            val tmpItem = Data(0, Date().time, MainActivity.tmpComment, viewModel.dataType, "","","", "")
            val itemID = viewModel.insert(tmpItem)
            if (itemID  != 0L) editItem(itemID.toInt())
            else userOutputService.showMessageAndWaitForLong("No entry with tmp value found!")
            return
        }

        // Close keyboard after entry is done
        (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken,0)

        // Check empty variables
        if (value1Value.isEmpty() && value2Value.isEmpty()) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.valuesMissing))
            return
        }
        var glucose = 0f
        if (value1Value.isNotEmpty() ) {
            try { glucose = value1Value.toFloat() } catch (e: Exception) {
                userOutputService.showMessageAndWaitForLong(getString(R.string.exception) + " " + this.getString(
                        R.string.invalid
                    ) + " " + this.getString(R.string.glucose) + " " + this.getString(R.string.value) + " $glucose")
                return
            }
        }

        var ketone = 0f
        if (logKetone && value2Value.isNotEmpty() ) {
            try {ketone = value2Value.toFloat() } catch (e: Exception) {
                userOutputService.showMessageAndWaitForLong(getString(R.string.exception) + " " + this.getString(R.string.invalid) + " " + this.getString(R.string.ketone) + " " + this.getString(R.string.value) + " $ketone")
                return
            }
        }
        val item = Data(0,  Date().time,commentValue, viewModel.dataType, value1Value, value2Value,"","") // Start with empty item

        viewModel.insert(item)

        if ((viewModel.filterEnd > 0L) && (viewModel.filterEnd < item.timestamp)) userOutputService.showMessageAndWaitForDuration(getString(R.string.filteredOut), 4000)

        value1Value = ""
        value2Value = ""
        commentValue = ""
        binding.itemList.setContent { ShowContent() }
    }

    // Measure up Glucose string
    @Composable
    fun MeasureValueString() {
        val sampleText = if (usStyle) "999" else "9.9"
        MeasureTextWidth(sampleText)
        {
            textWidth ->
            Text(text = "", fontSize = fontSize.sp)
            glycoseFieldWidth = textWidth //+ (textWidth / 10) // Add a 10% buffer
        }
        MeasureTextWidth("9.9")
        {
                textWidth ->
            Text(text = "", fontSize = fontSize.sp)
            ketoneFieldWidth = textWidth
            gkiFieldWidth = textWidth
        }
    }

    @Composable
    fun MeasureValueString2(sampleText: String) {
        MeasureTextWidth(sampleText)
        {
                textWidth ->
            Text(text = "", fontSize = fontSize.sp)
            measureFieldWidthResult = textWidth //+ (textWidth / 10) // Add a 10% buffer
        }
    }

    @Composable
    fun ShowRow(item: Data) {
        // Timestamp
        Text(dateFormat.format(item.timestamp) + " - " + timeFormat.format(item.timestamp),
            Modifier
                .padding(end = cellPadding.dp, top = rowPadding.dp, bottom = rowPadding.dp)
                .width(dateColumnWidth),color = MaterialTheme.colors.primary, fontSize = fontSize.sp)

        TabRowDefaults.Divider(color = MaterialTheme.colors.secondary, modifier = Modifier.width(1.dp).fillMaxHeight()) // Vertical separator

        val value1 = try { item.value1.toFloat() } catch (e: NumberFormatException) { 0f }

        // If usStyle cut off trailing .0
        val value1String = if (usStyle) value1.toInt().toString() else value1.toString()
        //var value1String = item.value1
//        if (value1String.isNotEmpty() && usStyle) value1String = value1String.substring(0, value1String.indexOf("."))

        // Glucose
        var textColor = MaterialTheme.colors.primary
        if (highlightValues && ((value1 < lowerThreshold) || (value1 > upperThreshold))) textColor = MaterialTheme.colors.error
        Text(value1String, Modifier.padding(start = cellPadding.dp, end = cellPadding.dp).width(glycoseFieldWidth),textAlign = TextAlign.Center, color = textColor, fontSize = fontSize.sp)

        TabRowDefaults.Divider(color = MaterialTheme.colors.secondary, modifier = Modifier.width(1.dp).fillMaxHeight()) // Vertical separator

        // Ketone
        textColor = MaterialTheme.colors.primary
        if (logKetone) {
//            val value2 = try { item.value2.toInt() } catch (e: NumberFormatException) { 0 }
//            if (highlightValues && ((value2 < lowerThreshold) || (value2 > upperThreshold))) textColor = MaterialTheme.colors.error
            Text(
                item.value2,
                Modifier
                    .padding(start = cellPadding.dp, end = cellPadding.dp)
                    .width(ketoneFieldWidth),
                textAlign = TextAlign.Center,
                color = textColor,
                fontSize = fontSize.sp
            )
            TabRowDefaults.Divider(color = MaterialTheme.colors.secondary, modifier = Modifier
                .width(1.dp)
                .fillMaxHeight()) // Vertical separator

            textColor = MaterialTheme.colors.primary
            Text(
                getGKI(item.value1, item.value2, usStyle),
                Modifier
                    .padding(start = cellPadding.dp, end = cellPadding.dp)
                    .width(gkiFieldWidth),
                textAlign = TextAlign.Center,
                color = textColor,
                fontSize = fontSize.sp
            )
            TabRowDefaults.Divider(
                color = MaterialTheme.colors.secondary, modifier = Modifier
                    .width(1.dp)
                    .fillMaxHeight()
            ) // Vertical separator
        }

        // Comment
        if (item.comment.isNotEmpty()) {
            Spacer(modifier = Modifier.width(cellPadding.dp))
            Text(item.comment,color = MaterialTheme.colors.primary, fontSize = fontSize.sp)
        }
    }

    @OptIn(ExperimentalComposeUiApi::class)
    @Composable
    fun ShowContent() {
        val listState = rememberLazyListState()
        var selection: Data? by remember { mutableStateOf(null)}
        val listItems = viewModel.getItems("DESC",true)

        var value1 by remember { mutableStateOf(value1Value) }
        var value2 by remember { mutableStateOf(value2Value) }
        var commentVisible by remember { mutableStateOf(false) }
        var triggerRefresh by remember { mutableStateOf(false) }
        var comment by remember { mutableStateOf(commentValue) }

        if (value1.isEmpty()) value1Value = ""
        if (value2.isEmpty()) value2Value = ""
        if (commentValue.isEmpty()) comment = ""

        // Data entry fields
        val value1Width = 150.dp
        val value2Width = 150.dp

        ListTheme {
            MeasureDateString()
            MeasureValueString()

          if (quickEntry) {
                Column(modifier = Modifier.fillMaxWidth()) {
                    val (textField1, textField2, textField3) = remember { FocusRequester.createRefs() }
                    Row {

                        TextField(
                            value = value1,
                            colors = TextFieldDefaults.textFieldColors(textColor = MaterialTheme.colors.primary, backgroundColor = androidx.compose.ui.graphics.Color.Transparent),
                            onValueChange = {
                                value1 = it
                                if (usStyle) {
                                    // 3 digit value expected
                                    val l = if (it.length > 0 && it.substring(0,1) == "1") 2 else 1
                                    if (it.length > l)
                                        if (logKetone) textField2.requestFocus() else if (showComment) textField3.requestFocus()
                                }
                                else {
                                    if (it.length > 3)
                                        if (logKetone) textField2.requestFocus() else if (showComment) textField3.requestFocus()
                                }
                            },
                            singleLine = true,
                            keyboardOptions = KeyboardOptions(
                                keyboardType = KeyboardType.Number
                            ),
                            textStyle = TextStyle(),
                            label = {
                                Text(
                                    text = stringResource(id = R.string.glucose),
                                    maxLines = 1,
                                    overflow = TextOverflow.Ellipsis
                                )
                            },
                            placeholder = { Text(text = glucoseHint) },
                            modifier = Modifier
                                .width(value1Width)
                                .focusRequester(textField1)
                                .padding(end = 10.dp),
                            keyboardActions = KeyboardActions(onDone = { addItem() })
                        )
                        if (logKetone) {
                            TextField(
                                value = value2,
                                colors = TextFieldDefaults.textFieldColors(textColor = MaterialTheme.colors.primary, backgroundColor = androidx.compose.ui.graphics.Color.Transparent),
                                onValueChange = {
                                    value2 = it
                                    if (it.length > 2 && commentVisible) textField3.requestFocus()
                                },
                                singleLine = true,
                                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                                textStyle = TextStyle(),
                                label = { Text(text = stringResource(id = R.string.ketone), maxLines = 1, overflow = TextOverflow.Ellipsis)},
                                placeholder = { Text(text = stringResource(id = R.string.ketoneEntryHint) + " " + stringResource(R.string.KETON_UNIT)
                                ) },
                                modifier = Modifier
                                    .width(value2Width)
                                    .focusRequester(textField2)
                                    .padding(end = cellPadding.dp),
                                keyboardActions = KeyboardActions( onDone = { addItem() })
                            )
                        }

                        IconButton(
                            onClick = {
                                commentVisible = !commentVisible
                                triggerRefresh = true },
                            modifier = Modifier.width(commentButtonWidth.dp).padding(top = (2 * cellPadding).dp)
                        ) {
                            Icon(painter = painterResource(R.drawable.ic_outline_comment_24),"Comment", tint = MaterialTheme.colors.primary)
                        }

                        if (triggerRefresh) {
                            this@GlucoseFragment.ShowContent()
                            triggerRefresh = false
                        }

                    } // Row

                    if (commentVisible) {
                        val tf = if (logKetone) textField3 else textField2
                        // Comment
                        TextField(
                            value = comment,
                            colors = TextFieldDefaults.textFieldColors(textColor = MaterialTheme.colors.primary, backgroundColor = androidx.compose.ui.graphics.Color.Transparent),
                            onValueChange = { comment = it },
                            keyboardOptions = KeyboardOptions(
                                keyboardType = KeyboardType.Text,
                                capitalization = KeyboardCapitalization.Sentences
                            ),
                            maxLines = 2,
                            textStyle = TextStyle(),
                            modifier = Modifier.fillMaxWidth().focusRequester(tf),
                            label = { Text(text = stringResource(id = R.string.comment)) },
                            keyboardActions = KeyboardActions(onDone = { addItem() })
                        )

                        // Need an add button because Enter is used for new line
                        binding.btAdd.visibility = View.VISIBLE
                    } else binding.btAdd.visibility = View.GONE // Hide Add Button

                    val tmp = value1
                    if (tmp.isNotEmpty()) {
                        value1Value = try { tmp.toFloat().toString() } catch (e: Exception) { "" }
                    }
                    value2Value = value2
                    commentValue = comment
                }
                topPadding = 80 + if (commentVisible) 65 else 0
            }
            LazyColumn(
                state = listState,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.padding(top = topPadding.dp)
            ) {
                items(listItems) { item ->
                    TabRowDefaults.Divider(color = MaterialTheme.colors.secondary, thickness = 1.dp) // Lines starting from the top
                    Row(modifier = Modifier
                        .fillMaxWidth()
                        .height(IntrinsicSize.Min)
                        .clickable { selection = item },verticalAlignment = Alignment.CenterVertically)
                    {
                        ShowRow(item)
                        if (selection != null) {
                            ItemClicked(selection!!._id)
                            selection = null
                        }
                    }
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        editActivityClass  = GlucoseEditActivity::class.java
        infoActivityClass  = GlucoseInfoActivity::class.java
        chartActivityClass = GlucoseChartActivity::class.java

        viewModel = ViewModelProvider(requireActivity())[GlucoseViewModel::class.java]  // This should return the MainActivity ViewModel
        configureButtons(binding.btAdd,binding.btInfo, binding.btChart)

        val unit = preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_UNIT, getString(R.string.GLUCOSE_UNIT))
        usStyle = (preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_UNIT, getString(R.string.GLUCOSE_UNIT)) == getString(R.string.GLUCOSE_UNIT_US))
        glucoseHint = (if (usStyle) getString(R.string.glucoseHintUS) else getString(R.string.glucoseHint)) + " " + unit

        // -------------------------------
        highlightValues = preferences.getBoolean(SettingsActivity.KEY_PREF_GLUCOSE_HIGHLIGHT_VALUES, getString(R.string.HIGHLIGHT_VALUES_DEFAULT).toBoolean())
        logKetone = preferences.getBoolean(SettingsActivity.KEY_PREF_LOG_KETONE, getString(R.string.LOG_KETONE_DEFAULT).toBoolean())

        val defaultThresholds = if(usStyle) getString(R.string.GLUCOSE_THRESHOLDS_DEFAULT_US) else getString(R.string.GLUCOSE_THRESHOLDS_DEFAULT)
        val s = "" + preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_THRESHOLDS, defaultThresholds)
        val th = checkThresholds(requireContext(), s, defaultThresholds, R.string.glucose)
        lowerThreshold = th[0].toFloat()
        upperThreshold = th[1].toFloat()

        // Preset values in case something was stored in viewModel during onDestroy
        commentValue = viewModel.comment
        value1Value = viewModel.value1


        binding.itemList.setContent {ShowContent() }
    }

    // Save values to survie orientation change
    override fun onDestroy() {
        viewModel.value1 = value1Value
        viewModel.comment = commentValue
        super.onDestroy()
    }
}