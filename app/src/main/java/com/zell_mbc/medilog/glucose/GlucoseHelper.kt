package com.zell_mbc.medilog.glucose

// Calculate glucose Ketone Index
fun getGKI(glucose: String, ketone: String, usStyle: Boolean = false): String {
    // GKI
    var gkiValue: Float
    var gkiString = ""

    if (glucose.isNotEmpty() && ketone.isNotEmpty()) {
        try {
            val gv = glucose.toFloat()
            val kv = ketone.toFloat()
            if (kv > 0 && gv > 0) {
                gkiValue = (gv / kv)
                if (usStyle) gkiValue /= 18
                gkiString = gkiValue.toString().substring(0,3)
            }
        } catch (e: Exception) { }

//        if (gkiValue > 0f) try { gkiString = gkiValue.toString().substring(0,3) } catch (e: Exception) { // Nothing to do, default value "" is save
    }
    return gkiString
}