package com.zell_mbc.medilog.glucose

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.utility.Preferences
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.GLUCOSE
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.databinding.GlucoseInfoformBinding
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import java.math.RoundingMode
import java.text.DateFormat
import java.text.DecimalFormat

class GlucoseInfoFragment : Fragment() {
    private var _binding: GlucoseInfoformBinding? = null
    private val binding get() = _binding!!
    private lateinit var userOutputService : UserOutputService

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = GlucoseInfoformBinding.inflate(inflater, container, false)
        initializeService(binding.root)

        return binding.root
    }
    private fun initializeService(view: View) {
        userOutputService = UserOutputServiceImpl(requireContext(),view)
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private lateinit var viewModel: GlucoseViewModel  //by viewModels() //factoryProducer = { SavedStateViewModelFactory(this, ) })

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(requireActivity())[GlucoseViewModel::class.java]
        viewModel.init(GLUCOSE)

        val count = viewModel.getSize(true)
        var item = viewModel.getLast(true)
        if (item == null) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.noDataToShow))
            return
        }

        binding.tvGlucoseHeader.text = ""// ""BloodSugar"         // Round to 2 digits

        val preferences = Preferences.getSharedPreferences(requireContext())
        val bloodSugarUnit = " " + preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_UNIT, getString(R.string.GLUCOSE_UNIT))
        val usStyle = (preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_UNIT, getString(R.string.GLUCOSE_UNIT)) == getString(R.string.GLUCOSE_UNIT_US))

        var s = if ((viewModel.filterStart + viewModel.filterEnd) == 0L) getString(R.string.measurementsInDB) + " $count"
        else getString(R.string.measurementsInFilter) + " $count"
        binding.tvMeasurementCount.text = s

        s = getString(R.string.max) + " ($bloodSugarUnit)"
        binding.tvMaxHeader.text = s
        val pattern = if (usStyle) "###" else "#.##"
        val df = DecimalFormat(pattern)
        df.roundingMode = RoundingMode.HALF_EVEN

        var timeframe = 7
        var avg = viewModel.getAvgFloat("value1", 7)
        var min = viewModel.getMinValue1( 7)
        var max = viewModel.getMaxValue1( 7)
        var ravg = df.format(avg)
        var minString = df.format(min)
        var maxString = df.format(max)
        s = "  " +getString(R.string.days7) + ":"
        binding.tv7Days.text = s
        binding.tvAvg7Days.text = ravg
        binding.tvMin7Days.text = minString
        binding.tvMax7Days.text = maxString

        avg = viewModel.getAvgFloat("value1",14)
        min = viewModel.getMinValue1( 14)
        max = viewModel.getMaxValue1( 14)
        ravg = df.format(avg)
        minString = df.format(min)
        maxString = df.format(max)
        s = "  " +getString(R.string.days14) + ":"
        binding.tv14Days.text = s
        binding.tvAvg14Days.text = ravg
        binding.tvMin14Days.text = minString
        binding.tvMax14Days.text = maxString

        avg = viewModel.getAvgFloat("value1",21)
        min = viewModel.getMinValue1( 21)
        max = viewModel.getMaxValue1( 21)
        ravg = df.format(avg)
        minString = df.format(min)
        maxString = df.format(max)
        s = "  " +getString(R.string.days21) + ":"
        binding.tv21Days.text = s
        binding.tvAvg21Days.text = ravg
        binding.tvMin21Days.text = minString
        binding.tvMax21Days.text = maxString

        avg = viewModel.getAvgFloat("value1",31)
        min = viewModel.getMinValue1( 31)
        max = viewModel.getMaxValue1( 31)
        ravg = df.format(avg)
        minString = df.format(min)
        maxString = df.format(max)
        s = "  " +getString(R.string.days31) + ":"
        binding.tv31Days.text = s
        binding.tvAvg31Days.text = ravg
        binding.tvMin31Days.text = minString
        binding.tvMax31Days.text = maxString

        avg = viewModel.getAvgFloat("value1",true)
        min = viewModel.getMinValue1(true)
        max = viewModel.getMaxValue1( true)
        ravg = df.format(avg)
        minString = df.format(min)
        maxString = df.format(max)
        s = "  " +getString(R.string.totalLabel) + ":"
        binding.tvTotal.text = s
        binding.tvAvgTotal.text = ravg
        binding.tvMinTotal.text = minString
        binding.tvMaxTotal.text = maxString

        val dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
        val endDate = dateFormat.format(item.timestamp)

        item = viewModel.getFirst(true)
        val startDate = dateFormat.format(item?.timestamp)

        s = getString(R.string.timePeriod) + " $startDate - $endDate"
        binding.tvTimePeriod.text = s
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
        viewModel.deleteTmpItem() // Deletes all items with comment == tmpComment
    }
}
