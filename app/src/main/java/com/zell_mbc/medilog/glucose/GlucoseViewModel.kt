package com.zell_mbc.medilog.glucose

import android.app.Application
import android.graphics.Paint
import android.graphics.pdf.PdfDocument
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.ViewModel
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utility.checkThresholds

class GlucoseViewModel(application: Application): ViewModel(application) {
    override var tabIcon  = R.drawable.ic_glucose
    override val filterStartPref = "GLUCOSEFILTERSTART"
    override val filterEndPref = "GLUCOSEFILTEREND"
    override val filterModePref =  "GLUCOSE_FILTER_MODE"
    override val rollingFilterValuePref = "GLUCOSE_ROLLING_FILTER_VALUE"
    override val rollingFilterTimeframePref = "GLUCOSE_ROLLING_FILTER_TIMEFRAME"

    override var itemName = app.getString(R.string.glucose)
    override var landscape = preferences.getBoolean(SettingsActivity.KEY_PREF_GLUCOSE_LANDSCAPE, app.getString(R.string.GLUCOSE_LANDSCAPE_DEFAULT).toBoolean())
    override var pageSize = preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_PAPER_SIZE, app.getString(R.string.GLUCOSE_PAPER_SIZE_DEFAULT))
    override var showAllTabs = false // Only used for Diary tab

    val logKetone = preferences.getBoolean(SettingsActivity.KEY_PREF_LOG_KETONE, app.getString(R.string.LOG_KETONE_DEFAULT).toBoolean())
    private val warningSign = app.getString(R.string.warningSign)
    private val usStyle = (preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_UNIT, app.getString(R.string.GLUCOSE_UNIT)) == app.getString(R.string.GLUCOSE_UNIT_US))

    private var warningTab = 0f
    private var ketoneTab = 0f
    private var gkiTab = 0f

    override fun createPdfDocument(filtered: Boolean): PdfDocument? {
        if (getSize(filtered) == 0) {
            userOutputService.showAndHideMessageForLong( app.getString(R.string.glucose) + ": " + app.getString(R.string.noDataToExport))
            return null
        }

        super.createPdfDocument(filtered)

        val defaultThresholds = if(usStyle) app.getString(R.string.GLUCOSE_THRESHOLDS_DEFAULT_US) else app.getString(R.string.GLUCOSE_THRESHOLDS_DEFAULT)
        val s = "" + preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_THRESHOLDS, defaultThresholds)
        val th = checkThresholds(app, s, defaultThresholds, R.string.glucose)

        val glucoseLowerThreshold = th[0].toInt()
        val glucoseUpperThreshold = th[1].toInt()

        setColumns(pdfPaintHighlight)
        drawHeader(pdfPaint, pdfPaintHighlight)
        var f = pdfDataTop + pdfLineSpacing
        availableWidth = measureColumn(pdfRightBorder - commentTab)

        for (item in pdfItems) {
            f = checkForNewPage(f)
            val dtString = toStringDate(item.timestamp) + "  " + toStringTime(item.timestamp)
            canvas.drawText(dtString, pdfLeftBorder, f, pdfPaint)

            val value = try { item.value1.toInt() } catch  (e: NumberFormatException) { 0 }
            if (value > glucoseUpperThreshold || value < glucoseLowerThreshold) canvas.drawText(warningSign, warningTab, f, pdfPaint)
            canvas.drawText(item.value1, dataTab, f, pdfPaint)
            if(logKetone) {
                canvas.drawText(item.value2, ketoneTab, f, pdfPaint)
                canvas.drawText(getGKI(item.value1, item.value2, usStyle), gkiTab, f, pdfPaint)
            }
            f = multipleLines(item.comment, f)
        }
        // finish the page
        document.finishPage(page)
        return document
    }

    override fun setColumns(pdfPaintHighlight: Paint) {
        val warningSignWidth = pdfPaintHighlight.measureText(app.getString(R.string.warningSign))
        val glucoseWidth = pdfPaintHighlight.measureText(app.getString(R.string.glucose))
        val ketoneWidth = pdfPaintHighlight.measureText(app.getString(R.string.ketone))
        val gkiWidth = pdfPaintHighlight.measureText(app.getString(R.string.glucoseKetoneIndex))

        warningTab = pdfLeftBorder + dateTabWidth + padding
        dataTab = warningTab + warningSignWidth + space
        ketoneTab = if(logKetone) dataTab + glucoseWidth + space else dataTab
        gkiTab = if(logKetone) ketoneTab + ketoneWidth + space else ketoneTab
        commentTab = gkiTab + gkiWidth + padding
    }

    override fun drawHeader(pdfPaint: Paint, pdfPaintHighlight: Paint) {
        drawHeader(pdfPaint) // VieModel function doesn't need to highlight

        // Data section
        canvas.drawText(app.getString(R.string.date), pdfLeftBorder, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.glucose), dataTab, pdfDataTop, pdfPaintHighlight)
        if (logKetone) {
            canvas.drawText(app.getString(R.string.ketone), ketoneTab, pdfDataTop, pdfPaintHighlight)
            canvas.drawText(app.getString(R.string.glucoseKetoneIndex), gkiTab, pdfDataTop, pdfPaintHighlight)
        }
        canvas.drawText(app.getString(R.string.comment), commentTab + space, pdfDataTop, pdfPaintHighlight)
        canvas.drawLine(warningTab, pdfHeaderBottom, warningTab, pdfDataBottom, pdfPaint)  // Line before weight
        canvas.drawLine(commentTab, pdfHeaderBottom, commentTab, pdfDataBottom, pdfPaint)  // Line before comment
    }
}
