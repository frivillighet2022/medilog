package com.zell_mbc.medilog.oximetry

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.DashPathEffect
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.androidplot.util.PixelUtils
import com.androidplot.xy.*
import com.zell_mbc.medilog.MainActivity.Companion.OXIMETRY
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utility.*
import java.text.*
import java.util.*
import kotlin.math.ceil
import kotlin.math.floor
import kotlin.math.roundToInt


// Chart manual
// https://github.com/halfhp/androidplot/blob/master/docs/xyplot.md
class OximetryChartFragment : Fragment() {
    private var oximetryThreshold = ArrayList<Float>()
    private var oximetrys = ArrayList<Float>()
    private var pulses = ArrayList<Float>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.oximetry_chart, container, false)
    }

    @SuppressLint("SimpleDateFormat")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // create a couple arrays of y-values to plot:
        val labels = ArrayList<String>()
        var wMax = 0f
        var wMin = 1000f

        val preferences = Preferences.getSharedPreferences(requireContext())

        val showGrid = preferences.getBoolean(SettingsActivity.KEY_PREF_OXIMETRY_SHOW_GRID, getString(R.string.OXIMETRY_SHOW_GRID_DEFAULT).toBoolean())
        val isLegendVisible = preferences.getBoolean(SettingsActivity.KEY_PREF_OXIMETRY_SHOW_LEGEND, getString(R.string.OXIMETRY_SHOW_LEGEND_DEFAULT).toBoolean())

        var sTmp: String
        val simpleDate = SimpleDateFormat("MM-dd")
        val lastDate = Calendar.getInstance()
        var currentDate: Date?

        val viewModel = ViewModelProvider(this)[OximetryViewModel::class.java]
        viewModel.init(OXIMETRY)

        val items = viewModel.getItems("ASC", filtered = true)
        for (wi in items) {
            sTmp = simpleDate.format(wi.timestamp)

            labels.add(sTmp)

            val w = try {
                wi.value1.toFloat()
            } catch (e: NumberFormatException) {
                0f
            }
            oximetrys.add(w)

            val f = try {
                wi.value2.toFloat()
            } catch (e: NumberFormatException) {
                0f
            }
            pulses.add(f)
            // Keep min and max values
            if (w > wMax) wMax = w
            if (w < wMin) wMin = w
        }
        if (oximetrys.size == 0) {
            return
        }

        // If threshold is set create dedicated chart, otherwise show as origin
        val threshold = preferences.getBoolean(SettingsActivity.KEY_PREF_OXIMETRY_SHOW_THRESHOLD, getString(R.string.OXIMETRY_SHOW_THRESHOLDS_DEFAULT).toBoolean())
        val thresholdString = preferences.getString(SettingsActivity.KEY_PREF_OXIMETRY_THRESHOLDS, getString(R.string.OXIMETRY_THRESHOLDS_DEFAULT))
        if (threshold && thresholdString != null) {
            val thresholdValue = try { thresholdString.toFloat() } catch (e: NumberFormatException) { 0f }
            for (item in oximetrys) {
                oximetryThreshold.add(thresholdValue)
            }
        }

        // initialize our XYPlot reference:
        // https://github.com/halfhp/androidplot/blob/master/demoapp/src/main/java/com/androidplot/demos/TouchZoomExampleActivity.java
        val plot: XYPlot = view.findViewById(R.id.oximetryPlot)
        PanZoom.attach(plot, PanZoom.Pan.HORIZONTAL, PanZoom.Zoom.STRETCH_HORIZONTAL, PanZoom.ZoomLimit.MIN_TICKS)

        // https://github.com/halfhp/androidplot/blob/master/androidplot-core/src/main/res/values/attrs.xml

        val series1 = SimpleXYSeries(oximetrys, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, getString(R.string.oximetry))
        val series2 = SimpleXYSeries(oximetryThreshold, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, getString(R.string.threshold))

        val backgroundColor = getBackgroundColor(requireContext())
        plot.graph.backgroundPaint.color = backgroundColor

        val fontSizeSmall: Int = getFontSizeSmallInPx(requireContext())

        val axisPaint = Paint()
        axisPaint.style = Paint.Style.FILL_AND_STROKE
        axisPaint.color = getTextColorPrimary(requireContext())
        axisPaint.textSize = fontSizeSmall.toFloat()
        axisPaint.isAntiAlias = true

        val gridPaint = Paint()
        gridPaint.style = Paint.Style.STROKE
        gridPaint.color = getTextColorSecondary(requireContext())
        gridPaint.isAntiAlias = false
        gridPaint.pathEffect = DashPathEffect(floatArrayOf(3f, 2f), 0F)

        // Format Grid
        if (!showGrid) {
            plot.graph.domainGridLinePaint = null
            plot.graph.rangeGridLinePaint = null
        }
        else {
            plot.graph.domainGridLinePaint = gridPaint
            plot.graph.rangeGridLinePaint = gridPaint
        }

        // Format Legend
        plot.legend.isVisible = isLegendVisible
        plot.legend.isDrawIconBackgroundEnabled = false

        // Format Y-Axis / Range
        val chartMin=floor(wMin)
        val chartMax=ceil(wMax)
        plot.outerLimits.set(0, oximetrys.size - 1, chartMin, chartMax) // For pan&zoom
        val diff = (chartMax - chartMin)
        if (diff > 10) {
            val step = floor(diff / 10).toDouble()
            plot.setRangeStep(StepMode.INCREMENT_BY_VAL, step)
        } // Always show 5 sections = 5 y-axis values
        else
            plot.setRangeStep(StepMode.INCREMENT_BY_VAL, 1.0) // Always show 5 sections = 5 y-axis values

//        plot.setUserRangeOrigin(wMin+(diff/2)) // Set to a 10er value <- draws a line not more
        plot.graph.getLineLabelStyle(XYGraphWidget.Edge.LEFT).format = DecimalFormat("###.#") // + oximetryUnit));  // Set integer y-Axis label
        plot.setRangeBoundaries(chartMin, chartMax, BoundaryMode.FIXED)

        // X-Axis ###################
        plot.setDomainStep(StepMode.SUBDIVIDE, 5.0)

        val border = ContextCompat.getColor(requireContext(), R.color.chart_blue_border)
        val fill = ContextCompat.getColor(requireContext(), R.color.chart_blue_fill)
        val series1Format = LineAndPointFormatter(border, null, fill, null)
        plot.addSeries(series1, series1Format)

        if (threshold) {
            val formatThreshold = LineAndPointFormatter(Color.BLUE, null, null, null)
            formatThreshold.linePaint.pathEffect = DashPathEffect(floatArrayOf( // always use DP when specifying pixel sizes, to keep things consistent across devices:
                PixelUtils.dpToPix(20f),
                PixelUtils.dpToPix(15f)), 0f)

            formatThreshold.isLegendIconEnabled = false
            formatThreshold.linePaint.strokeWidth = 2f
            plot.addSeries(series2, formatThreshold)
        }

        // Set X-Axis labels
        plot.graph.getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).format = object : Format() {
            override fun format(obj: Any, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer {
                val i = (obj as Number).toFloat().roundToInt()
                return toAppendTo.append(labels[i])
            }

            override fun parseObject(source: String, pos: ParsePosition): Any {
                return 0
            }
        }
    }
}