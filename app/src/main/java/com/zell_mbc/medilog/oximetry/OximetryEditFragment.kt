package com.zell_mbc.medilog.oximetry

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.OXIMETRY
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.EditFragment
import com.zell_mbc.medilog.databinding.OximetryEditformBinding
import com.zell_mbc.medilog.settings.SettingsActivity
import java.text.DateFormat

class OximetryEditFragment : EditFragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: OximetryEditformBinding? = null
    private val binding get() = _binding!!

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = OximetryEditformBinding.inflate(inflater,container,false)
        initializeService(binding.root)

        return binding.root
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun updateItem() {
        // Close keyboard after entry is done so error messages are visible
        (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken,0)

        val editItem = viewModel.getItem(viewModel.editItem)
        if (editItem == null) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.unknownError))
            return
        }

        // Check empty variables
        if (binding.etOximetry.text.toString().isEmpty() && binding.etPulse.text.toString().isEmpty()) {
            userOutputService.showMessageAndWaitForLong(this.getString(R.string.valuesMissing))
            return
        }

        // Valid oximetry?
        val oximetryString = binding.etOximetry.text.toString()
        var testValue = 0
        if (oximetryString.isNotBlank()) {
            try {testValue = oximetryString.toInt() } catch (e: Exception) {
                userOutputService.showMessageAndWaitForLong("Exception: " + this.getString(R.string.invalid) + " " + this.getString(
                        R.string.oximetry
                    ) + " " + this.getString(R.string.value) + " $oximetryString")
                return
            }
        }

        // Valid Pulse?
        val pulseString = binding.etPulse.text.toString()
        if (pulseString.isNotBlank()) {
            try { testValue = pulseString.toInt() } catch (e: Exception) {
                userOutputService.showMessageAndWaitForLong("Exception: " + this.getString(R.string.invalid) + " " + this.getString(R.string.pulse) + " " + this.getString(R.string.value) + " $pulseString")
                return
            }
            editItem.value2 = pulseString
        }

        editItem.timestamp = timestampCal.timeInMillis
        editItem.value1 = oximetryString
        editItem.value2 = pulseString
        editItem.comment = binding.etComment.text.toString()

        viewModel.update(editItem)
        requireActivity().onBackPressed()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val vm = MainActivity.getViewModel(OXIMETRY) as OximetryViewModel?
        if (vm != null) viewModel = vm
        else {
            userOutputService.showMessageAndWaitForLong(getString(R.string.unknownError))
            // Returning here will make sure none of the controls are initialized = no error when e.g. delete is selected
            return
        }

        saveButton = binding.btSave
        deleteButton = binding.btDelete
        super.onViewCreated(view, savedInstanceState)

        val editItem = viewModel.getItem(viewModel.editItem)
        if (editItem == null) {
            // Close keyboard so error is visible
            (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken, 0)
            userOutputService.showMessageAndWaitForLong(getString(R.string.unknownError))
            return
        }

        binding.tvUnit.text = preferences.getString(SettingsActivity.KEY_PREF_OXIMETRY_UNIT, getString(R.string.OXIMETRY_UNIT))
        binding.etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(editItem.timestamp)
        binding.etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(editItem.timestamp)

        // Check if we are really editing or if this is a new value, if new entry delete tmpComment
        if (viewModel.isTmpItem(editItem.comment))
            binding.etComment.setText("")
        else {
            binding.etOximetry.setText(editItem.value1)
            binding.etPulse.setText(editItem.value2)
            binding.etComment.setText(editItem.comment)
        }

        setDateTimePicker(editItem.timestamp, binding.etDate, binding.etTime)

        binding.etOximetry.hint = getString(R.string.oximetryHint)

        // Make sure first field is highlighted and keyboard is open
        binding.etOximetry.requestFocus()
    }
}
