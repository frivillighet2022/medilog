package com.zell_mbc.medilog.oximetry

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.*
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.data.TabFragment
import com.zell_mbc.medilog.databinding.OximetryTabBinding
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utility.ListTheme
import java.util.*

class OximetryFragment : TabFragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: OximetryTabBinding? = null
    private val binding get() = _binding!!

    private var oximetryHint = ""

    // String represenation of input values
    private var value1Value = ""
    private var value2Value = ""
    private var commentValue = ""
    private var oximetryFieldWidth = 0.dp
    private var pulseFieldWidth = 0.dp

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = OximetryTabBinding.inflate(inflater,container,false)
        initializeService(binding.root)

        return binding.root
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun addItem() {

        if (!quickEntry) {
            val tmpItem = Data(0, Date().time, MainActivity.tmpComment, viewModel.dataType, "","","", "")
            val itemID = viewModel.insert(tmpItem)
            if (itemID  != 0L) editItem(itemID.toInt())
            else userOutputService.showMessageAndWaitForLong("No entry with tmp value found!")
            return
        }

        // Close keyboard after entry is done
        (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken,0)

        // Check empty variables
        if (value1Value.isEmpty() && value2Value.isEmpty()) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.valuesMissing))
            return
        }
        var oximetry = 0
        if (value1Value.isNotEmpty() ) {
            try { oximetry = value1Value.toInt() } catch (e: Exception) {
                userOutputService.showMessageAndWaitForLong(getString(R.string.exception) + " " + this.getString(
                        R.string.invalid
                    ) + " " + this.getString(R.string.oximetry) + " " + this.getString(R.string.value) + " $oximetry")
                return
            }
        }

        var pulse = 0
        if (value2Value.isNotEmpty())  {
            try {pulse = value2Value.toInt() } catch (e: Exception) {
                userOutputService.showMessageAndWaitForLong(getString(R.string.exception) + " " + this.getString(R.string.invalid) + " " + this.getString(R.string.pulse) + " " + this.getString(R.string.value) + " $pulse")
                return
            }
        }
        val item = Data(0,  Date().time,commentValue, viewModel.dataType, value1Value, value2Value,"","") // Start with empty item

        viewModel.insert(item)

        if ((viewModel.filterEnd > 0L) && (viewModel.filterEnd < item.timestamp)) userOutputService.showMessageAndWaitForDuration(getString(R.string.filteredOut), 4000)

        value1Value = ""
        value2Value = ""
        commentValue = ""
        binding.itemList.setContent { ShowContent() }
    }

    // Measure up Oximetry string
    @Composable
    fun MeasureValueString() {
        val sampleText = "99"
        MeasureTextWidth(sampleText)
        {
            textWidth ->
            Text(text = "", fontSize = fontSize.sp)
            oximetryFieldWidth = textWidth //+ (textWidth / 10) // Add a 10% buffer
        }
        MeasureTextWidth("999")
        {
            textWidth ->
            Text(text = "", fontSize = fontSize.sp)
            pulseFieldWidth = textWidth
        }
    }

    @Composable
    fun ShowRow(item: Data) {
        // Timestamp
        Text(dateFormat.format(item.timestamp) + " - " + timeFormat.format(item.timestamp),
            Modifier
                .padding(end = cellPadding.dp, top = rowPadding.dp, bottom = rowPadding.dp)
                .width(dateColumnWidth),color = MaterialTheme.colors.primary, fontSize = fontSize.sp)

        TabRowDefaults.Divider(color = MaterialTheme.colors.secondary, modifier = Modifier.width(1.dp).fillMaxHeight()) // Vertical separator

        val value1 = try { item.value1.toInt() } catch (e: NumberFormatException) { 0 }

        // If usStyle cut off trailing .0
        val value1String = value1.toString()

        // Oximetry
        var textColor = MaterialTheme.colors.primary
        if (highlightValues && (value1 < lowerThreshold)) textColor = MaterialTheme.colors.error
        Text(value1String, Modifier.padding(start = cellPadding.dp, end = cellPadding.dp).width(oximetryFieldWidth),textAlign = TextAlign.Center, color = textColor, fontSize = fontSize.sp)

        TabRowDefaults.Divider(color = MaterialTheme.colors.secondary, modifier = Modifier.width(1.dp).fillMaxHeight()) // Vertical separator

        // Pulse
        textColor = MaterialTheme.colors.primary
        Text(
            item.value2,
            Modifier
                .padding(start = cellPadding.dp, end = cellPadding.dp)
                .width(pulseFieldWidth),
            textAlign = TextAlign.Center,
            color = textColor,
            fontSize = fontSize.sp
        )
        TabRowDefaults.Divider(color = MaterialTheme.colors.secondary, modifier = Modifier
            .width(1.dp)
            .fillMaxHeight()) // Vertical separator

        // Comment
        if (item.comment.isNotEmpty()) {
            Spacer(modifier = Modifier.width(cellPadding.dp))
            Text(item.comment,color = MaterialTheme.colors.primary, fontSize = fontSize.sp)
        }
    }

    @OptIn(ExperimentalComposeUiApi::class)
    @Composable
    fun ShowContent() {
        val listState = rememberLazyListState()
        var selection: Data? by remember { mutableStateOf(null)}
        val listItems = viewModel.getItems("DESC",true)

        var value1 by remember { mutableStateOf(value1Value) }
        var value2 by remember { mutableStateOf(value2Value) }
        var commentVisible by remember { mutableStateOf(false) }
        var triggerRefresh by remember { mutableStateOf(false) }
        var comment by remember { mutableStateOf(commentValue) }

        if (value1.isEmpty()) value1Value = ""
        if (value2.isEmpty()) value2Value = ""
        if (commentValue.isEmpty()) comment = ""

        // Data entry fields
        val value1Width = 150.dp
        val value2Width = 150.dp

        ListTheme {
            MeasureDateString()
            MeasureValueString()

            if (quickEntry) {
                Column(modifier = Modifier.fillMaxWidth()) {
                    val (textField1, textField2, textField3) = remember { FocusRequester.createRefs() }
                    Row {

                        TextField(
                            value = value1,
                            colors = TextFieldDefaults.textFieldColors(textColor = MaterialTheme.colors.primary, backgroundColor = androidx.compose.ui.graphics.Color.Transparent),
                            onValueChange = {
                                value1 = it
                                if (it.length > 1) textField2.requestFocus()
                            },
                            singleLine = true,
                            keyboardOptions = KeyboardOptions(
                                keyboardType = KeyboardType.Number
                            ),
                            textStyle = TextStyle(),
                            label = {
                                Text(
                                    text = stringResource(id = R.string.oximetry),
                                    maxLines = 1,
                                    overflow = TextOverflow.Ellipsis
                                )
                            },
                            placeholder = { Text(text = oximetryHint) },
                            modifier = Modifier
                                .width(value1Width)
                                .focusRequester(textField1)
                                .padding(end = 10.dp),
                            keyboardActions = KeyboardActions(onDone = { addItem() })
                        )
                        TextField(
                            value = value2,
                            colors = TextFieldDefaults.textFieldColors(textColor = MaterialTheme.colors.primary, backgroundColor = androidx.compose.ui.graphics.Color.Transparent),
                            onValueChange = {
                                value2 = it
                                if (it.length > 2 && commentVisible) textField3.requestFocus()
                            },
                            singleLine = true,
                            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                            textStyle = TextStyle(),
                            label = { Text(text = stringResource(id = R.string.pulse), maxLines = 1, overflow = TextOverflow.Ellipsis)},
                            placeholder = { Text(text = stringResource(id = R.string.PULSE_HINT) + " " + stringResource(R.string.pulseUnit)
                            ) },
                            modifier = Modifier
                                .width(value2Width)
                                .focusRequester(textField2)
                                .padding(end = cellPadding.dp),
                            keyboardActions = KeyboardActions( onDone = { addItem() })
                        )

                        IconButton(
                            onClick = {
                                commentVisible = !commentVisible
                                triggerRefresh = true },
                            modifier = Modifier.width(commentButtonWidth.dp).padding(top = (2 * cellPadding).dp)
                        ) {
                            Icon(painter = painterResource(R.drawable.ic_outline_comment_24),"Comment", tint = MaterialTheme.colors.primary)
                        }

                        if (triggerRefresh) {
                            this@OximetryFragment.ShowContent()
                            triggerRefresh = false
                        }

                    } // Row

                    if (commentVisible) {
                        // Comment
                        TextField(
                            value = comment,
                            colors = TextFieldDefaults.textFieldColors(textColor = MaterialTheme.colors.primary, backgroundColor = androidx.compose.ui.graphics.Color.Transparent),
                            onValueChange = { comment = it },
                            keyboardOptions = KeyboardOptions(
                                keyboardType = KeyboardType.Text,
                                capitalization = KeyboardCapitalization.Sentences
                            ),
                            maxLines = 2,
                            textStyle = TextStyle(),
                            modifier = Modifier.fillMaxWidth().focusRequester(textField3),
                            label = { Text(text = stringResource(id = R.string.comment)) },
                            keyboardActions = KeyboardActions(onDone = { addItem() })
                        )

                        // Need an add button because Enter is used for new line
                        binding.btAdd.visibility = View.VISIBLE
                    } else binding.btAdd.visibility = View.GONE // Hide Add Button

                    val tmp = value1
                    if (tmp.isNotEmpty()) { value1Value = try { tmp.toInt().toString() } catch (e: Exception) { "" }
                    }
                    value2Value = value2
                    commentValue = comment
                }
                topPadding = 80 + if (commentVisible) 65 else 0
            }
            LazyColumn(
                state = listState,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.padding(top = topPadding.dp)
            ) {
                items(listItems) { item ->
                    TabRowDefaults.Divider(color = MaterialTheme.colors.secondary, thickness = 1.dp) // Lines starting from the top
                    Row(modifier = Modifier
                        .fillMaxWidth()
                        .height(IntrinsicSize.Min)
                        .clickable { selection = item },verticalAlignment = Alignment.CenterVertically)
                    {
                        ShowRow(item)
                        if (selection != null) {
                            ItemClicked(selection!!._id)
                            selection = null
                        }
                    }
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        editActivityClass  = OximetryEditActivity::class.java
        infoActivityClass  = OximetryInfoActivity::class.java
        chartActivityClass = OximetryChartActivity::class.java

        viewModel = ViewModelProvider(requireActivity())[OximetryViewModel::class.java]  // This should return the MainActivity ViewModel
        configureButtons(binding.btAdd,binding.btInfo, binding.btChart)

        val unit = preferences.getString(SettingsActivity.KEY_PREF_OXIMETRY_UNIT, getString(R.string.OXIMETRY_UNIT))
        oximetryHint = getString(R.string.oximetryHint) + " " + unit

        // -------------------------------
        highlightValues = preferences.getBoolean(SettingsActivity.KEY_PREF_OXIMETRY_HIGHLIGHT_VALUES, getString(R.string.OXIMETRY_HIGHLIGHT_VALUES_DEFAULT).toBoolean())
        val thresholdString = preferences.getString(SettingsActivity.KEY_PREF_OXIMETRY_THRESHOLDS, getString(R.string.OXIMETRY_THRESHOLDS_DEFAULT))
        if (highlightValues && thresholdString != null)
            lowerThreshold = try { thresholdString.toFloat() } catch (e: NumberFormatException) { 0f }

        // Preset values in case something was stored in viewModel during onDestroy
        commentValue = viewModel.comment
        value1Value = viewModel.value1


        binding.itemList.setContent {ShowContent() }
    }

    // Save values to survie orientation change
    override fun onDestroy() {
        viewModel.value1 = value1Value
        viewModel.comment = commentValue
        super.onDestroy()
    }
}
