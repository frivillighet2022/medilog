package com.zell_mbc.medilog.oximetry

import android.app.Application
import android.graphics.Paint
import android.graphics.pdf.PdfDocument
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.ViewModel
import com.zell_mbc.medilog.settings.SettingsActivity

class OximetryViewModel(application: Application): ViewModel(application) {
    override var tabIcon  = R.drawable.ic_oximetry
    override val filterStartPref = "OXIMETRYFILTERSTART"
    override val filterEndPref = "OXIMETRYFILTEREND"
    override val filterModePref =  "OXIMETRY_FILTER_MODE"
    override val rollingFilterValuePref = "OXIMETRY_ROLLING_FILTER_VALUE"
    override val rollingFilterTimeframePref = "OXIMETRY_ROLLING_FILTER_TIMEFRAME"

    override var itemName = app.getString(R.string.oximetry)
    override var landscape = preferences.getBoolean(SettingsActivity.KEY_PREF_OXIMETRY_LANDSCAPE, app.getString(R.string.OXIMETRY_LANDSCAPE_DEFAULT).toBoolean())
    override var pageSize = preferences.getString(SettingsActivity.KEY_PREF_OXIMETRY_PAPER_SIZE, app.getString(R.string.OXIMETRY_PAPER_SIZE_DEFAULT))
    override var showAllTabs = false // Only used for Diary tab

    private val warningSign = app.getString(R.string.warningSign)

    private var warningTab = 0f
    private var pulseTab = 0f

    override fun createPdfDocument(filtered: Boolean): PdfDocument? {
        if (getSize(filtered) == 0) {
            userOutputService.showAndHideMessageForLong( app.getString(R.string.oximetry) + ": " + app.getString(R.string.noDataToExport))
            return null
        }

        super.createPdfDocument(filtered)

        highlightValues = preferences.getBoolean(SettingsActivity.KEY_PREF_OXIMETRY_HIGHLIGHT_VALUES, app.getString(R.string.OXIMETRY_HIGHLIGHT_VALUES_DEFAULT).toBoolean())
        val thresholdString = preferences.getString(SettingsActivity.KEY_PREF_OXIMETRY_THRESHOLDS, app.getString(R.string.OXIMETRY_THRESHOLDS_DEFAULT))
        var oximetryLowerThreshold = 0
        if (highlightValues && thresholdString != null)
            oximetryLowerThreshold = try { thresholdString.toInt() } catch (e: NumberFormatException) { 0 }

        setColumns(pdfPaintHighlight)
        drawHeader(pdfPaint, pdfPaintHighlight)
        var f = pdfDataTop + pdfLineSpacing
        availableWidth = measureColumn(pdfRightBorder - commentTab)

        for (item in pdfItems) {
            f = checkForNewPage(f)
            val dtString = toStringDate(item.timestamp) + "  " + toStringTime(item.timestamp)
            canvas.drawText(dtString, pdfLeftBorder, f, pdfPaint)

            val value = try { item.value1.toInt() } catch  (e: NumberFormatException) { 0 }
            if (value < oximetryLowerThreshold)
                canvas.drawText(warningSign, warningTab, f, pdfPaint)
            canvas.drawText(item.value1, dataTab, f, pdfPaint)
            canvas.drawText(item.value2, pulseTab, f, pdfPaint)
            f = multipleLines(item.comment, f)
        }
        // finish the page
        document.finishPage(page)
        return document
    }

    override fun setColumns(pdfPaintHighlight: Paint) {
        val warningSignWidth = pdfPaintHighlight.measureText(app.getString(R.string.warningSign))
        val oximetryWidth = pdfPaintHighlight.measureText(app.getString(R.string.oximetry))
        val pulseWidth = pdfPaintHighlight.measureText(app.getString(R.string.pulse))

        warningTab = pdfLeftBorder + dateTabWidth + padding
        dataTab = warningTab + warningSignWidth + space
        pulseTab = dataTab + oximetryWidth + space
        commentTab = pulseTab + pulseWidth + padding
    }

    override fun drawHeader(pdfPaint: Paint, pdfPaintHighlight: Paint) {
        drawHeader(pdfPaint) // VieModel function doesn't need to highlight

        // Data section
        canvas.drawText(app.getString(R.string.date), pdfLeftBorder, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.oximetry), dataTab, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.pulse), pulseTab, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.comment), commentTab + space, pdfDataTop, pdfPaintHighlight)
        canvas.drawLine(warningTab, pdfHeaderBottom, warningTab, pdfDataBottom, pdfPaint)  // Line before weight
        canvas.drawLine(commentTab, pdfHeaderBottom, commentTab, pdfDataBottom, pdfPaint)  // Line before comment
    }
}
