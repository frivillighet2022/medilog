package com.zell_mbc.medilog.settings

import android.os.Bundle
import androidx.preference.Preference
import com.takisoft.preferencex.PreferenceFragmentCompat
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.utility.Preferences
import java.text.DateFormat

class BackupSettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferencesFix(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.backup_preferences, rootKey)

        val lastBackup = preferenceManager.findPreference<Preference>(SettingsActivity.KEY_PREF_LAST_BACKUP)
        val preferences = Preferences.getSharedPreferences(requireContext())
        if (lastBackup != null) {
            var status = getString(R.string.unknown)
            val timestamp = preferences.getLong("LAST_BACKUP", 0L)
            if (timestamp > 0) {
                val dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
                val timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT)
                status = dateFormat.format(timestamp) + " " + timeFormat.format(timestamp)
            }
            status = getString(R.string.lastBackup) + " " + status
            lastBackup.title = status
        }

        val backupUri = preferenceManager.findPreference<Preference>(SettingsActivity.KEY_PREF_BACKUP_URI)
        if (backupUri != null) {
            val uriString = preferences.getString(SettingsActivity.KEY_PREF_BACKUP_URI, "")
            backupUri.summary = uriString
        }
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
    }

}