package com.zell_mbc.medilog.settings

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.text.InputFilter
import android.text.Spanned
import androidx.preference.Preference
import com.takisoft.preferencex.EditTextPreference
import com.takisoft.preferencex.PreferenceFragmentCompat
import com.zell_mbc.medilog.BiometricHelper
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.MAX_TEXT_SIZE
import com.zell_mbc.medilog.MainActivity.Companion.MIN_TEXT_SIZE
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl

class GeneralSettingsFragment : PreferenceFragmentCompat() {
    lateinit var userOutputService : UserOutputService

    private fun initializeService(context: Context) {
        userOutputService = UserOutputServiceImpl(context, requireView())
    }

    override fun onCreatePreferencesFix(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.general_preferences, rootKey)

        class DelimiterInputFilter(validDelimiters: String): InputFilter {
            private var filterString = ""
            private var filterHint = ""

            init{
                this.filterString = validDelimiters
                for (c in validDelimiters) this.filterHint = this.filterHint + c + " " // Add blanks for better visibility
            }

            override fun filter(source: CharSequence, start: Int, end: Int, dest: Spanned, dstart: Int, dend: Int): CharSequence? {
                try
                {
                    initializeService(requireContext())
                    val input = (dest.subSequence(0, dstart).toString() + source + dest.subSequence(dend, dest.length))
                    if (filterString.indexOf(input) >= 0) return null
                    else
                        if (input.isNotEmpty()) userOutputService.showMessageAndWaitForLong(requireContext().getString(R.string.allowedDelimiters) + " $filterHint")
                }
                catch (nfe: NumberFormatException) { return "" }  // Define abstract function
                return ""
            }
        }

        val editTextPreference = preferenceManager.findPreference<EditTextPreference>("delimiter")
        if (editTextPreference != null) {
            editTextPreference.setOnBindEditTextListener { editText ->
                val filterArray = arrayOfNulls<InputFilter>(2)
                filterArray[0] = InputFilter.LengthFilter(1)
                filterArray[1] = DelimiterInputFilter(this.getString(R.string.csvSeparators))
                editText.filters = filterArray
            }

            Preference.OnPreferenceChangeListener { _, newValue ->
                var rtnval = true
                if (newValue.toString().isEmpty()) {
                    val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
                    builder.setTitle(getString(R.string.invalidInput))
                    builder.setMessage(getString(R.string.emptySeparator))
                    builder.setPositiveButton(android.R.string.ok, null)
                    builder.show()
                    rtnval = false
                }
                rtnval
            }.also { editTextPreference.onPreferenceChangeListener = it }
        }

        val textSize = preferenceManager.findPreference<EditTextPreference>(SettingsActivity.KEY_PREF_TEXT_SIZE)
        if (textSize != null)
        {
            textSize.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
                initializeService(this.requireContext())

                val `val` = newValue.toString().toInt()
                if (`val` < MIN_TEXT_SIZE.toInt() || `val` > MAX_TEXT_SIZE.toInt()) {
                    // invalid you can show invalid message
                    //userOutputService.showMessageAndWaitForLong(requireContext().getString(R.string.invalid) + " " + requireContext().getString(R.string.value) + " " + requireContext().getString(R.string.chooseValue) + " $MIN_TEXT_SIZE " + requireContext().getString(R.string.and) + " $MAX_TEXT_SIZE")
                    userOutputService.showMessageAndWaitForLong(requireContext().getString(R.string.invalidTextSize, MIN_TEXT_SIZE, MAX_TEXT_SIZE))
                    false
                } else {
                    true
                }
            }
        }

        // Check if biometric device  exists, if not remove biometric setting in settings activity and set authenticated to always true
        val biometricHelper = BiometricHelper(requireContext())
        val canAuthenticate = biometricHelper.canAuthenticate(false)
        if (canAuthenticate != 0) {
            val p1: Preference? = findPreference(SettingsActivity.KEY_PREF_BIOMETRIC)
            if (p1 != null) {p1.isEnabled = false }
            val p2: Preference? = findPreference(SettingsActivity.KEY_PREF_FORCE_REAUTHENTICATION)
            if (p2 != null) {p2.isEnabled = false }
        }

    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
    }

}