package com.zell_mbc.medilog.settings

import android.os.Bundle
import androidx.preference.Preference
import com.takisoft.preferencex.PreferenceFragmentCompat
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.utility.Preferences

//import com.takisoft.preferencex.PreferenceFragmentCompat

class GlucoseSettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferencesFix(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.glucose_preferences, rootKey)

        val preferences = Preferences.getSharedPreferences(requireContext())
        val thresholdObject = preferenceManager.findPreference<Preference>(SettingsActivity.KEY_PREF_GLUCOSE_THRESHOLDS)
        if (thresholdObject != null) {
// todo
       //     val thresholdValue = preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_THRESHOLDS, "")
       //     thresholdObject.summary = getString(R.string.thresholdSummary,thresholdValue)
        }
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
    }

}
