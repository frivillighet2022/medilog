package com.zell_mbc.medilog.settings

import android.os.Bundle
import androidx.preference.Preference
import com.takisoft.preferencex.PreferenceFragmentCompat
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.utility.Preferences

//import com.takisoft.preferencex.PreferenceFragmentCompat

class OximetrySettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferencesFix(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.oximetry_preferences, rootKey)

        val preferences = Preferences.getSharedPreferences(requireContext())
        val thresholdObject = preferenceManager.findPreference<Preference>(SettingsActivity.KEY_PREF_OXIMETRY_THRESHOLDS)
        if (thresholdObject != null) thresholdObject.summary = getString(R.string.thresholdSummaryLower) //,thresholdValue)
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
    }

}
