package com.zell_mbc.medilog.settings

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.utility.Preferences

class SettingsActivity : AppCompatActivity(), PreferenceFragmentCompat.OnPreferenceStartFragmentCallback {

        override fun onPreferenceStartFragment(caller: PreferenceFragmentCompat, pref: Preference): Boolean {
            // Instantiate the new Fragment
            val args = pref.extras
            val fragment = supportFragmentManager.fragmentFactory.instantiate(
                    classLoader,
                    pref.fragment.toString()
            )
            fragment.arguments = args
            fragment.setTargetFragment(caller, 0)
            // Replace the existing Fragment with the new Fragment
            supportFragmentManager.beginTransaction()
                    .replace(android.R.id.content, fragment)
                    .addToBackStack(null)
                    .commit()
            return true
        }

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            MainActivity.setTheme(this)

            val preferences = Preferences.getSharedPreferences(this)
            val threshold = "" + preferences.getString(KEY_PREF_TEMPERATURE_THRESHOLDS,this.getString(R.string.TEMPERATURE_THRESHOLDS_DEFAULT))
                if (threshold.isEmpty()) {
                    val editor = preferences.edit()
                    editor.putString(KEY_PREF_TEMPERATURE_THRESHOLDS, this.getString(R.string.TEMPERATURE_THRESHOLDS_DEFAULT))
                    editor.apply()
                }
            supportFragmentManager.beginTransaction().replace(android.R.id.content, SettingsFragment()).commit()
        }


    companion object {
        const val KEY_PREF_DELIMITER = "delimiter"

        const val KEY_PREF_TEMPERATURE_UNIT              = "etTemperatureUnit"
        const val KEY_PREF_TEMPERATURE_THRESHOLDS        = "etTemperatureThresholds"
        const val KEY_PREF_TEMPERATURE_SHOW_THRESHOLDS   = "cbShowTemperatureThreshold"
        const val KEY_PREF_TEMPERATURE_SHOW_GRID         = "cbShowTemperatureGrid"
        const val KEY_PREF_TEMPERATURE_SHOW_LEGEND       = "cbShowTemperatureLegend"
        const val KEY_PREF_TEMPERATURE_LANDSCAPE         = "cbTemperatureLandscape"
        const val KEY_PREF_TEMPERATURE_PAPER_SIZE        = "liTemperaturePaperSize"
        const val KEY_PREF_TEMPERATURE_HIGHLIGHT_VALUES  = "cbTemperatureHighlightValues"

        const val KEY_PREF_GLUCOSE_SHOW_THRESHOLD    = "cbShowGlucoseThreshold"
        const val KEY_PREF_GLUCOSE_UNIT              = "lpGlucoseUnit"
        const val KEY_PREF_GLUCOSE_THRESHOLDS        = "etGlucoseThresholds"
        const val KEY_PREF_GLUCOSE_SHOW_GRID         = "cbShowGlucoseGrid"
        const val KEY_PREF_GLUCOSE_SHOW_LEGEND       = "cbShowGlucoseLegend"
        const val KEY_PREF_GLUCOSE_LANDSCAPE         = "cbGlucoseLandscape"
        const val KEY_PREF_GLUCOSE_PAPER_SIZE        = "liGlucosePaperSize"
        const val KEY_PREF_LOG_KETONE               = "cbLogKetone"
        const val KEY_PREF_GLUCOSE_HIGHLIGHT_VALUES = "cbGlucoseHighlightValues"

        const val KEY_PREF_OXIMETRY_SHOW_THRESHOLD    = "cbShowOximetryThreshold"
        const val KEY_PREF_OXIMETRY_UNIT              = "etOximetryUnit"
        const val KEY_PREF_OXIMETRY_THRESHOLDS        = "etOximetryThresholds"
        const val KEY_PREF_OXIMETRY_SHOW_GRID         = "cbShowOximetryGrid"
        const val KEY_PREF_OXIMETRY_SHOW_LEGEND       = "cbShowOximetryLegend"
        const val KEY_PREF_OXIMETRY_LANDSCAPE         = "cbOximetryLandscape"
        const val KEY_PREF_OXIMETRY_PAPER_SIZE        = "liOximetryPaperSize"
        const val KEY_PREF_OXIMETRY_HIGHLIGHT_VALUES  = "cbOximetryHighlightValues"

        const val KEY_PREF_WEIGHT_HIGHLIGHT_VALUES    = "cbWeightHighlightValues"
        const val KEY_PREF_WEIGHT_UNIT                = "etWeightUnit"
        const val KEY_PREF_BODY_HEIGHT                = "etHeight"
        const val KEY_PREF_WEIGHT_THRESHOLD           = "etWeightThreshold"
        const val KEY_PREF_LOG_FAT                    = "cbLogBodyFat"
        const val KEY_PREF_FAT_MIN_MAX                = "etFatMinMax"
        const val KEY_PREF_SHOW_WEIGHT_THRESHOLD = "cbShowWeightThreshold"
        const val KEY_PREF_SHOW_WEIGHT_GRID     = "cbShowWeightGrid"
        const val KEY_PREF_SHOW_WEIGHT_LEGEND   = "cbShowWeightLegend"
        const val KEY_PREF_WEIGHT_LINEAR_TRENDLINE = "cbWeightLinearTrendline"
        const val KEY_PREF_WEIGHT_MOVING_AVERAGE_TRENDLINE = "cbWeightMovingAverageTrendline"
        const val KEY_PREF_WEIGHT_MOVING_AVERAGE_SIZE = "etWeightMovingAverageSize"
        const val KEY_PREF_WEIGHT_DAY_STEPPING = "cbWeightDayStepping"
        const val KEY_PREF_WEIGHT_BAR_CHART = "cbWeightBarChart"
        const val KEY_PREF_WEIGHT_LANDSCAPE = "cbWeightLandscape"
        const val KEY_PREF_WEIGHT_PAPER_SIZE = "liWeightPaperSize"
        const val KEY_PREF_WEIGHT_TARE = "etTare"
        const val KEY_PREF_WEIGHT_SHOW_VALUES = "cbWeightShowValues"

        const val KEY_PREF_WATER_THRESHOLD     = "evWaterThreshold"
        const val KEY_PREF_WATER_UNIT          = "evWaterUnit"
        const val KEY_PREF_SUMMARYPDF         = "swShowSummaryinPDF"
        const val KEY_PREF_WATER_BAR_CHART      = "cbWaterBarChart"
        const val KEY_PREF_SHOW_WATER_THRESHOLD = "cbShowWaterThreshold"
        const val KEY_PREF_SHOW_WATER_GRID      = "cbShowWaterGrid"
        const val KEY_PREF_showWaterLegend      = "cbShowWaterLegend"
        const val KEY_PREF_WATER_LANDSCAPE      = "cbWaterLandscape"
        const val KEY_PREF_WATER_PAPER_SIZE     = "liWaterPaperSize"

        const val KEY_PREF_BLOODPRESSURE_HIGHLIGHT_VALUES = "cbBloodPressureHighlightValues"
        const val KEY_PREF_BLOODPRESSURE_UNIT         = "etBloodPressureUnit"
        const val KEY_PREF_LOG_HEART_RHYTHM           = "cbLogHeartRhythm"
        const val KEY_PREF_showBloodPressureThreshold = "cbShowBloodPressureThreshold"
        const val KEY_PREF_showBloodPressureGrid      = "cbShowBloodPressureGrid"
        const val KEY_PREF_showBloodPressureLegend    = "cbShowBloodPressureLegend"
        const val KEY_PREF_bpBarChart                 = "cbBloodPressureBarChart"
        const val KEY_PREF_bpDayStepping = "cbBloodPressureDayStepping"
        const val KEY_PREF_SHOWPULSE = "cbShowPulse"
        const val KEY_PREF_hyperGrade3 = "grade3"
        const val KEY_PREF_hyperGrade2 = "grade2"
        const val KEY_PREF_hyperGrade1 = "grade1"
        const val KEY_PREF_HYPOTENSION = "etHypotension"
        const val KEY_PREF_BLOODPRESSURE_SHOW_VALUES = "cbBloodPressureShowValues"
        const val KEY_PREF_BLOODPRESSURE_LINEAR_TRENDLINE = "cbBloodPressureLinearTrendline"
        const val KEY_PREF_BLOODPRESSURE_MOVING_AVERAGE_TRENDLINE = "cbBloodPressureMovingAverageTrendline"
        const val KEY_PREF_BLOODPRESSURE_MOVING_AVERAGE_SIZE = "etBloodPressureMovingAverageSize"
        const val KEY_PREF_BLOODPRESSURE_PAPER_SIZE = "liBloodPressurePaperSize"
        const val KEY_PREF_BLOODPRESSURE_LANDSCAPE = "cbBloodPressureLandscape"
        const val KEY_PREF_BLOODPRESSURE_TIMEZONES = "etTimezones"

        const val KEY_PREF_DIARY_PAPER_SIZE        = "liDiaryPaperSize"
        const val KEY_PREF_DIARY_LANDSCAPE         = "cbDiaryLandscape"
        const val KEY_PREF_DIARY_DIARY_SHOWALLTABS = "cbShowAllTabs"

        const val KEY_PREF_BACKUP_WARNING  = "etBackupWarning"
        const val KEY_PREF_AUTO_BACKUP     = "etAutoBackup"
        const val KEY_PREF_LAST_BACKUP     = "tvLastBackup"
        const val KEY_PREF_BACKUP_URI      = "tvBackupUri"
        const val KEY_PREF_ADD_TIMESTAMP   = "cbAddTimestamp"

        const val KEY_PREF_ACTIVE_TABS_SET      = "liActiveTabs"
        const val KEY_PREF_SHOWTABICON          = "showTabIcon"
        const val KEY_PREF_SHOWTABTEXT          = "showTabText"
        const val KEY_PREF_SCROLLABLETABS       = "swScrollableTabs"

        const val KEY_PREF_TEXT_SIZE = "listTextSize"
        const val KEY_PREF_ROW_PADDING = "etRowPadding"
        const val KEY_PREF_USER = "userName"
        const val KEY_PREF_COLOUR_STYLE = "colourStyle"
        const val KEY_PREF_PASSWORD = "zipPassword"
        const val KEY_PREF_BIOMETRIC = "enableBiometric"
        const val KEY_PREF_QUICKENTRY = "quickEntry"
        const val KEY_PREF_APP_THEME = "lpAppTheme"
        const val KEY_PREF_PDF_TEXT_SIZE = "etPdfTextSize"
        const val KEY_PREF_FORCE_REAUTHENTICATION = "etAuthenticationTimeout"
    }
}