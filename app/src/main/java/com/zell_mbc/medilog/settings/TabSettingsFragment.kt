package com.zell_mbc.medilog.settings

import android.os.Bundle
import androidx.preference.MultiSelectListPreference
import com.takisoft.preferencex.PreferenceFragmentCompat
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import java.util.ArrayList

class TabSettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferencesFix(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.tabs_preferences, rootKey)

        // Populate tab selection array
        findPreference<MultiSelectListPreference>(SettingsActivity.KEY_PREF_ACTIVE_TABS_SET)?.apply {
            val tabIDs = ArrayList<String>()
            val tabLabels = ArrayList<String>()
            this.values.clear()
            for (tab in MainActivity.availableTabs) {
                tabIDs.add(tab.id.toString())
                tabLabels.add(tab.label)
                if (tab.active) values.add(tab.id.toString())
            }
            val entryDisplay    = tabLabels.toTypedArray()
            val entryIDs = tabIDs.toTypedArray()

            entries = entryDisplay
            entryValues = entryIDs
        }
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
    }

}
