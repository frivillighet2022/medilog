package com.zell_mbc.medilog.settings

import android.os.Bundle
import androidx.preference.Preference
import com.takisoft.preferencex.PreferenceFragmentCompat
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.settings.SettingsActivity.Companion.KEY_PREF_TEMPERATURE_THRESHOLDS
import com.zell_mbc.medilog.utility.Preferences

class TemperatureSettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferencesFix(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.temperature_preferences, rootKey)

        val preferences = Preferences.getSharedPreferences(requireContext())
        val thresholdObject = preferenceManager.findPreference<Preference>(KEY_PREF_TEMPERATURE_THRESHOLDS)
        if (thresholdObject != null) {
            // Todo
//            val thresholdValue = preferences.getString(KEY_PREF_TEMPERATURE_THRESHOLDS, "")
//            thresholdObject.summary = getString(R.string.thresholdSummary,thresholdValue)
        }
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
    }

}
