package com.zell_mbc.medilog.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import com.zell_mbc.medilog.MainActivity.Companion.userFeedback
import com.zell_mbc.medilog.MainActivity.Companion.userFeedbackLevel
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.databinding.ActivityAboutBinding
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.utility.ListTheme
import com.zell_mbc.medilog.utility.Preferences
import java.text.DateFormat
import java.util.*

class UserFeedbackFragment : Fragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: ActivityAboutBinding? = null
    private val binding get() = _binding!!
    private lateinit var userOutputService : UserOutputService

    private fun initializeService(view: View) {
        userOutputService = UserOutputServiceImpl(requireContext(),view)
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = ActivityAboutBinding.inflate(inflater, container, false)
        initializeService(binding.root)
        return binding.root
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    @Composable
    fun ShowContent() {
        val preferences = Preferences.getSharedPreferences(requireContext())
        val scrollState = rememberScrollState()

        val feedbackString  = remember { mutableStateOf(preferences.getString("USER_FEEDBACK_STRING", getString(R.string.nothing)).toString()) }
        var feedbackStringVal1 = feedbackString.value
        var feedbackStringVal2 = ""
        if (feedbackStringVal1.contains(":")) {
            feedbackStringVal2 = feedbackStringVal1.substring(feedbackStringVal1.indexOf(":") + 1)
            feedbackStringVal1 =
                feedbackStringVal1.substring(0, (feedbackStringVal1.indexOf(":") - 1))
        }

        val feedbackEnabled = remember { mutableStateOf(false) }
        feedbackEnabled.value = userFeedbackLevel > 0 //preferences.getInt("USER_FEEDBACK_SETTING", 0) > 0

        val lastRun = preferences.getLong("LAST_FEEDBACK_RUN",0L)
        val lastRunString = if (lastRun > 0L) DateFormat.getDateInstance(DateFormat.SHORT).format(lastRun) + " - " + DateFormat.getTimeInstance().format(lastRun)
                            else getString(R.string.never)

        val lastFeedbackTime =  remember { mutableStateOf(lastRunString) }
        if (lastFeedbackTime.value.isEmpty()) lastFeedbackTime.value = getString(R.string.never)

        ListTheme {
            Column(
                modifier = Modifier.verticalScroll(state = scrollState)
                    .padding(start = 16.dp, top = 8.dp)
            ) {
                Row {
                    Surface(
                        modifier = Modifier.size(50.dp),
                        shape = CircleShape,
                        color = MaterialTheme.colors.onSurface.copy(alpha = 0.2f)
                    ) {
                        Image(
                            painter = painterResource(id = R.drawable.ic_medilog2),
                            contentDescription = null
                        )
                    }
                    Column(modifier = Modifier.padding(start = 8.dp, top = 6.dp)) {
                        Text(
                            stringResource(id = R.string.appName),
                            style = MaterialTheme.typography.body1,
                            color = MaterialTheme.colors.primaryVariant,
                            textAlign = TextAlign.End
                        )
                        Text(
                            stringResource(id = R.string.appDescription),
                            style = MaterialTheme.typography.caption,
                            color = MaterialTheme.colors.primaryVariant
                        )
                    }
                }

                val uriHandler = LocalUriHandler.current

                // Rational
                Text(
                    stringResource(id =R.string.whyUserFeedback),
                    modifier = Modifier.padding(top = 16.dp),
                    style = MaterialTheme.typography.caption,
                    color = MaterialTheme.colors.primaryVariant
                )
                val wikiUrl: AnnotatedString = buildAnnotatedString {
                    val str = stringResource(id = R.string.UserFeedbackUrl)
                    val startIndex = 0
                    val endIndex = str.length
                    append(str)
                    addStyle(
                        style = SpanStyle(
                            color = Color(0xff64B5F6),
                            textDecoration = TextDecoration.Underline
                        ), start = startIndex, end = endIndex
                    )

                    // attach a string annotation that stores a URL to the text "link"
                    addStringAnnotation(
                        tag = "URL",
                        annotation = str,
                        start = startIndex,
                        end = endIndex
                    )
                }
                // Clickable text returns position of text that is clicked in onClick callback
                ClickableText(
                    modifier = Modifier.fillMaxWidth().padding(top = 8.dp),
                    text = wikiUrl,
                    onClick = {
                        wikiUrl
                            .getStringAnnotations("URL", it, it)
                            .firstOrNull()?.let { stringAnnotation ->
                                uriHandler.openUri(stringAnnotation.item)
                            }
                    }, style = MaterialTheme.typography.caption
                )
                Row(modifier = Modifier.padding(top = 6.dp)) {
                    Text(
                        getString(R.string.feedbackEnabled),
                        modifier = Modifier.padding(top = 16.dp),
                        style = MaterialTheme.typography.caption,
                        color = MaterialTheme.colors.primaryVariant
                    )
                    Switch(
                        checked = feedbackEnabled.value,
                        onCheckedChange = { feedbackEnabled.value = it
                            userFeedbackLevel = if(it) 1 else 0
                            val editor = preferences.edit()
                            editor.putInt("USER_FEEDBACK_SETTING", userFeedbackLevel)
                            editor.apply()
                        }
                    )
                }
                Text(getString(R.string.lastFeedback) + " " + lastFeedbackTime.value,
                    modifier = Modifier.padding(top = 4.dp),
                    style = MaterialTheme.typography.caption,
                    color = MaterialTheme.colors.primaryVariant
                )
                Text(
                    getString(R.string.dataSent),
                    modifier = Modifier.padding(top = 4.dp),
                    style = MaterialTheme.typography.caption,
                    color = MaterialTheme.colors.primaryVariant
                )
                Text(
                    feedbackStringVal1,
                    modifier = Modifier.padding(start = 8.dp, top = 0.dp),
                    style = MaterialTheme.typography.caption,
                    color = MaterialTheme.colors.primaryVariant
                )
                Text(
                    feedbackStringVal2,
                    modifier = Modifier.padding(start = 8.dp, top = 0.dp),
                    style = MaterialTheme.typography.caption,
                    color = MaterialTheme.colors.primaryVariant
                )
                Text(
                    getString(R.string.otherFeedbackMeans),
                    modifier = Modifier.padding(top = 16.dp),
                    style = MaterialTheme.typography.caption,
                    color = MaterialTheme.colors.primaryVariant
                )

                // Email
                val eMail: AnnotatedString = buildAnnotatedString {
                    val str = stringResource(id = R.string.email)
                    val startIndex = 0
                    val endIndex = str.length
                    append(str)
                    addStyle(
                        style = SpanStyle(
                            color = Color(0xff64B5F6),
                            textDecoration = TextDecoration.Underline
                        ), start = startIndex, end = endIndex
                    )

                    // attach a string annotation that stores a URL to the text "link"
                    addStringAnnotation(
                        tag = "URL",
                        annotation = "mailto://$str",
                        start = startIndex,
                        end = endIndex
                    )
                }
                // Clickable text returns position of text that is clicked in onClick callback
                ClickableText(
                    modifier = Modifier.fillMaxWidth().padding(start = 8.dp),
                    text = eMail,
                    onClick = {
                        eMail
                            .getStringAnnotations("URL", it, it)
                            .firstOrNull()?.let { stringAnnotation ->
                                uriHandler.openUri(stringAnnotation.item)
                            }
                    }, style = MaterialTheme.typography.caption
                )

                // AppUrl
                val appUrl: AnnotatedString = buildAnnotatedString {
                    val str = stringResource(id = R.string.AppURL)
                    val startIndex = 0
                    val endIndex = str.length
                    append(str)
                    addStyle(
                        style = SpanStyle(
                            color = Color(0xff64B5F6),
                            textDecoration = TextDecoration.Underline
                        ), start = startIndex, end = endIndex
                    )

                    // attach a string annotation that stores a URL to the text "link"
                    addStringAnnotation(
                        tag = "URL",
                        annotation = str,
                        start = startIndex,
                        end = endIndex
                    )
                }
                // Clickable text returns position of text that is clicked in onClick callback
                ClickableText(
                    modifier = Modifier.fillMaxWidth().padding(start = 8.dp),
                    text = appUrl,
                    onClick = {
                        appUrl
                            .getStringAnnotations("URL", it, it)
                            .firstOrNull()?.let { stringAnnotation ->
                                uriHandler.openUri(stringAnnotation.item)
                            }
                    }, style = MaterialTheme.typography.caption
                )
                Button(modifier = Modifier.padding(start = 8.dp, top = 16.dp),
                    onClick = {
                        feedbackString.value   = userFeedback(requireContext())
                        lastFeedbackTime.value = DateFormat.getDateInstance(DateFormat.SHORT).format(Date()) + " - " + DateFormat.getTimeInstance().format(Date())
                        //feedbackString.value   = preferences.getString("USER_FEEDBACK_STRING", getString(R.string.nothing)).toString()
                    }) {
                    Text(getString(R.string.instantFeedback))
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.aboutView.setContent {
            ShowContent()
        }
    }
}