package com.zell_mbc.medilog.settings

import android.os.Bundle
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.takisoft.preferencex.PreferenceFragmentCompat

class WaterSettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferencesFix(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.water_preferences, rootKey)
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
    }

}