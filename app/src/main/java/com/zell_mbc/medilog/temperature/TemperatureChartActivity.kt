package com.zell_mbc.medilog.temperature

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.zell_mbc.medilog.MainActivity

class TemperatureChartActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainActivity.setTheme(this)

        supportFragmentManager.beginTransaction()
                .replace(android.R.id.content, TemperatureChartFragment())
                .commit()
    }
}
