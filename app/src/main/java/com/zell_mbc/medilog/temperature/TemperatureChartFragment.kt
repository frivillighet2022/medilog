package com.zell_mbc.medilog.temperature

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.DashPathEffect
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.androidplot.util.PixelUtils
import com.androidplot.xy.*
import com.zell_mbc.medilog.MainActivity.Companion.TEMPERATURE
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utility.*
import java.text.*
import java.util.*
import kotlin.math.ceil
import kotlin.math.floor
import kotlin.math.roundToInt

// Chart manual
// https://github.com/halfhp/androidplot/blob/master/docs/xyplot.md
class TemperatureChartFragment : Fragment() {
    private var temperatureLowerThreshold = ArrayList<Float>()
    private var temperatureUpperThreshold = ArrayList<Float>()
    private var temperatures = ArrayList<Float>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.temperature_chart, container, false)
    }

    @SuppressLint("SimpleDateFormat")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // create a couple arrays of y-values to plot:
        val labels = ArrayList<String>()
        var wMax = 0f
        var wMin = 1000f

        val preferences = Preferences.getSharedPreferences(requireContext())
        var sTmp: String
        val simpleDate = SimpleDateFormat("MM-dd")

        val viewModel = ViewModelProvider(this)[TemperatureViewModel::class.java]
        viewModel.init(TEMPERATURE)
        val items = viewModel.getItems("ASC", filtered = true)
        for (item in items) {
            sTmp = simpleDate.format(item.timestamp)

            labels.add(sTmp)
            val temperature = try {
                item.value1.toFloat()
            } catch (e: NumberFormatException) {
                0f
            }
            temperatures.add(temperature)

            // Keep min and max values
            if (temperature > wMax) wMax = temperature
            if (temperature < wMin) wMin = temperature
        }
        if (temperatures.size == 0) {
            return
        }

        // If threshold is set create dedicated chart, otherwise show as origin
        val thresholds = preferences.getBoolean(SettingsActivity.KEY_PREF_TEMPERATURE_SHOW_THRESHOLDS, getString(R.string.TEMPERATURE_SHOW_THRESHOLDS_DEFAULT).toBoolean())

        if (thresholds) {
            val temperatureLowerThresholdValue: Float
            val temperatureUpperThresholdValue: Float
            val defaultThresholds = getString(R.string.TEMPERATURE_THRESHOLDS_DEFAULT)
            val s = "" + preferences.getString(SettingsActivity.KEY_PREF_TEMPERATURE_THRESHOLDS, defaultThresholds)
            val th = checkThresholds(requireContext(), s, defaultThresholds, R.string.temperature)

            temperatureLowerThresholdValue = th[0].toFloat()
            temperatureUpperThresholdValue = th[1].toFloat()

            for (item in temperatures) {
                temperatureLowerThreshold.add(temperatureLowerThresholdValue)
                temperatureUpperThreshold.add(temperatureUpperThresholdValue)
            }
        }

        // initialize our XYPlot reference:
        val plot: XYPlot = view.findViewById(R.id.temperaturePlot)
        PanZoom.attach(plot)

        val backgroundColor = getBackgroundColor(requireContext())
        plot.graph.backgroundPaint.color = backgroundColor

        val fontSizeSmall: Int = getFontSizeSmallInPx(requireContext())

        val axisPaint = Paint()
        axisPaint.style = Paint.Style.FILL_AND_STROKE
        axisPaint.color = getTextColorPrimary(requireContext())
        axisPaint.textSize = fontSizeSmall.toFloat()
        axisPaint.isAntiAlias = true

        val gridPaint = Paint()
        gridPaint.style = Paint.Style.STROKE
        gridPaint.color = getTextColorSecondary(requireContext())
        gridPaint.isAntiAlias = false
        gridPaint.pathEffect = DashPathEffect(floatArrayOf(3f, 2f), 0F)

        val showGrid = preferences.getBoolean(SettingsActivity.KEY_PREF_TEMPERATURE_SHOW_GRID, true)
        if (!showGrid) {
            plot.graph.domainGridLinePaint = null
            plot.graph.rangeGridLinePaint = null
        }
        else {
            plot.graph.domainGridLinePaint = gridPaint
            plot.graph.rangeGridLinePaint = gridPaint // Horizontal lines
        }

        val isLegendVisible = preferences.getBoolean(SettingsActivity.KEY_PREF_TEMPERATURE_SHOW_LEGEND, false)
        plot.legend.isVisible = isLegendVisible
        plot.legend.isDrawIconBackgroundEnabled = false

        val series1: XYSeries = SimpleXYSeries(temperatures, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, getString(R.string.temperature))
        val series2: XYSeries = SimpleXYSeries(temperatureLowerThreshold, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "")
        val series3: XYSeries = SimpleXYSeries(temperatureUpperThreshold, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "")

 //       val diff = (wMax - wMin).toInt()
//        if (diff < 5) plot.setRangeStep(StepMode.SUBDIVIDE, 3.0) // Always show 5 sections = 5 y-axis values
//        else
        plot.setRangeStep(StepMode.SUBDIVIDE, 5.0) // Always show 5 sections = 5 y-axis values

        val chartMin= floor(wMin)
        val chartMax= ceil(wMax)
        plot.outerLimits.set( 0, temperatures.size-1, chartMin, chartMax) // For pan&zoom
        plot.setRangeBoundaries(chartMin, chartMax, BoundaryMode.FIXED)

        if (series1.size() < 10) plot.setDomainStep(StepMode.SUBDIVIDE, series1.size().toDouble()) // Avoid showing the same day multiple times

        val temperatureBorder = ContextCompat.getColor(requireContext(), R.color.chart_blue_border)
        val temperatureFill = ContextCompat.getColor(requireContext(), R.color.chart_blue_fill)
        val series1Format = LineAndPointFormatter(temperatureBorder, null, temperatureFill, null)
        plot.addSeries(series1, series1Format)

        if (thresholds) {
            val formatThreshold = LineAndPointFormatter(Color.BLUE, null, null, null)
            formatThreshold.linePaint.pathEffect = DashPathEffect(floatArrayOf( // always use DP when specifying pixel sizes, to keep things consistent across devices:
                PixelUtils.dpToPix(20f),
                PixelUtils.dpToPix(15f)), 0f)

            formatThreshold.isLegendIconEnabled = false
            formatThreshold.linePaint.strokeWidth = 2f
            plot.addSeries(series2, formatThreshold)
            plot.addSeries(series3, formatThreshold)
        }

        plot.graph.getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).format = object : Format() {
            override fun format(obj: Any, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer {
                val i = (obj as Number).toFloat().roundToInt()
                return toAppendTo.append(labels[i])
            }

            override fun parseObject(source: String, pos: ParsePosition): Any {
                return 0
            }
        }
    }

}