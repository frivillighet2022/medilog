package com.zell_mbc.medilog.temperature

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.TEMPERATURE
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.EditFragment
import com.zell_mbc.medilog.databinding.TemperatureEditformBinding
import com.zell_mbc.medilog.settings.SettingsActivity
import java.text.DateFormat


class TemperatureEditFragment : EditFragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: TemperatureEditformBinding? = null
    private val binding get() = _binding!!


    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = TemperatureEditformBinding.inflate(inflater,container,false)
        initializeService(binding.root)

        return binding.root
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun updateItem() {
        val editItem = viewModel.getItem(viewModel.editItem)
        if (editItem == null) {
            // Close keyboard so error is visible
            (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken, 0)
            userOutputService.showMessageAndWaitForLong(getString(R.string.unknownError))
            return
        }

        // Check empty variables
        val value = binding.etTemperature.text.toString()
        if (value.isEmpty()) {
            userOutputService.showMessageAndWaitForLong(this.getString(R.string.temperatureMissing))
            return
        }

        // Valid temperature?
        var temperatureValue = 0f
        try {
            temperatureValue = value.toFloat()
            if (temperatureValue <= 0) {
                userOutputService.showMessageAndWaitForLong(this.getString(R.string.invalid) + " " + this.getString(R.string.temperature) + " " + this.getString(R.string.value) + " $temperatureValue")
                return
            }
        } catch (e: Exception) {
            userOutputService.showMessageAndWaitForLong("Exception: " + this.getString(R.string.invalid) + " " + this.getString(R.string.temperature) + " " + this.getString(R.string.value) + " $temperatureValue")
            return
        }

        editItem.timestamp = timestampCal.timeInMillis
        editItem.value1 = temperatureValue.toString()
        editItem.comment = binding.etComment.text.toString()

        viewModel.update(editItem)

        // Close keyboard after entry is done
        (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken,0)

        userOutputService.showMessageAndWaitForLong(requireContext().getString(R.string.itemUpdated))
        //requireActivity().onBackPressed()
        requireActivity().finish()
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val vm = MainActivity.getViewModel(TEMPERATURE) as TemperatureViewModel?
        if (vm != null) viewModel = vm
        else {
            userOutputService.showMessageAndWaitForLong(getString(R.string.unknownError))
            // Returning here will make sure none of the controls are initialized = no error when e.g. delete is selected
            return
        }

        saveButton = binding.btSave
        deleteButton = binding.btDelete
        super.onViewCreated(view, savedInstanceState)

        val editItem = viewModel.getItem(viewModel.editItem)
        if (editItem == null) {
            // Close keyboard so error is visible
            (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken, 0)
            userOutputService.showMessageAndWaitForLong(getString(R.string.unknownError))
            return
        }

        binding.tvUnit.text = preferences.getString(SettingsActivity.KEY_PREF_TEMPERATURE_UNIT, getString(R.string.TEMPERATURE_UNIT_DEFAULT))
        binding.etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(editItem.timestamp)
        binding.etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(editItem.timestamp)

        // Check if we are really editing or if this is a new value, if new entry delete tmpComment
        if (viewModel.isTmpItem(editItem.comment))
            binding.etComment.setText("")
        else {
            binding.etTemperature.setText(editItem.value1)
            binding.etComment.setText(editItem.comment)
        }

        setDateTimePicker(editItem.timestamp, binding.etDate, binding.etTime)

        // Make sure first field is highlighted and keyboard is open
        binding.etTemperature.requestFocus()
    }
}
