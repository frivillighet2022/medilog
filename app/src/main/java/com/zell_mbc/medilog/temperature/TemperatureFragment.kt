package com.zell_mbc.medilog.temperature

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.MaterialTheme.colors
import androidx.compose.material.TabRowDefaults.Divider
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.*
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.data.TabFragment
import com.zell_mbc.medilog.databinding.TemperatureTabBinding
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utility.*
import java.util.*
import androidx.compose.material.Text as Text

class TemperatureFragment : TabFragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: TemperatureTabBinding? = null
    private val binding get() = _binding!!

    // String represenation of input values
    private var temperatureValue = ""
    private var commentValue = ""
    private var temperatureFieldWidth = 0.dp

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = TemperatureTabBinding.inflate(inflater, container, false)
        initializeService(binding.root)
        return binding.root
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun addItem() {

        if (!quickEntry) {
            val tmpItem =
                Data(0, Date().time, MainActivity.tmpComment, viewModel.dataType, "", "", "", "")
            val itemID = viewModel.insert(tmpItem)
            if (itemID != 0L) editItem(itemID.toInt())
            else userOutputService.showMessageAndWaitForLong("No entry with tmp value found!")
            return
        }

        // Close keyboard after entry is done
        (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken,0)

        // Check empty variables
        if (temperatureValue.isEmpty()) {
            (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken,0)
            userOutputService.showMessageAndWaitForLong(getString(R.string.temperatureMissing))
            return
        }
        var temp = 0f
        try {
            temp = temperatureValue.toFloat()
            if (temp <= 0) {
                // Close keyboard after entry is done
                (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken,0)
                userOutputService.showMessageAndWaitForLong(this.getString(R.string.invalid) + " " + this.getString(R.string.temperature) + " " + this.getString(R.string.value) + " $temp")
                return
            }
        } catch (e: Exception) {
            // Close keyboard after entry is done
            (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken,0)
            userOutputService.showMessageAndWaitForLong(                "Exception: " + this.getString(R.string.invalid) + " " + this.getString(                    R.string.temperature                ) + " " + this.getString(R.string.value) + " $temp")
            return
        }
        val item = Data(
            0,
            Date().time,
            commentValue,
            viewModel.dataType,
            temperatureValue,
            "",
            "",
            ""
        ) // Start with empty item

        viewModel.insert(item)

        if ((viewModel.filterEnd > 0L) && (viewModel.filterEnd < item.timestamp)) userOutputService.showMessageAndWaitForDuration(
            getString(R.string.filteredOut),
            4000
        )

        // Empty fields and refresh
        temperatureValue = ""
        commentValue = ""
        binding.itemList.setContent { ShowContent() }

    }

    // Measure up Glycol string
    @Composable
    fun MeasureValueString() {
        MeasureTextWidth("37,50")
        {
            textWidth ->
            Text(text = "", fontSize = fontSize.sp)
            temperatureFieldWidth = textWidth //+ (textWidth / 10) // Add a 10% buffer
        }
    }

    @Composable
    fun ShowRow(item: Data) {
        // Timestamp
        Text(dateFormat.format(item.timestamp) + " - " + timeFormat.format(item.timestamp),
            Modifier
                .padding(end = cellPadding.dp, top = rowPadding.dp, bottom = rowPadding.dp)
                .width(dateColumnWidth),
            color = colors.primary, fontSize = fontSize.sp)
        Divider(color = colors.secondary, modifier = Modifier
            .width(1.dp)
            .fillMaxHeight()) // Vertical separator

        val value1 = try { item.value1.toFloat() } catch (e: NumberFormatException) { 0f }

        // Value
        var themeColor = colors.primary
        if (highlightValues && (value1 < lowerThreshold || value1 > upperThreshold)) themeColor = colors.error
        Text(item.value1,
            Modifier
                .padding(start = cellPadding.dp, end = cellPadding.dp)
                .width(temperatureFieldWidth),textAlign = TextAlign.Center, color = themeColor, fontSize = fontSize.sp)
        Divider(color = colors.secondary, modifier = Modifier
            .width(1.dp)
            .fillMaxHeight()) // Vertical separator

        // Comment
        if (item.comment.isNotEmpty()) {
            Spacer(modifier = Modifier.width(cellPadding.dp))
            Text(item.comment,color = colors.primary, fontSize = fontSize.sp)
        }
    }


    @OptIn(ExperimentalComposeUiApi::class)
    @Composable
    fun ShowContent() {
        val listState = rememberLazyListState()
        var selection: Data? by remember { mutableStateOf(null)}
        val listItems = viewModel.getItems("DESC",true)

        var comment by remember { mutableStateOf(commentValue) }
        var triggerRefresh by remember { mutableStateOf(false) }
        var commentVisible by remember { mutableStateOf(false) }
//        val temperature = remember { mutableStateOf(TextFieldValue(temperatureValue)) }
        var temperature by remember { mutableStateOf(temperatureValue) }

        if (temperatureValue.isEmpty()) temperature = ""
        if (commentValue.isEmpty()) comment = ""

        ListTheme {
            MeasureDateString()
            MeasureValueString()

            if (quickEntry) {

                Column (modifier = Modifier.fillMaxWidth()){
                        val (textField1, textField2) = remember { FocusRequester.createRefs() }
                        Row {
                            TextField(
//                                value = temperature.value,
                                value = temperature,
                                colors = TextFieldDefaults.textFieldColors(textColor = colors.primary, backgroundColor = androidx.compose.ui.graphics.Color.Transparent),
//                                onValueChange = {   temperature.value = it },
                                onValueChange = {   temperature = it },
                                singleLine = true,
                                keyboardOptions = KeyboardOptions(
                                    keyboardType = KeyboardType.Number
                                ),
                                textStyle = TextStyle(),
                                label = { Text(text = stringResource(id = R.string.temperature)+"*", maxLines = 1, overflow = TextOverflow.Ellipsis) },

                                placeholder  = { Text(text = stringResource(id = R.string.temperatureHint) + " $itemUnit") },
                                modifier = Modifier
                                    .width(140.dp)
                                    .focusRequester(textField1)
                                    .padding(end = 10.dp),
                                keyboardActions = KeyboardActions( onDone = { addItem() })
                            )

                            IconButton(onClick = { commentVisible = !commentVisible
                                                   triggerRefresh = true
                                                 },
                                modifier = Modifier
                                    .width(commentButtonWidth.dp)
                                    .padding(top = (2 * cellPadding).dp)) {
                                Icon(painter = painterResource(R.drawable.ic_outline_comment_24),"Comment", tint = colors.primary) }
                        } // Row

                        if (triggerRefresh) {
                            this@TemperatureFragment.ShowContent()
                            triggerRefresh = false
                        }

                        if (commentVisible) {
                            // Comment
                            TextField(
                                value = comment,
                                colors = TextFieldDefaults.textFieldColors(textColor = colors.primary, backgroundColor = androidx.compose.ui.graphics.Color.Transparent),
                                onValueChange = { comment = it },
                                keyboardOptions = KeyboardOptions(
                                    keyboardType = KeyboardType.Text,
                                    capitalization = KeyboardCapitalization.Sentences
                                ),
                                maxLines = 2,
                                textStyle = TextStyle(),
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .focusRequester(textField2),
                                label = { Text(text = stringResource(id = R.string.comment)) },
                                keyboardActions = KeyboardActions( onDone = { addItem() })
                            )

                            // Need an add button because Enter is used for new line
                            binding.btAdd.visibility = View.VISIBLE
                        }
                        else binding.btAdd.visibility = View.GONE // Hide Add Button

    //                    val tmp = temperature.value.text
                        val tmp = temperature
                        if (tmp.isNotEmpty()) temperatureValue = try { tmp.toFloat().toString() } catch (e: Exception) { "" }
                        commentValue = comment
                    }
                topPadding = 80 + if (commentVisible) 65 else 0
            }
            LazyColumn(
                state = listState,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.padding(top = topPadding.dp)
            ) {
                items(listItems) { item ->
                    Divider(color = colors.secondary, thickness = 1.dp) // Lines starting from the top
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .width(IntrinsicSize.Min)
                            .height(IntrinsicSize.Min)
                            .clickable { selection = item },
                        verticalAlignment = Alignment.CenterVertically
                    )
                    { ShowRow(item)}

                    if (selection != null) {
                        ItemClicked(selection!!._id)
                        selection = null
                    }
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        editActivityClass  = TemperatureEditActivity::class.java
        infoActivityClass  = TemperatureInfoActivity::class.java
        chartActivityClass = TemperatureChartActivity::class.java

        viewModel =
            ViewModelProvider(requireActivity())[TemperatureViewModel::class.java]  // This should return the MainActivity ViewModel
        configureButtons(binding.btAdd,binding.btInfo, binding.btChart)

        // -------------------------------
        itemUnit = " " + preferences.getString(SettingsActivity.KEY_PREF_TEMPERATURE_UNIT, getString(R.string.TEMPERATURE_UNIT_DEFAULT))
        highlightValues = preferences.getBoolean(SettingsActivity.KEY_PREF_TEMPERATURE_HIGHLIGHT_VALUES, getString(R.string.HIGHLIGHT_VALUES_DEFAULT).toBoolean())

        val defaultThresholds = getString(R.string.TEMPERATURE_THRESHOLDS_DEFAULT)
        val s = "" + preferences.getString(SettingsActivity.KEY_PREF_TEMPERATURE_THRESHOLDS, defaultThresholds)
        val th = checkThresholds(requireContext(), s, defaultThresholds, R.string.temperature)
        lowerThreshold = th[0].toFloat()
        upperThreshold = th[1].toFloat()

        // Preset values in case something was stored in viewModel during onDestroy
        commentValue = viewModel.comment
        temperatureValue = viewModel.value1

        binding.itemList.setContent {ShowContent() }
    }

    // Save values to survie orientation change
    override fun onDestroy() {
        viewModel.value1 = temperatureValue
        viewModel.comment = commentValue
        super.onDestroy()
    }
}
