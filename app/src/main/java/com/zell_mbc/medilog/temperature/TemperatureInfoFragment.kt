package com.zell_mbc.medilog.temperature

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.utility.Preferences
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.TEMPERATURE
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.databinding.TemperatureInfoformBinding
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import java.math.RoundingMode
import java.text.DateFormat
import java.text.DecimalFormat

class TemperatureInfoFragment : Fragment() {
    private var _binding: TemperatureInfoformBinding? = null
    private val binding get() = _binding!!
    private lateinit var userOutputService : UserOutputService

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = TemperatureInfoformBinding.inflate(inflater, container, false)
        initializeService(binding.root)

        return binding.root
    }
    private fun initializeService(view: View) {
        userOutputService = UserOutputServiceImpl(requireContext(),view)
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private lateinit var viewModel: TemperatureViewModel  //by viewModels() //factoryProducer = { SavedStateViewModelFactory(this, ) })

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(requireActivity())[TemperatureViewModel::class.java]
        viewModel.init(TEMPERATURE)

        val count = viewModel.getSize(true)
        var item = viewModel.getLast(true)
        if (item == null) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.noDataToShow))
            return
        }

        binding.tvTemperatureHeader.text = ""

        val preferences = Preferences.getSharedPreferences(requireContext())
        val temperatureUnit = " " + preferences.getString(SettingsActivity.KEY_PREF_TEMPERATURE_UNIT, getString(R.string.TEMPERATURE_UNIT_DEFAULT))

        var s = if ((viewModel.filterStart + viewModel.filterEnd) == 0L) getString(R.string.measurementsInDB) + " $count"
        else getString(R.string.measurementsInFilter) + " $count"
        binding.tvMeasurementCount.text = s

        val avg = viewModel.getAvgInt("value1",true)
        val df = DecimalFormat("#.##")
        df.roundingMode = RoundingMode.HALF_EVEN
        val ravg = df.format(avg)
        s = getString(R.string.avg) + ": $ravg $temperatureUnit"
        binding.tvAvg.text = s

        val min = viewModel.getMinValue1(true)
        val max = viewModel.getMaxValue1(true)
        s = getString(R.string.minMaxValues) + " $min - $max $temperatureUnit"
        binding.tvMinMax.text = s

        val dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
        val endDate = dateFormat.format(item.timestamp)

        item = viewModel.getFirst(true)
        val startDate = dateFormat.format(item?.timestamp)

        s = getString(R.string.timePeriod) + " $startDate - $endDate"
        binding.tvTimePeriod.text = s
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
        viewModel.deleteTmpItem() // Deletes all items with comment == tmpComment
    }
}
