package com.zell_mbc.medilog.temperature

import android.app.Application
import android.graphics.Paint
import android.graphics.pdf.PdfDocument
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.graphics.drawable.toBitmap
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.ViewModel
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utility.checkThresholds
import java.util.*

class TemperatureViewModel(application: Application): ViewModel(application) {
    override var tabIcon  = R.drawable.ic_outline_device_thermostat_24
    override val filterStartPref = "TEMPERATUREFILTERSTART"
    override val filterEndPref = "TEMPERATUREFILTEREND"
    override val filterModePref =  "TEMPERATURE_FILTER_MODE"
    override val rollingFilterValuePref = "TEMPERATURE_ROLLING_FILTER_VALUE"
    override val rollingFilterTimeframePref = "TEMPERATURE_ROLLING_FILTER_TIMEFRAME"

    override var itemName = app.getString(R.string.temperature)
    override var landscape = preferences.getBoolean(SettingsActivity.KEY_PREF_TEMPERATURE_LANDSCAPE, app.getString(R.string.TEMPERATURE_LANDSCAPE_DEFAULT).toBoolean())
    override var pageSize = preferences.getString(SettingsActivity.KEY_PREF_TEMPERATURE_PAPER_SIZE, app.getString(R.string.TEMPERATURE_PAPER_SIZE_DEFAULT))
    override var showAllTabs = false // Only used for Diary tab

    private val temperatureUnit = preferences.getString(SettingsActivity.KEY_PREF_TEMPERATURE_UNIT, app.getString(R.string.TEMPERATURE_UNIT_DEFAULT))
    private val quantity = "$temperatureUnit  "
    private val warningSign = app.getString(R.string.warningSign)

    private var warningTab = 0f

    override fun createPdfDocument(filtered: Boolean): PdfDocument? {
        if (getSize(filtered) == 0) {
            userOutputService.showAndHideMessageForLong( app.getString(R.string.temperature) + ": " + app.getString(R.string.noDataToExport))
            return null
        }

        super.createPdfDocument(filtered)

        val defaultThresholds = app.getString(R.string.TEMPERATURE_THRESHOLDS_DEFAULT)
        val s = "" + preferences.getString(SettingsActivity.KEY_PREF_TEMPERATURE_THRESHOLDS, defaultThresholds)
        val th = checkThresholds(app, s, defaultThresholds, R.string.temperature)
        val temperatureLowerThreshold = th[0].toFloat()
        val temperatureUpperThreshold = th[1].toFloat()

        setColumns(pdfPaintHighlight)
        drawHeader(pdfPaint, pdfPaintHighlight)
        var f = pdfDataTop + pdfLineSpacing
        availableWidth = measureColumn(pdfRightBorder - commentTab)

        for (item in pdfItems) {
            f = checkForNewPage(f)
            val dtString = toStringDate(item.timestamp) + "  " + toStringTime(item.timestamp)
            canvas.drawText(dtString, pdfLeftBorder, f, pdfPaint)

            val value = try {
                item.value1.toFloat()
            } catch  (e: NumberFormatException) {
                0f
            }
            if (value > temperatureUpperThreshold || value < temperatureLowerThreshold) canvas.drawText(warningSign, warningTab, f, pdfPaint)
            canvas.drawText(item.value1, dataTab, f, pdfPaint)
            f = multipleLines(item.comment, f)
        }
        // finish the page
        document.finishPage(page)
        // Add statistics page
        createPage(document)
        drawStatsPage(pdfPaint, pdfPaintHighlight)

        document.finishPage(page)
        return document
    }

    override fun setColumns(pdfPaintHighlight: Paint) {
        val warningSignWidth = pdfPaintHighlight.measureText(app.getString(R.string.warningSign))
        val temperatureWidth = pdfPaintHighlight.measureText(quantity)

        warningTab = pdfLeftBorder + dateTabWidth + padding
        dataTab = warningTab + warningSignWidth + space
        commentTab = dataTab + temperatureWidth + padding
    }

    override fun drawHeader(pdfPaint: Paint, pdfPaintHighlight: Paint) {
        drawHeader(pdfPaint) // VieModel function doesn't need to highlight

        // Data section
        canvas.drawText(app.getString(R.string.date), pdfLeftBorder, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(quantity, dataTab, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.comment), commentTab + space, pdfDataTop, pdfPaintHighlight)
        canvas.drawLine(warningTab, pdfHeaderBottom, warningTab, pdfDataBottom, pdfPaint)  // Line before weight
        canvas.drawLine(commentTab, pdfHeaderBottom, commentTab, pdfDataBottom, pdfPaint)  // Line before comment
    }

    private fun drawStatsPage(pdfPaint: Paint, pdfPaintHighlight: Paint) {
        // Header
        val pdfHeaderDateColumn = pdfRightBorder - 150
        val logoTab = 30
        val bitmap = AppCompatResources.getDrawable(app, R.mipmap.ic_launcher)?.toBitmap(logoTab,30)
        if (bitmap != null) canvas.drawBitmap(bitmap, 5f, 5f, pdfPaint)

        // Draw header
        canvas.drawText(headerText, pdfLeftBorder + logoTab, pdfHeaderTop, pdfPaint)
        canvas.drawText(app.getString(R.string.date) + ": " + formatedDate, pdfHeaderDateColumn, pdfHeaderTop, pdfPaint)
        canvas.drawLine(pdfLeftBorder, pdfHeaderBottom, pdfRightBorder, pdfHeaderBottom, pdfPaint)
        canvas.drawLine(pdfLeftBorder, pdfDataBottom, pdfRightBorder, pdfDataBottom, pdfPaint)

        val timeframeLabel = app.getString(R.string.timeframeLabel)
        val totalLabel = app.getString(R.string.totalLabel)
        val annualLabel = app.getString(R.string.annualLabel)
        val monthLabel = app.getString(R.string.monthLabel)

        val space = 25
        val totalColumn =  pdfPaintHighlight.measureText(timeframeLabel) + space
        val annualColumn = totalColumn + pdfPaintHighlight.measureText(totalLabel) + space
        val monthColumn =  annualColumn + pdfPaintHighlight.measureText(annualLabel) + space

        var f = pdfDataTop + pdfLineSpacing
        canvas.drawText(app.getString(R.string.statistics) + ":", pdfLeftBorder, f, pdfPaintHighlight)

        f += pdfLineSpacing
        f += pdfLineSpacing
        canvas.drawText(totalLabel, totalColumn, f, pdfPaint)
        canvas.drawText(annualLabel, annualColumn, f, pdfPaint)
        canvas.drawText(monthLabel, monthColumn, f, pdfPaint)

        // Data section
        val msToDay: Long = 1000 * 60 * 60 * 24
        var totalDays = 0L
        val item = getLast(true)
        if (item != null) {
            val lastDay = item.timestamp
            val tmpItem = getFirst(true)
            if (tmpItem != null) {
                val firstDay = tmpItem.timestamp
                val timeFrame = lastDay - firstDay
                totalDays = timeFrame / msToDay // From ms to days
            }
        }

        val totalMeasurements = getSize(false)

        val now = Calendar.getInstance().time
        val aYearAgo = now.time - 31557600000
        var sql = "SELECT count(*) FROM data WHERE timestamp >= $aYearAgo and type=${dataType}"
        val annualMeasurements = getInt(sql)

        val aMonthAgo = now.time - 2629800000
        sql = "SELECT count(*) FROM data WHERE timestamp >= $aMonthAgo and type=${dataType}"
        val monthlyMeasurements = getInt(sql)

        f += pdfLineSpacing
        canvas.drawText(timeframeLabel, pdfLeftBorder, f, pdfPaint)
        canvas.drawText(totalDays.toString(), totalColumn, f, pdfPaint)
        canvas.drawText("365", annualColumn, f, pdfPaint)
        canvas.drawText("30", monthColumn, f, pdfPaint)

        f += pdfLineSpacing
        canvas.drawText(app.getString(R.string.measurementLabel), pdfLeftBorder, f, pdfPaint)
        canvas.drawText(totalMeasurements.toString(), totalColumn, f, pdfPaint)
        canvas.drawText(annualMeasurements.toString(), annualColumn, f, pdfPaint)
        canvas.drawText(monthlyMeasurements.toString(), monthColumn, f, pdfPaint)

        // **** Weight values
        // Total
        sql = "SELECT MIN(CAST(value1 as float)) FROM data WHERE type=${dataType}"
        val minT = getFloatAsString(sql)
        sql = "SELECT MAX(CAST(value1 as float)) FROM data WHERE type=${dataType}"
        val maxT = getFloatAsString(sql)
        sql = "SELECT AVG(CAST(value1 as float)) FROM data WHERE type=${dataType}"
        val avgT = getFloatAsString(sql)

        // Annual
        sql = "SELECT MIN(CAST(value1 as float)) FROM data WHERE timestamp >= $aYearAgo and type=${dataType}"
        val minA = getFloatAsString(sql)
        sql = "SELECT MAX(CAST(value1 as float)) FROM data WHERE timestamp >= $aYearAgo and type=${dataType}"
        val maxA = getFloatAsString(sql)
        sql = "SELECT AVG(CAST(value1 as float)) FROM data WHERE timestamp >= $aYearAgo and type=${dataType}"
        val avgA = getFloatAsString(sql)

        // Monthly
        sql = "SELECT MIN(CAST(value1 as float)) FROM data WHERE timestamp >= $aMonthAgo and type=${dataType}"
        val minM = getFloatAsString(sql)
        sql = "SELECT MAX(CAST(value1 as float)) FROM data WHERE timestamp >= $aMonthAgo and type=${dataType}"
        val maxM = getFloatAsString(sql)
        sql = "SELECT AVG(CAST(value1 as float)) FROM data WHERE timestamp >= $aMonthAgo and type=${dataType}"
        val avgM = getFloatAsString(sql)

        f += pdfLineSpacing
        f += pdfLineSpacing
        canvas.drawText(app.getString(R.string.temperature) + " " + app.getString(R.string.min), pdfLeftBorder, f, pdfPaint)
        canvas.drawText(minT, totalColumn, f, pdfPaint)
        canvas.drawText(minA, annualColumn, f, pdfPaint)
        canvas.drawText(minM, monthColumn, f, pdfPaint)
        f += pdfLineSpacing
        canvas.drawText(app.getString(R.string.max), pdfLeftBorder, f, pdfPaint)
        canvas.drawText(maxT, totalColumn, f, pdfPaint)
        canvas.drawText(maxA, annualColumn, f, pdfPaint)
        canvas.drawText(maxM, monthColumn, f, pdfPaint)
        f += pdfLineSpacing
        canvas.drawText(app.getString(R.string.avg), pdfLeftBorder, f, pdfPaint)
        canvas.drawText(avgT, totalColumn, f, pdfPaint)
        canvas.drawText(avgA, annualColumn, f, pdfPaint)
        canvas.drawText(avgM, monthColumn, f, pdfPaint)
    }

}

