package com.zell_mbc.medilog.utility

import androidx.compose.ui.graphics.Color
val Gray = Color(0xff455a64)
val DarkGray = Color(0xff1c313a)
val LightGray = Color(0xFFbbbbbb)
val Amber = Color(0xFFFFA000)
val Red   = Color(0xffdd0d3c)

