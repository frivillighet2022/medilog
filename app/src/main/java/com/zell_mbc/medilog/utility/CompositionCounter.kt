@file:Suppress("NOTHING_TO_INLINE")
package com.zell_mbc.medilog.utility

import com.zell_mbc.medilog.BuildConfig
import android.util.Log
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.remember

class Ref(var value: Int)

const val EnableDebugCompositionLogs = true

@Composable
inline fun CompositionCounter(tag: String) {
    if (EnableDebugCompositionLogs && BuildConfig.DEBUG) {
        val ref = remember { Ref(0) }
        SideEffect { ref.value++ }
        Log.d(tag, "Compositions: ${ref.value}")
    }
}
