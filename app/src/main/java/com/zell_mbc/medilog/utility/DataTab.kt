package com.zell_mbc.medilog.utility

// Class to capture the individual tabs, built in and custom
class DataTab(val id: Int, var label: String) {
    var active: Boolean = false // Turned off by default
    var order = 999 // Make sure uninitialized comes last
}