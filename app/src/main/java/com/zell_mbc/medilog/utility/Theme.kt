package com.zell_mbc.medilog.utility

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion.White

private val LightColors = lightColors(
    primary = Gray,
    primaryVariant = DarkGray,
    onPrimary = White,
    secondary = LightGray,
    onSecondary = White,
    error = Color.Red
)

private val DarkColors = darkColors(
    primary = LightGray,
    primaryVariant = White,
    secondary = DarkGray,
    onSecondary = Color.Black,
    error = Color.Red
)

@Composable
fun ListTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    MaterialTheme(
        colors = if (darkTheme) DarkColors else LightColors,
//        typography = MediLogTypography,
//        shapes = MediLogShapes,
        content = content
    )
}
