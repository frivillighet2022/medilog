package com.zell_mbc.medilog.utility

import android.content.Context
import android.content.res.TypedArray
import android.util.TypedValue
import androidx.core.content.ContextCompat
import com.zell_mbc.medilog.R

fun getFontSizeSmallInPx(context: Context): Int {
    val typedArray: TypedArray = context.obtainStyledAttributes(R.style.TextSmall, intArrayOf(android.R.attr.textSize))
    val fontSize = typedArray.getDimensionPixelSize(0, 12)
    typedArray.recycle()
    return fontSize
}

fun getFontSizeMediumInPx(context: Context): Int {
    val typedArray: TypedArray = context.obtainStyledAttributes(R.style.TextMedium, intArrayOf(android.R.attr.textSize))
    val fontSize = typedArray.getDimensionPixelSize(0, 15)
    typedArray.recycle()
    return fontSize
}

fun getTextColorSecondary(context: Context): Int {
    val typedValue = TypedValue()
    val theme = context.theme
    theme.resolveAttribute(android.R.attr.textColorSecondary, typedValue, true)
    return ContextCompat.getColor(context, typedValue.resourceId)
}
fun getBackgroundColor(context: Context): Int {
    val typedValue = TypedValue()
    val theme = context.theme
    theme.resolveAttribute(android.R.attr.colorBackground, typedValue, true)
    return ContextCompat.getColor(context, typedValue.resourceId)
}

fun getTextColorPrimary(context: Context): Int {
    val typedValue = TypedValue()
    val theme = context.theme
    theme.resolveAttribute(android.R.attr.textColorPrimary, typedValue, true)
    return ContextCompat.getColor(context, typedValue.resourceId)
}


