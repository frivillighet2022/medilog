package com.zell_mbc.medilog.utility

import android.content.Context
import com.zell_mbc.medilog.MainActivity.Companion.THRESHOLD_DELIMITER
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl

fun checkThresholds(context: Context, value: String, default: String, type: Int): Array<String> {
    val userOutputService: UserOutputService = UserOutputServiceImpl(context,null)
    var lowerThreshold = 0f
    var upperThreshold = 99999f

    // Check if value is empty, if yes set to default
    var checkValue = value.ifEmpty { default }

    // Is only the lower limit supplied, if yes add fake upper threshold
    if (checkValue[checkValue.length-1].toString() == THRESHOLD_DELIMITER)
        checkValue += upperThreshold.toString()

    // Is only the upper limit supplied, if yes add fake lower threshold
    if (checkValue[0].toString() == THRESHOLD_DELIMITER)
        checkValue = lowerThreshold.toString() + checkValue

    if (checkValue.isNotEmpty()) {
        val minMax = checkValue.split(THRESHOLD_DELIMITER)
        // Check if really 2 values were present
        if ( minMax.size == 2 ) {
            try {
                lowerThreshold = minMax[0].toFloat()
            } catch (e: NumberFormatException) {
                userOutputService.showAndHideMessageForLong(context.getString(type) + ", " + context.getString(R.string.invalidLowerThreshold) + ": $checkValue")
                checkValue = default
            }
            try {
                upperThreshold = minMax[1].toFloat()
            } catch (e: NumberFormatException) {
                userOutputService.showAndHideMessageForLong(context.getString(type) + ", " + context.getString(R.string.invalidUpperThreshold) + ": $checkValue")
                checkValue = default
            }

            if (upperThreshold <= lowerThreshold) {
                userOutputService.showAndHideMessageForLong(context.getString(type) + ", " + context.getString(R.string.invalidThresholds) + ": $checkValue")
                checkValue = default
            }
        }
        else {
            userOutputService.showAndHideMessageForLong(context.getString(type) + ", " + context.getString(R.string.invalidThresholds) + ": $checkValue")
            checkValue = default
        }
    }
    else checkValue = default

    val finalMinMax = checkValue.split(THRESHOLD_DELIMITER)
    val arr = try {
        arrayOf(finalMinMax[0], finalMinMax[1])
    } catch (e: IndexOutOfBoundsException) {
        // This exception can only happen if the default value is wrong -> Should never happen…
        userOutputService.showAndHideMessageForLong(context.getString(type) + ", " + context.getString(R.string.invalidThresholds) + ": $checkValue")
        arrayOf("0","0")
    }
    return arr
}
