package com.zell_mbc.medilog.water

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.WATER
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.EditFragment
import com.zell_mbc.medilog.databinding.WaterEditformBinding
import com.zell_mbc.medilog.settings.SettingsActivity
import java.text.DateFormat
import java.text.SimpleDateFormat

class WaterEditFragment : EditFragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: WaterEditformBinding? = null
    private val binding get() = _binding!!

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = WaterEditformBinding.inflate(inflater,container,false)
        initializeService(binding.root)
        return binding.root
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    @SuppressLint("SimpleDateFormat")
    override fun updateItem() {
        val editItem = viewModel.getItem(viewModel.editItem)
        if (editItem == null) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.unknownError))
            return
        }

        // Check empty variables
        val value = binding.etEditWater.text.toString()
        if (value.isEmpty()) {
            userOutputService.showMessageAndWaitForLong(this.getString(R.string.waterMissing))
            return
        }

        // Valid water?
        var waterValue = 0
        try {
            waterValue = value.toInt()
            if (waterValue <= 0) {
                userOutputService.showMessageAndWaitForLong(this.getString(R.string.invalid) + " " + this.getString(R.string.water) + " "+ this.getString(R.string.value) + " $waterValue")
                return
            }
        } catch (e: Exception) {
            userOutputService.showMessageAndWaitForLong("Exception: " + this.getString(R.string.invalid) + " " + this.getString(R.string.water) + " "+ this.getString(R.string.value) + " $waterValue")
            return
        }

        editItem.timestamp = timestampCal.timeInMillis
        editItem.value1 = binding.etEditWater.text.toString()
        editItem.comment = binding.etComment.text.toString()
        editItem.value2 = SimpleDateFormat("yyyyMMdd").format(editItem.timestamp)

        viewModel.update(editItem)

        userOutputService.showMessageAndWaitForLong(requireContext().getString(R.string.itemUpdated))
        requireActivity().onBackPressed()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val vm = MainActivity.getViewModel(WATER) as WaterViewModel?
        if (vm != null) viewModel = vm
        else {
            userOutputService.showMessageAndWaitForLong(getString(R.string.unknownError))
            // Returning here will make sure none of the controls are initialized = no error when e.g. delete is selected
            return
        }

        saveButton = binding.btSave
        deleteButton = binding.btDelete
        super.onViewCreated(view, savedInstanceState)

        binding.tvEditUnit.text = preferences.getString(SettingsActivity.KEY_PREF_WATER_UNIT, getString(R.string.WATER_UNIT_DEFAULT))

        val editItem = viewModel.getItem(viewModel.editItem)
        if (editItem == null) {
            // Close keyboard so error is visible
            (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken, 0)
            userOutputService.showMessageAndWaitForLong(getString(R.string.unknownError))
            return
        }

        binding.etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(editItem.timestamp)
        binding.etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(editItem.timestamp)

        // Check if we are really editing or if this is a new value, if new entry delete tmpComment
        if (viewModel.isTmpItem(editItem.comment))
            binding.etComment.setText("")
        else {
            binding.etEditWater.setText(editItem.value1)
            binding.etComment.setText(editItem.comment)
        }

        // ---------------------
        setDateTimePicker(editItem.timestamp, binding.etDate, binding.etTime)

        // Make sure first field is highlighted and keyboard is open
        binding.etEditWater.requestFocus()
    }
}

