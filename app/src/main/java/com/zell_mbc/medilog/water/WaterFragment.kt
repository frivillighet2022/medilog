package com.zell_mbc.medilog.water

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color.Companion.Green
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.data.TabFragment
import com.zell_mbc.medilog.databinding.WaterTabBinding
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utility.ListTheme
import com.zell_mbc.medilog.utility.checkThresholds
import java.text.SimpleDateFormat
import java.util.*

class WaterFragment : TabFragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: WaterTabBinding? = null
    private val binding get() = _binding!!

    // String represenation of input values
    private var value1Value = ""
    private var commentValue = ""

    private var waterToday = 0
    private var waterFieldWidth = 0.dp


    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = WaterTabBinding.inflate(inflater,container,false)
        initializeService(binding.root)

        return binding.root
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    @SuppressLint("SimpleDateFormat")
    override fun addItem() {
        if (!quickEntry) {
            val tmpItem = Data(0, Date().time, MainActivity.tmpComment, viewModel.dataType, "", "", "", "")
            val itemID = viewModel.insert(tmpItem)
            if (itemID  != 0L) editItem(itemID.toInt())
            else userOutputService.showMessageAndWaitForLong("No entry with tmp value found!")
            return
        }

        // Close keyboard after entry is done
        (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken, 0)

        // Check empty variables
        if (value1Value.isEmpty()) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.waterMissing))
            return
        }
        var value = 0
        try {
            value = value1Value.toInt()
            if (value <= 0) {
                userOutputService.showMessageAndWaitForLong(this.getString(R.string.invalid) + " " + this.getString(R.string.water) + " " + this.getString(R.string.value) + " $value")
                return
            }
        } catch (e: Exception) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.exception) + " "  + this.getString(R.string.invalid) + " " + this.getString(R.string.water) + " " + this.getString(R.string.value) + " $value")
            return
        }
        val item = Data(0, Date().time, commentValue, viewModel.dataType, value1Value, SimpleDateFormat("yyyyMMdd").format(Date().time), "", "")

        viewModel.insert(item)

        if ((viewModel.filterEnd > 0L) && (viewModel.filterEnd < item.timestamp)) userOutputService.showMessageAndWaitForDuration(getString(R.string.filteredOut), 4000)

        value1Value = ""
        commentValue = ""
        binding.itemList.setContent { ShowContent() }
    }

    // Measure up Water string
    @Composable
    fun MeasureValueString() {
        MeasureTextWidth("9999")
        {
                textWidth ->
            Text(text = "", fontSize = fontSize.sp)
            waterFieldWidth = textWidth //+ (textWidth / 10) // Add a 10% buffer
        }
    }

    @Composable
    fun ShowRow(item: Data) {
        // Timestamp
        Text(dateFormat.format(item.timestamp) + " - " + timeFormat.format(item.timestamp),
            Modifier
                .padding(end = cellPadding.dp, top = rowPadding.dp, bottom = rowPadding.dp)
                .width(dateColumnWidth),
            color = MaterialTheme.colors.primary, fontSize = fontSize.sp)
        TabRowDefaults.Divider(color = MaterialTheme.colors.secondary, modifier = Modifier
            .width(1.dp)
            .fillMaxHeight()) // Vertical separator

        val value1 = try { item.value1.toInt() } catch (e: NumberFormatException) { 0 }

        // Value
        Text(value1.toString(),
            Modifier
                .padding(start = cellPadding.dp, end = cellPadding.dp)
                .width(waterFieldWidth),textAlign = TextAlign.Center, color = MaterialTheme.colors.primary, fontSize = fontSize.sp)
        TabRowDefaults.Divider(color = MaterialTheme.colors.secondary, modifier = Modifier
            .width(1.dp)
            .fillMaxHeight()) // Vertical separator

        // Comment
        if (item.comment.isNotEmpty()) {
            Spacer(modifier = Modifier.width(cellPadding.dp))
            Text(item.comment,color = MaterialTheme.colors.primary, fontSize = fontSize.sp)
        }
    }


    @OptIn(ExperimentalComposeUiApi::class)
    @Composable
    fun ShowContent() {
        val listState = rememberLazyListState()
        var selection: Data? by remember { mutableStateOf(null)}
        val listItems = viewModel.getItems("DESC",true)

        var value1 by remember { mutableStateOf(value1Value) }
        var triggerRefresh by remember { mutableStateOf(false) }
        var commentVisible by remember { mutableStateOf(false) }
        var comment by remember { mutableStateOf(commentValue) }

        if (value1Value.isEmpty()) value1 = ""
        if (commentValue.isEmpty()) comment = ""

        ListTheme {
            MeasureDateString()
            MeasureValueString()

            if (quickEntry) {
                waterToday = viewModel.getToday()

                Column (modifier = Modifier.fillMaxWidth()){
                    Row {
                        TextField(
                            value = value1,
                            colors = TextFieldDefaults.textFieldColors(textColor = MaterialTheme.colors.primary, backgroundColor = androidx.compose.ui.graphics.Color.Transparent),
                            onValueChange = { value1 = it.filter  { it.isDigit() } },
                            singleLine = true,
                            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                            textStyle = TextStyle(),
                            label = { Text(text = stringResource(id = R.string.water) +"*", maxLines = 1, overflow = TextOverflow.Ellipsis) },
                            placeholder  = { Text(text = stringResource(id = R.string.waterHint) + " $itemUnit") },

                            modifier = Modifier
                                .width(140.dp)
                                .padding(end = cellPadding.dp),
                            keyboardActions = KeyboardActions( onDone = { addItem() })
                        )

                        IconButton(onClick = { commentVisible = !commentVisible
                            triggerRefresh = true },
                            modifier = Modifier
                                .width(commentButtonWidth.dp)
                                .padding(top = (2 * cellPadding).dp)) {
                            Icon(painter = painterResource(R.drawable.ic_outline_comment_24),"Comment", tint = MaterialTheme.colors.primary) }

                        if (triggerRefresh) {
                            this@WaterFragment.ShowContent()
                            triggerRefresh = false
                        }

                    } // Row

                    if (commentVisible) {
                        // Comment
                        TextField(
                            value = comment,
                            colors = TextFieldDefaults.textFieldColors(textColor = MaterialTheme.colors.primary, backgroundColor = androidx.compose.ui.graphics.Color.Transparent),
                            onValueChange = { comment = it },
                            keyboardOptions = KeyboardOptions(
                                keyboardType = KeyboardType.Text,
                                capitalization = KeyboardCapitalization.Sentences
                            ),
                            maxLines = 2,
                            textStyle = TextStyle(),
                            modifier = Modifier.fillMaxWidth(),
                            label = { Text(text = stringResource(id = R.string.comment)) },
                            keyboardActions = KeyboardActions( onDone = { addItem() })
                        )

                        // Need an add button because Enter is used for new line
                        binding.btAdd.visibility = View.VISIBLE
                    }
                    else binding.btAdd.visibility = View.GONE // Hide Add Button

                    Spacer(modifier = Modifier.height(fontSize.dp))
                    val s = getString(R.string.today) + " " + waterToday.toString() + " / $lowerThreshold" + itemUnit
                    if (waterToday >= lowerThreshold.toInt()) Text(s, color = Green)
                    else Text(s, color = MaterialTheme.colors.error)

                    value1Value = value1
                    commentValue = comment
                }
                topPadding = 110 + if (commentVisible) 65 else 0
            }
            LazyColumn(
                state = listState,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.padding(top = topPadding.dp)
            ) {
                items(listItems) { item ->
                    TabRowDefaults.Divider(color = MaterialTheme.colors.secondary, thickness = 1.dp) // Lines starting from the top
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(IntrinsicSize.Min) //.height(rowHeight.dp)
                            .clickable { selection = item },
                        verticalAlignment = Alignment.CenterVertically
                    )
                    { ShowRow(item)}

                    if (selection != null) {
                        // Check if we are dealing with a genuine Diary item
                        ItemClicked(selection!!._id)
                        selection = null
                    }
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        editActivityClass  = WaterEditActivity::class.java
        infoActivityClass  = WaterInfoActivity::class.java
        chartActivityClass = WaterChartActivity::class.java

        viewModel =
            ViewModelProvider(requireActivity())[WaterViewModel::class.java]  // This should return the MainActivity ViewModel
        configureButtons(binding.btAdd,binding.btInfo, binding.btChart)

        // -------------------------------
        itemUnit = " " + preferences.getString(SettingsActivity.KEY_PREF_WATER_UNIT, getString(R.string.WATER_UNIT_DEFAULT))

        val s = preferences.getString(SettingsActivity.KEY_PREF_WATER_THRESHOLD, getString(R.string.WATER_THRESHOLD_DEFAULT)) + "-"
        val th = checkThresholds(requireContext(), s, getString(R.string.WATER_THRESHOLD_DEFAULT), R.string.water) // Add - to string to reflect that it's a lower threshold
        lowerThreshold = th[0].toFloat()

        // Preset values in case something was stored in viewModel during onDestroy
        commentValue = viewModel.comment
        value1Value = viewModel.value1

        binding.itemList.setContent { ShowContent() }
    }

    // Save values to survive orientation change
    override fun onDestroy() {
        viewModel.value1 = value1Value
        viewModel.comment = commentValue
        super.onDestroy()
    }
}