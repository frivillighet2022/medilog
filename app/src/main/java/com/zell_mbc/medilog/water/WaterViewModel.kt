package com.zell_mbc.medilog.water

import android.app.Application
import android.graphics.Paint
import android.graphics.pdf.PdfDocument
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.data.ViewModel
import com.zell_mbc.medilog.settings.SettingsActivity

class WaterViewModel(application: Application): ViewModel(application) {
    override var tabIcon  = R.drawable.ic_water
    override val filterStartPref = "WATERFILTERSTART"
    override val filterEndPref = "WATERFILTEREND"
    override val filterModePref =  "WATER_FILTER_MODE"
    override val rollingFilterValuePref = "WATER_ROLLING_FILTER_VALUE"
    override val rollingFilterTimeframePref = "WATER_ROLLING_FILTER_TIMEFRAME"

    override var itemName = app.getString(R.string.water)

    override var landscape = preferences.getBoolean(SettingsActivity.KEY_PREF_WATER_LANDSCAPE, app.getString(R.string.WATER_LANDSCAPE_DEFAULT).toBoolean())
    override var pageSize = preferences.getString(SettingsActivity.KEY_PREF_WATER_PAPER_SIZE, app.getString(R.string.WATER_PAPER_SIZE_DEFAULT))
    override var showAllTabs = false // Only used for Diary tab

    private val waterUnit = preferences.getString(SettingsActivity.KEY_PREF_WATER_UNIT, app.getString(R.string.WATER_UNIT_DEFAULT))
    private val quantity = app.getString(R.string.quantity) + " (" + waterUnit + ")"

    private val warningSign = app.getString(R.string.warningSign)
    private var warningTab = 0f

    fun getDays(filtered: Boolean): List<Data> {
        var string = "SELECT _id, timestamp, comment, type, SUM(CAST(value1 as int)) as value1, value2, value3, value4 from data WHERE type=$dataType"
        if (filtered) {
            getFilter()
            if ((filterStart != 0L) && (filterEnd == 0L)) string += " AND timestamp >= $filterStart AND timestamp <= $filterEnd"
            if ((filterStart == 0L) && (filterEnd != 0L)) string += " AND timestamp <= $filterEnd"
        }
        string += " GROUP BY value2"
        return getDataList(string)
    }

    override fun createPdfDocument(filtered: Boolean): PdfDocument? {
        if (getSize(filtered) == 0) {
            userOutputService.showAndHideMessageForLong( app.getString(R.string.water) + ": " + app.getString(R.string.noDataToExport))
            return null
        }

        super.createPdfDocument(filtered)

        val waterThreshold = preferences.getString(SettingsActivity.KEY_PREF_WATER_THRESHOLD, app.getString(R.string.WATER_THRESHOLD_DEFAULT))!!.toInt()

        // -----------
        val summarizedPDF = preferences.getBoolean(SettingsActivity.KEY_PREF_SUMMARYPDF, app.getString(R.string.WATER_SHOW_PDF_SUMMARY_DEFAULT).toBoolean())
        val pdfItems: List<Data> = if (summarizedPDF) {
            getDays(filtered)
        }
        else {
            getItems("ASC", filtered)
        }

        setColumns(pdfPaintHighlight)
        drawHeader(pdfPaint, pdfPaintHighlight)
        var f = pdfDataTop + pdfLineSpacing
        availableWidth = measureColumn(pdfRightBorder - commentTab)
        for (item in pdfItems) {
            f = checkForNewPage(f)
            canvas.drawText(toStringDate(item.timestamp), pdfLeftBorder, f, pdfPaint)
            if (!summarizedPDF) canvas.drawText(toStringTime(item.timestamp), pdfLeftBorder, f, pdfPaint)

            val value = try {
                item.value1.toInt()
            } catch  (e: NumberFormatException) {
                0
            }

            if (value < waterThreshold) canvas.drawText(warningSign, warningTab, f, pdfPaint)
            canvas.drawText(item.value1, dataTab, f, pdfPaint)
            f = multipleLines(item.comment, f)
        }
        // finish the page
        document.finishPage(page)
        return document
    }

    override fun setColumns(pdfPaintHighlight: Paint) {
        val warningSignWidth = pdfPaintHighlight.measureText(app.getString(R.string.warningSign))
        val waterTabWidth = pdfPaintHighlight.measureText(quantity)

        warningTab = pdfLeftBorder + dateTabWidth + padding
        dataTab    = warningTab + warningSignWidth
        commentTab = dataTab + waterTabWidth
    }

    override fun drawHeader(pdfPaint: Paint, pdfPaintHighlight: Paint) {
        drawHeader(pdfPaint) // VieModel function doesn't need to highlight

        // Data section
        canvas.drawText(app.getString(R.string.date), pdfLeftBorder, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(quantity, warningTab + space, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.comment), commentTab + space, pdfDataTop, pdfPaintHighlight)
        canvas.drawLine(warningTab - space, pdfHeaderBottom, warningTab - space, pdfDataBottom, pdfPaint)  // Line before data
        canvas.drawLine(commentTab, pdfHeaderBottom, commentTab, pdfDataBottom, pdfPaint)  // Line before comment
    }
}