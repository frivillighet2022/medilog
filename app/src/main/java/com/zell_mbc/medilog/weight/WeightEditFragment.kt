package com.zell_mbc.medilog.weight

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.WEIGHT
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.EditFragment
import com.zell_mbc.medilog.databinding.WeightEditformBinding
import com.zell_mbc.medilog.settings.SettingsActivity
import java.text.DateFormat

class WeightEditFragment : EditFragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    var _binding: WeightEditformBinding? = null
    val binding get() = _binding!!
    private var logBodyFat = false

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = WeightEditformBinding.inflate(inflater,container,false)
        initializeService(binding.root)

        return binding.root
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun updateItem() {
        val editItem = viewModel.getItem(viewModel.editItem)
        if (editItem == null) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.unknownError))
            return
        }

        // Check empty variables
        val weightString = binding.etWeight.text.toString()
        if (weightString.isEmpty()) {
            // Close keyboard so error is visible
            (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken, 0)
            userOutputService.showMessageAndWaitForLong(this.getString(R.string.weightMissing))
            return
        }

        // Valid weight?
        val weightValue: Float
        try {
            weightValue = weightString.toFloat()
            if (weightValue <= 0f) {
                // Close keyboard so error is visible
                (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken, 0)
                userOutputService.showMessageAndWaitForLong(this.getString(R.string.invalid) + " " + this.getString(R.string.weight) + " " + this.getString(R.string.value) + " $weightString")
                return
            }
        } catch (e: Exception) {
            // Close keyboard so error is visible
            (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken, 0)
            userOutputService.showMessageAndWaitForLong("Exception: " + this.getString(R.string.invalid) + " " + this.getString(R.string.weight) + " " + this.getString(R.string.value) + " $weightString")
            return
        }

        // Valid bodyFat?
        val bodyFatString = binding.etBodyFat.text.toString()
        if (bodyFatString.isNotBlank()) {
            val bodyFatValue: Float
            try {
                bodyFatValue = bodyFatString.toFloat()
                if (bodyFatValue <= 0) {
                    // Close keyboard so error is visible
                    (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken, 0)
                    userOutputService.showMessageAndWaitForLong(this.getString(R.string.invalid) + " " + this.getString(R.string.bodyFat) + " " + this.getString(R.string.value) + " $bodyFatString")
                    return
                }
            } catch (e: Exception) {
                // Close keyboard so error is visible
                (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken, 0)
                userOutputService.showMessageAndWaitForLong("Exception: " + this.getString(R.string.invalid) + " " + this.getString(R.string.bodyFat) + " " + this.getString(R.string.value) + " $bodyFatString")
                return
            }
            editItem.value2 = bodyFatString
        }

        editItem.timestamp = timestampCal.timeInMillis
        editItem.value1 = weightString

        editItem.comment = binding.etComment.text.toString()

        viewModel.update(editItem)

//        userOutputService.showMessageAndWaitForLong(requireContext().getString(R.string.itemUpdated))
        requireActivity().finish()

        // Close keyboard after entry is done
        (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken, 0)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // Avoid exception when for some reason ViewModel isn't found
        val vm = MainActivity.getViewModel(WEIGHT) as WeightViewModel?
        if (vm != null) viewModel = vm
        else {
            userOutputService.showMessageAndWaitForLong(getString(R.string.unknownError))
            // Returning here will make sure none of the controls are initialized = no error when e.g. delete is selected
            return
        }

        saveButton = binding.btSave
        deleteButton = binding.btDelete
        super.onViewCreated(view, savedInstanceState)

        logBodyFat = preferences.getBoolean(SettingsActivity.KEY_PREF_LOG_FAT, getString(R.string.LOG_FAT_DEFAULT).toBoolean())

        binding.tvWeightUnit.text = preferences.getString(SettingsActivity.KEY_PREF_WEIGHT_UNIT, getString(R.string.WEIGHT_UNIT_DEFAULT))

        val editItem = viewModel.getItem(viewModel.editItem)
        if (editItem == null) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.unknownError))
            return
        }

        binding.etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(editItem.timestamp)
        binding.etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(editItem.timestamp)

        // Check if we are really editing or if this is a new value, if new entry delete tmpComment
        if (viewModel.isTmpItem(editItem.comment))
            binding.etComment.setText("")
        else {
            binding.etWeight.setText(editItem.value1)
            if (logBodyFat) binding.etBodyFat.setText(editItem.value2)
            binding.etComment.setText(editItem.comment)
        }

        if (logBodyFat) {
            binding.tvBodyFat.visibility = View.VISIBLE
            binding.etBodyFat.visibility = View.VISIBLE
            binding.tvFatUnit.visibility = View.VISIBLE
        }
        else {
            binding.tvBodyFat.visibility = View.GONE
            binding.etBodyFat.visibility = View.GONE
            binding.tvFatUnit.visibility = View.GONE
        }

        // ---------------------
        setDateTimePicker(editItem.timestamp, binding.etDate, binding.etTime)
        // Make sure first field is highlighted and keyboard is open
        binding.etWeight.requestFocus()
    }
}
