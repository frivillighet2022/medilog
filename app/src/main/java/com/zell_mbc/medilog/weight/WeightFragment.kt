package com.zell_mbc.medilog.weight

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.*
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.data.TabFragment
import com.zell_mbc.medilog.databinding.WeightFatTabBinding
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utility.ListTheme
import com.zell_mbc.medilog.utility.checkThresholds
import java.util.*

class WeightFragment : TabFragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: WeightFatTabBinding? = null
    private val binding get() = _binding!!

    private var logFat = false
    private val separator = "."

    // String representation of input values
    private var weightValue = ""
    private var fatValue = ""
    private var commentValue = ""
    private var showComment = false

    private var fatLowerThreshold = 0f
    private var fatUpperThreshold = 0f

    private var weightColumnWidth = 0.dp
    private val fatColumnWidth = 45

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = WeightFatTabBinding.inflate(inflater, container, false)
        initializeService(binding.root)
        return binding.root
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun addItem() {
        if (!quickEntry) {
            val tmpItem = Data(0, Date().time, MainActivity.tmpComment, viewModel.dataType, "", "", "", "")
            val itemID = viewModel.insert(tmpItem)
            if (itemID  != 0L) editItem(itemID.toInt())
            else userOutputService.showMessageAndWaitForLong("No entry with tmp value found!")
            return
        }

        // Check empty variables
        if (weightValue.isEmpty()) {
            (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken,0)
            userOutputService.showMessageAndWaitForLong(getString(R.string.weightMissing))
            return
        }
        var w = 0f
        try {
            w = weightValue.toFloat()
            if (w <= 0f) {
                userOutputService.showMessageAndWaitForLong(this.getString(R.string.invalid) + " " + this.getString(R.string.weight) + " " + this.getString(R.string.value) + " $w")
                return
            }
        } catch (e: Exception) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.exception) + " " + this.getString(R.string.invalid) + " " + this.getString(R.string.weight) + " " + this.getString(R.string.value) + " $w")
            return
        }

        // Grab tare
        val tare = preferences.getString(SettingsActivity.KEY_PREF_WEIGHT_TARE, getString(R.string.WEIGHT_TARE_DEFAULT))
        if (!tare.isNullOrBlank()) {
            w -= tare.toFloat()
            if (w < 1) {
                (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken,0)
                userOutputService.showMessageAndWaitForLong(getString(R.string.exception) + " " + this.getString(R.string.invalid) + " " + this.getString(R.string.weight) + " " + this.getString(R.string.value) + " $w (tare applied)")
                return
            }
        }

        val item = Data(0, Date().time, commentValue, viewModel.dataType, w.toString(), fatValue, "", "") // Start with empty item

        viewModel.insert(item)

        if ((viewModel.filterEnd > 0L) && (viewModel.filterEnd < item.timestamp)) userOutputService.showMessageAndWaitForDuration(getString(R.string.filteredOut), 4000)

        weightValue = ""
        fatValue = ""
        commentValue = ""

        binding.itemList.setContent { ShowContent() }

        // Close keyboard after entry is done
        (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken, 0)
    }

    // Measure up date string
    @Composable
    fun MeasureWeightString() {
        MeasureTextWidth("100,00")
        {
                textWidth ->
            Text(text = "", fontSize = fontSize.sp)
            weightColumnWidth = textWidth //+ (textWidth / 10) // Add a 10% buffer
        }
    }

    @Composable
    fun ShowRow(item: Data) {

        // Timestamp
        Text(dateFormat.format(item.timestamp) + " - " + timeFormat.format(item.timestamp),
            Modifier.padding(end = cellPadding.dp, top = rowPadding.dp, bottom = rowPadding.dp).width(dateColumnWidth),
            color = MaterialTheme.colors.primary, fontSize = fontSize.sp)
        TabRowDefaults.Divider(color = MaterialTheme.colors.secondary, modifier = Modifier.width(1.dp).fillMaxHeight()) // Vertical separator

        val weight = try { item.value1.toFloat()} catch (e: NumberFormatException) {0f}

        // Value
        var textColor = MaterialTheme.colors.primary
        if (highlightValues && weight > upperThreshold) textColor = MaterialTheme.colors.error
        Text(item.value1, Modifier.padding(start = cellPadding.dp, end = cellPadding.dp, top = rowPadding.dp, bottom = rowPadding.dp).width(weightColumnWidth), textAlign = TextAlign.Center, color = textColor, fontSize = fontSize.sp)
        TabRowDefaults.Divider(color = MaterialTheme.colors.secondary, modifier = Modifier.width(1.dp).fillMaxHeight()) // Vertical separator

        // Body fat
        if (logFat) {
            val s = if (item.value2.isNotEmpty()) {
                item.value2
            } else ""

            textColor = MaterialTheme.colors.primary
            if (highlightValues) {
                val bodyFat = try {
                    item.value2.toFloat()
                } catch (e: NumberFormatException) {
                    0f
                }
                if ((bodyFat < fatLowerThreshold) || (bodyFat > fatUpperThreshold)) textColor = MaterialTheme.colors.error
            }

            Text(s ,Modifier.padding(start = cellPadding.dp, top = rowPadding.dp, bottom = rowPadding.dp).width(fatColumnWidth.dp), color = textColor, fontSize = fontSize.sp, overflow = TextOverflow.Ellipsis)
            TabRowDefaults.Divider(color = MaterialTheme.colors.secondary, modifier = Modifier.width(1.dp).fillMaxHeight()) // Vertical separator
        }

        // Comment
        if (item.comment.isNotEmpty())
            Text(item.comment,Modifier.padding(start = cellPadding.dp), color = MaterialTheme.colors.primary, fontSize = fontSize.sp)
    }

    @OptIn(ExperimentalComposeUiApi::class)
    @Composable
    fun ShowContent() {
        val weightFieldWidth = 120
        val fatFieldWidth = 110
        var selection: Data? by remember { mutableStateOf(null)}
        val listItems = viewModel.getItems("DESC",true)

        var weight by remember { mutableStateOf(weightValue) }
        var fat by remember { mutableStateOf(fatValue) }
        var comment by remember { mutableStateOf(commentValue) }
        var triggerRefresh by remember { mutableStateOf(false) }
        var commentVisible by remember { mutableStateOf(false) }

        if (weightValue.isEmpty()) weight = ""
        if (fatValue.isEmpty()) fat = ""
        if (commentValue.isEmpty()) comment = ""

        ListTheme {
            MeasureDateString()
            MeasureWeightString()
  //          Log.d("DateColumnWidth: ", dateColumnWidth2.toString())

            if (quickEntry) {
//                CompositionCounter("Weight")
                Column (modifier = Modifier.fillMaxWidth()){
                    val (textField1, textField2, textField3) = remember { FocusRequester.createRefs() }
                    Row {
                        // Unit
                        TextField(
                            value = weight,
                            onValueChange = {  weight = it
                                val tmp = it
                                if (((tmp.length == 4) && (!tmp.endsWith(separator))) || (tmp.length >= 5) ) {
                                    if (logFat) textField2.requestFocus()
                                    else if (showComment) textField3.requestFocus()
                                }
                            },
                            singleLine = true,
                            colors = TextFieldDefaults.textFieldColors(textColor = MaterialTheme.colors.primary, backgroundColor = androidx.compose.ui.graphics.Color.Transparent),
                            keyboardOptions = KeyboardOptions(
                                keyboardType = KeyboardType.Number
                            ),
                            textStyle = TextStyle(),
                            label = { Text(text = stringResource(id = R.string.weight) +"*", maxLines = 1, overflow = TextOverflow.Ellipsis) },
                            placeholder  = { Text(text = stringResource(id = R.string.weightHint) + " $itemUnit") },
                            modifier = Modifier
                                .width(weightFieldWidth.dp)
                                .padding(end = cellPadding.dp)
                                .focusRequester(textField1),
                            keyboardActions = KeyboardActions( onDone = { addItem() })
                        )
//                        Log.d("Width:", weightFieldWidth.toString())
                        if (logFat) {
                            TextField(
                                value = fat,
                                colors = TextFieldDefaults.textFieldColors(textColor = MaterialTheme.colors.primary, backgroundColor = androidx.compose.ui.graphics.Color.Transparent),
                                onValueChange = {
                                    fat = it
                                    if (showComment && it.length > 3) textField3.requestFocus()
                                },
                                singleLine = true,
                                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                                textStyle = TextStyle(),
                                label = {
                                    Text(
                                        text = stringResource(id = R.string.bodyFat),
                                        maxLines = 1,
                                        overflow = TextOverflow.Ellipsis
                                    )
                                },

                                placeholder = { Text(text = stringResource(id = R.string.bodyFatEntryHint) + (" %")) },
                                modifier = Modifier
                                    .width(fatFieldWidth.dp)
                                    .focusRequester(textField2)
                                    .padding(end = cellPadding.dp),
                                keyboardActions = KeyboardActions( onDone = { addItem() })
                            )
                        }

                        IconButton(onClick = { commentVisible = !commentVisible
                            triggerRefresh = true },
                            modifier = Modifier.width(commentButtonWidth.dp).padding(top = (2 * cellPadding).dp)) {
                            Icon(painter = painterResource(R.drawable.ic_outline_comment_24),"Comment", tint = MaterialTheme.colors.primary) }
                    } // Row

                    if (triggerRefresh) {
                        this@WeightFragment.ShowContent()
                        triggerRefresh = false
                    }

                    if (commentVisible) {
                        // Comment
                        TextField(
                            value = comment,
                            colors = TextFieldDefaults.textFieldColors(textColor = MaterialTheme.colors.primary, backgroundColor = androidx.compose.ui.graphics.Color.Transparent),
                            onValueChange = { comment = it },
                            keyboardOptions = KeyboardOptions(
                                keyboardType = KeyboardType.Text,
                                capitalization = KeyboardCapitalization.Sentences
                            ),
                            maxLines = 2,
                            textStyle = TextStyle(),
                            modifier = Modifier
                                .fillMaxWidth()
                                .focusRequester(textField3),
                            label = { Text(text = stringResource(id = R.string.comment)) },
                            keyboardActions = KeyboardActions( onDone = { addItem() })
                        )

                        // Need an add button because Enter is used for new line
                        binding.btAdd.visibility = View.VISIBLE
                    }
                    else binding.btAdd.visibility = View.GONE // Hide Add Button

                    val tmp = weight
                    if (tmp.isNotEmpty()) {
                        try {
                            weightValue = tmp.toFloat().toString()
                        } catch (e: Exception) {
                            userOutputService.showMessageAndWaitForLong(getString(R.string.exception) + " " + getString(R.string.invalid) + " " + getString(R.string.temperature) + " " + getString(R.string.value) + " $tmp")
                        }
                    }
                    fatValue = fat
                    commentValue = comment
                }
                topPadding = 80 + if (commentVisible) 65 else 0
            }
        // Data section
        LazyColumn(
         // state = listState,
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.padding(top = topPadding.dp)
            ) {
            items(listItems, { listItem: Data -> listItem._id }) { item ->
                TabRowDefaults.Divider(color = MaterialTheme.colors.secondary, thickness = 1.dp) // Lines starting from the top
                Row(
                    modifier = Modifier.fillMaxWidth().height(IntrinsicSize.Min).clickable { selection = item },
                    verticalAlignment = Alignment.CenterVertically
                )
                {
                        ShowRow(item)
                }

                //------------------------------
                if (selection != null) {
                    ItemClicked(selection!!._id)
                    selection = null
                }
            }
        }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        editActivityClass  = WeightEditActivity::class.java
        infoActivityClass  = WeightInfoActivity::class.java
        chartActivityClass = WeightChartActivity::class.java

        viewModel =
            ViewModelProvider(requireActivity())[WeightViewModel::class.java]  // This should return the MainActivity ViewModel
        configureButtons(binding.btAdd,binding.btInfo, binding.btChart)

        // -------------------------------
        highlightValues = preferences.getBoolean(SettingsActivity.KEY_PREF_WEIGHT_HIGHLIGHT_VALUES, false)
        logFat = preferences.getBoolean(SettingsActivity.KEY_PREF_LOG_FAT, getString(R.string.LOG_FAT_DEFAULT).toBoolean())
        itemUnit = " " + preferences.getString(SettingsActivity.KEY_PREF_WEIGHT_UNIT, getString(R.string.WEIGHT_UNIT_DEFAULT))

        val s = "-" + preferences.getString(SettingsActivity.KEY_PREF_WEIGHT_THRESHOLD, getString(R.string.WEIGHT_THRESHOLD_DEFAULT))
        var th = checkThresholds(requireContext(), s, getString(R.string.WEIGHT_THRESHOLD_DEFAULT), R.string.temperature)
        upperThreshold = th[1].toFloat()

        if (logFat && highlightValues) {
            th = checkThresholds(
                requireContext(),
                "" + preferences.getString(SettingsActivity.KEY_PREF_FAT_MIN_MAX,getString(R.string.FAT_MIN_MAX_DEFAULT)),
                getString(R.string.FAT_MIN_MAX_DEFAULT),
                R.string.bodyFat
            )
            fatLowerThreshold = th[0].toFloat()
            fatUpperThreshold = th[1].toFloat()
        }

        // Preset values in case something was stored in viewModel during onDestroy
        commentValue = viewModel.comment
        weightValue = viewModel.value1
        fatValue = viewModel.value2

        binding.itemList.setContent {ShowContent() }
    }

    // Save values to survive orientation change
    override fun onDestroy() {
        viewModel.value1 = weightValue
        viewModel.value2 = fatValue
        viewModel.comment = commentValue
        super.onDestroy()
    }
}