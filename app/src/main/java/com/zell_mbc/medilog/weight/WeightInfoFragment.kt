package com.zell_mbc.medilog.weight

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.utility.Preferences
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.WEIGHT
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.databinding.WeightInfoformBinding
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import java.math.RoundingMode
import java.text.DateFormat
import java.text.DecimalFormat
import kotlin.math.roundToLong

class WeightInfoFragment : Fragment() {
    private var _binding: WeightInfoformBinding? = null
    private val binding get() = _binding!!
    private lateinit var userOutputService : UserOutputService

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = WeightInfoformBinding.inflate(inflater, container, false)
        initializeService(binding.root)

        return binding.root
    }
    private fun initializeService(view: View) {
        userOutputService = UserOutputServiceImpl(requireContext(),view)
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private lateinit var viewModel: WeightViewModel  //by viewModels() //factoryProducer = { SavedStateViewModelFactory(this, ) })

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(requireActivity())[WeightViewModel::class.java]
        viewModel.init(WEIGHT)

        val preferences = Preferences.getSharedPreferences(requireContext())
        val weightUnit = " " + preferences.getString(SettingsActivity.KEY_PREF_WEIGHT_UNIT, getString(R.string.WEIGHT_UNIT_DEFAULT))

        // Calculate BMI
        val count = viewModel.getSize(true)
        var item = viewModel.getLast(true)
        if (item == null) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.noDataToShow))
            return
        }

        var sBMI = "N/A"
        val sBodyHeight = preferences.getString(SettingsActivity.KEY_PREF_BODY_HEIGHT, "")
        if (!sBodyHeight.isNullOrEmpty()){
            var fBodyHeight: Float
            try { fBodyHeight = sBodyHeight.toFloat()
                }
            catch (e: NumberFormatException) {
                fBodyHeight = 0f
                userOutputService.showMessageAndWaitForLong(getString(R.string.invalidBodyHeight))
            }

            val weight = try {
                item.value1.toFloat()
            } catch (e: NumberFormatException) {
                0f
            }

            if (weight > 0f && fBodyHeight > 0f) {
                var fBMI = weight / fBodyHeight / fBodyHeight * 100 * 100 // Height is entered in cm, formula expects m
                if (weightUnit.contains("lb")) fBMI *= 703f         // If imperial measures
                sBMI = ((fBMI * 100).roundToLong() / 100f).toString() // Round to 2 digits
            }
        }
        binding.tvBmiValue.text =  sBMI

        // Measurements
        var s = if ((viewModel.filterStart + viewModel.filterEnd) == 0L) getString(R.string.measurementsInDB) + " $count"
        else getString(R.string.measurementsInFilter) + " $count"
        binding.tvMeasurementCount.text = s

        val avg = viewModel.getAvgFloat("value1",true)
        val df = DecimalFormat("#.##")
        df.roundingMode = RoundingMode.HALF_EVEN
        val ravg = df.format(avg)
        s = getString(R.string.avg).replaceFirstChar { it.uppercase() } + ": $ravg $weightUnit"
        binding.tvAvg.text = s

        var min = viewModel.getMinValue1(true)
        var max = viewModel.getMaxValue1(true)
        s = " $min - $max $weightUnit"
        binding.tvMinMaxWeight.text = s

        val logBodyFat = preferences.getBoolean(SettingsActivity.KEY_PREF_LOG_FAT, getString(R.string.LOG_FAT_DEFAULT).toBoolean())
        if (logBodyFat) {
            min = viewModel.getMinValue2(true)
            max = viewModel.getMaxValue2(true)
            s = " $min - $max %"
            binding.tvMinMaxFat.text = s
        }

        val dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
        val endDate = dateFormat.format(item.timestamp)

        item = viewModel.getFirst(true)
        val startDate = dateFormat.format(item?.timestamp)

        s = getString(R.string.timePeriod) + " $startDate - $endDate"
        binding.tvTimePeriod.text = s

    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
        viewModel.deleteTmpItem() // Deletes all items with comment == tmpComment
    }
}
