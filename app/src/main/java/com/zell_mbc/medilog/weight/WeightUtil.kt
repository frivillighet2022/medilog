package com.zell_mbc.medilog.weight

import android.content.Context
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utility.Preferences

fun checkBodyFatThresholds(context: Context): Array<Int> {
    val userOutputService: UserOutputService = UserOutputServiceImpl(context,null)
    var fatLowerThreshold = 0
    var fatUpperThreshold = 100
    val preferences = Preferences.getSharedPreferences(context)
    val delimiter = "-"
    var checkValue = preferences.getString(SettingsActivity.KEY_PREF_FAT_MIN_MAX, context.resources.getString(R.string.FAT_MIN_MAX_DEFAULT))
    if (!checkValue.isNullOrEmpty()) {
        val minMax = checkValue.split(delimiter)

        try {
            fatLowerThreshold = minMax[0].toInt()
        } catch (e: NumberFormatException) {
            userOutputService.showAndHideMessageForLong("Invalid Body Fat thresholds: $checkValue")
            checkValue = context.resources.getString(R.string.FAT_MIN_MAX_DEFAULT)
        }
        try {
            fatUpperThreshold = minMax[1].toInt()
        } catch (e: NumberFormatException) {
            userOutputService.showAndHideMessageForLong("Invalid Body Fat thresholds: $checkValue")
            checkValue = context.resources.getString(R.string.FAT_MIN_MAX_DEFAULT)
        }

        if (fatUpperThreshold <= fatLowerThreshold) {
            userOutputService.showAndHideMessageForLong("Invalid Body Fat thresholds: $checkValue")
            checkValue = context.resources.getString(R.string.FAT_MIN_MAX_DEFAULT)
        }

        if (fatUpperThreshold > 100) {
            userOutputService.showAndHideMessageForLong("Invalid Body Fat thresholds: $checkValue")
            checkValue = context.resources.getString(R.string.FAT_MIN_MAX_DEFAULT)
        }

    }
    else checkValue = ""

    val minMax = checkValue.split(delimiter)
    return arrayOf(minMax[0].toInt(), minMax[1].toInt())
}