package com.zell_mbc.medilog.weight

import android.app.Application
import android.graphics.Paint
import android.graphics.pdf.PdfDocument
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.graphics.drawable.toBitmap
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.ViewModel
import com.zell_mbc.medilog.settings.SettingsActivity
import java.util.*

class WeightViewModel(application: Application): ViewModel(application) {
    override var tabIcon  = R.drawable.ic_weight
    override val filterStartPref = "WEIGHTFILTERSTART"
    override val filterEndPref = "WEIGHTFILTEREND"
    override val filterModePref =  "WEIGHT_FILTER_MODE"
    override val rollingFilterValuePref = "WEIGHT_ROLLING_FILTER_VALUE"
    override val rollingFilterTimeframePref = "WEIGHT_ROLLING_FILTER_TIMEFRAME"

    override var itemName = app.getString(R.string.weight)

    override var landscape = preferences.getBoolean(SettingsActivity.KEY_PREF_WEIGHT_LANDSCAPE, app.getString(R.string.WEIGHT_LANDSCAPE_DEFAULT).toBoolean())
    override var pageSize = preferences.getString(SettingsActivity.KEY_PREF_WEIGHT_PAPER_SIZE, app.getString(R.string.WEIGHT_PAPER_SIZE_DEFAULT))
    private val weightUnit = preferences.getString(SettingsActivity.KEY_PREF_WEIGHT_UNIT, app.getString(R.string.WEIGHT_UNIT_DEFAULT))
    private val logBodyFat = preferences.getBoolean(SettingsActivity.KEY_PREF_LOG_FAT, app.getString(R.string.LOG_FAT_DEFAULT).toBoolean())
    override var showAllTabs = false // Only used for Diary tab

    private var fatTab = 0f

    private var weightTabWidth = 0f
    private var fatTabWidth = 0f

    private var fatLowerThreshold = 0
    private var fatUpperThreshold = 100

    override fun createPdfDocument(filtered: Boolean): PdfDocument? {
        if (getSize(filtered) == 0) {
            userOutputService.showAndHideMessageForLong( app.getString(R.string.weight) + ": " + app.getString(R.string.noDataToExport))
            return null
        }

        super.createPdfDocument(filtered)

        val s = preferences.getString(SettingsActivity.KEY_PREF_WEIGHT_THRESHOLD, app.getString(R.string.WEIGHT_THRESHOLD_DEFAULT))
        var weightThreshold = 0
        if (!s.isNullOrEmpty()) weightThreshold = s.toInt()

        // Only check thresholds if bodyFat is logged
        if (logBodyFat) {
            val minMax = checkBodyFatThresholds(app)
            fatLowerThreshold = minMax[0]
            fatUpperThreshold = minMax[1]
        }

        // -----------
        setColumns(pdfPaintHighlight)

        drawHeader(pdfPaint, pdfPaintHighlight)

        var f = pdfDataTop + pdfLineSpacing
        availableWidth = measureColumn(pdfRightBorder - commentTab)

        for (item in pdfItems) {
            f = checkForNewPage(f)
            val dtString = toStringDate(item.timestamp) + "  " + toStringTime(item.timestamp)
            canvas.drawText(dtString, pdfLeftBorder, f, pdfPaint)

            val weightValue = try {
                item.value1.toFloat()
            } catch (e: NumberFormatException) {
                0f
            }

            val roundedString = weightValue.toString()+ weightUnit
            if (highlightValues && weightValue > weightThreshold) canvas.drawText(roundedString, dataTab + space, f, pdfPaintHighlight)
            else canvas.drawText(roundedString, dataTab + space, f, pdfPaint)

            if (logBodyFat) {
                val fatValue = try {
                    item.value2.toInt()
                } catch (e: NumberFormatException) {
                    0
                }

                val bodyFat = if (item.value2.isNotBlank()) item.value2 + "%" else ""
                if (highlightValues && (fatValue < fatLowerThreshold || fatValue > fatUpperThreshold))
                    canvas.drawText(bodyFat, fatTab + space, f, pdfPaintHighlight)
                else {
                    canvas.drawText(bodyFat, fatTab + space, f, pdfPaint)
                }
            }
            f = multipleLines(item.comment, f)
        }
        // finish the page
        document.finishPage(page)
        // Add statistics page
        createPage(document)
        drawStatsPage(pdfPaint, pdfPaintHighlight)

        document.finishPage(page)
        return document
    }

    override fun setColumns(pdfPaintHighlight: Paint) {
        weightTabWidth = pdfPaintHighlight.measureText(app.getString(R.string.weight))

        dataTab = pdfLeftBorder + dateTabWidth + padding // Need a bit more space to cater for month name length
        if (logBodyFat) {
            fatTabWidth = pdfPaintHighlight.measureText(app.getString(R.string.bodyFat))
            fatTab = dataTab + weightTabWidth + padding
            commentTab = fatTab + fatTabWidth + padding
        }
        else commentTab = dataTab + weightTabWidth + padding
    }

    override fun drawHeader(pdfPaint: Paint, pdfPaintHighlight: Paint) {
        drawHeader(pdfPaint)

        // Data section
        canvas.drawLine(dataTab, pdfHeaderBottom, dataTab, pdfDataBottom, pdfPaint)  // Line before weight
        canvas.drawLine(commentTab, pdfHeaderBottom, commentTab, pdfDataBottom, pdfPaint) // Line before comment

        canvas.drawText(app.getString(R.string.date), pdfLeftBorder, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.weight), dataTab + space, pdfDataTop, pdfPaintHighlight)
        if (logBodyFat) {
            canvas.drawText(app.getString(R.string.bodyFat), fatTab + space, pdfDataTop, pdfPaintHighlight)
            canvas.drawText(app.getString(R.string.comment), commentTab + space, pdfDataTop, pdfPaintHighlight)
            canvas.drawLine(fatTab, pdfHeaderBottom, fatTab, pdfDataBottom, pdfPaint) // Line before body fat
        }
        else canvas.drawText(app.getString(R.string.comment), commentTab + space, pdfDataTop, pdfPaintHighlight)
    }

    private fun drawStatsPage(pdfPaint: Paint, pdfPaintHighlight: Paint) {
        // Header
        val pdfHeaderDateColumn = pdfRightBorder - 150
        val logoTab = 30
        val bitmap = AppCompatResources.getDrawable(app, R.mipmap.ic_launcher)?.toBitmap(logoTab,30)
        if (bitmap != null) canvas.drawBitmap(bitmap, 5f, 5f, pdfPaint)

        // Draw header
        canvas.drawText(headerText, pdfLeftBorder + logoTab, pdfHeaderTop, pdfPaint)
        canvas.drawText(app.getString(R.string.date) + ": " + formatedDate, pdfHeaderDateColumn, pdfHeaderTop, pdfPaint)
        canvas.drawLine(pdfLeftBorder, pdfHeaderBottom, pdfRightBorder, pdfHeaderBottom, pdfPaint)
        canvas.drawLine(pdfLeftBorder, pdfDataBottom, pdfRightBorder, pdfDataBottom, pdfPaint)

        val timeframeLabel = app.getString(R.string.timeframeLabel)
        val totalLabel = app.getString(R.string.totalLabel)
        val annualLabel = app.getString(R.string.annualLabel)
        val monthLabel = app.getString(R.string.monthLabel)

        val space = 25
        val totalColumn =  pdfPaintHighlight.measureText(timeframeLabel) + space
        val annualColumn = totalColumn + pdfPaintHighlight.measureText(totalLabel) + space
        val monthColumn =  annualColumn + pdfPaintHighlight.measureText(annualLabel) + space

        var f = pdfDataTop + pdfLineSpacing
        canvas.drawText(app.getString(R.string.statistics) + ":", pdfLeftBorder, f, pdfPaintHighlight)

        f += pdfLineSpacing
        f += pdfLineSpacing
        canvas.drawText(totalLabel, totalColumn, f, pdfPaint)
        canvas.drawText(annualLabel, annualColumn, f, pdfPaint)
        canvas.drawText(monthLabel, monthColumn, f, pdfPaint)

        // Data section
        val msToDay: Long = 1000 * 60 * 60 * 24
        var totalDays = 0L
        val item = getLast(true)
        if (item != null) {
            val lastDay = item.timestamp
            val tmpItem = getFirst(true)
            if (tmpItem != null) {
                val firstDay = tmpItem.timestamp
                val timeFrame = lastDay - firstDay
                totalDays = timeFrame / msToDay // From ms to days
            }
        }

        val totalMeasurements = getSize(false)

        val now = Calendar.getInstance().time
        val aYearAgo = now.time - 31557600000
        var sql = "SELECT count(*) FROM data WHERE timestamp >= $aYearAgo and type=${dataType}"
        val annualMeasurements = getInt(sql)

        val aMonthAgo = now.time - 2629800000
        sql = "SELECT count(*) FROM data WHERE timestamp >= $aMonthAgo and type=${dataType}"
        val monthlyMeasurements = getInt(sql)

        f += pdfLineSpacing
        canvas.drawText(timeframeLabel, pdfLeftBorder, f, pdfPaint)
        canvas.drawText(totalDays.toString(), totalColumn, f, pdfPaint)
        canvas.drawText("365", annualColumn, f, pdfPaint)
        canvas.drawText("30", monthColumn, f, pdfPaint)

        f += pdfLineSpacing
        canvas.drawText(app.getString(R.string.measurementLabel), pdfLeftBorder, f, pdfPaint)
        canvas.drawText(totalMeasurements.toString(), totalColumn, f, pdfPaint)
        canvas.drawText(annualMeasurements.toString(), annualColumn, f, pdfPaint)
        canvas.drawText(monthlyMeasurements.toString(), monthColumn, f, pdfPaint)

        // **** Weight values
        // Total
        sql = "SELECT MIN(CAST(value1 as float)) FROM data WHERE type=${dataType}"
        var minT = getFloatAsString(sql)
        sql = "SELECT MAX(CAST(value1 as float)) FROM data WHERE type=${dataType}"
        var maxT = getFloatAsString(sql)
        sql = "SELECT AVG(CAST(value1 as float)) FROM data WHERE type=${dataType}"
        var avgT = getFloatAsString(sql)

        // Annual
        sql = "SELECT MIN(CAST(value1 as float)) FROM data WHERE timestamp >= $aYearAgo and type=${dataType}"
        var minA = getFloatAsString(sql)
        sql = "SELECT MAX(CAST(value1 as float)) FROM data WHERE timestamp >= $aYearAgo and type=${dataType}"
        var maxA = getFloatAsString(sql)
        sql = "SELECT AVG(CAST(value1 as float)) FROM data WHERE timestamp >= $aYearAgo and type=${dataType}"
        var avgA = getFloatAsString(sql)

        // Monthly
        sql = "SELECT MIN(CAST(value1 as float)) FROM data WHERE timestamp >= $aMonthAgo and type=${dataType}"
        var minM = getFloatAsString(sql)
        sql = "SELECT MAX(CAST(value1 as float)) FROM data WHERE timestamp >= $aMonthAgo and type=${dataType}"
        var maxM = getFloatAsString(sql)
        sql = "SELECT AVG(CAST(value1 as float)) FROM data WHERE timestamp >= $aMonthAgo and type=${dataType}"
        var avgM = getFloatAsString(sql)

        f += pdfLineSpacing
        f += pdfLineSpacing
        canvas.drawText(app.getString(R.string.weight) + " " + app.getString(R.string.min), pdfLeftBorder, f, pdfPaint)
        canvas.drawText(minT, totalColumn, f, pdfPaint)
        canvas.drawText(minA, annualColumn, f, pdfPaint)
        canvas.drawText(minM, monthColumn, f, pdfPaint)
        f += pdfLineSpacing
        canvas.drawText(app.getString(R.string.max), pdfLeftBorder, f, pdfPaint)
        canvas.drawText(maxT, totalColumn, f, pdfPaint)
        canvas.drawText(maxA, annualColumn, f, pdfPaint)
        canvas.drawText(maxM, monthColumn, f, pdfPaint)
        f += pdfLineSpacing
        canvas.drawText(app.getString(R.string.avg), pdfLeftBorder, f, pdfPaint)
        canvas.drawText(avgT, totalColumn, f, pdfPaint)
        canvas.drawText(avgA, annualColumn, f, pdfPaint)
        canvas.drawText(avgM, monthColumn, f, pdfPaint)

        // **** Body Fat values
        if (logBodyFat) {
            // Total
            sql = "SELECT MIN(CAST(value2 as float)) FROM data WHERE type=${dataType}"
            minT = getFloatAsString(sql)
            sql = "SELECT MAX(CAST(value2 as float)) FROM data WHERE type=${dataType}"
            maxT = getFloatAsString(sql)
            sql = "SELECT AVG(CAST(value2 as float)) FROM data WHERE type=${dataType}"
            avgT = getFloatAsString(sql)

            // Annual
            sql = "SELECT MIN(CAST(value2 as float)) FROM data WHERE timestamp >= $aYearAgo and type=${dataType}"
            minA = getFloatAsString(sql)
            sql = "SELECT MAX(CAST(value2 as float)) FROM data WHERE timestamp >= $aYearAgo and type=${dataType}"
            maxA = getFloatAsString(sql)
            sql = "SELECT AVG(CAST(value2 as float)) FROM data WHERE timestamp >= $aYearAgo and type=${dataType}"
            avgA = getFloatAsString(sql)

            // Monthly
            sql = "SELECT MIN(CAST(value2 as float)) FROM data WHERE timestamp >= $aMonthAgo and type=${dataType}"
            minM = getFloatAsString(sql)
            sql = "SELECT MAX(CAST(value2 as float)) FROM data WHERE timestamp >= $aMonthAgo and type=${dataType}"
            maxM = getFloatAsString(sql)
            sql = "SELECT AVG(CAST(value2 as float)) FROM data WHERE timestamp >= $aMonthAgo and type=${dataType}"
            avgM = getFloatAsString(sql)

            f += pdfLineSpacing
            f += pdfLineSpacing
            canvas.drawText(
                app.getString(R.string.bodyFat) + " " + app.getString(R.string.min),
                pdfLeftBorder,
                f,
                pdfPaint
            )
            canvas.drawText(minT, totalColumn, f, pdfPaint)
            canvas.drawText(minA, annualColumn, f, pdfPaint)
            canvas.drawText(minM, monthColumn, f, pdfPaint)
            f += pdfLineSpacing
            canvas.drawText(app.getString(R.string.max), pdfLeftBorder, f, pdfPaint)
            canvas.drawText(maxT, totalColumn, f, pdfPaint)
            canvas.drawText(maxA, annualColumn, f, pdfPaint)
            canvas.drawText(maxM, monthColumn, f, pdfPaint)
            f += pdfLineSpacing
            canvas.drawText(app.getString(R.string.avg), pdfLeftBorder, f, pdfPaint)
            canvas.drawText(avgT, totalColumn, f, pdfPaint)
            canvas.drawText(avgA, annualColumn, f, pdfPaint)
            canvas.drawText(avgM, monthColumn, f, pdfPaint)
        }
    }

}