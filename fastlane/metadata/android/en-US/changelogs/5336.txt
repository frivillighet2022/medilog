## v2.2.0, build 5336
New 

- Allow to select paper size (A4 & Letter)
- Allow to select Landscape/Portrait paper orientation for PDF reports
- Added MediLog Logo to PDF reports
- Added support for Dark Mode 
- Added ability to log Body Fat to Weight tab
- Added Weight/BloodSugar average to info screens
- Added possibility to blend tab comments into Diary, https://gitlab.com/toz12/medilog/-/issues/150

Fixed/Changed

- Improved blood pressure pdf report
- Use warning sign for heart rhythm issues throughout the app
- Fixed Weight Chart issues https://gitlab.com/toz12/medilog/-/issues/141
- Fixed a few tab related issues: https://gitlab.com/toz12/medilog/-/issues/142, https://gitlab.com/toz12/medilog/-/issues/122


Known issues

- Sometimes the first Edit field (E.g. Systolic or Weight) in a tab loses focus as soon as the first digit is entered

