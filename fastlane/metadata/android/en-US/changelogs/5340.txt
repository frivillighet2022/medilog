## v2.2.4, build 5340
New 

- New general RowPadding setting change the height of the data rows (0 no padding, 10 = default)

Fixed/Changed

- Language "bug" in Water, header showed "Weight" instead of "Quantity"
- Fixed inconsistency in German translation for pdf report, it's now "Bericht" everywhere
- Added separation lines in Diary tab for better visibility


Known issues

- Sometimes the first Edit field (E.g. Systolic or Weight) in a tab loses focus as soon as the first digit is entered

