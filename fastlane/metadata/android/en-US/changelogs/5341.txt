## v2.2.5, build 5341
New 

- First iteration of progress bar to show progress during data imports

Fixed/Changed

- Changed somewhat misleading import error message with more precise language
- Fixed a regresion introduced in v2.2.4 which prevented setting the row padding for the weight tab
- Substantial update to Finish translation


Known issues

- Sometimes the first Edit field (E.g. Systolic or Weight) in a tab loses focus as soon as the first digit is entered
- Some older Android versions can't show the warning sign ISO character and will display a strange Ü instead

