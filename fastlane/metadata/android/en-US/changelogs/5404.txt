## v2.4.0, build 5404 RC2
New 

- Support for rolling filters


Fixed/Changed

- Some code cleanup
- Weight: Fixed https://gitlab.com/toz12/medilog/-/issues/202
- Weight: Fixed bug which revented BodyFat to be highlighted
- Final (hopefully) release candidate (RC3)

    
Known issues

- Screen refresh after editing/deleting values is a little clunky
- Filter doesn't refresh immedeatly after filter changes were made, needs another activity/tab to be launched first
